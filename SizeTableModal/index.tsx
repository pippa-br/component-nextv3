"use client";

import { useResponsive } from "@/core-nextv3/util/useResponsive";
import { THEME_SETTING } from "@/setting/setting";
import { Tabs } from "@chakra-ui/react";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

export function SizeTableModal({ table, setModal, tablePerson }: any) 
{
    // console.log("table", table)
    // console.log("tablePerson", tablePerson)
    const { isDesktop } = useResponsive();

    const getDefaultTab = () => 
    {
        if (tablePerson) 
        {
            return "2";
        }

        if (!THEME_SETTING?.disableOptionModalSize && table) 
        {
            return "3";
        }
    };

    return (
    // <div className={styles.trackingModal}>
        <div className={styles.trackingModal}>
            <div className={styles.content} onClick={(e) => e.stopPropagation()}>
                <ImageSet
                    className={styles.close}
                    onClick={() => setModal(false)}
                    width={40}
                    height={40}
                    src="/assets/icons/GrFormClose.svg"
                    responsive={false}
                    alt=""
                />
                <div
                    className={`${!THEME_SETTING?.disabledImageAndDescriptionModalSize ? styles.wrapper : styles.tableContent}`}
                >
                    {isDesktop &&
						!THEME_SETTING?.disabledImageAndDescriptionModalSize && (
                        <div className={styles.imgContent}>
                            <ImageSet
                                width={254}
                                height={494}
                                responsive={false}
                                src="/assets/sizeimage.png"
                                alt=""
                            />
                        </div>
                    )}
                    <div className={styles.tableContent}>
                        <p className={styles.message}>
							QUAL É O MEU TAMANHO?
                            <br />
							`` Encontre o seu tamanho através das medidas corporais na tabela
							abaixo:
                        </p>

                        <Tabs.Root
                            className={styles.tabsTable}
                            defaultValue={getDefaultTab()}
                        >
                            <Tabs.List className={styles.titleTabs}>
                                {/* {THEME_SETTING?.disabledOptionModaSize && <Tabs.Trigger value="1"><span>Como Medir</span></Tabs.Trigger> } */}
                                {tablePerson ? (
                                    <Tabs.Trigger value="2">
                                        <span>Tabela de Medidas</span>
                                    </Tabs.Trigger>
                                ) : null}
                                {!THEME_SETTING?.disableOptionModalSize && (
                                    <>
                                        {" "}
                                        {table && (
                                            <Tabs.Trigger value="3">
                                                <span>Medidas Técnicas</span>
                                            </Tabs.Trigger>
                                        )}
                                    </>
                                )}
                            </Tabs.List>

                            {/* {THEME_SETTING?.disabledOptionModaSize &&
                    <Tabs.Content value="1">
                      <div className={styles.imgContent}>
                        <Image width={254} height={494} src="/assets/sizeimage.png" loader={cloudflareLoader} alt=""/>
                    </div>
                    <p>1 - Busto: Meça a parte mais larga do seu peito, envolvendo a fita métrica ao redor dele. Certifique-se de que a fita está nivelada na parte de trás e não está muito apertada nem muito solta.</p>
                    <p>2 - Cintura: Encontre a parte mais estreita da sua cintura, geralmente acima do umbigo e abaixo das costelas. Enrole a fita métrica ao redor desta área, mantendo-a confortável e nivelada.</p>
                    <p>3 - Quadril: Meça a parte mais larga dos seus quadris e bumbum. Enrole a fita métrica ao redor desta área. Novamente, mantenha a fita nivelada e confortável.</p>
                    </Tabs.Content>
                  } */}

                            {tablePerson ? (
                                <Tabs.Content value="2">
                                    <table>
                                        <thead>
                                            <tr>
                                                {tablePerson?.[0]?.items?.map(
                                                    (head: any, index: number) => (
                                                        <td key={index}>{head?.label}</td>
                                                    ),
                                                )}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {tablePerson?.map((row: any) => (
                                                <tr key={row?.id}>
                                                    {row?.items?.map((column: any) => (
                                                        <td key={column?.id}>{column?.value}</td>
                                                    ))}
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </Tabs.Content>
                            ) : null}

                            {table && (
                                <Tabs.Content value="3">
                                    <table>
                                        <thead>
                                            <tr>
                                                {table[0]?.items?.map((head: any, index: number) => (
                                                    <td key={index}>{head?.label}</td>
                                                ))}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {table?.map((row: any, index: number) => (
                                                <tr key={index}>
                                                    {row?.items?.map((column: any, index: number) => (
                                                        <td key={index}>{column?.value}</td>
                                                    ))}
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </Tabs.Content>
                            )}
                        </Tabs.Root>
                        {/* <table>
                    <thead>
                      <tr>
                        {table[0].items.map((head: any) => (
                          <td key={head.id}>{head.label}</td>
                        ))}
                      </tr>
                    </thead>
                    <tbody>
                      {table.map((row: any) => (
                        <tr key={row.id} className={row.items.length == 0 ? styles.rowEmpty : ''}>
                          {row.items.length > 0 ? row.items.map((column: any) => (
                            <td key={column.id}>{column.value}</td>
                          )) : <td key={generateKey()}><> &nbsp; </></td>}
                        </tr>
                      ))}
                    </tbody>
                </table> */}
                        {!THEME_SETTING?.disabledImageAndDescriptionModalSize && (
                            <>
                                <p>
									1 - Busto: Passe a fita métrica sobre o busto, ela precisa
									passar sob a área mais saliente.
                                </p>
                                <p>
									2 - Cintura: Localize o ponto mais alto do osso de seu quadril
									e a parte inferior das suas costelas. Logo depois coloque uma
									fita métrica no local que fica no meio do caminho entre esses
									dois, o mais estreito será sua cintura.
                                </p>
                                <p>
									3 - Quadril: O quadril fica a dois palmos abaixo da cintura.
									Certifique-se de que a fita métrica está na metade do bumbum.
									a área mais saliente.
                                </p>
                            </>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
}
