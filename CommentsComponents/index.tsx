import { formatDate } from "@/core-nextv3/util/util";
import ReactStars from "react-stars";
import styles from "./styles.module.scss";

export const CommentsComponents = ({ coment }: any) => 
{
    return (
        <div className={styles.commentsPerson}>
            <p className={styles.nameDate}>
                {coment?.owner?.name} - Publicado em {formatDate(coment.postdate)}
            </p>
            <div className={styles.avaliation}>
                <ReactStars
                    count={5}
                    size={25}
                    edit={false}
                    value={coment.rating}
                    color2="#ffd700"
                />
            </div>
            <p className={styles.description}>{coment?.message}</p>
            {/* <div className={styles.question}>
                <p className={styles.text}>Este comentário foi útil?</p>
                <BsHandThumbsUp />
                <p className={styles.number}>5</p>
                <BsHandThumbsDown />
                <p className={styles.number}>2</p>
            </div> */}
        </div>
    );
};
