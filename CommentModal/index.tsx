"use client";

import { Rating } from "@smastrom/react-rating";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { AnimatedLoading } from "../../component-nextv3/AnimatedLoading";
import { addComment } from "../../core-nextv3/comment/comment.api";
import { COMMET_SETTING, THEME_SETTING } from "../../setting/setting";
import ErrorMessage from "../ErrorMessage";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

export const CommentModal = ({ setAssessment, item, order }: any) => 
{
    // console.log('item', item)
    const [ loadingShipping, setLoadingShipping ] = useState(false);
    const [ starValue, setStarValue ] = useState(0);

    const {
        register,
        formState: { errors },
        handleSubmit,
        setValue,
        reset,
    } = useForm();

    function ratingChanged(e: any) 
    {
        setValue("rating", e, { shouldValidate : true });
        setStarValue(e);
    }

    const onSubmit = async (data: any) => 
    {
        setLoadingShipping(true);

        data.variant = item.variant;
        data.status = false;

        await addComment(
            COMMET_SETTING.merge({
                client : {
                    referencePath : order?.client?.referencePath,
                },
                product : {
                    referencePath : item.product.referencePath,
                },
                data : data,
            }),
        );

        reset();

        toast.success("Mensagem enviada com sucesso!", {
            duration : 1000,
        });

        setLoadingShipping(false);
        setAssessment(false);
    };

    useEffect(() => 
    {
        register("rating", { required : "Campo Obrigatório" });
    }, []);

    return (
        <div className={styles.assesmentModal} onClick={() => setAssessment(false)}>
            <div className={styles.content} onClick={(e) => e.stopPropagation()}>
                <ImageSet
                    className={styles.closeModal}
                    onClick={() => setAssessment(false)}
                    width={20}
                    height={20}
                    src="/assets/icons/GrFormClose.svg"
                    responsive={false}
                    alt=""
                />

                <h1>
					Gostaria de avaliar nosso produto <br />
                    <span>{item?.name}</span>
                </h1>

                <form onSubmit={(e) => e.preventDefault()}>
                    <p className={styles.text}>
						Valorizamos muito a opinião dos nossos clientes! Sua experiência é
						fundamental para nós.
                    </p>
                    <div className={styles.addStars}>
                        {/* <Rating
                            value={5}
                            onChange={ratingChanged}
                            size={60}
                            color2="#ffd700"
                            value={start}
                            half={false}
                            className={styles.ReactStars}
                        /> */}
                        <Rating
                            style={{ maxWidth : 300 }}
                            value={starValue}
                            onChange={ratingChanged}
                        />
                        {errors.rating && <ErrorMessage message={errors.rating.message} />}
                    </div>

                    <div className={styles.formInput}>
                        <label>Mensagem</label>
                        <textarea
                            cols={30}
                            rows={10}
                            placeholder="Digite sua mensagem..."
                            {...register("message", { required : "Campo Obrigatório" })}
                        />
                        {errors.message && (
                            <ErrorMessage message={errors.message.message} />
                        )}
                    </div>

                    {!THEME_SETTING.disabledAditionalComment && (
                        <>
                            <small>
								Para oferecer o melhor atendimento personalizado, gostaríamos de
								conhecer um pouco mais sobre você. Por favor, compartilhe sua
								altura e peso conosco. Essas informações nos ajudarão a adaptar
								nossos produtos/serviços de acordo com suas necessidades
								individuais. Lembre-se de que suas informações serão tratadas
								com total confidencialidade e usadas apenas para fins de
								personalização. Agradecemos antecipadamente por compartilhar
								essas informações conosco.
                            </small>

                            <div className={styles.inputs}>
                                <div className={styles.inputItem}>
                                    <label>Altura cm(Opicional)</label>
                                    <input
                                        type="number"
                                        maxLength={4}
                                        placeholder="Altura em cm"
                                        {...register("height", {
                                            validate : (value) => value.length <= 4,
                                        })}
                                    />
                                </div>

                                <div className={styles.inputItem}>
                                    <label>Peso kg(Opicional)</label>
                                    <input
                                        type="number"
                                        maxLength={4}
                                        placeholder="Peso em Kg"
                                        {...register("weigth", {
                                            validate : (value) => value.length <= 4,
                                        })}
                                    />
                                </div>
                            </div>
                        </>
                    )}

                    <div className={styles.btn}>
                        <button type="submit" onClick={handleSubmit(onSubmit)}>
                            {loadingShipping ? <AnimatedLoading /> : "Enviar"}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
};
