"use client";

import { THEME_SETTING } from "@/setting/setting";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import type { Options } from "@splidejs/splide";
import { ImageSet } from "../../component-nextv3/ImageSet";
import CustomLink from "../CustomLink";
import styles from "./styles.module.scss";

export const BannerCarousel = ({ banners }: any) => 
{
    const mainOptions: Options = {
        rewind     : true,
        perPage    : 1,
        perMove    : 1,
        gap        : "1rem",
        pagination : true,
        slideFocus : true,
        arrows     : true,
        start      : 0,
    };

    return (
        <div className={styles.bannersCarousel}>
            <Splide
                options={mainOptions}
                aria-labelledby=""
                className={styles.imagesContent}
            >
                {banners?.map(
                    (banner: any, index: any) =>
                        banner.status && (
                            <SplideSlide key={index}>
                                <div className={styles.desktop}>
                                    <CustomLink href={banner?.url || ""}>
                                        <ImageSet
                                            image={banner.desktop}
                                            width={THEME_SETTING.widthBannerMain}
                                            height={THEME_SETTING.heightBannerMain}
                                        />
                                    </CustomLink>
                                </div>
                                <div className={styles.mobile}>
                                    <CustomLink href={banner?.url || ""}>
                                        <ImageSet
                                            image={banner.mobile}
                                            width={THEME_SETTING.widthBannerMainMobile}
                                            height={THEME_SETTING.heightBannerMainMobile}
                                        />
                                    </CustomLink>
                                </div>
                            </SplideSlide>
                        ),
                )}
            </Splide>
        </div>
    );
};
