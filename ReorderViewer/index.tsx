"use client";

import { TDate } from "@/core-nextv3/model/TDate";
import { cloudflareLoader } from "@/core-nextv3/util/util";
import { useTranslations } from "next-intl";
import Image from "next/image";
import { Checkbox } from "pretty-checkbox-react";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import Select from "react-select";
import { addReorder } from "../../core-nextv3/order/order.api";
import { customSelectStyles } from "../../core-nextv3/util/util.style";
import { REORDER_INFO, REORDER_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
//import { useRouter } from "next/navigation"
import ErrorMessage from "../ErrorMessage";
import styles from "./styles.module.scss";

declare const navigator: any;

export function ReorderViewer({ order, reorderType, reasonsType }: any) 
{
    //const router = useRouter()

    const [ loadingRequest, setLoadingRequest ] = useState(false);
    const [ selectedItems, setSelectedItems ] = useState<any>([]);
    const [ reorderTypeItems, setReorderTypetems ] = useState<any>([]);
    const [ reversalCode, setReversalCode ] = useState(order?.reversalCode);
    const [ hasReorder, setHasReorder ] = useState(order?.hasReorder);
    const t = useTranslations();

    const {
        register,
        handleSubmit,
        setValue,
        formState: { errors, isSubmitted },
    } = useForm();

    function handleChangeType(e: any) 
    {
        setValue("type", e, { shouldValidate : true });
    }

    function handleChangeReason(e: any) 
    {
        setValue("reason", e, { shouldValidate : true });
    }

    const submitRequest = async (data: any) => 
    {
        if (selectedItems.length === 0) 
        {
            return;
        }

        toast.success("Enviando Solicitação!", {
            duration : 5000,
        });

        setLoadingRequest(true);

        const newData = {
            document : {
                referencePath : order.referencePath,
            },
            data : {
                items         : [ ...selectedItems ],
                type          : data.type,
                reason        : data.reason,
                noteClient    : data.noteClient,
                reorderStatus : {
                    label : "Em Análise",
                    value : "analysis",
                },
            },
        };

        const result: any = await addReorder(REORDER_SETTING.merge(newData));

        toast.success("Pedido feito com sucesso!", {
            duration : 5000,
        });

        if (result.status) 
        {
            setReversalCode(result.data.reversalCode);
        }

        setHasReorder(result.status);
        setLoadingRequest(false);
    };

    function handleChangeCheckboxes(e: any) 
    {
        if (e.target.checked === true) 
        {
            selectedItems.push(
                ...order.items.filter((item: any) => item.id === e.target.value),
            );

            setSelectedItems([ ...selectedItems ]);
        }
        else 
        {
            setSelectedItems([
                ...selectedItems.filter(
                    (element: any) => element.id !== e.target.value,
                ),
            ]);
        }
    }

    async function copieReversalCode() 
    {
        toast.success(t("Código Reverso copoiado com sucesso!"));
    }

    useEffect(() => 
    {
        if (!REORDER_INFO?.disabledType) 
        {
            register("type", { required : "Campo Obrigatório" });
        }

        if (!REORDER_INFO?.disabledReason) 
        {
            register("reason", { required : "Campo Obrigatório" });
        }

        if (reorderType?.items) 
        {
            const items = [];
            const deliveredDate = new TDate({ value : order.deliveredDate });

            for (const item of reorderType.items) 
            {
                if (REORDER_INFO.validateDate) 
                {
                    if (REORDER_INFO.validateDate[item.value]) 
                    {
                        const diff = Math.abs(deliveredDate.diff(new TDate(), "days"));

                        if (diff < REORDER_INFO.validateDate[item.value]) 
                        {
                            items.push(item);
                        }
                    }
                }
                else 
                {
                    items.push(item);
                }
            }

            setReorderTypetems(items);
        }
    }, []);

    return (
        <div className={styles.reOrderModal}>
            <div className={styles.content}>
                {hasReorder ? (
                    <div className={styles.reorderActions}>
                        <p className={styles.information}>
                            {t("As instruções de postagem foram enviadas para o seu e-mail.")}{" "}
                            <br />{" "}
                            {t(
                                "Verifique a sua caixa de entrada ou a caixa de spam de seu e-mail.",
                            )}
                        </p>
                        {reversalCode && (
                            <button
                                className={styles.reverseCodeButton}
                                type="button"
                                onClick={() => 
                                {
                                    navigator.clipboard.writeText(`${reversalCode}`);
                                    copieReversalCode();
                                }}
                            >
                                <Image
                                    width={20}
                                    height={20}
                                    src="/assets/icons/BiCopy.svg"
                                    loader={cloudflareLoader}
                                    alt=""
                                />
                                {t("Código Reverso")}: <small>{reversalCode}</small>
                            </button>
                        )}
                    </div>
                ) : (
                    <div className={styles.selectProducts}>
                        <p className={styles.title}>
							Selecione quais produtos deseja {REORDER_INFO?.name}:
                        </p>
                        <form>
                            {!REORDER_INFO?.disabledType && (
                                <div className={styles.formItem}>
                                    <Select
                                        placeholder={reorderType.name}
                                        className={styles.selectOption}
                                        options={reorderTypeItems}
                                        styles={customSelectStyles}
                                        isClearable
                                        onChange={(e) => handleChangeType(e)}
                                    />
                                    {errors.type && (
                                        <ErrorMessage message={errors.type.message} />
                                    )}
                                </div>
                            )}

                            <div className={styles.formItem}>
                                {order?.items?.map((item: any, index: number) => (
                                    <div key={index} className={styles.formList}>
                                        <Checkbox
                                            color="success"
                                            value={item.id}
                                            width={"100%"}
                                            onChange={(e: any) => handleChangeCheckboxes(e)}
                                        >
                                            <span className={styles.text}>
                                                {item?.productCode} - {item?.name}
                                                {item?.variant && item?.variant?.map(
                                                    (item2: any) => ` ${item2?.label} `,
                                                )}
                                            </span>
                                        </Checkbox>
                                        {/* <input
                                      type="checkbox"
                                      value={item.code}
                                      name={item.name}
                                      onChange={(e: any) => handleChangeCheckboxes(e.target)}
                                      />
                                      <label>
                                      {item?.name} | {item.variant[0].label} |{" "}
                                      {item.variant[1].label}
                                    </label> */}
                                    </div>
                                ))}
                                {isSubmitted && selectedItems.length === 0 && (
                                    <ErrorMessage message={"Selecione os produtos"} />
                                )}
                            </div>

                            {!REORDER_INFO?.disabledReason && (
                                <div className={styles.formItem}>
                                    <Select
                                        placeholder={reasonsType.name}
                                        className={styles.selectOption}
                                        options={reasonsType.items}
                                        styles={customSelectStyles}
                                        isClearable
                                        onChange={(e) => handleChangeReason(e)}
                                    />
                                    {errors.reason && (
                                        <ErrorMessage message={errors.reason.message} />
                                    )}
                                </div>
                            )}

                            <div className={styles.formItem}>
                                <textarea
                                    placeholder={REORDER_INFO?.labelNoteClient || "Motivos"}
                                    cols={30}
                                    rows={10}
                                    {...register("noteClient", { required : false })}
                                />
                                {errors.noteClient && (
                                    <ErrorMessage message={errors.noteClient.message} />
                                )}
                            </div>

                            <button
                                className={`${styles.reOrderButton} block buttonBlock`}
                                type="button"
                                disabled={loadingRequest && true}
                                onClick={handleSubmit(submitRequest)}
                            >
                                {loadingRequest ? <AnimatedLoading /> : "Solicitar"}
                            </button>
                        </form>
                    </div>
                )}
            </div>
        </div>
    );
}
