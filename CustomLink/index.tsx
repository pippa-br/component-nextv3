"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";

export function useCustomLink(href: any) 
{
    const pathname = usePathname();
    const isMobile = pathname.startsWith("/mobile");

    if (typeof href === "string") 
    {
        if (href === "#") 
        {
            return href;
        }

        return isMobile ? `/mobile${href}` : href;
    }

    if (typeof href === "object" && href.pathname) 
    {
        return {
            ...href,
            pathname : isMobile ? `/mobile${href.pathname}` : href.pathname,
        };
    }

    return href;
}

export default function CustomLink({ href, children, ...props }: any) 
{
    const url = useCustomLink(href);

    return (
        <Link href={url} {...props} prefetch={true}>
            {children}
        </Link>
    );
}
