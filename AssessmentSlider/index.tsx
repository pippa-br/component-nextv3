import { THEME_SETTING } from "@/setting/setting";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import type { Options } from "@splidejs/splide";
import { useRouter } from "next/navigation";
import { useRef } from "react";
import ReactStars from "react-stars";
import styles from "./styles.module.scss";

export const AssessmentsSlider = ({ comments }: any) => 
{
    const router = useRouter();
    const mainRef = useRef<any>(undefined);

    const mainOptions: Options = {
        rewind      : true,
        autoplay    : true,
        interval    : 3000,
        perPage     : 5,
        perMove     : 1,
        gap         : "1rem",
        height      : "auto",
        start       : 0,
        breakpoints : {
            1024 : {
                perPage : 1,
            },
        },
    };

    return (
        <div className={styles.assessmentsSlider}>
            <div className={styles.content}>
                <p className={styles.title}>Avaliações</p>
                <div className={styles.sliderComments}>
                    <Splide
                        options={mainOptions}
                        aria-labelledby=""
                        ref={mainRef}
                        className={styles.imagesContent}
                    >
                        {comments?.map((comment: any, index: any) => (
                            <SplideSlide key={index}>
                                <div className={styles.cardComments}>
                                    <div className={styles.top}>
                                        {comment?.owner ? (
                                            <p className={styles.title}>{comment?.owner?.name}</p>
                                        ) : (
                                            <p className={styles.title}>{comment?.name}</p>
                                        )}
                                        <div className={styles.avaliation}>
                                            <ReactStars
                                                count={5}
                                                size={20}
                                                edit={false}
                                                value={comment?.rating}
                                                color2="#ffd700"
                                            />
                                        </div>
                                        <p className={styles.comment}>{comment?.message}</p>
                                        <p
                                            className={styles.nameProduct}
                                            onClick={() =>
                                                router.push(
                                                    `${THEME_SETTING.productPath}${comment?.product?.slug}/${comment?.variant[0]?.value}/`,
                                                )
                                            }
                                        >
                                            {comment?.product?.name}
                                        </p>
                                    </div>
                                    {/* <p className={styles.date}>{formatDate(comment.postdate)}</p> */}
                                </div>
                            </SplideSlide>
                        ))}
                    </Splide>
                </div>

                <div className={styles.more}>
                    <p onClick={() => router.push("/avaliacoes/")}>Ver mais +</p>
                </div>
            </div>
        </div>
    );
};
