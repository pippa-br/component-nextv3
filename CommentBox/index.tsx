"use client";

import { useState } from "react";
import { Comments } from "../Comments";
import { CommentsRating } from "../CommentsRating";
import { ModalComments } from "../ModalComments";
import styles from "./styles.module.scss";

export const CommentBox = ({ comments, product }: any) => 
{
    const [ openModalComments, setOpenModalComments ] = useState(false);

    return (
        <>
            <div className={styles.commentBox} id="comments">
                <div>
                    {comments?.length > 0 && (
                        <CommentsRating comments={comments} product={product} />
                    )}
                </div>

                <div className={styles.commentsGrid}>
                    {comments?.slice(0, 4)?.map((coment: any, index: any) => (
                        <Comments
                            coment={coment}
                            comments={comments}
                            product={product}
                            key={index}
                        />
                    ))}
                </div>

                {comments?.length > 4 && (
                    <button
                        className={styles.buttonMoreComments}
                        onClick={() => setOpenModalComments(true)}
                    >
						Ler mais comentários
                    </button>
                )}
            </div>

            {openModalComments && (
                <ModalComments
                    comments={comments}
                    product={product}
                    close={setOpenModalComments}
                />
            )}
        </>
    );
};
