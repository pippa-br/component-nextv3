import {
    collectionDocument,
    getDocument,
} from "../../core-nextv3/document/document.api";
import { innerHTML } from "../../core-nextv3/util/util";
import { PAGE_SETTING } from "../../setting/setting";
import { PageTitle } from "../PageTitle";
import styles from "./styles.module.scss";

const GenerateStaticParams = async () => 
{
    const pages = await collectionDocument(
        PAGE_SETTING.merge({
            perPage : 100,
        }),
    );

    let paths = [];

    if (pages.collection) 
    {
        paths = pages.collection.map((item: any) => ({
            params : { slug : item.slug || "" },
        }));
    }

    console.info("(Content Static:", paths.length, ")");

    return paths;
};

const getPageData = async (slug: string) => 
{
    const result = await getDocument(
        PAGE_SETTING.merge({
            slug : slug,
        }),
    );

    return {
        pageData : result.data,
    };
};

const GenerateMetadata = async ({ params }: any) => 
{
    const { slug } = await params;
    const { pageData } = await getPageData(slug);

    return {
        title : pageData.name,
    };
};

const ContentPage = async ({
    params,
}: {
	params: Promise<{ slug: string }>;
}) => 
{
    const { slug } = await params;
    const { pageData } = await getPageData(slug);

    return (
        <div className={styles.contentPage}>
            <div className={styles.content}>
                <PageTitle name={pageData.name} />
                <div
                    className={styles.pageContent}
                    dangerouslySetInnerHTML={innerHTML(pageData.content)}
                />
            </div>
        </div>
    );
};

export { GenerateMetadata, GenerateStaticParams, ContentPage };
