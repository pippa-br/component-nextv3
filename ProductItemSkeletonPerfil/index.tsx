import { Box, Skeleton } from "@material-ui/core";
import styles from "./styles.module.scss";

export const ProductItemSkeletonPerfil = () => 
{
    return (
        <Box sx={{ width : 300 }} className={styles.productItemSkeletonPerfil}>
            <div className={styles.productImage}>
                <Skeleton
                    animation="wave"
                    variant="rectangular"
                    style={{ height : 20 }}
                />
            </div>
            <div className={styles.skeletonBody}>
                <div>
                    <div className={styles.productActions}>
                        <Skeleton
                            animation="wave"
                            variant="text"
                            width={"100%"}
                            height={40}
                        />
                    </div>
                    <div className={styles.productActions}>
                        <Skeleton
                            animation="wave"
                            variant="text"
                            width={"100%"}
                            height={40}
                        />
                    </div>
                    <div className={styles.productActions}>
                        <Skeleton
                            animation="wave"
                            variant="text"
                            width={"100%"}
                            height={40}
                        />
                    </div>
                    <div className={styles.productActions}>
                        <Skeleton
                            animation="wave"
                            variant="text"
                            width={"100%"}
                            height={40}
                        />
                    </div>
                    <div className={styles.productActions}>
                        <Skeleton
                            animation="wave"
                            variant="text"
                            width={"100%"}
                            height={40}
                        />
                    </div>
                </div>
                <div>
                    <div className={styles.productActions}>
                        <Skeleton
                            animation="wave"
                            variant="text"
                            width={"100%"}
                            height={40}
                        />
                    </div>
                    <div className={styles.productActions}>
                        <Skeleton
                            animation="wave"
                            variant="text"
                            width={"100%"}
                            height={40}
                        />
                    </div>
                    <div className={styles.productActions}>
                        <Skeleton
                            animation="wave"
                            variant="text"
                            width={"100%"}
                            height={40}
                        />
                    </div>
                    <div className={styles.productActions}>
                        <Skeleton
                            animation="wave"
                            variant="text"
                            width={"100%"}
                            height={40}
                        />
                    </div>
                    <div className={styles.productActions}>
                        <Skeleton
                            animation="wave"
                            variant="text"
                            width={"100%"}
                            height={40}
                        />
                    </div>
                </div>
            </div>
        </Box>
    );
};
