import { useResponsive } from "../../core-nextv3/util/useResponsive";
import { innerHTML } from "../../core-nextv3/util/util";
import styles from "./styles.module.scss";

export const CartMessage = ({ cartPageData }: any) => 
{
    // console.log("aqui", cartMessage);
    const { isDesktop } = useResponsive();

    return (
        <div className={styles.cartMessage}>
            <div className={styles.content}>
                {/* {cartMessage?.announcement?.map((message: any, index: any) => (
                  <div className={styles.top} key={index}>
                    {message?.imageDesktop && (
                      <img src={message?.imageDesktop?._url} alt="" />
                    )}
                    <div className={styles.text}>
                      <h1>{message?.name}</h1>
                      <div
                        dangerouslySetInnerHTML={innerHTML(message?.content)}
                        className={styles.text}
                      ></div>
                    </div>
                  </div>
                ))} */}
                <div className={styles.description}>
                    {/* <h1>{cartMessage?.name}</h1> */}

                    {isDesktop && cartPageData?.desktop && (
                        <img src={cartPageData?.desktop?._url} alt="" />
                    )}

                    {!isDesktop && cartPageData?.mobile && (
                        <img src={cartPageData?.mobile?._url} alt="" />
                    )}
                    <div
                        dangerouslySetInnerHTML={innerHTML(cartPageData?.description)}
                        className={styles.text}
                    />
                </div>
            </div>
        </div>
    );
};
