"use client";

import { useResponsive } from "@/core-nextv3/util/useResponsive";
import { getRecaptcha } from "@/core-nextv3/util/util";
import { Step, StepLabel, Stepper } from "@material-ui/core";
import { initMercadoPago } from "@mercadopago/sdk-react";
import { createCardToken } from "@mercadopago/sdk-react/esm/coreMethods";
import { useTranslations } from "next-intl";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import {
    couponCheckoutAnalytics,
    finishedCheckoutAnalytics,
    initCheckoutAnalytics,
    paymentMethodCheckoutAnalytics,
    shippingCheckoutAnalytics,
} from "../../core-nextv3/analytics/analytics.api";
import {
    delCouponCart,
    delDiscountClientCart,
    setDiscountClientCart,
    setShippingMethodCart,
} from "../../core-nextv3/cart/cart.api";
import {
    checkoutCart,
    setCouponCart,
    setCreditCardCart,
    setInstallmentCart,
    setPaymentMethodCart,
} from "../../core-nextv3/cart/cart.api";
import {
    firstImageItemCart,
    hasVariantItemCart,
    variantItemCart,
} from "../../core-nextv3/cart/cart.util";
import { useCore } from "../../core-nextv3/core/core";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { currencyMask } from "../../core-nextv3/util/mask";
import {
    CART_SETTING,
    ORDER_SETTING,
    THEME_SETTING,
} from "../../setting/setting";
import { AccordionCheckout } from "../AccordionCheckout";
import { CheckoutInformationTwo } from "../CheckoutInformationTwo";
import { CheckoutPayment } from "../CheckoutPayment";
import { CheckoutProductsTable } from "../CheckoutProductsTable";
import { DiscountPaymentMethod } from "../DiscountPaymentMethod";
import ErrorMessage from "../ErrorMessage";
import { Gifts } from "../Gifts";
import { ImageSet } from "../ImageSet";
import { InputCalculateShipping } from "../InputCalculateShipping";
import styles from "./styles.module.scss";
declare const window: any;

export const CheckoutFormTwo = ({
    account,
    redirectUrl,
    hasShipping = true,
    giftPage,
    installmentRuleData,
}: any) => 
{
    const t = useTranslations();
    const steps = getSteps();
    const router = useRouter();
    const { isDesktop } = useResponsive();
    const [ skipped, setSkipped ] = useState(new Set());
    const [ checkedOut, setCheckedOut ] = useState(false);
    const [ activeStep, setActiveStep ] = useState<number>(0);
    const [ loadingCart, setLoadingCart ] = useState(false);
    const [ checkoutHeader, setCheckoutHeader ] = useState(true);
    const { user, cart, setCart } = useCore();
    const [ paymentMethodsList ] = useState(() => 
    {
        if (installmentRuleData?.paymentMethods) 
        {
            const list: any = [];

            for (const item of installmentRuleData.paymentMethods) 
            {
                list.push(item.type);
            }

            return list;
        }

        return [];
    });

    const {
        register,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm();

    useEffect(() => 
    {
        if (!checkedOut && cart) 
        {
            tagManager4.checkoutStep(cart, "initiateCheckout", 1, user);

            // INICIA CHECKOUT
            initCheckoutAnalytics();
            setCheckedOut(true);
        }
    }, [ cart ]);

    function getSteps() 
    {
        if (hasShipping) 
        {
            return [ "Carrinho", "Informações", "Pagamento" ];
        }

        return [ "Informações", "Pagamento" ];
    }

    const goBack = () => 
    {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    function getStepContent(step: any) 
    {
        const steps = [];

        steps.push(
            <CheckoutProductsTable productsUrl="/produtos" handleNext={handleNext} />,
        );

        steps.push(
            <CheckoutInformationTwo
                loadingCart={loadingCart}
                setLoadingCart={setLoadingCart}
                handleNext={handleNext}
                hasShipping={hasShipping}
                goBack={goBack}
                onSubmitShipping={onSubmitCartShipping}
            />,
        );

        steps.push(
            <CheckoutPayment
                setLoadingCart={setLoadingCart}
                loadingCart={loadingCart}
                installmentRule={installmentRuleData}
                paymentMethods={paymentMethodsList}
                hasShipping={hasShipping}
                onSubmitPayment={onSubmitCartPaymentMethod}
                onCreditCardInstallment={handleCreditCardInstallment}
                onSubmit={onSubmitCheckout}
            />,
        );

        return steps[step];
    }

    const isStepSkipped = (step: any) => 
    {
        return skipped.has(step);
    };

    const handleNext = () => 
    {
        let newSkipped = skipped;

        if (isStepSkipped(activeStep)) 
        {
            newSkipped = new Set(newSkipped.values());
            newSkipped.delete(activeStep);
        }

        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        setSkipped(newSkipped);

        window.scroll({
            top      : 0,
            left     : 0,
            behavior : "instant",
        });
    };

    const goStep = (index: number) => 
    {
        if (index < activeStep) 
        {
            setActiveStep(index);
        }
    };

    async function handleCreditCardInstallment(installment: any) 
    {
        const newData = {
            data : installment,
        };

        setLoadingCart(true);

        const result = await setInstallmentCart(CART_SETTING.merge(newData));

        setLoadingCart(false);

        if (result.status) 
        {
            setCart(result.data);
        }
        else 
        {
            return toast.error(
                t("Verifique se foi selecionado um parcelamento válido."),
                { duration : 2000 },
            );
        }
    }

    const onSubmitCartShipping = async (data: any) => 
    {
        if (!data.id) 
        {
            return toast.error(t("Selecione uma das formas de entrega!"), {
                duration : 2000,
            });
        }

        const newData = {
            data : data,
        };

        setLoadingCart(true);

        const result = await setShippingMethodCart(CART_SETTING.merge(newData));

        setLoadingCart(false);

        if (result?.data?.shipping) 
        {
            const shippingData = {
                key   : "shipping_tier",
                value : result.data.shipping.id,
            };
            tagManager4.checkoutStep(result.data, "checkoutShipping", 2, user, [
                shippingData,
            ]);

            // ADDRESS ANALYTICS
            shippingCheckoutAnalytics(data.name);

            setCart(result.data);
            handleNext();
        }
        else 
        {
            return toast.error(t("Verifique se foi selecionado um método válido."), {
                duration : 2000,
            });
        }
    };

    const onSubmitCartPaymentMethod = async (data: any) => 
    {
        const newData = {
            data : data,
        };

        setLoadingCart(true);

        const result = await setPaymentMethodCart(CART_SETTING.merge(newData));

        setLoadingCart(false);

        if (result?.data?.paymentMethod) 
        {
            // PAYMENT ANALYTICS
            const paymentData = {
                key   : "payment_type",
                value : result.data.paymentMethod.value,
            };
            tagManager4.checkoutStep(result.data, "checkoutPayment", 3, user, [
                paymentData,
            ]);
            paymentMethodCheckoutAnalytics(data.label);

            setCart(result.data);
        }
        else 
        {
            return toast.error(t("Verifique se foi selecionado um método válido."), {
                duration : 2000,
            });
        }
    };

    const onSubmitCheckout = async (data?: any) => 
    {
        if (!cart?.paymentMethod) 
        {
            setLoadingCart(false);
            return toast.error(t("Selecione a forma de pagamento."), {
                duration : 2000,
            });
        }

        setLoadingCart(true);

        if (cart?.paymentMethod?.value === "credit_card" && data) 
        {
            const creditCardData: any = {
                data : {
                    cardnumber : data.cardnumber,
                    owner      : data.owner,
                    cvv        : data.cvv,
                    expirydate : data.expirydate,
                    docType    : {
                        value : "cpf",
                        label : "CPF",
                        id    : "cpf",
                        type  : "individual",
                    },
                    _cpf               : data.cpf,
                    sameAddressShiping : data.sameAddressShiping,
                },
            };

            if (THEME_SETTING.mercadoPago) 
            {
                if (!process.env.NEXT_PUBLIC_MERCADO_PAGO_PUBLIC_KEY) 
                {
                    return toast.error(t("Mercado Pago não configurado!"), {
                        duration : 2000,
                    });
                }

                initMercadoPago(process.env.NEXT_PUBLIC_MERCADO_PAGO_PUBLIC_KEY);

                // https://www.mercadopago.com.br/developers/pt/docs/your-integrations/test/cards
                // cartao test caso aprovado aprovado
                // {
                //   cardNumber: "5031433215406351",
                //   securityCode: "123",
                //   cardExpirationMonth: "11",
                //   cardExpirationYear: "2025",
                //   cardholderName: "APRO",
                //   identificationType: "CPF",
                //   identificationNumber: "12345678909",
                // }

                // const identificationNumber = data?.cpf && typeof data.cpf === 'string' ? data.cpf.replace(/\D/g, '') : null;
                // const identificationType = identificationNumber?.length === 11 ? 'CPF' : 'CNPJ';

                const cardToken = await createCardToken({
                    cardNumber           : data?.cardnumber,
                    securityCode         : data?.cvv,
                    cardExpirationMonth  : data?.expirydate.slice(0, 2),
                    cardExpirationYear   : data?.expirydate.slice(-2),
                    cardholderName       : data?.owner,
                    identificationType   : "CPF",
                    identificationNumber : data?.cpf,
                });

                creditCardData.data.cardToken = cardToken?.id;
            }

            if (data.zipcode) 
            {
                creditCardData.data.address = {
                    zipcode     : data?.zipcode || "",
                    street      : data?.street || "",
                    housenumber : data?.housenumber || "",
                    complement  : data?.complement || "",
                    district    : data?.district || "",
                    city        : data?.city || "",
                    state       : data?.state || "",
                    country     : { id : "br", label : "Brasil", value : "br", selected : true },
                };
            }

            const token = await getRecaptcha("setCreditCard");

            if (token) 
            {
                creditCardData.token = token;
                const result = await setCreditCardCart(
                    CART_SETTING.merge(creditCardData),
                );

                if (result.status) 
                {
                    setCart(result.data);
                }
                else 
                {
                    setLoadingCart(false);

                    return toast.error(t("Verifique os dados do cartão."), {
                        duration : 2000,
                    });
                }
            }
            else 
            {
                setLoadingCart(false);
                return toast.error(t("token invalido!"), { duration : 2000 });
            }
        }

        const token = await getRecaptcha("checkout");

        if (token) 
        {
            const result2 = await checkoutCart(
                ORDER_SETTING.merge({
                    token : token,
                    cart  : {
                        referencePath : cart?.referencePath,
                    },
                }),
            );

            if (!result2.status) 
            {
                let message = "";

                if (typeof result2.error === "object") 
                {
                    for (const key in result2.error) 
                    {
                        message += result2.error[key];
                    }
                }
                else 
                {
                    message = result2.error;
                }

                setLoadingCart(false);

                return toast.error(message, { duration : 2000 });
            }

            // CHECKOUT ANALYTICS
            finishedCheckoutAnalytics();

            toast.success(
                t("Pedido sendo processado! Estamos te redirecionando..."),
                { icon : "👏", duration : 2000 },
            );

            setCart(null);

            router.push(`${redirectUrl}/${result2.data.id}/?success=true`);
        }
        else 
        {
            toast.error(t("token invalido!"), { duration : 2000 });
            setLoadingCart(false);
        }
    };

    const onApplyCoupon = async (data: any) => 
    {
        const newData = {
            _code : data.coupon.toUpperCase().replace(/\s/g, ""),
        };

        setLoadingCart(true);

        const result = await setCouponCart(CART_SETTING.merge(newData));

        setLoadingCart(false);

        if (result.error) 
        {
            return toast.error(result.error, { duration : 2000 });
        }

        // COUPON ANALYTICS
        couponCheckoutAnalytics(data.coupon.toUpperCase());

        setCart(result.data);
    };

    const onRemoveCoupon = async () => 
    {
        setLoadingCart(true);

        const result = await delCouponCart(CART_SETTING);

        setLoadingCart(false);

        if (result.error) 
        {
            return toast.error(result.error, { duration : 2000 });
        }

        setValue("coupon", "");

        setCart(result.data);
    };

    const onApplyDiscountClient = async () => 
    {
        setLoadingCart(true);

        const result = await setDiscountClientCart(CART_SETTING);

        setLoadingCart(false);

        if (result.error) 
        {
            return toast.error(result.error, { duration : 2000 });
        }

        setCart(result.data);
    };

    const onRemoveDiscountClient = async () => 
    {
        setLoadingCart(true);

        const result = await delDiscountClientCart(CART_SETTING);

        setLoadingCart(false);

        if (result.error) 
        {
            return toast.error(result.error, { duration : 2000 });
        }

        setCart(result.data);
    };

    // const onDelete = (item: any) =>
    // {
    //     return toast(
    //         (t) => (
    //             <span>
    //       Deseja deletar esse item?
    //                 <br />
    //                 <br />
    //                 <button
    //                     onClick={() =>
    //                     {
    //                         toast.dismiss(t.id)
    //                         handleDeleteProduct(item)
    //                     }}
    //                 >
    //         Sim
    //                 </button>
    //                 <button onClick={() => toast.dismiss(t.id)}>Não</button>
    //             </span>
    //         ),
    //         {
    //             className : "toast-custom",
    //             duration  : 999999,
    //         }
    //     )
    // }

    // async function handleDeleteProduct(item: any)
    // {
    //     const product = item.product
    //     const variant = item.variant
    //     product.id = item.id

    //     product.id = item.id
    //     product.variant = variant

    //     tagManager4.removeFromCart(pathname, product, item.quantity, user)

    //     const newData: any = {
    //         data : {
    //             product : {
    //                 referencePath : product.referencePath,
    //             },
    //         },
    //     }

    //     // PRODUCT VARIANT
    //     if (variant)
    //     {
    //         newData.data.variant = [ ...variant ]
    //     }

    //     let result

    //     if (item.gift)
    //     {
    //         result = await delGiftCart(CART_SETTING.merge(newData))
    //     }
    //     else
    //     {
    //         result = await delItemCart(CART_SETTING.merge(newData))
    //     }

    //     toast.success("Produto removido!", { icon : "💔", duration : 2000 });

    //     setCart(result.data)
    // }

    return (
        <div
            className={`${styles.checkoutFormTwo} ${styles[`step-${activeStep}`]}`}
        >
            <div className={styles.checkoutContent}>
                <div className={styles.checkoutTop}>
                    <ImageSet
                        onClick={() => router.push("/")}
                        className={styles.logo}
                        src="/assets/logo.png"
                        responsive={false}
                        alt={account?.name}
                        width={THEME_SETTING.widthLogo}
                        height={THEME_SETTING.heightLogo}
                    />

                    <div className={styles.steps}>
                        <Stepper activeStep={activeStep}>
                            {steps.map((label, index) => 
                            {
                                const stepProps = {};
                                const labelProps = {};
                                return (
                                    <Step
                                        key={label}
                                        {...stepProps}
                                        className={styles.checkoutStep}
                                        onClick={() => goStep(index)}
                                    >
                                        <StepLabel {...labelProps}>{t(label)}</StepLabel>
                                    </Step>
                                );
                            })}
                        </Stepper>
                    </div>
                </div>

                <div
                    className={
                        cart?.items?.length > 0 ? styles.content : styles.contentFlex
                    }
                >
                    <div className={styles.checkoutStepper}>
                        {activeStep >= 1 && user?.name && user?.email && (
                            <div
                                className={styles.checkoutInfo}
                                onClick={() => 
                                {
                                    setActiveStep(0);
                                }}
                            >
                                <p>
									1 - <b>{t("Dados")}:</b> <strong>{t("Nome:")}</strong>{" "}
                                    {user?.name}, <strong>{t("Email:")}</strong> {user?.email}
                                </p>
                                <ImageSet
                                    width={20}
                                    height={20}
                                    src="/assets/icons/BiEditAlt.svg"
                                    responsive={false}
                                    alt=""
                                />
                            </div>
                        )}

                        {activeStep >= 2 && cart?.address && (
                            <div
                                className={styles.checkoutInfo}
                                onClick={() => 
                                {
                                    setActiveStep(1);
                                }}
                            >
                                <p>
									2 - <b>{t("Enviar para")}:</b> {cart?.address?.street},
                                    {t("Nº")} {cart?.address?.housenumber},{" "}
                                    {cart?.address?.district}, {cart?.address?.city},{" "}
                                    {cart?.address?.state}. {t("CEP")}: {
                                        cart?.address?.zipcode
                                    }{" "}
                                </p>
                                <ImageSet
                                    width={20}
                                    height={20}
                                    src="/assets/icons/BiEditAlt.svg"
                                    responsive={false}
                                    alt=""
                                />
                            </div>
                        )}

                        {/* {activeStep >= 3 && cart?.shipping && <div className={styles.checkoutInfo}  onClick={() => {
                          setActiveStep(2)
                        }}>
                        <p>
                          3 - <b>{t('Método')}:</b> {cart?.shipping?.label}
                        </p>
                        <Image
                          width={20}
                          height={20}
                          src="/assets/icons/BiEditAlt.svg"
                          loader={cloudflareLoader}
                          alt=""
                        />
                      </div>} */}

                        {getStepContent(activeStep)}
                    </div>

                    {cart?.items?.length > 0 ? (
                        <div className={`${styles.checkoutCart} checkoutCart`}>
                            <div
                                className={styles.checkoutHeader}
                                onClick={() => setCheckoutHeader(!checkoutHeader)}
                            >
                                <p className={styles.title}>
									Resumo do pedido:{" "}
                                    {!checkoutHeader && (
                                        <ImageSet
                                            className={styles.noActive}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/TiArrowSortedDown.svg"
                                            responsive={false}
                                            alt=""
                                        />
                                    )}
                                    {checkoutHeader && (
                                        <ImageSet
                                            className={styles.active}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/TiArrowSortedUp.svg"
                                            responsive={false}
                                            alt=""
                                        />
                                    )}
                                </p>
                            </div>
                            {checkoutHeader && (
                                <>
                                    {activeStep ? (
                                        <div className={styles.body}>
                                            {isDesktop && (
                                                <div className={styles.titles}>
                                                    <p className={styles.title}>Produtos</p>
                                                    <div className={styles.right}>
                                                        <p className={styles.title}>Valor Unitário</p>
                                                        <p className={styles.title}>Valor Total</p>
                                                    </div>
                                                </div>
                                            )}
                                            {cart?.items?.length > 0 &&
												cart?.items.map((item: any, index: any) => (
												    <div className={styles.item} key={index}>
												        <div className={styles.product}>
												            <div className={styles.img}>
												                <ImageSet
												                    image={firstImageItemCart(item)}
												                    width={THEME_SETTING.widthProductThumb}
												                    height={THEME_SETTING.heightProductThumb}
												                    sizes="5vw"
												                />
												            </div>
												            <div className={styles.info}>
												                <div className={styles.productName}>
												                    {item.product.name}
												                </div>
												                {hasVariantItemCart(item, 0) && (
												                    <p className={styles.productColor}>
												                        {t("Cor")}: {variantItemCart(item, 0)}
												                    </p>
												                )}
												                {hasVariantItemCart(item, 1) && (
												                    <p className={styles.productColor}>
												                        {t("Tamanho")}: {variantItemCart(item, 1)}
												                    </p>
												                )}
												                <p className={styles.productQuantity}>
												                    {t("Quantidade")}: {item.quantity}
												                </p>
												                {!isDesktop && (
												                    <div className={styles.prices}>
												                        <div className={styles.priceItem}>
												                            {item?.promotionalPrice > 0 && (
												                                <p className={styles.pricePromotional}>
												                                    {currencyMask(item.realPrice)}
												                                </p>
												                            )}
												                            <p className={styles.price}>
												                                {currencyMask(item.price)}
												                            </p>
												                        </div>
												                        <div className={styles.price}>
												                            {currencyMask(item.total)}
												                        </div>
												                    </div>
												                )}
												            </div>
												        </div>
												        {isDesktop && (
												            <div className={styles.prices}>
												                <div className={styles.priceItem}>
												                    {item?.promotionalPrice > 0 && (
												                        <p className={styles.pricePromotional}>
												                            {currencyMask(item.realPrice)}
												                        </p>
												                    )}
												                    <p className={styles.price}>
												                        {currencyMask(item.price)}
												                    </p>
												                </div>
												                <div className={styles.price}>
												                    {currencyMask(item.total)}
												                </div>
												            </div>
												        )}
												    </div>
												))}
                                            {cart?.items?.length === 0 && (
                                                <div className={styles.noItem}>
                                                    <ImageSet
                                                        width={20}
                                                        height={20}
                                                        src="/assets/icons/BsBagX.svg"
                                                        responsive={false}
                                                        alt=""
                                                    />
                                                    <span>Carrinho Vazio!</span>
                                                </div>
                                            )}
                                        </div>
                                    ) : null}

                                    {activeStep === 0 ? (
                                        <div className={styles.checkoutCoupon}>
                                            {giftPage?.published && <Gifts page={giftPage} />}
                                            <label>{t("Cupom")}: </label>
                                            <div className={styles.inputController}>
                                                <input
                                                    defaultValue={cart?.coupon?._code}
                                                    {...register("coupon", {
                                                        validate : (value) =>
                                                            value.length >= 1 || t("Cupom inválido!"),
                                                    })}
                                                    readOnly={cart?.coupon?._code}
                                                    type="text"
                                                    placeholder={t("Digite aqui seu cupom")}
                                                />
                                                {cart?.coupon?._code ? (
                                                    <button
                                                        type="button"
                                                        onClick={() => onRemoveCoupon()}
                                                    >
                                                        {t("Remover Cupom")}
                                                    </button>
                                                ) : (
                                                    <button
                                                        type="button"
                                                        onClick={handleSubmit(onApplyCoupon)}
                                                    >
                                                        {t("Aplicar Cupom")}
                                                    </button>
                                                )}
                                            </div>
                                            {errors.coupon && (
                                                <ErrorMessage message={errors.coupon.message} />
                                            )}

                                            <AccordionCheckout title="Informações Frete e Prazo de Entrega">
                                                <div className={styles.calculateShipping}>
                                                    <InputCalculateShipping
                                                        disabledDiff={true}
                                                        title={t("Calcule o frete e prazo de entrega")}
                                                        disabledTitles={true}
                                                    />
                                                </div>
                                            </AccordionCheckout>

                                            {!THEME_SETTING.disabledInformationPaymenteCart && (
                                                <AccordionCheckout title="Informações de Pagamentos">
                                                    <DiscountPaymentMethod
                                                        totalItems={cart?.totalItems}
                                                        installmentRule={installmentRuleData}
                                                        totalDiscount={cart?.totalDiscount}
                                                        totalShipping={cart?.totalShipping}
                                                        discountPaymentMethod={cart?.discountPaymentMethod}
                                                    />
                                                </AccordionCheckout>
                                            )}
                                        </div>
                                    ) : (
                                        ""
                                    )}

                                    {cart && cart?.creditClient > 0 && (
                                        <div className={styles.creditClient}>
                                            <label>
                                                {t("Você possui")} {currencyMask(cart?.creditClient)}{" "}
                                                {t("em crédito")}
                                                {cart?.creditClient > 0 && (
                                                    <small>
														Utilizado: - {currencyMask(cart?.discountClient)}
                                                    </small>
                                                )}
                                            </label>
                                            <div className={styles.inputController}>
                                                {cart?.discountClient ? (
                                                    <button
                                                        className={styles.cancelButton}
                                                        type="button"
                                                        onClick={onRemoveDiscountClient}
                                                    >
                                                        {t("Remover Crédito Neste Pedido")}
                                                    </button>
                                                ) : (
                                                    <button type="button" onClick={onApplyDiscountClient}>
                                                        {t("Aplicar Crédito Neste Pedido")}
                                                    </button>
                                                )}
                                            </div>
                                            {errors.coupon && (
                                                <ErrorMessage message={errors.coupon.message} />
                                            )}
                                        </div>
                                    )}

                                    <div className={styles.checkoutResume}>
                                        <div className={styles.checkoutSubtotal}>
                                            <p>
                                                {t("Subtotal")}:{" "}
                                                <span>{currencyMask(cart?.totalItems)}</span>
                                            </p>
                                            {cart?.totalCreditDiscount && (
                                                <p>
                                                    {t("Desconto em crédito")}:{" "}
                                                    <span>{currencyMask(cart?.totalCreditDiscount)}</span>
                                                </p>
                                            )}
                                            <p>
                                                <span>
                                                    {t("Desconto")}:
                                                    {cart?.discountPaymentMethod < 0 && (
                                                        <small>({cart?.paymentMethod?.label})</small>
                                                    )}
                                                    {cart?.coupon && <small>({t("Cupom")})</small>}
                                                    {cart?.discountClient > 0 && (
                                                        <small>({t("Crédito")})</small>
                                                    )}
                                                </span>
                                                <span>{currencyMask(cart?.totalDiscount)}</span>
                                            </p>
                                            <p>
                                                {t("Frete")}:
                                                <span>{currencyMask(cart?.shipping?.value || 0)}</span>
                                            </p>
                                            {cart && cart?.totalInterest > 0 && (
                                                <p>
                                                    {t("Juros")}:
                                                    <span>{currencyMask(cart?.totalInterest || 0)}</span>
                                                </p>
                                            )}
                                        </div>
                                        <div className={styles.checkoutTotal}>
                                            <p>
                                                {t("Total")}: <span>{currencyMask(cart?.total)}</span>
                                            </p>
                                        </div>
                                    </div>
                                </>
                            )}
                        </div>
                    ) : (
                        <div className={`${styles.checkoutCart} checkoutCart`} />
                    )}
                </div>

                {cart?.items?.length > 0 && (
                    <div className={styles.security}>
                        <p>
                            <strong>{t("Pagamento Seguro SSL")}</strong>
                            <br />
                            {t(
                                "Sua criptografia é protegida por criptografia SSL de 256 bits",
                            )}
							.
                        </p>
                        <ImageSet
                            width={300}
                            height={58}
                            src="/assets/seguranca.png"
                            alt="Segurança"
                            responsive={false}
                        />
                    </div>
                )}
            </div>
        </div>
    );
};
