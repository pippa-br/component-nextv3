import { useProtectedAuth } from "@/core-nextv3/auth/user.auth.api";
import type { GetStaticProps } from "next";
import LoginForm from "../../component-nextv3/LoginForm";
import { PageTitle } from "../../component-nextv3/PageTitle";
import { AUTH_SETTING, THEME_SETTING } from "../../setting/setting";
import withHeader from "../../utils/withHeader";
import { LoadingPage } from "../LoadingPage";
import styles from "./styles.module.scss";

const LoginPage = () => 
{
    const loadProtectedAuth = useProtectedAuth(AUTH_SETTING, "/", "");

    if (!loadProtectedAuth) 
    {
        return <LoadingPage />;
    }

    return (
        <>
            <div className={styles.loginPage}>
                <div className={styles.content}>
                    <PageTitle name="Login" />

                    <LoginForm />
                </div>
            </div>
        </>
    );
};

const getStaticProps: GetStaticProps = () =>
    withHeader(async (props: any) => 
    {
        return {
            revalidate : THEME_SETTING.revalidate,
            props      : {
                seo : props?.seo?.merge({ title : "Login" }),
            },
        };
    });

export { getStaticProps as GetStaticProps, LoginPage };
