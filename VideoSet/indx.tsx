"use client";

import { useEffect, useRef } from "react";
import { AspectRatio } from "react-aspect-ratio";
import videojs from "video.js";
import styles from "./styles.module.scss";

export const VideosSet = ({
    video,
    autoplay = true,
    controls = false,
    loop = true,
    field = "_url",
    width = 1920,
    height = 813,
}: any) => 
{
    const videoRef = useRef<any>(null);
    const playerRef = useRef<any>(null);

    useEffect(() => 
    {
        const initializePlayer = async () => 
        {
            if (video && videoRef.current) 
            {
                // if (playerRef.current)
                // {
                //     playerRef.current.dispose();
                // }

                playerRef.current = videojs(videoRef.current, {
                    controls    : controls,
                    autoplay    : autoplay,
                    muted       : true,
                    loop        : loop,
                    playsinline : true,
                    preload     : "metadata",
                    //poster      : video.thumb,
                    html5       : {
                        hls : {
                            overrideNative               : true,
                            smoothQualityChange          : true,
                            useBandwidthFromLocalStorage : true,
                        },
                    },
                    sources : [
                        {
                            src  : video[field],
                            type : "application/x-mpegURL",
                        },
                    ],
                });

                playerRef.current.on("error", () => 
                {
                    const error = playerRef.current.error(); // Captura o objeto de erro
                    console.info("Erro ao carregar vídeo:", error);

                    if (error) 
                    {
                        console.info(
                            `Erro: ${error.message || "Falha ao carregar o vídeo."}`,
                        );
                    }
                });
            }
        };

        initializePlayer();

        return () => 
        {
            // // Dispose para evitar vazamento de memória
            // if (playerRef.current)
            // {
            //     playerRef.current.dispose();
            // }
        };
    }, [ video ]);

    return (
        <div className={styles.videosSet}>
            <AspectRatio ratio={width / height}>
                <div className={styles.videoContent}>
                    {/* biome-ignore lint/a11y/useMediaCaption: <explanation> */}
                    <video
                        ref={videoRef}
                        className={styles.player}
                        controls
                        preload="auto"
                        width="100%"
                        height="100%"
                        poster="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw=="
                    />
                </div>
            </AspectRatio>
        </div>
    );
};
