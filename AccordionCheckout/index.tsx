"use client";

import { useState } from "react";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

export const AccordionCheckout = ({ children, title }: any) => 
{
    const [ toggle, setToggle ] = useState(false);

    return (
        <div
            className={styles.accordionCheckout}
            onClick={() => setToggle(!toggle)}
        >
            <p
                className={`${styles.accordionTitle} ${toggle ? styles.open : styles.closed}`}
            >
                {title}
                {toggle ? (
                    <ImageSet
                        className={styles.accordionSvg}
                        width={20}
                        height={20}
                        src="/assets/icons/TiArrowSortedUp.svg"
                        responsive={false}
                        alt=""
                    />
                ) : (
                    <ImageSet
                        className={styles.accordionSvg}
                        width={20}
                        height={20}
                        src="/assets/icons/TiArrowSortedDown.svg"
                        responsive={false}
                        alt=""
                    />
                )}
            </p>
            <div
                onClick={(e) => e.stopPropagation()}
                className={`${styles.accordionContent} ${
                    toggle ? styles.open : styles.closed
                }`}
            >
                {children}
            </div>
        </div>
    );
};
