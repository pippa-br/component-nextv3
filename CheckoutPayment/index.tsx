"use client";

import { useCore } from "@/core-nextv3/core/core";
import ccValid from "card-validator";
import { useTranslations } from "next-intl";
import Image from "next/image";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { PatternFormat } from "react-number-format";
import Select from "react-select";
import { percentageByPaymentMethod } from "../../core-nextv3/price/price.util";
import {
    buscaCep2,
    cloudflareLoader,
    validateCreditCardDate,
} from "../../core-nextv3/util/util";
import { validateCpf, validateName } from "../../core-nextv3/util/validate";
import { AnimatedLoading } from "../AnimatedLoading";
import ErrorMessage from "../ErrorMessage";
import styles from "./styles.module.scss";

export const CheckoutPayment = ({
    loadingCart,
    onSubmitPayment,
    onCreditCardInstallment,
    paymentMethods,
    onSubmit,
    hasShipping = true,
    installmentRule = null,
}: any) => 
{
    const { cart } = useCore();
    const [ monthCreditCard, setMonthCreditCard ] = useState();
    const [ yearCreditCard, setYearCreditCard ] = useState();
    const [ sameAddressShiping, setSameAddressShiping ] = useState(hasShipping);
    const t = useTranslations();

    const customSelectStyles: any = {
        control : (base: any, state: any) => ({
            ...base,
            width        : "100%",
            height       : "45px",
            borderRadius : "4px",
            borderColor  : state.isFocused
                ? "var(--border-color)"
                : "var(--border-color)",
            boxShadow : state.isFocused ? "none" : "",
            "&:hover" : {
                borderColor : "var(--border-color)",
            },
            color : "#000",
        }),
        indicatorContainer : (styles: any) => ({ ...styles, padding : "0 8px" }),
        indicatorSeparator : () => "",
        placeholder        : (styles: any) => ({ ...styles, fontSize : "16px" }),
        menu               : (styles: any) => ({ ...styles, color : "#000", fontSize : "16px" }),
        singleValue        : (styles: any) => ({
            ...styles,
            color    : "#000",
            fontSize : "16px",
        }),
    };

    const {
        register,
        handleSubmit,
        setValue,
        trigger,
        control,
        formState: { errors, isSubmitted },
    } = useForm();

    const getMonthOptions = () => 
    {
        return [
            { value : "01", label : "01" },
            { value : "02", label : "02" },
            { value : "03", label : "03" },
            { value : "04", label : "04" },
            { value : "05", label : "05" },
            { value : "06", label : "06" },
            { value : "07", label : "07" },
            { value : "08", label : "08" },
            { value : "09", label : "09" },
            { value : "10", label : "10" },
            { value : "11", label : "11" },
            { value : "12", label : "12" },
        ];
    };

    const getYearOptions = () => 
    {
        const options = [];
        let fullYear = new Date().getFullYear();

        for (let i = 1; i <= 20; i++) 
        {
            options.push({ value : fullYear.toString().substr(-2), label : fullYear });
            fullYear++;
        }

        return options;
    };

    useEffect(() => 
    {
        register("sameAddressShiping", { value : sameAddressShiping });
        register("expirydate", { value : null, required : true });
        register("installment", { value : cart?.installment, required : true });
    }, []);

    const [ selectedPaymentMethod, setSelectedPaymentMethod ] = useState(
        cart?.paymentMethod || {
            id    : "",
            label : "",
            value : "",
        },
    );

    function handleChangeMonth(data: any) 
    {
        setMonthCreditCard(data.value);
        handleChangeExpirydate(data.value, yearCreditCard);
    }

    function handleChangeYear(data: any) 
    {
        setYearCreditCard(data.value);
        handleChangeExpirydate(monthCreditCard, data.value);
    }

    function handleChangeExpirydate(month: any, year: any) 
    {
        if (month && year) 
        {
            if (validateCreditCardDate(month, year)) 
            {
                setValue("expirydate", `${month}/${year}`);
            }
            else 
            {
                setValue("expirydate", null);
            }

            trigger("expirydate");
        }
    }

    function handleChangeInstallment(e: any) 
    {
        setValue("installment", e);
        trigger("installment");
        onCreditCardInstallment(e);
    }

    function handleSameAddressShiping(value: boolean) 
    {
        setValue("sameAddressShiping", value);
        trigger("sameAddressShiping");
        setSameAddressShiping(value);
    }

    function handleSetPIX() 
    {
        for (const item of paymentMethods) 
        {
            if (item.value === "pix") 
            {
                onSubmitPayment(item);
                setSelectedPaymentMethod(item);
                break;
            }
        }
    }

    async function populateAddres(value: any) 
    {
        const data: any = await buscaCep2(value);

        for (const key in data) 
        {
            setValue(`address.${key}`, data[key]);
        }

        trigger();
    }

    return (
        <div className={styles.checkoutPayment}>
            {/* <DiscountPaymentMethod totalItems={cart?.totalItems} 
								   totalDiscount={cart?.totalDiscount}
								   totalShipping={cart?.totalShipping}
								   discountPaymentMethod={cart?.discountPaymentMethod}
								   installmentRule={installmentRule}/> */}

            <div className={styles.formHeader}>
                <p className={styles.title}>{t("Forma de Pagamento")}</p>
                <p className={styles.subtitle}>
					Escolha entre nossas opções de pagamento seguras e finalize sua compra
					com tranquilidade
                </p>
            </div>

            <div className={styles.paymentMethods}>
                {paymentMethods.map((payment: any) => (
                    <p
                        onClick={() => 
                        {
                            onSubmitPayment(payment);
                            setSelectedPaymentMethod(payment);
                        }}
                        className={
                            payment.label === selectedPaymentMethod.label
                                ? `${styles.paymentOption} ${styles.active}`
                                : `${styles.paymentOption} ${payment.value}_paymentOption`
                        }
                        key={payment.id}
                    >
                        <Image
                            className={styles.noCheck}
                            width={20}
                            height={20}
                            src="/assets/icons/BsCircle.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                        <Image
                            className={styles.check}
                            width={20}
                            height={20}
                            src="/assets/icons/AiOutlineCheckCircle.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                        {t(payment.label)}{" "}
                        {percentageByPaymentMethod(payment.value, installmentRule) > 0 && (
                            <span>
								( {percentageByPaymentMethod(payment.value, installmentRule)}%
								OFF )
                            </span>
                        )}
                    </p>
                ))}
            </div>

            {selectedPaymentMethod.value === "pix" && (
                <div className={styles.infos}>
                    <div className={styles.card}>
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/BsClock.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                        <label>Aprovação imediata</label>
                        <p>O pagamento com Pix leva pouco tempo para ser processado</p>
                    </div>
                    <div className={styles.card}>
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/MdOutlineSecurity.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                        <label>Transação segura</label>
                        <p>
							O PIX foi desenvolvido pelo Banco Central para facilitar suas
							compras, garantindo a proteção dos seus dados
                        </p>
                    </div>
                    <div className={styles.card}>
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/IoQrCode.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                        <label>Finalize sua compra com facilidade</label>
                        <p>
							É só acessar a área PIX no aplicativo do seu banco e escanear o QR
							code ou digitar o código
                        </p>
                    </div>
                </div>
            )}

            {selectedPaymentMethod.value === "boleto" && (
                <div className={styles.infos}>
                    <div className={styles.card}>
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/BiCalendar.svg"
                            alt=""
                            loader={cloudflareLoader}
                        />
                        <label>Pague até a data de vencimento</label>
                        <p>
							Faça o pagamento até a data de vencimento e garanta seu acesso ao
							produto
                        </p>
                    </div>
                    <div className={styles.card}>
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/MdOutlineSecurity.svg"
                            alt=""
                            loader={cloudflareLoader}
                        />
                        <label>Aguarde a aprovação do pagamento</label>
                        <p>
							Pagamentos com boleto levam até 3 dias úteis para serem
							compensados.
                        </p>
                    </div>
                    <div className={styles.card}>
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/IoQrCode.svg"
                            alt=""
                            loader={cloudflareLoader}
                        />
                        <label>Pague com Pix e tenha acesso imediato ao produto</label>
                        <p>
							O pagamento leva apenas alguns segundos para ser processado. Pague
							com Pix{" "}
                            <a
                                onClick={() => 
                                {
                                    handleSetPIX();
                                }}
                            >
								clicando aqui.
                            </a>
                        </p>
                    </div>
                </div>
            )}

            {selectedPaymentMethod.label === "Transferência" && (
                <>
                    <p className={styles.title}>
                        {t("Selecione qual banco irá transferir")}:
                    </p>
                    <div className={styles.shippingMethods}>
                        {paymentMethods.map(
                            (payment: any) =>
                                payment.label === "Transferência" && (
                                    <p
                                        onClick={() => 
                                        {
                                            onSubmitPayment(payment);
                                            setSelectedPaymentMethod(payment);
                                        }}
                                        className={
                                            payment.label === selectedPaymentMethod.label
                                                ? `${styles.paymentOption} ${styles.active}`
                                                : styles.paymentOption
                                        }
                                        key={payment.id}
                                    >
                                        <Image
                                            className={styles.noCheck}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/BsCircle.svg"
                                            loader={cloudflareLoader}
                                            alt=""
                                        />
                                        <Image
                                            className={styles.check}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiOutlineCheckCircle.svg"
                                            loader={cloudflareLoader}
                                            alt=""
                                        />
                                        {payment.bank_name} | Ag:{payment.bank_agency} | C/C:{" "}
                                        {payment.bank_account}
                                    </p>
                                ),
                        )}
                    </div>
                </>
            )}

            {selectedPaymentMethod.label === "Cartão de Credito" && (
                <>
                    <div className={styles.formHeader}>
                        <p className={styles.title}>{t("Preencha os dados do cartão")}</p>
                        <p className={styles.subtitle}>
							Digite as informações do seu cartão de crédito com segurança.
							Todas as transações são protegidas
                        </p>
                    </div>

                    <form className={styles.form}>
                        {/* onSubmit={() => handleSubmit(onSubmit)} */}

                        <div className={styles.formContent}>
                            <div className={styles.formItem}>
                                <label>{t("Nome do titular do cartão")}</label>
                                <input
                                    type="text"
                                    autoComplete="new-off"
                                    placeholder="Ex: José da Silva"
                                    {...register("owner", {
                                        validate : (value) =>
                                            validateName(value) || t("Nome inválido!"),
                                    })}
                                />
                                {errors.owner && (
                                    <ErrorMessage message={errors.owner?.message} />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("CPF")}</label>
                                <Controller
                                    name="cpf"
                                    control={control}
                                    rules={{
                                        validate : (value) =>
                                            validateCpf(value) || t("CPF inválido!"),
                                    }}
                                    render={({ field }) => (
                                        <PatternFormat
                                            {...field}
                                            format="###.###.###-##"
                                            type="tel"
                                            autoComplete="new-off"
                                            placeholder="Ex: 123.456.789-10"
                                            allowEmptyFormatting={false}
                                            onValueChange={(values) => 
                                            {
                                                field.onChange(values.value);
                                            }}
                                        />
                                    )}
                                />
                                {errors.cpf && <ErrorMessage message={errors.cpf?.message} />}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Número do cartão")}</label>
                                <Controller
                                    name="cardnumber"
                                    control={control}
                                    rules={{
                                        validate : (value) =>
                                            (value &&
												ccValid.number(value.toString()).isValid === true) ||
											t("Insira um número de cartão válido!"),
                                    }}
                                    render={({ field }) => (
                                        <PatternFormat
                                            {...field}
                                            format="################"
                                            type="tel"
                                            autoComplete="new-off"
                                            placeholder="Ex: 1234567809101112"
                                            allowEmptyFormatting={false}
                                            mask=""
                                            onValueChange={(values) => 
                                            {
                                                field.onChange(values.value); // Captura o valor sem formatação
                                            }}
                                        />
                                    )}
                                />
                                {errors.cardnumber && (
                                    <ErrorMessage message={errors.cardnumber?.message} />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Data de expiração")}</label>
                                <div className={styles.formSelects}>
                                    <div className={styles.select}>
                                        <Select
                                            placeholder={t("Mês")}
                                            options={getMonthOptions()}
                                            styles={customSelectStyles}
                                            isSearchable={false}
                                            defaultValue={monthCreditCard || null}
                                            onChange={handleChangeMonth}
                                        />
                                    </div>
                                    <div className={styles.select}>
                                        <Select
                                            placeholder={t("Ano")}
                                            options={getYearOptions()}
                                            styles={customSelectStyles}
                                            isSearchable={false}
                                            defaultValue={yearCreditCard || null}
                                            onChange={handleChangeYear}
                                        />
                                    </div>
                                </div>
                                {errors.expirydate && (
                                    <ErrorMessage message={"Data do cartão invalida!"} />
                                )}
                                {/* <InputMask
                                    mask="99/99"
                                    placeholder="Ex: 03/24"
                                    autoComplete="new-off"
                                    maskChar=""
                                    {...register("expirydate", {
                                    validate: (value) =>
                                    value.length >= 5 ||
                                    "Sua data de expiração deve ser do tipo DD/AA",
                                    })}
                                /> */}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("CVV")}</label>
                                <Controller
                                    name="cvv"
                                    control={control}
                                    rules={{
                                        validate : (value) =>
                                            (value && value.length >= 3) ||
											t("Seu CVV tem que possuir no mínimo 3 números"),
                                    }}
                                    render={({ field }) => (
                                        <PatternFormat
                                            {...field}
                                            format="####"
                                            type="tel"
                                            autoComplete="new-off"
                                            placeholder="Ex: 123"
                                            allowEmptyFormatting={false}
                                            mask=""
                                            onValueChange={(values) => 
                                            {
                                                field.onChange(values.value);
                                            }}
                                        />
                                    )}
                                />
                                {errors.cvv && <ErrorMessage message={errors.cvv?.message} />}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Parcelas")}</label>
                                <Select
                                    placeholder={t("Parcelamento")}
                                    options={cart?.installments}
                                    styles={customSelectStyles}
                                    isSearchable={false}
                                    defaultValue={cart?.installment || null}
                                    onChange={handleChangeInstallment}
                                />
                                {isSubmitted && errors.installment && (
                                    <ErrorMessage message={t("Selecione o parcelamento")} />
                                )}
                            </div>
                        </div>

                        <p className={styles.formItemTitle}>{t("Endereço de cobrança")}</p>

                        {hasShipping && (
                            <div className={styles.formContent}>
                                <div className={styles.adressTypes}>
                                    <p
                                        onClick={() => 
                                        {
                                            handleSameAddressShiping(true);
                                        }}
                                        className={
                                            sameAddressShiping
                                                ? `${styles.addressOption} ${styles.active}`
                                                : styles.addressOption
                                        }
                                    >
                                        <Image
                                            className={styles.noCheck}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/BsCircle.svg"
                                            loader={cloudflareLoader}
                                            alt=""
                                        />
                                        <Image
                                            className={styles.check}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiOutlineCheckCircle.svg"
                                            loader={cloudflareLoader}
                                            alt=""
                                        />
                                        {t("Usar o endereço de entrega")}
                                    </p>
                                    <p
                                        onClick={() => 
                                        {
                                            handleSameAddressShiping(false);
                                        }}
                                        className={
                                            !sameAddressShiping
                                                ? `${styles.addressOption} ${styles.active}`
                                                : styles.addressOption
                                        }
                                    >
                                        <Image
                                            className={styles.noCheck}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/BsCircle.svg"
                                            loader={cloudflareLoader}
                                            alt=""
                                        />
                                        <Image
                                            className={styles.check}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiOutlineCheckCircle.svg"
                                            loader={cloudflareLoader}
                                            alt=""
                                        />
                                        {t("Usar um endereço diferente")}
                                    </p>
                                </div>

                                {!sameAddressShiping && (
                                    <>
                                        <div className={styles.formItem}>
                                            <label>{t("CEP")}</label>
                                            <Controller
                                                name="address.zipcode"
                                                control={control}
                                                rules={{
                                                    validate : (value) =>
                                                        (value && value.length >= 9) ||
														t("Seu CEP deve possuir 8 números"),
                                                }}
                                                render={({ field }) => (
                                                    <PatternFormat
                                                        {...field}
                                                        format="#####-###"
                                                        type="tel"
                                                        autoComplete="new-off"
                                                        placeholder="Ex: 12345-678"
                                                        allowEmptyFormatting={false}
                                                        onValueChange={(values) => 
                                                        {
                                                            field.onChange(values.value);

                                                            if (values.value.length === 8) 
                                                            {
                                                                populateAddres(values.formattedValue);
                                                            }
                                                        }}
                                                    />
                                                )}
                                            />
                                            {(errors?.address as any)?.zipcode?.message && (
                                                <ErrorMessage
                                                    message={(errors?.address as any)?.zipcode?.message}
                                                />
                                            )}
                                        </div>
                                        <div className={styles.formItem}>
                                            <label>{t("Rua")}</label>
                                            <input
                                                type="text"
                                                placeholder="Ex: Rua Guaranabara"
                                                autoComplete="new-off"
                                                {...register("address.street", {
                                                    required : t("Este campo é obrigatório!"),
                                                })}
                                            />
                                            {(errors?.address as any)?.street?.message && (
                                                <ErrorMessage
                                                    message={(errors?.address as any)?.street?.message}
                                                />
                                            )}
                                        </div>
                                        <div className={styles.formItem}>
                                            <label>{t("Número")}</label>
                                            <input
                                                type="text"
                                                autoComplete="new-off"
                                                placeholder="Ex: 17"
                                                {...register("address.housenumber", {
                                                    required : t("Este campo é obrigatório!"),
                                                })}
                                            />
                                            {(errors?.address as any)?.housenumber?.message && (
                                                <ErrorMessage
                                                    message={
                                                        (errors?.address as any)?.housenumber?.message
                                                    }
                                                />
                                            )}
                                        </div>
                                        <div className={styles.formItem}>
                                            <label>{t("Complemento")}</label>
                                            <input
                                                type="text"
                                                autoComplete="new-off"
                                                placeholder="Ex: Ap 15B"
                                                {...register("address.complement", {})}
                                            />
                                            {(errors?.address as any)?.complement?.message && (
                                                <ErrorMessage
                                                    message={
                                                        (errors?.address as any)?.complement?.message
                                                    }
                                                />
                                            )}
                                        </div>
                                        <div className={styles.formItem}>
                                            <label>{t("Bairro")}</label>
                                            <input
                                                type="text"
                                                autoComplete="new-off"
                                                placeholder="Ex: Dom Bosco"
                                                {...register("address.district", {
                                                    required : t("Este campo é obrigatório!"),
                                                })}
                                            />
                                            {(errors?.address as any)?.district?.message && (
                                                <ErrorMessage
                                                    message={(errors?.address as any)?.district?.message}
                                                />
                                            )}
                                        </div>
                                        <div className={styles.formItem}>
                                            <label>{t("Cidade")}</label>
                                            <input
                                                type="text"
                                                placeholder="Ex: São Paulo"
                                                autoComplete="new-off"
                                                {...register("address.city", {
                                                    required : t("Este campo é obrigatório!"),
                                                })}
                                            />
                                            {(errors?.address as any)?.city?.message && (
                                                <ErrorMessage
                                                    message={(errors?.address as any)?.city?.message}
                                                />
                                            )}
                                        </div>
                                        <div className={styles.formItem}>
                                            <label>{t("Estado")}</label>
                                            <input
                                                type="text"
                                                placeholder="Ex: SP"
                                                maxLength={2}
                                                autoComplete="new-off"
                                                {...register("address.state", {
                                                    required : t("Este campo é obrigatório!"),
                                                })}
                                            />
                                            {(errors?.address as any)?.state?.message && (
                                                <ErrorMessage
                                                    message={(errors?.address as any)?.state?.message}
                                                />
                                            )}
                                        </div>
                                    </>
                                )}
                            </div>
                        )}
                    </form>
                </>
            )}
            <div className={styles.buttons}>
                {/* {!loadingCart && (
					<button
						className={'block ' + styles.cinza}
						type="button"
						onClick={() => router.back()}
					>
						{t("Voltar a loja")}
					</button>
				)} */}
                {/* {loadingCart ? null : (
					<button
						className={'block ' + styles.cinza}
						type="button"
						onClick={() => goBack()}
					>
						{t("Voltar")}
					</button>
				)} */}
                <button
                    disabled={loadingCart}
                    type="button"
                    className={`block ${styles.submit}`}
                    onClick={
                        selectedPaymentMethod?.value === "credit_card"
                            ? handleSubmit(onSubmit)
                            : () => onSubmit()
                    }
                >
                    {loadingCart ? <AnimatedLoading /> : t("Finalizar")}
                </button>

                {isSubmitted && Object.keys(errors).length > 0 && (
                    <ErrorMessage
                        message={
                            "Dados inválidos. Por favor, verifique os campos preenchidos."
                        }
                    />
                )}
            </div>
        </div>
    );
};
