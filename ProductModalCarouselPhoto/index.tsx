"use client";

import { Splide, SplideSlide } from "@splidejs/react-splide";
import type { Options } from "@splidejs/splide";
import React from "react";
import { useEffect, useRef } from "react";
import { TransformComponent, TransformWrapper } from "react-zoom-pan-pinch";
import { ImageSet } from "../../component-nextv3/ImageSet";
import { useResponsive } from "../../core-nextv3/util/useResponsive";
import { replaceDomain } from "../../core-nextv3/util/util";
import { THEME_SETTING } from "../../setting/setting";
import styles from "./styles.module.scss";

export const ProductModalCarouselPhoto: React.FC<any> = ({
    setOpenModalCarouselPhoto,
    images,
    width = 1920,
    height = 400,
    startIndex = 0,
}: any) => 
{
    const { isDesktop } = useResponsive();

    const mainRef = useRef<any>(undefined);
    const thumbRef = useRef<any>(undefined);

    const mainOptions: Options = {
        rewind     : true,
        perPage    : 1,
        perMove    : 1,
        width      : "100vw",
        height     : "100vh",
        pagination : false,
        arrows     : true,
        start      : startIndex,
    };

    const thumbsOptions: Options = {
        perPage      : 5,
        pagination   : true,
        padding      : "6rem",
        fixedWidth   : 110 + 2,
        fixedHeight  : 110 / (width / height),
        cover        : true,
        isNavigation : true,
        arrows       : true,
        focus        : "center",
        start        : startIndex,
        direction    : "ttb",
        height       : "110vh",
    };

    useEffect(() => 
    {
        if (mainRef.current && thumbRef.current && thumbRef.current.splide) 
        {
            mainRef.current.sync(thumbRef.current.splide);
        }
    }, []);

    return (
        <section className={styles.productModalCarouselPhoto}>
            <div className={styles.top}>
                <ImageSet
                    onClick={() => setOpenModalCarouselPhoto(false)}
                    width={30}
                    height={30}
                    src="/assets/icons/GrFormClose.svg"
                    responsive={false}
                    alt=""
                />
            </div>

            <Splide
                options={thumbsOptions}
                aria-label=""
                ref={thumbRef}
                className={styles.thumbsContent}
            >
                {images?.map((image: any, index: number) => (
                    <SplideSlide key={index} tabIndex={index}>
                        <ImageSet
                            //aspectRatio={aspectRatio}
                            //   domainFrom={THEME_SETTING.domainFrom}
                            //   domainTo={THEME_SETTING.domainTo}
                            image={image}
                            width={width}
                            height={height}
                            sizes="25vw"
                        />
                    </SplideSlide>
                ))}
            </Splide>

            <Splide
                options={mainOptions}
                aria-labelledby=""
                ref={mainRef}
                className={styles.imagesContent}
            >
                {images?.map((image: any, index: number) => (
                    <SplideSlide key={index} className={styles.productImage}>
                        <TransformWrapper>
                            {({ zoomIn, zoomOut, resetTransform }) => (
                                <React.Fragment>
                                    <TransformComponent>
                                        {isDesktop && (
                                            <img
                                                src={replaceDomain(
                                                    image?._url,
                                                    THEME_SETTING.domainFrom,
                                                    THEME_SETTING.domainTo,
                                                )}
                                                alt=""
                                                loading="lazy"
                                            />
                                        )}
                                        {!isDesktop && (
                                            <img
                                                src={replaceDomain(
                                                    image?._1920x1920?.replace(".webp", ".jpeg"),
                                                    THEME_SETTING.domainFrom,
                                                    THEME_SETTING.domainTo,
                                                )}
                                                alt=""
                                                loading="lazy"
                                            />
                                        )}
                                    </TransformComponent>
                                    <div className={styles.zoom}>
                                        <ImageSet
                                            className={styles.symbol}
                                            alt="Diminuir Zoom"
                                            onClick={() => zoomOut()}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiOutlineMinus.svg"
                                            responsive={false}
                                        />
                                        <ImageSet
                                            className={styles.ico}
                                            onClick={() => resetTransform()}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/BiSearch.svg"
                                            responsive={false}
                                            alt=""
                                        />
                                        <ImageSet
                                            className={styles.symbol}
                                            alt="Aumentar Zoom"
                                            onClick={() => zoomIn()}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiOutlinePlus.svg"
                                            responsive={false}
                                        />
                                    </div>
                                </React.Fragment>
                            )}
                        </TransformWrapper>
                    </SplideSlide>
                ))}
            </Splide>
        </section>
    );
};
