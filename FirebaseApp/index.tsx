import type React from "react";
//import { initializeApp } from "firebase/app";

//const base64EncodedServiceAccount: any = process.env.NEXT_PUBLIC_FIREBASE_CONFIG;
//const decodedServiceAccount            = Buffer.from(base64EncodedServiceAccount, "base64").toString("utf-8");
//const firebaseConfig                   = JSON.parse(decodedServiceAccount);

//const app = initializeApp(firebaseConfig);

export default async function FirebaseApp({
    children,
}: { children: React.ReactNode }) 
{
    return <>{children}</>;
}
