"use client";

import { THEME_SETTING } from "@/setting/setting";
import Image from "next/image";
//import { AspectRatio } from 'react-aspect-ratio';
import { cloudflareLoader, replaceDomain } from "../../core-nextv3/util/util";
interface ImageSetProps {
	src?: string;
	image?: {
		name: string;
		id: string;
		type: string;
		_url: string;
		_150x150: string;
		_300x300: string;
		_480x480: string;
		_1024x1024: string;
		_1920x1920: string;
	};
	width?: number;
	height?: number;
	aspectRatio?: number;
	className?: any;
	enabledGif?: any;
	domainFrom?: string;
	domainTo?: string;
	alt?: string;
	sizes?: string;
	onClick?: any;
	priority?: boolean;
	responsive?: boolean;
    style?: any;
    objectFit?: any;
}

export const ImageSet = ({
    src,
    image,
    width = 1920,
    height = 400,
    sizes = "",
    enabledGif = false,
    alt = "",
    priority = false,
    responsive = true,
    className = {},
    onClick,
    style = {},
    objectFit,
}: ImageSetProps) => 
{
    //const [imageData, setImageData] = useState<any>();
    //const [loading, setLoading] = useState(false);

    //   useEffect(() =>
    //   {
    //       if(image)
    //       {
    //           setImageData(image)
    //       }
    //   }, [image])

    const placeholder = "/assets/product_placeholder.png";

    //   const imageLoaderApi = ({ src, width, quality }:any) =>
    //   {
    //     return '/api/image/?url=' + encodeURIComponent(`${src}`) + `&w=${width}&q=${quality || 75}`;
    //   }

    const imageLoader = () => 
    {
        if (src) 
        {
            return src;
        }

        let url = encodeURI(image?._url || "") || placeholder;

        if (THEME_SETTING.imageCDN) 
        {
            //
        }
        else 
        {
            // console.log("imageLoader", image);

            if (enabledGif && url.toLowerCase().match(/\.(gif)/g)) 
            {
                return url;
            }

            if (width <= 480 && (image?._480x480 || image?._300x300)) 
            {
                url = image?._480x480
                    ? encodeURI(image?._480x480)
                    : encodeURI(image?._300x300);
            }
            else if (width <= 1024 && image?._1024x1024) 
            {
                url = encodeURI(image?._1024x1024);
            }
            else if (width <= 1920 && image?._1920x1920) 
            {
                url = encodeURI(image?._1920x1920);
            }

            url = url.replace(".webp", ".jpeg");

            if (url.indexOf("?") > -1) 
            {
                url += "auto=format";
            }
            else 
            {
                url += "?auto=format";
            }

            if (THEME_SETTING.domainFrom && THEME_SETTING.domainTo) 
            {
                url = replaceDomain(
                    url,
                    THEME_SETTING.domainFrom,
                    THEME_SETTING.domainTo,
                );
            }
        }

        return url;
    };

    const getStyle = (): any => 
    {
        if (responsive) 
        {
            return {
                width     : "100%",
                height    : "auto",
                objectFit : objectFit || "cover",
                ...style,
            };
        }

        return {
            ...style,
        };
    };

    //<AspectRatio ratio={width/height} className={styles.imageSet + ' ' + (loading ? '' : styles.loading) + ' ' + className}>
    //</AspectRatio>

    return (
        (image || src) &&
		(THEME_SETTING.imageCDN ? (
		    <Image
		        className={className}
		        onClick={onClick}
		        loader={cloudflareLoader}
		        src={imageLoader()}
		        sizes={sizes === "" ? `${width}px` : sizes}
		        width={width}
		        height={height}
		        alt={alt}
		        priority={priority}
		        loading={priority ? "eager" : "lazy"}
		        //placeholder="blur"
		        blurDataURL={image?._150x150 || image?._url || src}
		        style={getStyle()}
		    />
		) : (
		// <AspectRatio ratio={width/height}>
		    <Image
		        loader={imageLoader}
		        src={placeholder}
		        onClick={onClick}
		        //src={imageLoader()}
		        className={className}
		        sizes={sizes}
		        width={width}
		        height={height}
		        // fill
		        alt={alt}
		        priority={priority}
		        loading={priority ? "eager" : "lazy"}
		        style={getStyle()}
		        // style={{
		        //     width: '100%',
		        //     height: 'auto',
		        // }}
		    />
		))
		// </AspectRatio>
    );
};
