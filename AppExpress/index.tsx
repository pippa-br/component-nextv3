import type { AppProps } from "next/app";
import { Toaster } from "react-hot-toast";
import "react-aspect-ratio/aspect-ratio.css";

import { disableReactDevTools } from "@/core-nextv3/util/disableReactDevTools";
//GLOBALSTYLES
import { useEffect } from "react";
declare const document: any;

function AppExpress({ Component, pageProps }: AppProps) 
{
    //const { setInstallmentRule } = useCore();

    useEffect(() => 
    {
        disableReactDevTools();
    }, []);

    useEffect(() => 
    {
        const script = document.createElement("script");
        script.src = `https://www.google.com/recaptcha/enterprise.js?render=${process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}`;
        script.async = true;
        document.head.appendChild(script);
    }, []);

    // useEffect(() =>
    // {
    //     if (pageProps?.gateway && pageProps.gateway.installmentRule)
    //     {
    //         setInstallmentRule(pageProps.gateway.installmentRule);
    //     }
    // }, [ pageProps?.gateway ])

    return (
        <>
            <Component {...pageProps} />
            <Toaster
                position="top-center"
                reverseOrder={true}
                containerStyle={{
                    fontSize : 16,
                }}
                toastOptions={{
                    duration : 2500,
                }}
            />
        </>
    );
}

export default AppExpress;
