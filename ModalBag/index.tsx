import { motion } from "framer-motion";
import { Fragment, useEffect } from "react";
import { useCore } from "../../core-nextv3/core/core";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { currencyMask } from "../../core-nextv3/util/mask";
import { CartMessage } from "../CartMessage";
import { Gifts } from "../Gifts";
import { ImageSet } from "../ImageSet";
import { CartItem } from "../ModalCart/CartItem";
import { modalBagAnimation } from "../animation";
import styles from "./styles.module.scss";

export const ModalBag = ({
    pageGifts,
    navigation = "produtos",
    cartMessage,
}: any) => 
{
    const { cart, user, setIsOpenCart, isLoadingCart } = useCore();

    useEffect(() => 
    {
        if (cart) 
        {
            tagManager4.cartView(cart, user);
        }
    }, [ cart ]);

    return (
        <motion.div
            className={styles.modalMenuOverlay}
            variants={modalBagAnimation}
            initial="hidden"
            animate="show"
            exit="exit"
            onClick={() => setIsOpenCart(false)}
        >
            <div className={styles.modalMenu} onClick={(e) => e.stopPropagation()}>
                <div className={styles.modalHeader}>
                    <ImageSet
                        className={styles.closeIcon}
                        onClick={() => setIsOpenCart(false)}
                        width={20}
                        height={20}
                        src="/assets/icons/GrFormClose.svg"
                        responsive={false}
                        alt=""
                    />
                    <div className={styles.center}>
                        <p className={styles.title}>
							Carrinho de Compras ({cart?.items?.length || 0})
                        </p>
                        {/* <BsHandbag /> */}
                    </div>

                    <div className={styles.left} />
                </div>
                {cartMessage?.published && <CartMessage cartMessage={cartMessage} />}

                {pageGifts?.published && (
                    <section className={styles.cartGifts}>
                        <Gifts page={pageGifts} cart={cart} loadingCart={isLoadingCart} />
                    </section>
                )}

                {cart?.freeShipping?.status && (
                    <section className={styles.cartFreeShipping}>
                        <div className={styles.content}>
                            {cart?.freeShipping?.diff !== -1 && (
                                <Fragment>
                                    {cart?.freeShipping?.diff > 0 ? (
                                        <>
											Falta {currencyMask(cart?.freeShipping?.diff)} para FRETE
											GRÁTIS!
                                            <ImageSet
                                                width={20}
                                                height={20}
                                                src="/assets/icons/FaTruck.svg"
                                                responsive={false}
                                                alt=""
                                            />
                                        </>
                                    ) : (
                                        <>
											Você Ganhou FRETE GRÁTIS!
                                            <ImageSet
                                                width={20}
                                                height={20}
                                                src="/assets/icons/FaTruck.svg"
                                                responsive={false}
                                                alt=""
                                            />
                                        </>
                                    )}
                                </Fragment>
                            )}
                            {cart?.freeShipping?.diffQuantity !== -1 && (
                                <Fragment>
                                    {cart?.freeShipping?.diffQuantity > 0 ? (
                                        <>
											Falta {cart?.freeShipping?.diffQuantity} produto(s) para
											FRETE GRÁTIS!
                                            <ImageSet
                                                width={20}
                                                height={20}
                                                src="/assets/icons/FaTruck.svg"
                                                responsive={false}
                                                alt=""
                                            />
                                        </>
                                    ) : (
                                        <>
											Você Ganhou FRETE GRÁTIS!
                                            <ImageSet
                                                width={20}
                                                height={20}
                                                src="/assets/icons/FaTruck.svg"
                                                responsive={false}
                                                alt=""
                                            />
                                        </>
                                    )}
                                </Fragment>
                            )}
                        </div>
                    </section>
                )}

                <div className={styles.products}>
                    <CartItem
                        cart={cart}
                        setModalBag={setIsOpenCart}
                        user={user}
                        navigation={navigation}
                    />
                </div>
            </div>
        </motion.div>
    );
};

export default ModalBag;

// export const getStaticProps: GetStaticProps = () => withHeader(async (props: any) =>
// {
//     // const productsResult = await(
//     // 	collectionProduct(FULL_PRODUCT_SETTING.merge({
//     // 		perPage: 1,
//     // 		where: [
//     // 			{
//     // 				field: "published",
//     // 				operator: "==",
//     // 				value: true,
//     // 			},
//     // 		]
//     // 	}))
//     // );

//     // const cartMessageResult = await getDocument(TOP_CART_PAGE_SETTING)

//     return {
//         revalidate : THEME_SETTING.revalidate,
//         props      : {
//             seo : props.seo.merge({ title : "Carrinho" }),
//             //products : productsResult?.collection || [],
//             // cartMessage : cartMessageResult?.data || [],
//         },
//     };
// });
