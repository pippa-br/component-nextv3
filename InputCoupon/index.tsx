import { useTranslations } from "next-intl";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { couponCheckoutAnalytics } from "../../core-nextv3/analytics/analytics.api";
import { delCouponCart, setCouponCart } from "../../core-nextv3/cart/cart.api";
import { CART_SETTING } from "../../setting/setting";
import ErrorMessage from "../ErrorMessage";
import styles from "./styles.module.scss";

export const InputCoupon = ({ cart, setCart }: any) => 
{
    const t = useTranslations();
    const {
        register,
        handleSubmit,
        setValue,
        setError,
        formState: { errors },
    } = useForm();

    const onRemoveCoupon = async () => 
    {
        const result = await delCouponCart(CART_SETTING);

        if (result.error) 
        {
            return toast.error(result.error, { duration : 2000 });
        }

        setValue("coupon", "");

        setCart(result.data);
    };

    const onApplyCoupon = async (data: any) => 
    {
        const newData = {
            _code : data.coupon.toUpperCase().replace(/\s/g, ""),
        };

        const result = await setCouponCart(CART_SETTING.merge(newData));

        if (result.error) 
        {
            setError("coupon", {
                message : result.error,
            });
        }

        // COUPON ANALYTICS
        couponCheckoutAnalytics(data.coupon.toUpperCase());

        setCart(result.data);
    };

    return (
        <div className={styles.inputCoupon}>
            <label>{t("Cupom de desconto")}</label>
            <div>
                <input
                    defaultValue={cart?.coupon?._code}
                    {...register("coupon", {
                        validate : (value) => value.length >= 1 || `${t("Cupom inválido")}!`,
                    })}
                    readOnly={cart?.coupon?._code}
                    type="text"
                    placeholder={t("Digite aqui")}
                />
                {cart?.coupon?._code ? (
                    <button type="button" onClick={() => onRemoveCoupon()}>
                        {t("Remover Cupom")}
                    </button>
                ) : (
                    <button
                        className="block"
                        type="button"
                        onClick={handleSubmit(onApplyCoupon)}
                    >
                        {t("Aplicar Cupom")}
                    </button>
                )}
            </div>
            {errors.coupon && <ErrorMessage message={errors.coupon.message} />}
        </div>
    );
};
