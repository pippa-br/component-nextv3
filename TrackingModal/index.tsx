import Image from "next/image";
import { cloudflareLoader, formatDate } from "../../core-nextv3/util/util";
import styles from "./styles.module.scss";

export function TrackingModal({ trackingCode, trackingEvents, setModal }: any) 
{
    return (
        <div className={styles.trackingModal}>
            <div className={styles.content}>
                <div className={styles.close}>
                    <Image
                        onClick={() => setModal(false)}
                        width={40}
                        height={40}
                        src="/assets/icons/GrFormClose.svg"
                        loader={cloudflareLoader}
                        alt=""
                    />
                </div>
                <p className={styles.message}>
					Código de rastreio: <span>{trackingCode}</span>
                </p>
                <table>
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Situação</th>
                            <th>Local</th>
                        </tr>
                    </thead>
                    <tbody>
                        {trackingEvents?.map((item: any, index: any) => (
                            <tr key={`${index}tracking`}>
                                <td>
                                    <span>{formatDate(item.date)}</span>
                                </td>
                                <td>
                                    <span>{item.description}</span>
                                </td>
                                <td>
                                    <span>{item.locale}</span>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
