import { motion } from "framer-motion";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { useCore } from "../../core-nextv3/core/core";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { removeAccents } from "../../core-nextv3/util/util";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

export const searchAnimation = {
    hidden : {
        opacity : 0,
        y       : -250,
    },
    show : {
        opacity    : 1,
        y          : 0,
        transition : {
            duration : 0.5,
            type     : "tween",
        },
    },
    exit : {
        opacity    : 0,
        y          : -250,
        transition : {
            duration : 0.5,
            type     : "tween",
        },
    },
};

export const SearchBar = ({ setSearchBar }: any) => 
{
    const router = useRouter();
    const [ inputValue, setInputValue ] = useState("");
    const { user } = useCore();

    return (
        <motion.div
            className={styles.searchBar}
            variants={searchAnimation}
            initial="hidden"
            animate="show"
            exit="exit"
        >
            <div className={styles.content}>
                <ImageSet
                    onClick={() => setSearchBar(false)}
                    width={20}
                    height={20}
                    src="/assets/icons/GrFormClose.svg"
                    responsive={false}
                    alt=""
                />
                <input
                    onChange={(value) => setInputValue(value.target.value)}
                    type="search"
                    placeholder="Pesquisar"
                    onKeyUp={(e) => 
                    {
                        if (e.key === "Enter") 
                        {
                            setSearchBar(false);
                            tagManager4.registerEvent(
                                "click:search",
                                "search",
                                inputValue.trim(),
                                0,
                                user,
                            );
                            router.push(
                                `/busca/?term=${removeAccents(inputValue).toLowerCase()}`,
                            );
                            return true;
                        }

                        return false;
                    }}
                />
            </div>
        </motion.div>
    );
};
