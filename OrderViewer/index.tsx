import { Rating } from "@smastrom/react-rating";
import { AnimatePresence } from "framer-motion";
import { useTranslations } from "next-intl";
import { useRouter } from "next/navigation";
import { useState } from "react";
import toast from "react-hot-toast";
import { firstImageItemCart } from "../../core-nextv3/cart/cart.util";
import { useCore } from "../../core-nextv3/core/core";
import {
    orderStatusClass,
    orderStatusLabel,
    validateReorder,
} from "../../core-nextv3/order/order.util";
import { currencyMask } from "../../core-nextv3/util/mask";
import { formatDate, updateQueryString } from "../../core-nextv3/util/util";
import { REORDER_INFO, THEME_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import { CommentModal } from "../CommentModal";
import { CustomCountdown } from "../CustomCountdown";
import CustomLink from "../CustomLink";
import { ImageSet } from "../ImageSet";
import { ModalRecompra } from "../ModalRecompra";
import { TrackBack } from "../TrackBack";
import { TrackingModal } from "../TrackingModal";
import styles from "./styles.module.scss";

declare const window: any;
declare const navigator: any;

export const OrderViewer = ({
    order,
    account,
    countDownPage,
    stausBarPage,
    installmentRule,
    reorderUrl = "/troca/",
    avaliationUrl = "/avaliar/",
}: any) => 
{
    const router = useRouter();
    const { user } = useCore();
    const [ selected ] = useState();
    const [ openModal, setOpenModal ] = useState(false);
    const [ assessment, setAssessment ] = useState(false);
    const [ avliationItem, setAvliationItem ] = useState<any>();
    const [ navigationPage, setNavigationPage ] = useState(false);
    const [ openModalRecompra, setOpenModalRecompra ] = useState(false);
    const [ navigationPageAvliation, setNavigationPageAvliation ] = useState(false);
    const t = useTranslations();

    //console.error('order', order);

    const reorderBadge = (product: any) => 
    {
        if (!order?.reorder?.items) 
        {
            return;
        }

        const hasReorder = order?.reorder?.items?.filter(
            (item: any) => item.id === product.id,
        );

        if (hasReorder?.length > 0) 
        {
            return (
                <small className={styles.reoderBadge}>
                    {order?.reorder?.type?.value === "troca"
                        ? "Produto trocado"
                        : order?.reorder?.type?.value === "devolucao"
                            ? "Produto devolvido"
                            : null}
                </small>
            );
        }

        return;
    };

    const normalizeImageUrl = (url: string) => 
    {
        if (url.indexOf("http") === -1) 
        {
            // DEVE SER BASE64
            return `data:image/jpeg;base64,${url}`;
        }

        return url;
    };

    async function copiedBarcode() 
    {
        toast.success(t("Código de barras copoiado com sucesso!"), {
            duration : 2000,
        });
    }

    async function copiedPix() 
    {
        toast.success(t("Código PIX copoiado com sucesso!"), { duration : 2000 });
    }

    const updateLinkOrder = () => 
    {
        toast.success(t("Atualizando Pedido..."), { duration : 2000 });
        updateQueryString("v", `${new Date().getTime()}`);
        router.refresh();
    };

    const handleNavigation = async (location: any) => 
    {
        await setNavigationPage(true);

        router.push(location);
    };

    const handleNavigationAvaliation = async () => 
    {
        await setNavigationPageAvliation(true);

        router.push(avaliationUrl + order?.id);
    };

    return (
        <>
            <div className={styles.orderViewer}>
                <div className={styles.content}>
                    {order?.statusPayment === "Aguardando" &&
						order?.paymentMethod?.value === "pix" &&
						countDownPage?.published === true && (
                        <CustomCountdown
                            date={order?.expires_at}
                            phrasePix={countDownPage}
                        />
                    )}

                    {order?.statusPayment === "Aguardando" &&
						order?.paymentMethod?.value === "boleto" && (
                        <div className={styles.subtitlePayment}>
                            <div className={styles.right}>
                                <div className={styles.infos}>
                                    <div className={styles.card}>
                                        <p>
                                            <span>1</span> Você deve pagar o boleto em qualquer
												agência bancária, casa lotérica, ou através do seu
												internet banking até o dia {order?.due_at}
                                        </p>
                                    </div>
                                </div>

                                <span className={styles.boletoCode}>
                                    {order?.boletoBarcode}
                                </span>

                                <button
                                    className={styles.copyCode}
                                    onClick={() => 
                                    {
                                        navigator.clipboard.writeText(`${order?.boletoBarcode}`);
                                        copiedBarcode();
                                    }}
                                >
                                    <ImageSet
                                        width={20}
                                        height={20}
                                        src="/assets/icons/BiCopy.svg"
                                        responsive={false}
                                        alt=""
                                    />
                                    {t("Copiar Código de barras")}
                                </button>
                            </div>
                            <div className={styles.left}>
                                <p>{t("Boleto para pagamento")}</p>
                                <a
                                    href={order?.boletoUrl}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    {t("Link para o boleto")}
                                </a>
                            </div>
                        </div>
                    )}

                    {order?.statusPayment === "Aguardando" &&
						order?.paymentMethod?.value === "pix" &&
						order?.pixQrCode && (
                        <div className={styles.subtitlePayment}>
                            <div className={styles.right}>
                                <div className={styles.infos}>
                                    <div className={styles.card}>
                                        <p>
                                            <span>1</span> Abra o aplicativo do seu banco e acesse a
												área
                                        </p>
                                    </div>
                                    <div className={styles.card}>
                                        <p>
                                            <span>2</span> Selecione a opção pagar com código Pix
												Copia e Cola e cole o código no espaço indicado no
												aplicativo
                                        </p>
                                    </div>
                                    <div className={styles.card}>
                                        <p>
                                            <span>3</span> Após o pagamento, você receberá por email
												as informações de acesso à sua compra
                                        </p>
                                    </div>
                                </div>

                                <span className={styles.pixCode}>
                                    {order?.pixQrCode?.split("\"")}
                                </span>

                                <button
                                    className={styles.copyCode}
                                    onClick={() => 
                                    {
                                        navigator.clipboard.writeText(`${order?.pixQrCode}`);
                                        copiedPix();
                                    }}
                                >
                                    <ImageSet
                                        width={20}
                                        height={20}
                                        src="/assets/icons/BiCopy.svg"
                                        responsive={false}
                                        alt=""
                                    />
                                    {t("Copiar PIX")}
                                </button>
                            </div>
                            <div className={styles.left}>
                                <p>{t("QR code para pagamento PIX")}</p>
                                <img
                                    className={styles.pixImage}
                                    src={normalizeImageUrl(order?.pixQrCodeUrl)}
                                    alt="PIX"
                                />
                            </div>
                        </div>
                    )}

                    {stausBarPage?.published === true && (
                        <TrackBack order={order} trackBack={stausBarPage} />
                    )}

                    <div className={styles.top}>
                        <div className={styles.topRight}>
                            <p className={styles.title}>
                                {t("Pedido")}{" "}
                                <span>
                                    {t("Nº")} {order?._sequence}
                                </span>
                            </p>
                            <p className={styles.date}>{formatDate(order?.postdate)}</p>
                            <a
                                className={styles.reload}
                                onClick={() => updateLinkOrder()}
                                // onClick={() => router.push("/checkout")}
                            >
                                {t("Atualizar Pedido")}
                            </a>
                        </div>
                        <div className={styles.subtitle}>
                            <p>{t("Forma de Pagamento")}</p>
                            <span>{order?.paymentMethod?.label}</span>
                            <span>{order?.installment?.label}</span>
                        </div>
                        <div className={styles.subtitle}>
                            <p>{t("Status do Pagamento")}</p>
                            <div className={styles.buttons}>
                                <span
                                    className={`${styles.status} ${
                                        styles[orderStatusClass(order)]
                                    }`}
                                >
                                    {t(orderStatusLabel(order))}
                                </span>
                                {order?.status?.value === "new" &&
									(order?.chargeStatus === "not_authorized" ||
										order?.chargeStatus === "failed" ||
										order?.chargeStatus === "") && (
                                    <button
                                        onClick={() => setOpenModalRecompra(true)}
                                        // onClick={() => router.push("/checkout")}
                                    >
                                        {t("Alterar Forma de Pagamento")}
                                    </button>
                                )}
                            </div>
                        </div>
                    </div>
                    {order?.paymentMethod?.value === "credit_card" && (
                        <div className={styles.subtitlePaymentCard}>
                            <p>{t("Cartão Utilizado")}</p>
                            <span>
                                {t("Nome do titular")}:{" "}
                                <strong>{order?.creditCard?.owner}</strong>
                            </span>
                            <span>
                                {t("Número do Cartão")}{" "}
                                <strong>{order?.creditCard?.cardnumber}</strong>{" "}
                            </span>
                            <span>
                                {t("Expiração")}:{" "}
                                <strong>{order?.creditCard?.expirydate}</strong>{" "}
                            </span>
                        </div>
                    )}
                    {!THEME_SETTING?.disabledShipping && (
                        <div className={styles.shippingOrder}>
                            <div className={styles.subtitle}>
                                <p>{t("Tipo de entrega")}</p>
                                <span>
                                    {order?.shipping?.type?.label === "Correios"
                                        ? `${order?.shipping?.type?.label} - ${order?.shipping?.label}`
                                        : order?.shipping?.type?.label}
                                </span>
                                <div className={styles.reorderActions}>
                                    {validateReorder(order, account?.addDaysReorder) &&
										!order?.hasReorder && (
                                        <button
                                            className={`${styles.reOrderButton} block`}
                                            type="button"
                                            onClick={() => handleNavigation(reorderUrl + order.id)}
                                        >
                                            {navigationPage ? (
                                                <AnimatedLoading />
                                            ) : (
                                                <>{REORDER_INFO?.name}</>
                                            )}
                                        </button>
                                    )}
                                </div>
                            </div>

                            <div className={styles.subtitle}>
                                <p>{t("Endereço de entrega")}</p>
                                {order?.shipping?.type?.label === "Retirada no Local" ? (
                                    <span>{order?.shipping?.label}</span>
                                ) : (
                                    <>
                                        <span>
                                            {order?.address?.street} {t("Nº")}{" "}
                                            {order?.address?.housenumber},{" "}
                                            {order?.address?.complement}
                                            {" - "}
                                            {order?.address?.district}, {order?.address?.city} -{" "}
                                            {order?.address?.state} / {t("CEP")}:{" "}
                                            {order?.address?.zipcode}
                                        </span>
                                    </>
                                )}
                                {order?.trackingCode && (
                                    <div className={styles.trackingCode}>
                                        <CustomLink href="#" onClick={() => setOpenModal(true)}>
                                            {t("Código de rastreio")} - {order?.trackingCode}
                                        </CustomLink>
                                    </div>
                                )}
                            </div>

                            {order?.status?.value === "nf" ||
							order?.status?.value === "sent" ||
							order?.status?.value === "available" ||
							order?.status?.value === "posted" ||
							order?.status?.value === "movement" ||
							order?.status?.value === "delivered" ? (
                                    <div className={styles.interaction}>
                                        <h2>{t("Nota Fiscal")}</h2>
                                        <p className={styles.message}>
                                            {t("Segue em anexo a sua Nota Fiscal")}!
                                        </p>
                                        <button
                                            className="block"
                                            onClick={() => window.open(order?.linkDanfe)}
                                        >
                                            {t("Nota Fiscal")}
                                        </button>
                                    </div>
                                ) : null}
                        </div>
                    )}

                    {order?.couponReference?.name || order?.discountClient ? (
                        <div className={styles.information}>
                            {order?.couponReference?.name && (
                                <div className={styles.subtitle}>
                                    <p>{t("Cupom")}</p>
                                    <span>{order?.couponReference?.name}</span>
                                </div>
                            )}

                            {order?.discountClient !== 0 && (
                                <div className={styles.subtitle}>
                                    <p>{t("Pontos")}</p>
                                    <span>{currencyMask(order?.discountClient)}</span>
                                </div>
                            )}
                        </div>
                    ) : null}

                    {/* <div className={styles.subtitle}>
						<p>Produtos</p>
					</div> */}
                    {/* <div className={styles.titles}>
						<p>Item</p>
						<p>Quantidade</p>
						<p>Preço</p>
						<p>Desconto</p>
						<p>Valor Pago</p>
						<p>&nbsp;</p>
					</div> */}
                    <div className={styles.products}>
                        {order?.items?.map((item: any) => (
                            <div key={item.id} className={styles.item}>
                                {reorderBadge(item)}
                                <div className={styles.product}>
                                    <div className={styles.img}>
                                        <ImageSet
                                            image={firstImageItemCart(item)}
                                            width={THEME_SETTING.widthProductThumb}
                                            height={THEME_SETTING.heightProductThumb}
                                            sizes="25vw"
                                        />
                                    </div>
                                    <div>
                                        <div className={styles.productName}>{item?.name}</div>
                                        {item?.variant && <div className={styles.productDetails}>
                                            {item?.variant?.map((variant: any) => (
                                                <span key={variant?.id}>{variant?.label}&nbsp;</span>
                                            ))}
                                        </div>}
                                    </div>
                                </div>
                                <div className={styles.quantity}>
                                    <span className={styles.label}>{t("Quantidade")}</span>
                                    <span className={styles.value}>{item.quantity}</span>
                                </div>
                                <div className={styles.price}>
                                    <span className={styles.label}>{t("Preço")}</span>
                                    <div className={styles.prices}>
                                        {item?.promotionalPrice > 0 && (
                                            <span className={styles.valuePromotional}>
                                                {currencyMask(item?.realPrice)}
                                            </span>
                                        )}
                                        <span className={styles.value}>
                                            {currencyMask(item?.price)}
                                        </span>
                                    </div>
                                </div>
                                <div className={styles.price}>
                                    <span className={styles.label}>{t("Desconto")}</span>
                                    <span className={styles.value}>
                                        {currencyMask(item?.discountTotal || 0)}
                                    </span>
                                </div>
                                <div className={styles.price}>
                                    <span className={styles.label}>{t("Valor Pago")}</span>
                                    <span className={styles.value}>
                                        {currencyMask(item?.total + (item?.discountTotal || 0))}
                                    </span>
                                </div>
                                {user && order?.statusPayment === "Pago" ? (
                                    <div className={styles.btn}>
                                        {/* <ReactStars
                        count={5}
                        // onChange={ratingChanged}
                        edit={false}
                        size={23}
                        //activeColor={false}
                        //isHalf={true}
                      /> */}
                                        <Rating
                                            style={{ maxWidth : 100 }}
                                            readOnly={true}
                                            value={5}
                                        />

                                        <button
                                            // onClick={() => { setAssessment(true), setSelected(item) }}
                                            onClick={() => 
                                            {
                                                handleNavigationAvaliation();
                                                setAvliationItem(item);
                                            }}
                                        >
                                            {avliationItem?.id === item?.id ? (
                                                <>{navigationPageAvliation && <AnimatedLoading />}</>
                                            ) : (
                                                t("Avaliar Produto")
                                            )}
                                        </button>
                                    </div>
                                ) : null}
                            </div>
                        ))}
                    </div>
                    <div className={styles.total}>
                        <p>
                            <span className={styles.label}>{t("Subtotal")}</span>
                            <span className={styles.value}>
                                {currencyMask(order?.totalItems)}
                            </span>
                        </p>
                        <p>
                            <span className={styles.label}>{t("Frete")}</span>
                            <span className={styles.value}>
                                {currencyMask(order?.totalShipping)}
                            </span>
                        </p>
                        <p className={styles.discount}>
                            <span className={styles.label}>{t("Desconto")}</span>
                            <span className={styles.value}>
                                {currencyMask(order?.totalDiscount || 0)}
                            </span>
                        </p>
                        <p>
                            <span className={styles.label}>{t("Total")}</span>
                            <span className={styles.value}>{currencyMask(order?.total)}</span>
                        </p>
                    </div>
                </div>
            </div>

            {order?.trackingCode && order?.trackingEvents && openModal && (
                <TrackingModal
                    trackingCode={order?.trackingCode}
                    trackingEvents={order?.trackingEvents}
                    setModal={setOpenModal}
                />
            )}

            {assessment && (
                <CommentModal
                    order={order}
                    item={selected}
                    setAssessment={setAssessment}
                />
            )}

            <AnimatePresence initial={false} mode="wait" onExitComplete={() => null}>
                {openModalRecompra && (
                    <ModalRecompra
                        installmentRule={installmentRule}
                        setOpenModalRecompra={setOpenModalRecompra}
                        order={order}
                    />
                )}
            </AnimatePresence>
        </>
    );
};
