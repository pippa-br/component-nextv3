import { useRouter } from "next/navigation";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { PatternFormat } from "react-number-format";
import { addDocument } from "../../core-nextv3/document/document.api";
import { AUTH_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import ErrorMessage from "../ErrorMessage";
import styles from "./styles.module.scss";

export const ModalHomeForm = ({ onClose, popupPage }: any) => 
{
    const router = useRouter();
    const [ loadingShipping, setLoadingShipping ] = useState(false);
    const {
        register,
        formState: { errors },
        handleSubmit,
        control,
        reset,
    } = useForm();

    const onSubmitPray = async (data: any) => 
    {
        setLoadingShipping(true);

        await addDocument(
            AUTH_SETTING.merge({
                data : data,
            }),
        );
        reset();

        setLoadingShipping(false);
        menssageEmailSuccess();
        onClose();
    };

    async function menssageEmailSuccess() 
    {
        toast(
            (t: any) => (
                <span>
                    <p>Mensagem enviada com sucesso</p>
                    <button onClick={() => toast.dismiss(t.id)}>Ok</button>
                </span>
            ),
            {
                className : "toast-custom",
                duration  : 999999,
            },
        );
    }

    return (
        <div className={styles.modalForm} onClick={() => onClose()}>
            <div className={styles.container} onClick={(e) => e.stopPropagation()}>
                {/* <div className={styles.top}>
          <AiOutlineClose onClick={() => setOpenModalForm(false)} />
        </div> */}
                {popupPage?.typePopup?.value === "form" ? (
                    <>
                        <img
                            className={styles.desktop}
                            src={popupPage?.image?._url}
                            alt=""
                        />
                        <img
                            className={styles.mobile}
                            src={popupPage?.imageMobile?._url}
                            alt=""
                        />

                        <div className={styles.content}>
                            <h1>
								Fique por dentro de todas as novidades e tenha acesso exclusivo
								as coleções
                            </h1>

                            <form onSubmit={(e) => e.preventDefault()}>
                                <div className={styles.inputForm}>
                                    <input
                                        type="text"
                                        placeholder="Nome"
                                        {...register("name", {
                                            required : "Este Campo é Obrigatório",
                                            validate : (value) =>
                                                value.length >= 8 ||
												"Seu nome deve possuir 8 caracteres no mínimo!",
                                        })}
                                    />
                                    {errors.name && (
                                        <ErrorMessage message={errors.name.message} />
                                    )}
                                </div>

                                <div className={styles.inputForm}>
                                    <Controller
                                        name="phone"
                                        control={control} // O `control` é extraído de `useForm()`
                                        rules={{
                                            validate : (value) =>
                                                value.length >= 9 ||
												"Seu número deve ter pelo menos 9 números",
                                        }}
                                        render={({ field }) => (
                                            <PatternFormat
                                                {...field}
                                                format="(##) #####-####"
                                                allowEmptyFormatting={false}
                                                placeholder="Whatsapp"
                                            />
                                        )}
                                    />
                                    {errors.phone && (
                                        <ErrorMessage message={errors.phone.message} />
                                    )}
                                </div>

                                <div className={styles.inputForm}>
                                    <input
                                        type="text"
                                        placeholder="E-mail"
                                        {...register("email", {
                                            required : "Este campo é obrigatório!",
                                            pattern  : {
                                                value   : /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                                message : "Email inválido",
                                            },
                                        })}
                                    />
                                    {errors.email && (
                                        <ErrorMessage message={errors.email.message} />
                                    )}
                                </div>
                                <button type="submit" onClick={handleSubmit(onSubmitPray)}>
                                    {loadingShipping ? <AnimatedLoading /> : "Enviar"}
                                </button>
                            </form>
                        </div>
                    </>
                ) : (
                    <>
                        <img
                            className={styles.desktop}
                            src={popupPage?.image?._url}
                            onClick={() => router.push(popupPage?.link)}
                            alt=""
                        />
                        <img
                            className={styles.mobile}
                            src={popupPage?.imageMobile?._url}
                            onClick={() => router.push(popupPage?.link)}
                            alt=""
                        />
                    </>
                )}
            </div>
            <button onClick={() => onClose()} className={styles.closeButton}>
				Fechar
            </button>
        </div>
    );
};
