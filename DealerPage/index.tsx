import type { GetStaticProps } from "next";
import { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { PatternFormat } from "react-number-format";
import { addUserAuth } from "../../core-nextv3/auth/auth.api";
import { getDocument } from "../../core-nextv3/document/document.api";
import { cnpjMaskClean, phoneMaskClean } from "../../core-nextv3/util/mask";
import { innerHTML } from "../../core-nextv3/util/util";
import { validateCNPJ, validatePhone } from "../../core-nextv3/util/validate";
import {
    PAGE_SELLER_SETTING,
    SELLER_CONTACT_SETTING,
    THEME_SETTING,
} from "../../setting/setting";
import withHeader from "../../utils/withHeader";
import { AnimatedLoading } from "../AnimatedLoading";
import ErrorMessage from "../ErrorMessage";
import { PageTitle } from "../PageTitle";
import styles from "./styles.module.scss";

const DealerPage = ({ dealer }: any) => 
{
    // console.log("aqui", dealer)
    const [ cnpjInput, setCnpjInput ] = useState("");
    const [ phoneInput, setPhoneInput ] = useState("");
    const [ loadingUserSeller, setLoadingUserSeller ] = useState(false);

    const {
        register,
        handleSubmit,
        reset,
        control,
        formState: { errors },
    } = useForm();

    const clearFormInputs = () => 
    {
        setPhoneInput("");
        setCnpjInput("");
    };

    const onSubmit = async (data: any) => 
    {
        setLoadingUserSeller(true);

        await addUserAuth(
            SELLER_CONTACT_SETTING.merge({
                data : {
                    name          : data.name,
                    corporateName : data.corporateName,
                    fantasyName   : data.fantasyName,
                    cnpj          : data.cnpj,
                    phone         : data.phone,
                    city          : data.city,
                    state         : data.state,
                    instagram     : data.instagram,
                },
            }),
        );

        reset({
            name          : "",
            corporateName : "",
            fantasyName   : "",
            cnpj          : "",
            phone         : "",
            city          : "",
            state         : "",
            instagram     : "",
        });
        // console.log(data);
        clearFormInputs();

        toast("Cadastro realizado com sucesso!", { icon : "👏", duration : 2000 });

        await setLoadingUserSeller(false);
    };

    return (
        <div className={styles.dealerPage}>
            {dealer?.image && (
                <img className={styles.desk} src={dealer?.image?._url} alt="" />
            )}
            {dealer?.mobile && (
                <img className={styles.mobile} src={dealer?.mobile?._url} alt="" />
            )}
            <div className={styles.content}>
                <PageTitle name={dealer?.name} />

                <div className={styles.contentWrapper}>
                    <div
                        className={styles.dealerContent}
                        dangerouslySetInnerHTML={innerHTML(dealer.content)}
                    />

                    <form>
                        <div className={styles.inputs}>
                            <div className={styles.formItem}>
                                <label>Nome Completo</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    autoComplete="new-password"
                                    {...register("name", {
                                        validate : (value) => value.length >= 1 || "Campo Requerido",
                                    })}
                                />
                                {errors.name && <ErrorMessage message={errors.name?.message} />}
                            </div>

                            <div className={styles.formItem}>
                                <label>Razão Social</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    autoComplete="new-password"
                                    {...register("corporateName", {
                                        validate : (value) => value.length >= 1 || "Campo Requerido",
                                    })}
                                />
                                {errors.corporateName && (
                                    <ErrorMessage message={errors.corporateName?.message} />
                                )}
                            </div>

                            <div className={styles.formItem}>
                                <label>Nome Fantasia</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    autoComplete="new-password"
                                    {...register("fantasyName", {
                                        validate : (value) => value.length >= 1 || "Campo Requerido",
                                    })}
                                />
                                {errors.fantasyName && (
                                    <ErrorMessage message={errors.fantasyName?.message} />
                                )}
                            </div>

                            <div className={styles.formItem}>
                                <label>CNPJ</label>
                                <Controller
                                    name="cnpj"
                                    control={control} // `control` é extraído de `useForm()`
                                    rules={{
                                        validate : (value) => validateCNPJ(value) || "CNPJ inválido",
                                    }}
                                    render={({ field }) => (
                                        <PatternFormat
                                            {...field}
                                            format="##.###.###/####-##"
                                            autoComplete="new-password"
                                            allowEmptyFormatting={false}
                                            value={cnpjInput} // Valor controlado
                                            onValueChange={(values) => 
                                            {
                                                setCnpjInput(cnpjMaskClean(values.formattedValue)); // Atualiza o valor do input sem formatação
                                            }}
                                        />
                                    )}
                                />
                                {errors.cnpj && <ErrorMessage message={errors.cnpj?.message} />}
                            </div>

                            <div className={styles.formItem}>
                                <label>Celular</label>
                                <Controller
                                    name="phone"
                                    control={control} // O `control` é extraído de `useForm()`
                                    rules={{
                                        validate : (value) =>
                                            validatePhone(value) || "Celular inválido",
                                    }}
                                    render={({ field }) => (
                                        <PatternFormat
                                            {...field}
                                            format="(##) #####-####"
                                            type="tel"
                                            autoComplete="new-password"
                                            allowEmptyFormatting={false}
                                            className={styles.input}
                                            value={phoneInput} // Valor controlado externamente
                                            onValueChange={(values) => 
                                            {
                                                setPhoneInput(phoneMaskClean(values.value)); // Atualiza o valor sem formatação
                                            }}
                                        />
                                    )}
                                />
                                {errors.phone && (
                                    <ErrorMessage message={errors.phone?.message} />
                                )}
                            </div>

                            <div className={styles.formItem}>
                                <label>Cidade</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    autoComplete="new-password"
                                    {...register("city", {
                                        validate : (value) => value.length >= 1 || "Campo Requerido",
                                    })}
                                />
                                {errors.city && <ErrorMessage message={errors.city?.message} />}
                            </div>

                            <div className={styles.formItem}>
                                <label>Estado</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    autoComplete="new-password"
                                    {...register("state", {
                                        validate : (value) => value.length >= 1 || "Campo Requerido",
                                    })}
                                />
                                {errors.state && (
                                    <ErrorMessage message={errors.state?.message} />
                                )}
                            </div>

                            <div className={styles.formItem}>
                                <label>Instagram</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    autoComplete="new-password"
                                    {...register("instagram", {
                                        validate : (value) => value.length >= 1 || "Campo Requerido",
                                    })}
                                />
                                {errors.instagram && (
                                    <ErrorMessage message={errors.instagram?.message} />
                                )}
                            </div>
                        </div>

                        <div className={styles.btn}>
                            <button type="button" onClick={handleSubmit(onSubmit)}>
                                {loadingUserSeller ? <AnimatedLoading /> : "Cadastrar"}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

const getStaticProps: GetStaticProps = () =>
    withHeader(async (props: any) => 
    {
        const dealerResult = await getDocument(PAGE_SELLER_SETTING);

        return {
            revalidate : THEME_SETTING.revalidate,
            props      : {
                seo    : props?.seo?.merge({ title : "Contato Revendedor" }),
                dealer : dealerResult?.data ?? {},
            },
        };
    });

export { getStaticProps as GetStaticProps, DealerPage };
