import type { GetServerSideProps } from "next";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useCore } from "../../core-nextv3/core/core";
import { getDocument } from "../../core-nextv3/document/document.api";
import { getTrackCorreios } from "../../core-nextv3/shipping/shipping.api";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { calls } from "../../core-nextv3/util/call.api";
import { innerHTML, updateQueryString } from "../../core-nextv3/util/util";
import {
    COUNTDOWN_PAGE_SETTING,
    ORDER_PAGE_SETTING,
    ORDER_SETTING,
    STATUS_BAR_PAGE_SETTING,
    TRACK_SETTING,
} from "../../setting/setting";
import withHeader from "../../utils/withHeader";
import { OrderViewer } from "../OrderViewer";
import { PageTitle } from "../PageTitle";
import styles from "./styles.module.scss";

const OrderSucessPage = ({
    order,
    account,
    orderTrack,
    countDownPage,
    stausBarPage,
    orderPage,
}: any) => 
{
    const { query, isReady } = useRouter();
    const { user } = useCore();

    useEffect(() => 
    {
        if (isReady && query.success) 
        {
            tagManager4.purchase(order, user);
            updateQueryString("success", "");
        }
    }, [ isReady, query ]);

    return (
        <>
            <div className={`${styles.orderPage} orderPage`}>
                <div className={styles.content}>
                    <PageTitle name={`Pedido Nº ${order._sequence}`} noTitle={true} />

                    {orderPage?.content && (
                        <div
                            className={styles.pageContent}
                            dangerouslySetInnerHTML={innerHTML(orderPage.content)}
                        />
                    )}

                    <OrderViewer
                        order={order}
                        account={account}
                        countDownPage={countDownPage}
                        stausBarPage={stausBarPage}
                        orderTrack={orderTrack}
                        redirectUrl="/pedido/"
                    />
                </div>
            </div>
        </>
    );
};

const getServerSideProps: GetServerSideProps = ({ params }: any) =>
    withHeader(async (props: any) => 
    {
        const [ resultOrder ] = await calls(
            getDocument(
                ORDER_SETTING.merge({
                    id : `${params.order}`,
                }),
            ),
        );

        if (!resultOrder.status) 
        {
            return {
                notFound : !resultOrder.serverError,
            };
        }

        const promises = [];

        if (resultOrder.data?.trackingCode) 
        {
            promises.push(
                getTrackCorreios(
                    TRACK_SETTING.merge({
                        code : resultOrder.data?.trackingCode,
                    }),
                ),
            );
        }
        else 
        {
            promises.push({});
        }

        promises.push(getDocument(COUNTDOWN_PAGE_SETTING));
        promises.push(getDocument(STATUS_BAR_PAGE_SETTING));
        promises.push(getDocument(ORDER_PAGE_SETTING));

        const [ orderTrack, phrasePixResult, trackBackResult, orderPage ] =
			await calls(...promises);

        return {
            props : {
                seo           : props?.seo?.merge({ title : "Pedido" }) || {},
                order         : resultOrder?.data || {},
                countDownPage : phrasePixResult?.data || {},
                orderTrack    : orderTrack?.data || {},
                stausBarPage  : trackBackResult?.data || {},
                orderPage     : orderPage?.data || {},
            },
        };
    });

// const getStaticPaths = async () => {
//   const order = await collectionDocument(
//     ORDER_SETTING.merge({
//       perPage: 1,
//     })
//   )

//   let paths = [];

//   if (order.collection) {
//     paths = order.collection.map((item: any) => ({
//       params: { id: item.id },
//     }))
//   }

//     console.error('xxxx', paths);

//   return { paths, fallback: 'blocking' }
// }

export {
    getServerSideProps as GetServerSideProps,
    //getStaticPaths as GetStaticPaths,
    OrderSucessPage,
};
