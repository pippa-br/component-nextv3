import { usePathname, useSearchParams } from "next/navigation";
import { useState } from "react";
import CustomLink from "../CustomLink";
import styles from "./styles.module.scss";

export const FilterProductsSize = ({ sizes }: any) => 
{
    const pathname = usePathname();
    const searchParams = useSearchParams();

    const [ size ] = useState(() => 
    {
        return searchParams.get("size");
    });

    return (
        <div className={styles.filterProductsSize}>
            <h3>SHOP BY SIZE</h3>
            <div className={styles.sizeList}>
                {sizes?.map((item: any) => (
                    <CustomLink
                        href={{
                            pathname : pathname,
                            query    : { size : item.value },
                        }}
                        scroll={false}
                        key={item?.id}
                        // onChange={(e:any) => onSize(e)}
                    >
                        <small className={`${size === item.value && styles.active}`}>
                            {item?.label}
                        </small>
                    </CustomLink>
                ))}
            </div>
        </div>
    );
};
