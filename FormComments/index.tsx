import { useEffect, useState } from "react";
import ReactStars from "react-stars";
import styles from "./styles.module.scss";

export const FormComments = ({ comments }: any) => 
{
    const [ totalRating, setTotalRating ] = useState(0);
    //const { user }              = useCore();

    useEffect(() => 
    {
        if (comments && comments.length > 0) 
        {
            let total = 0;

            for (const item of comments) 
            {
                total += item.rating;
            }

            total = +Number(total / comments.length).toFixed(1);

            setTotalRating(total);
        }
    }, [ comments ]);

    // const [ loadingShipping, setLoadingShipping ] = useState(false);
    // const [ star, setStar ] = useState(0);
    // const {
    //     //register,
    //     formState: { errors },
    //     // handleSubmit,
    //     // watch,
    //     // setValue,
    //     //reset,
    // } = useForm();

    // function ratingChanged(e: any)
    // {
    //     setStar(e);
    // }

    // const onSubmitPray = async (data: any) =>
    // {
    //     setLoadingShipping(true);
    //     // console.log(data);

    //     data.client = {
    //         referencePath : user.referencePath
    //     };
    //     data.product = {
    //         referencePath : product.referencePath
    //     };
    //     data.status = true;

    //     data.rating = star;
    //     await addDocument(COMMET_SETTING.merge({
    //         data : data
    //     }));

    //     reset();
    //     menssageEmailSuccess();
    //     setLoadingShipping(false);
    // };

    // async function menssageEmailSuccess()
    // {
    //     toast(
    //         (t) => (
    //             <span>
    //                 <p>Coméntario enviado com sucesso</p>
    //                 <button onClick={() => toast.dismiss(t.id)}>Ok</button>
    //             </span>
    //         ),
    //         {
    //             className : "toast-custom",
    //             duration  : 999999,
    //         }
    //     );

    // }

    return (
        <div className={styles.comments}>
            <div className={styles.content}>
                <h1>Avaliações</h1>

                <div className={styles.assessment}>
                    <p className={styles.assessmentNumber}>
                        {totalRating}
                        <span>/ 5</span>
                    </p>
                    <div className={styles.assessmentStar}>
                        <ReactStars
                            count={5}
                            size={25}
                            edit={false}
                            value={totalRating}
                            color2="#ffd700"
                            half={true}
                        />
                    </div>
                    <p className={styles.text}>
						Calculado de {comments?.length} revisão(ões) de clientes.
                    </p>
                </div>

                {/* {user ? (
                    <form onSubmit={(e) => e.preventDefault()}>
                        <div className={styles.addStars}>
                            <ReactStars
                                count={5}
                                onChange={ratingChanged}
                                size={25}
                                activeColor="#ffd700"
                                isHalf={true}
                                emptyIcon={<i className="far fa-star"></i>}
                                halfIcon={<i className="fa fa-star-half-alt"></i>}
                                fullIcon={<i className="fa fa-star"></i>}
                            />
                            {errors.message && <ErrorMessage message={errors.message.message} />}
                        </div>

                        <div className={styles.input}>
                            <textarea
                                id=""
                                cols={30}
                                rows={10}
                                placeholder="Deixe seu coméntario"
                                {...register("message", { required: "Campo Obrigatório" })}
                            />
                            {errors.message && <ErrorMessage message={errors.message.message} />}
                        </div>
                        <button
                            type="submit"
                            onClick={handleSubmit(onSubmitPray)}
                        >
                            {loadingShipping ? <AnimatedLoading /> : "Enviar"}
                        </button>
                    </form>
                ) : null} */}
            </div>
        </div>
    );
};
