import { cloudflareLoader } from "@/core-nextv3/util/util";
import Image from "next/image";
import styles from "./styles.module.scss";

export const ModalPhotos = ({ setOpenModalPhotosHome, photo }: any) => 
{
    //   console.log("modal", photo);

    // const [viewportRef, embla] = useEmblaCarousel({
    //   loop: true,
    //   skipSnaps: false,
    //   startIndex: 0,
    //   slidesToScroll: 1,
    //   align: "start",
    // });

    // const scrollPrev = useCallback(() => embla && embla.scrollPrev(), [embla]);
    // const scrollNext = useCallback(() => embla && embla.scrollNext(), [embla]);

    // useEffect(() => {
    //   if (!embla) return;
    // }, [embla]);

    return (
        <div className={styles.embla}>
            <div className={styles.top}>
                <Image
                    onClick={() => setOpenModalPhotosHome(false)}
                    className="modal-close-menu"
                    width={20}
                    height={20}
                    src="/assets/icons/GrFormClose.svg"
                    loader={cloudflareLoader}
                    alt=""
                />
            </div>
            <div
                className={styles.embla__viewport}
                onClick={(e) => e.stopPropagation()}
            >
                <div className={styles.embla__container}>
                    {photo?.photos?.slice(0, 12)?.map((item: any) => (
                        <div className={styles.emble__slide} key={item?.id}>
                            <img src={item?._url} alt="" />
                        </div>
                    ))}
                </div>
            </div>
            {/* <BsChevronLeft className={styles.prevBtn} onClick={scrollPrev} />
      <BsChevronRight className={styles.nextBtn} onClick={scrollNext} /> */}
        </div>
    );
};
