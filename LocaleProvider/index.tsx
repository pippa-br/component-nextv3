import type React from "react";
import {
    type Dispatch,
    type SetStateAction,
    createContext,
    useContext,
    useState,
} from "react";

// Definindo o tipo correto para o contexto
interface LocaleContextType {
	locale: string;
	setLocale: Dispatch<SetStateAction<string>>; // Tipagem correta para setLocale
}

// Criação do Contexto com um valor padrão inicial
const LocaleContext = createContext<LocaleContextType>({
    locale    : "pt", // Valor padrão
    setLocale : () => 
    {}, // Função vazia por padrão
});

// Hook personalizado para acessar o contexto
export const useLocale = () => useContext(LocaleContext);

// Provider para envolver os componentes e fornecer o `locale`
export const LocaleProvider = ({
    children,
    initialLocale,
}: { children: React.ReactNode; initialLocale: string }) => 
{
    const [ locale, setLocale ] = useState(initialLocale || "pt"); // Inicializa o locale com o valor recebido

    return (
        <LocaleContext.Provider value={{ locale, setLocale }}>
            {children}
        </LocaleContext.Provider>
    );
};
