"use client";

import { cloudflareLoader } from "@/core-nextv3/util/util";
import Image from "next/image";
import { ItemGifts } from "./item";
import styles from "./styles.module.scss";

export const ModalGifts = ({ closeModalGifts, page }: any) => 
{
    return (
        <div className={styles.modalGifts} onClick={() => closeModalGifts(false)}>
            <div className={styles.container} onClick={(e) => e.stopPropagation()}>
                <div className={styles.top}>
                    <Image
                        onClick={() => closeModalGifts(false)}
                        width={40}
                        height={40}
                        src="/assets/icons/GrFormClose.svg"
                        loader={cloudflareLoader}
                        alt=""
                    />
                </div>

                <div className={styles.body}>
                    {page?.items?.map((item: any, index: number) => (
                        <ItemGifts page={page} item={item} key={index} />
                    ))}
                </div>
            </div>
        </div>
    );
};
