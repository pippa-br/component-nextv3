"use client";

import { hasStockProduct } from "@/core-nextv3/stock/stock.util";
import { useState } from "react";
import toast from "react-hot-toast";
import { setGiftCart } from "../../core-nextv3/cart/cart.api";
import { useCore } from "../../core-nextv3/core/core";
import {
    firstProductImage,
    validateListVariant,
} from "../../core-nextv3/product/product.util";
import { CART_SETTING, THEME_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import { ImageSet } from "../ImageSet";
import { VariantSelector } from "../VariantSelector";
import styles from "./styles.module.scss";

export const ItemGifts = ({ page, item }: any) => 
{
    const [ loadingAddToCart, setLoadingAddToCart ] = useState<any>(false);
    const [ listVariant, setListVariant ] = useState<any>([]);
    const { setCart } = useCore();

    const handleAddToCart = async () => 
    {
        if (!validateListVariant(item.product, listVariant)) 
        {
            toast.error("Seleciona a variante do produto", {
                duration : 1000,
            });

            return;
        }

        setLoadingAddToCart(true);

        const newData = {
            giftPage : {
                referencePath : page.referencePath,
            },
            data : {
                product : {
                    referencePath : item?.product?.referencePath,
                },
                variant  : listVariant,
                quantity : 1,
            },
        };

        const result = await setGiftCart(CART_SETTING.merge(newData));

        setLoadingAddToCart(false);

        if (result.status === false) 
        {
            return toast.error(result.error, {
                duration : 1000,
            });
        }

        setCart(result.data);

        toast.success("Brinde adiconado com sucesso!", {
            icon     : "👏",
            duration : 2000,
        });
    };

    async function handleChangeVariant(listVariant: any) 
    {
        setListVariant(listVariant);
    }

    return (
        <>
            {hasStockProduct(item.product) &&
				firstProductImage(item.product, [ listVariant[0] ]) && (
                <div key={item.product?.id} className={styles.item}>
                    <div className={styles.product}>
                        <div className={styles.img}>
                            <ImageSet
                                image={firstProductImage(item.product, [ listVariant[0] ])}
                                width={THEME_SETTING.widthProductThumb}
                                height={THEME_SETTING.heightProductThumb}
                                sizes="(max-width: 992px) 50vw, 20vw"
                            />
                        </div>
                        <p className={styles.productName}>{item.product?.name}</p>
                    </div>
                    <VariantSelector
                        product={item.product}
                        changeVariant={(event: any) => 
                        {
                            handleChangeVariant(event);
                        }}
                        disabledQuantity={true}
                        disabledAddCart={true}
                    />
                    <div className={styles.actions}>
                        <button
                            type="button"
                            className="block"
                            onClick={() => handleAddToCart()}
                        >
                            {loadingAddToCart ? <AnimatedLoading /> : "Adicionar Brinde"}
                        </button>
                    </div>
                </div>
            )}
        </>
    );
};
