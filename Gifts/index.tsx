"use client";

import { useTranslations } from "next-intl";
import Image from "next/image";
import { useEffect, useState } from "react";
import { useCore } from "../../core-nextv3/core/core";
import { currencyMask } from "../../core-nextv3/util/mask";
import { cloudflareLoader, innerHTML } from "../../core-nextv3/util/util";
import { ProductItemSkeletonCart } from "../ProductItemSkeletonCart";
import { ModalGifts } from "./ModalGifts";
import styles from "./styles.module.scss";

export const Gifts = ({ page }: any) => 
{
    const [ hasProducts, setHasProducts ] = useState(false);
    const [ selectProduct, setSelectProduct ] = useState(false);
    const [ openModalGifts, setOpenModalGifts ] = useState(false);
    const { isLoadingCart, cart } = useCore();
    const t = useTranslations();

    useEffect(() => 
    {
        setHasProducts(false);

        if (cart && cart.items?.length > 0) 
        {
            // POIS PODE CONTER APENAS O BRIND
            for (const item of cart.items) 
            {
                if (!item.gift) 
                {
                    setHasProducts(true);
                    break;
                }
            }
        }
    }, [ cart ]);

    useEffect(() => 
    {
        if (page && page.items?.length > 0) 
        {
            // POIS PODE CONTER APENAS O BRIND
            for (const item of page.items) 
            {
                if (item.auto) 
                {
                    setSelectProduct(true);
                    break;
                }
            }
        }
    }, [ page ]);

    return (
        <>
            {hasProducts && (
                <div className={styles.gifts}>
                    {/* <h2>Tem um brinde disponível para você!</h2> */}
                    {!isLoadingCart ? (
                        <>
                            {Array.from({ length : 3 }).map((_product, index) => (
                                <ProductItemSkeletonCart key={`productSkeleton-${index}`} />
                            ))}
                        </>
                    ) : (
                        <>
                            {cart?.totalItems < page?.value ? (
                                <div className={styles.card}>
                                    <div className={styles.cardBody}>
                                        <div className={styles.title}>
                                            <h1>{page?.name}</h1>
                                            <Image
                                                width={20}
                                                height={20}
                                                src="/assets/icons/GiPresent.svg"
                                                loader={cloudflareLoader}
                                                alt=""
                                            />
                                        </div>
                                        <div className={styles.message}>
                                            <h4>
                                                {t("Compras acima de")} R$ {currencyMask(page?.value)}
                                            </h4>
                                            <p>
												Faltam apenas{" "}
                                                <strong>
                                                    {currencyMask(page?.value - cart?.totalItems)}
                                                </strong>{" "}
												para você ganhar!
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                <>
                                    <div className={styles.card}>
                                        <div className={styles.cardBody}>
                                            <div className={styles.title}>
                                                <h1>{page?.name}</h1>
                                                <Image
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/GiPresent.svg"
                                                    loader={cloudflareLoader}
                                                    alt=""
                                                />
                                            </div>
                                            <div className={styles.message}>
                                                <div
                                                    dangerouslySetInnerHTML={innerHTML(page?.description)}
                                                    className={styles.description}
                                                />
                                                {selectProduct && (
                                                    <button
                                                        className="block"
                                                        onClick={() => setOpenModalGifts(true)}
                                                    >
														Você Ganhou
                                                    </button>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                    {/* <div className={styles.body}>
                        {page?.products?.map((product:any, index:number) => (
                            <ItemGifts page={page} product={product} key={index}/>
                        ))}
                      </div> */}
                                </>
                            )}
                        </>
                    )}
                </div>
            )}

            {openModalGifts && (
                <ModalGifts page={page} closeModalGifts={setOpenModalGifts} />
            )}
        </>
    );
};
