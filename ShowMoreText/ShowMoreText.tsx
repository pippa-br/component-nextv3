import { cloudflareLoader } from "@/core-nextv3/util/util";
import Image from "next/image";
import { useState } from "react";
import styles from "./styles.module.scss";

const ShowMore = ({ children }: any) => 
{
    const [ showMore, setShowMore ] = useState(false);

    return (
        <div
            className={`${styles.showMore} ${showMore ? styles.more : styles.less}`}
        >
            <div className={styles.content}>{children}</div>
            <div className={styles.showMoreButton}>
                <button onClick={() => setShowMore(!showMore)}>
                    {showMore ? "Mostrar Menos" : "Mostrar Mais"}
                    {showMore ? (
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/TiArrowSortedUp.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    ) : (
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/TiArrowSortedDown.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    )}
                </button>
            </div>
        </div>
    );
};

export default ShowMore;
