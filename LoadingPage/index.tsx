import { AnimatedLoading } from "../AnimatedLoading";
import styles from "./styles.module.scss";

const LoadingPage = () => 
{
    return (
        <div className={styles.loadingPage}>
            <div className={styles.content}>
                <AnimatedLoading />
            </div>
        </div>
    );
};

export { LoadingPage };
