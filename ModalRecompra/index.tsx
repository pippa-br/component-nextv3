import { useCore } from "@/core-nextv3/core/core";
import { initMercadoPago } from "@mercadopago/sdk-react";
import { createCardToken } from "@mercadopago/sdk-react/esm/coreMethods";
import type { CardTokenParams } from "@mercadopago/sdk-react/esm/coreMethods/cardToken/types";
import ccValid from "card-validator";
import { useTranslations } from "next-intl";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { PatternFormat } from "react-number-format";
import Select from "react-select";
import {
    addPaymentOrder,
    setCreditCardOrder,
    setInstallmentOrder,
    setPaymentMethodOrder,
} from "../../core-nextv3/order/order.api";
import { buscaCep, getRecaptcha } from "../../core-nextv3/util/util";
import { customSelectStyles } from "../../core-nextv3/util/util.style";
import { validateCpf } from "../../core-nextv3/util/validate";
import {
    CART_SETTING,
    ORDER_SETTING,
    THEME_SETTING,
} from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import ErrorMessage from "../ErrorMessage";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

type AddressProps = {
	cep: string;
	uf: string;
	bairro: string;
	complemento: string;
	localidade: string;
	logradouro: string;
};

export const ModalRecompra = ({
    installmentRule,
    setOpenModalRecompra,
    order,
    hasShipping = true,
}: any) => 
{
    const router = useRouter();
    const [ monthCreditCard, setMonthCreditCard ] = useState();
    const [ yearCreditCard, setYearCreditCard ] = useState();
    const [ postData, setPostData ] = useState();
    const [ showConfirmation, setShowConfirmation ] = useState(false);
    const [ installment, setInstallment ] = useState({ label : "" });
    const [ loadingPayment, setLoadingPayment ] = useState(false);
    const { cart, setCart } = useCore();
    const [ selectedPaymentAddressMethod, setSelectedPaymentAddressMethod ] =
		useState(true);
    const [ address, setAddress ] = useState<AddressProps>();
    const t = useTranslations();
    const [ paymentMethodsList ] = useState(() => 
    {
        const list: any = [];

        if (installmentRule?.paymentMethods) 
        {
            for (const item of installmentRule.paymentMethods) 
            {
                list.push(item.type);
            }
        }

        return list;
    });

    const [ selectedPaymentMethod, setSelectedPaymentMethod ] = useState(
        cart?.paymentMethod || {
            id    : "",
            label : "",
            value : "",
        },
    );

    const {
        register,
        handleSubmit,
        setValue,
        control,
        formState: { errors, isSubmitted },
    } = useForm();

    const getMonthOptions = () => 
    {
        return [
            { value : "01", label : "01" },
            { value : "02", label : "02" },
            { value : "03", label : "03" },
            { value : "04", label : "04" },
            { value : "05", label : "05" },
            { value : "06", label : "06" },
            { value : "07", label : "07" },
            { value : "08", label : "08" },
            { value : "09", label : "09" },
            { value : "10", label : "10" },
            { value : "11", label : "11" },
            { value : "12", label : "12" },
        ];
    };

    const getYearOptions = () => 
    {
        const options = [];
        let fullYear = new Date().getFullYear();

        for (let i = 1; i <= 20; i++) 
        {
            options.push({ value : fullYear.toString().substr(-2), label : fullYear });
            fullYear++;
        }

        return options;
    };

    function handleChangeMonth(data: any) 
    {
        setMonthCreditCard(data.value);
        handleChangeExpirydate(data.value, yearCreditCard);
    }

    function handleChangeYear(data: any) 
    {
        setYearCreditCard(data.value);
        handleChangeExpirydate(monthCreditCard, data.value);
    }

    function handleChangeExpirydate(month: any, year: any) 
    {
        if (month && year) 
        {
            setValue("expirydate", `${month}/${year}`);
        }
    }

    const handleInstallmentChange = async (option: any) => 
    {
        setLoadingPayment(true);
        const newData = {
            document : {
                referencePath : order?.referencePath,
            },
            data : option,
        };
        setInstallment(option);

        const result = await setInstallmentOrder(ORDER_SETTING.merge(newData));

        if (result.status) 
        {
            setCart(result.data);
        }
        else 
        {
            toast.error(
                t(
                    "Ocorreu um erro! Verifique se foi selecionado um parcelamento válido.",
                    { duration : 2000 },
                ),
            );
        }

        setLoadingPayment(false);
    };

    const onSubmitCartPaymentMethod = async (data: any) => 
    {
        const newData = {
            document : {
                referencePath : order?.referencePath,
            },
            data : data,
        };

        const result = await setPaymentMethodOrder(CART_SETTING.merge(newData));

        if (result.data.paymentMethod) 
        {
            setCart(result.data);
        }
        else 
        {
            toast.error(
                t("Ocorreu um erro! Verifique se foi selecionado um método válido."),
                { duration : 2000 },
            );
        }
    };

    const onSubmit = async (data: any) => 
    {
        if (selectedPaymentMethod.value) 
        {
            if (
                selectedPaymentMethod.value !== "credit_card" ||
				(selectedPaymentMethod.value === "credit_card" && installment.label)
            ) 
            {
                setPostData(data);
                setShowConfirmation(true);
                // toast(
                //     <div>
                //         <p>Deseja concluir o pagamento via {selectedPaymentMethod.label}{selectedPaymentMethod.value == "credit_card" ? ' em ' + (installment.label ? installment.label : order.installment.label) : ''}?</p>
                //         <button onClick={(e) => {finishPayment(data)}}>SIM</button>
                //         <button onClick={(e) => {dismissToast()}}>NÃO</button>
                //     </div>
                // , {duration: Infinity, position: 'bottom-center'});
            }
        }
    };

    const dismissToast = () => 
    {
        toast.dismiss();
    };

    const finishPayment = async () => 
    {
        let result: any;
        let data: any = postData;
        dismissToast();
        setLoadingPayment(true);
        toast.loading(
            "Seu pagamento está sendo processado e você será redirecionado em breve...",
        );

        if (selectedPaymentAddressMethod) 
        {
            data = { ...data, ...order.address };
        }

        const newData: any = {
            document : {
                referencePath : order?.referencePath,
            },
            data : selectedPaymentMethod,
        };

        result = await setPaymentMethodOrder(ORDER_SETTING.merge(newData));

        if (selectedPaymentMethod.value === "credit_card") 
        {
            const creditCardData: any = {
                document : {
                    referencePath : order?.referencePath,
                },
                data : {
                    cardnumber : data.cardnumber,
                    owner      : data.owner,
                    cvv        : data.cvv,
                    expirydate : data.expirydate,
                    docType    : data.cpf
                        ? { value : "cpf", label : "CPF", id : "cpf", type : "individual" }
                        : { value : "cnpj", label : "CNPJ", id : "cnpj", type : "company" },
                    address : {
                        zipcode     : data.zipcode,
                        street      : data.street,
                        housenumber : data.housenumber,
                        complement  : data.complement,
                        district    : data.district,
                        city        : data.city,
                        state       : data.state,
                        country     : {
                            id       : "br",
                            label    : "Brasil",
                            value    : "br",
                            selected : true,
                        },
                    },
                },
            };

            if (THEME_SETTING.mercadoPago) 
            {
                if (!process.env.NEXT_PUBLIC_MERCADO_PAGO_PUBLIC_KEY) 
                {
                    return toast.error(t("Mercado Pago não configurado!"), {
                        duration : 2000,
                    });
                }

                initMercadoPago(process.env.NEXT_PUBLIC_MERCADO_PAGO_PUBLIC_KEY);

                // const identificationNumber = data?.cpf && typeof data.cpf === 'string' ? data.cpf.replace(/\D/g, '') : null;
                // const identificationType = identificationNumber?.length === 11 ? 'CPF' : 'CNPJ';

                const payloadCreateCardToken: CardTokenParams = {
                    cardNumber           : data?.cardnumber,
                    securityCode         : data?.cvv,
                    cardExpirationMonth  : data?.expirydate.slice(0, 2),
                    cardExpirationYear   : data?.expirydate.slice(-2),
                    cardholderName       : data?.owner,
                    identificationType   : "CPF",
                    identificationNumber : data?.cpf.replace(/[.-]/g, ""),
                };

                console.log("payload createCardToken", payloadCreateCardToken);

                const cardToken = await createCardToken(payloadCreateCardToken);

                console.log("cardToken", cardToken);

                creditCardData.data.cardToken = cardToken?.id;
            }

            const newData = {
                document : {
                    referencePath : order?.referencePath,
                },
                data : installment,
            };

            await setInstallmentOrder(ORDER_SETTING.merge(newData));

            creditCardData.data[`_${creditCardData.data.docType.value}`] = data.cpf
                ? data.cpf
                : data.cnpj;

            const token = await getRecaptcha("setCreditCardOrder");

            if (token) 
            {
                creditCardData.token = token;
                result = await setCreditCardOrder(ORDER_SETTING.merge(creditCardData));
            }
            else 
            {
                setOpenModalRecompra(false);
                setLoadingPayment(false);
                return toast.error(t("token invalido!"), { duration : 2000 });
            }
        }

        const token = await getRecaptcha("addPaymentOrder");

        if (token) 
        {
            result = await addPaymentOrder(
                ORDER_SETTING.merge({
                    token    : token,
                    document : {
                        referencePath : order?.referencePath,
                    },
                }),
            );
        }
        else 
        {
            setOpenModalRecompra(false);
            setLoadingPayment(false);
            return toast.error(t("token invalido!"), { duration : 2000 });
        }

        setOpenModalRecompra(false);
        setLoadingPayment(false);

        if (result.status) 
        {
            //router.push("/pedido/" + order.id + "/?t=" + new Date().getTime())
            router.refresh();
        }
    };

    // async function handleCreditCardInstallment(installment: any)
    // {
    //     const newData = {
    //         data: installment,
    //     }

    //     //setLoadingCart(true);

    //     const result = await setInstallmentCart(CART_SETTING.merge(newData))

    //     //setLoadingCart(false);

    //     if (result.status)
    //     {
    //         setCart(result.data)
    //     }
    //     else
    //     {
    //         toast.error(t('Ocorreu um erro! Verifique se foi selecionado um parcelamento válido.', { duration : 2000 }))
    //     }
    // }

    useEffect(() => 
    {
        register("sameAddressShiping", { value : selectedPaymentAddressMethod });
        register("expirydate", { value : null, required : true });
    }, []);

    return (
        <div
            className={styles.modalRecompra}
            onClick={() => setOpenModalRecompra(false)}
        >
            <div className={styles.container} onClick={(e) => e.stopPropagation()}>
                <div className={styles.closeIcon}>
                    <ImageSet
                        onClick={() => setOpenModalRecompra(false)}
                        width={20}
                        height={20}
                        src="/assets/icons/GrFormClose.svg"
                        responsive={false}
                        alt=""
                    />
                </div>
                {!showConfirmation ? (
                    <div className={styles.content}>
                        <h1>Forma de pagamento:</h1>

                        <div className={styles.paymentMethods}>
                            {paymentMethodsList.map((payment: any) => (
                                <p
                                    onClick={() => 
                                    {
                                        onSubmitCartPaymentMethod(payment);
                                        setSelectedPaymentMethod(payment);
                                    }}
                                    className={
                                        payment.label === selectedPaymentMethod.label
                                            ? `${styles.paymentOption} ${styles.active}`
                                            : styles.paymentOption
                                    }
                                    key={payment.id}
                                >
                                    <ImageSet
                                        className={styles.noCheck}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/BsCircle.svg"
                                        responsive={false}
                                        alt=""
                                    />
                                    <ImageSet
                                        className={styles.check}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/AiOutlineCheckCircle.svg"
                                        responsive={false}
                                        alt=""
                                    />
                                    {payment.label}
                                </p>
                            ))}
                        </div>

                        {/* {selectedPaymentMethod.label == "Transferência" && (
                            <>
                                <p className={styles.title}>Selecione qual banco irá transferir:</p>
                                <div className={styles.shippingMethods}>
                                    {paymentMethods.map(
                                        (payment: any) =>
                                            payment.label == "Transferência" && (
                                                <p
                                                    onClick={() => {
                                                        onSubmitPayment(payment);
                                                        setSelectedPaymentMethod(payment);
                                                    }}
                                                    className={
                                                        payment.label == selectedPaymentMethod.label
                                                            ? `${styles.paymentOption} ${styles.active}`
                                                            : styles.paymentOption
                                                    }
                                                    key={payment.id}
                                                >
                                                    <BsCircle className={styles.noCheck} />
                                                    <AiOutlineCheckCircle className={styles.check} />
                                                    {payment.bank_name} | Ag:{payment.bank_agency} | C/C:{" "}
                                                    {payment.bank_account}
                                                </p>
                                            )
                                    )}
                                </div>
                            </>
                        )} */}

                        {selectedPaymentMethod.label === "Cartão de Credito" && (
                            <>
                                <p className={styles.title}>Preencha os dados abaixo:</p>
                                <form
                                    className={styles.form}
                                    onSubmit={() => handleSubmit(onSubmit)}
                                >
                                    <div className={styles.formItem}>
                                        <label>Nome do titular do cartão</label>
                                        <input
                                            type="text"
                                            autoComplete="new-off"
                                            placeholder="Ex: José da Silva"
                                            {...register("owner", {
                                                required : "Este campo é obrigatório!",
                                            })}
                                        />
                                        {errors.owner && (
                                            <ErrorMessage message={errors.owner?.message} />
                                        )}
                                    </div>
                                    <div className={styles.formItem}>
                                        <label>CPF</label>
                                        <Controller
                                            name="cpf"
                                            control={control}
                                            rules={{
                                                validate : (value) =>
                                                    validateCpf(value) || "CPF inválido!",
                                            }}
                                            render={({ field }) => (
                                                <PatternFormat
                                                    {...field}
                                                    format="###.###.###-##"
                                                    placeholder="Ex: 123.456.789-10"
                                                    autoComplete="new-off"
                                                    allowEmptyFormatting={false}
                                                />
                                            )}
                                        />
                                        {errors.cpf && (
                                            <ErrorMessage message={errors.cpf?.message} />
                                        )}
                                    </div>
                                    <div className={styles.formItem}>
                                        <label>Número do cartão</label>
                                        <Controller
                                            name="cardnumber"
                                            control={control} // `control` é extraído de `useForm()`
                                            rules={{
                                                validate : (value) =>
                                                    ccValid.number(value?.toString()).isValid === true ||
													"Insira um número de cartão válido!",
                                            }}
                                            render={({ field }) => (
                                                <PatternFormat
                                                    {...field}
                                                    format="################"
                                                    autoComplete="new-off"
                                                    placeholder="Ex: 1234567809101112"
                                                    allowEmptyFormatting={false}
                                                    mask=""
                                                />
                                            )}
                                        />
                                        {errors.cardnumber && (
                                            <ErrorMessage message={errors.cardnumber?.message} />
                                        )}
                                    </div>
                                    <div className={styles.formItem}>
                                        <label>Data de expiração</label>
                                        <div className={styles.formSelects}>
                                            <div className={styles.select}>
                                                <Select
                                                    placeholder="Mês"
                                                    options={getMonthOptions()}
                                                    styles={customSelectStyles}
                                                    isSearchable={false}
                                                    defaultValue={monthCreditCard || null}
                                                    onChange={handleChangeMonth}
                                                />
                                            </div>
                                            <div className={styles.select}>
                                                <Select
                                                    placeholder="Ano"
                                                    options={getYearOptions()}
                                                    styles={customSelectStyles}
                                                    isSearchable={false}
                                                    defaultValue={yearCreditCard || null}
                                                    onChange={handleChangeYear}
                                                />
                                            </div>
                                        </div>
                                        {/* <InputMask
                                mask="99/99"
                                placeholder="Ex: 03/24"
                                autoComplete="new-off"
                                maskChar=""
                                {...register("expirydate", {
                                validate: (value) =>
                                    value.length >= 5 ||
                                    "Sua data de expiração deve ser do tipo DD/AA",
                                })}
                            /> */}
                                        {errors.expirydate && (
                                            <span className={styles.errorMessage}>
                                                <ImageSet
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/AiOutlineExclamationCircle.svg"
                                                    responsive={false}
                                                    alt=""
                                                />
												Selecione a data de expiração!
                                            </span>
                                        )}
                                    </div>
                                    <div className={styles.formItem}>
                                        <label>CVV</label>
                                        <Controller
                                            name="cvv"
                                            control={control}
                                            rules={{
                                                validate : (value) =>
                                                    (value && value.length >= 3) ||
													"Seu CVV tem que possuir no mínimo 3 números",
                                            }}
                                            render={({ field }) => (
                                                <PatternFormat
                                                    {...field}
                                                    format="####"
                                                    placeholder="Ex: 123"
                                                    autoComplete="new-off"
                                                    allowEmptyFormatting={false}
                                                    mask=""
                                                />
                                            )}
                                        />
                                        {errors.cvv && (
                                            <ErrorMessage message={errors.cvv?.message} />
                                        )}
                                    </div>
                                    <div className={styles.selectOption}>
                                        <label>Parcelas</label>
                                        <Select
                                            placeholder="Parcelamento"
                                            options={cart?.installments}
                                            styles={customSelectStyles}
                                            isSearchable={false}
                                            defaultValue={order?.installment || null}
                                            onChange={handleInstallmentChange}
                                        />
                                        {isSubmitted && !installment.label && (
                                            <span className={styles.errorMessage}>
                                                <ImageSet
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/AiOutlineExclamationCircle.svg"
                                                    responsive={false}
                                                    alt=""
                                                />
												Selecione o Parcelamento
                                            </span>
                                        )}
                                    </div>

                                    <p className={styles.formItemTitle}>Endereço de cobrança:</p>

                                    {hasShipping && (
                                        <>
                                            <p
                                                onClick={() => 
                                                {
                                                    setSelectedPaymentAddressMethod(true);
                                                }}
                                                className={
                                                    selectedPaymentAddressMethod
                                                        ? `${styles.paymentOption} ${styles.active}`
                                                        : styles.paymentOption
                                                }
                                            >
                                                <ImageSet
                                                    className={styles.noCheck}
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/BsCircle.svg"
                                                    responsive={false}
                                                    alt=""
                                                />
                                                <ImageSet
                                                    className={styles.check}
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/AiOutlineCheckCircle.svg"
                                                    responsive={false}
                                                    alt=""
                                                />
												Usar o endereço de entrega
                                            </p>
                                            <p
                                                onClick={() => 
                                                {
                                                    setSelectedPaymentAddressMethod(false);
                                                }}
                                                className={
                                                    !selectedPaymentAddressMethod
                                                        ? `${styles.paymentOption} ${styles.active}`
                                                        : styles.paymentOption
                                                }
                                            >
                                                <ImageSet
                                                    className={styles.noCheck}
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/BsCircle.svg"
                                                    responsive={false}
                                                    alt=""
                                                />
                                                <ImageSet
                                                    className={styles.check}
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/AiOutlineCheckCircle.svg"
                                                    responsive={false}
                                                    alt=""
                                                />
												Usar um endereço diferente
                                            </p>
                                        </>
                                    )}

                                    {!selectedPaymentAddressMethod && (
                                        <>
                                            <div className={styles.formItem}>
                                                <label>CEP</label>
                                                <Controller
                                                    name="zipcode"
                                                    control={control}
                                                    rules={{
                                                        validate : (value) =>
                                                            (value && value.length >= 9) ||
															"Seu CEP deve possuir 8 números",
                                                    }}
                                                    render={({ field }) => (
                                                        <PatternFormat
                                                            {...field}
                                                            format="#####-###"
                                                            placeholder="Ex: 12345-678"
                                                            allowEmptyFormatting={false}
                                                            autoComplete="new-off"
                                                            onValueChange={(values) => 
                                                            {
                                                                field.onChange(values.value);

                                                                if (values.value.length === 8) 
                                                                {
                                                                    buscaCep(
                                                                        values.formattedValue,
                                                                        setAddress,
                                                                        setValue,
                                                                    );
                                                                }
                                                            }}
                                                        />
                                                    )}
                                                />
                                                {errors.zipcode && (
                                                    <ErrorMessage message={errors.zipcode?.message} />
                                                )}
                                            </div>
                                            <div className={styles.formItem}>
                                                <label>Rua</label>
                                                <input
                                                    defaultValue={address?.logradouro}
                                                    type="text"
                                                    placeholder="Ex: Rua Guaranabara"
                                                    autoComplete="new-off"
                                                    {...register("street", {
                                                        required : "Este campo é obrigatório!",
                                                    })}
                                                />
                                                {errors.street && (
                                                    <ErrorMessage message={errors.street?.message} />
                                                )}
                                            </div>
                                            <div className={styles.formItem}>
                                                <label>Número</label>
                                                <input
                                                    type="text"
                                                    autoComplete="new-off"
                                                    placeholder="Ex: 17"
                                                    {...register("housenumber", {
                                                        required : "Este campo é obrigatório!",
                                                    })}
                                                />
                                                {errors.housenumber && (
                                                    <ErrorMessage message={errors.housenumber?.message} />
                                                )}
                                            </div>
                                            <div className={styles.formItem}>
                                                <label>Complemento</label>
                                                <input
                                                    type="text"
                                                    autoComplete="new-off"
                                                    placeholder="Ex: Ap 15B"
                                                    {...register("complement", {})}
                                                />
                                                {errors.complement && (
                                                    <ErrorMessage message={errors.complement?.message} />
                                                )}
                                            </div>
                                            <div className={styles.formItem}>
                                                <label>Bairro</label>
                                                <input
                                                    defaultValue={address?.bairro}
                                                    type="text"
                                                    autoComplete="new-off"
                                                    placeholder="Ex: Dom Bosco"
                                                    {...register("district", {
                                                        required : "Este campo é obrigatório!",
                                                    })}
                                                />
                                                {errors.district && (
                                                    <ErrorMessage message={errors.district?.message} />
                                                )}
                                            </div>
                                            <div className={styles.formItem}>
                                                <label>Cidade</label>
                                                <input
                                                    defaultValue={address?.localidade}
                                                    type="text"
                                                    placeholder="Ex: São Paulo"
                                                    autoComplete="new-off"
                                                    {...register("city", {
                                                        required : "Este campo é obrigatório!",
                                                    })}
                                                />
                                                {errors.city && (
                                                    <ErrorMessage message={errors.city?.message} />
                                                )}
                                            </div>
                                            <div className={styles.formItem}>
                                                <label>Estado</label>
                                                <input
                                                    defaultValue={address?.uf}
                                                    type="text"
                                                    placeholder="Ex: São Paulo"
                                                    autoComplete="new-off"
                                                    {...register("state", {
                                                        required : "Este campo é obrigatório!",
                                                    })}
                                                />
                                                {errors.state && (
                                                    <ErrorMessage message={errors.state?.message} />
                                                )}
                                            </div>
                                        </>
                                    )}
                                </form>
                            </>
                        )}

                        <div className={styles.btn}>
                            {!loadingPayment ? (
                                <button
                                    // disabled={loadingCart && true}
                                    type="button"
                                    className={`block ${styles.submit}`}
                                    onClick={
                                        selectedPaymentMethod?.value === "credit_card"
                                            ? handleSubmit(onSubmit)
                                            : () => onSubmit({})
                                    }
                                >
									Próximo
                                </button>
                            ) : (
                                <button type="button">
                                    <AnimatedLoading />
                                </button>
                            )}
                        </div>
                    </div>
                ) : (
                    <div className={styles.contentInformation}>
                        <h1>Confirmação</h1>
                        <div className={styles.test}>
                            <div className={styles.confimationPayment}>
                                <p className={styles.confirmationTitle}>
									Deseja concluir o pagamento via {selectedPaymentMethod.label}
                                    {selectedPaymentMethod.value === "credit_card"
                                        ? ` em ${
                                            installment.label
                                                ? installment.label
                                                : order.installment.label
                                        }`
                                        : ""}
									?
                                </p>
                            </div>
                            <div className={styles.btn}>
                                <button
                                    className="block"
                                    type="button"
                                    onClick={() => 
                                    {
                                        setShowConfirmation(false);
                                    }}
                                >
									NÃO
                                </button>
                                <button
                                    className="block"
                                    type="button"
                                    onClick={() => 
                                    {
                                        finishPayment();
                                    }}
                                >
                                    {!loadingPayment ? "SIM" : <AnimatedLoading />}
                                </button>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
};
