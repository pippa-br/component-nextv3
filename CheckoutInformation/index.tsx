import { TDate } from "@/core-nextv3/model/TDate";
import cleanDeep from "clean-deep";
import { useTranslations } from "next-intl";
import { Checkbox } from "pretty-checkbox-react";
import { useEffect, useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { PatternFormat } from "react-number-format";
import Select from "react-select";
import {
    addressCheckoutAnalytics,
    userLoginAnalytics,
} from "../../core-nextv3/analytics/analytics.api";
import {
    addUserAuth,
    loginAuth,
    recoveryPasswordAuth,
    setUserAuth,
} from "../../core-nextv3/auth/auth.api";
import { mergeCart, setAddressCart } from "../../core-nextv3/cart/cart.api";
import { useCore } from "../../core-nextv3/core/core";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import {
    buscaCep2,
    getCnpjInfo,
    getRecaptcha,
    randomNumber,
} from "../../core-nextv3/util/util";
import { customSelectStyles } from "../../core-nextv3/util/util.style";
import {
    validateCEP,
    validateCNPJ,
    validateCpf,
    validateDate,
    validateEmail,
    validateFullName,
    validatePhone,
} from "../../core-nextv3/util/validate";
import {
    AUTH_SETTING,
    CART_SETTING,
    THEME_SETTING,
} from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import ErrorMessage from "../ErrorMessage";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";
declare const window: any;

export const CheckoutInformation = ({
    handleNext,
    loadingCart,
    setLoadingCart,
    goBack,
}: any) => 
{
    const t = useTranslations();
    const [ gender, setGender ] = useState<any>();
    //const [valueCPF, setValueCPF]                               = useState('');
    const [ formType, setFormType ] = useState("signup");
    const [ hasWhatsapp, setHasWhatsapp ] = useState(false);
    const [ passwordType, setPasswordType ] = useState("password");
    const [ birthday, setBirthday ] = useState<any>(null);
    const [ typePersonFormSelected, setTypePersonFormSelected ] =
		useState<any>("F");
    const [ testePaulo, setTestepaulo ] = useState<any>("F");

    const { cart, user, setUser, setCart, setCnpjWs } = useCore();

    const {
        register: registerSignup,
        handleSubmit: handleSubmitSignup,
        formState: { errors: errorsSignup },
    } = useForm();

    const {
        register: registerInfo,
        handleSubmit: handleSubmitInfo,
        watch: watchInfo,
        reset: resetInfo,
        setValue: setValueInfo,
        trigger: triggerInfo,
        control: controlInfo,
        formState: { errors: errorsInfo, isSubmitted: isSubmittedInfo },
    } = useForm();

    const {
        register: registerLogin,
        setValue: setValueLogin,
        handleSubmit: handleSubmitLogin,
        formState: { errors: errorsLogin, isSubmitted: isSubmittedLogin },
    } = useForm();

    const {
        //register: registerDocuments,
        setValue: setValueDocuments,
        handleSubmit: handleSubmitDocuments,
        control: controlDocuments,
        formState: { errors: errorsDocuments },
    } = useForm();

    const {
        register: registerAddress,
        setValue: setValueAddress,
        //setError : setErrorAddress,
        handleSubmit: handleSubmitAddress,
        control: controlAddress,
        trigger: triggerAddress,
        formState: { errors: errorsAddress, isSubmitted: isSubmittedAddress },
    } = useForm();

    const {
        register: registerRecoveryPassword,
        handleSubmit: handleSubmitRecoveryPassword,
        setValue: setValueRecoveryPassword,
        formState: {
            errors: errorsRecoveryPassword,
            isSubmitted: isSubmittedRecoveryPassword,
        },
    } = useForm();

    const password = useRef({});
    password.current = watchInfo("password", "");

    const genderOptions = [
        { label : "Não Quero Informar", value : "I", id : "IsQ9FFhx" },
        { label : "Masculino", value : "M", id : "yfcqOV15" },
        { label : "Feminino", value : "F", id : "BoXeFDWZ" },
    ];

    const typesPersonsForm = [
        { label : "Pessoa Fisica", value : "F", id : "f" },
        { label : "Pessoa Jurídica", value : "J", id : "j" },
    ];

    useEffect(() => 
    {
        registerInfo("gender", { value : null });

        if (THEME_SETTING.registrationLoginTypeForm === "CPFCNPJ") 
        {
            setTypePersonFormSelected("F");
        }
        else if (THEME_SETTING.registrationLoginTypeForm === "CNPJ") 
        {
            setTypePersonFormSelected("J");
        }
        else 
        {
            setTypePersonFormSelected("F");
        }
    }, []);

    useEffect(() => 
    {
        resetInfo(cleanDeep(user));

        if (user) 
        {
            if (!user.phone) 
            {
                setFormType("info");
            }
            else if (!(user.cpf || user.cnpj)) 
            {
                setFormType("documents");
            }
            else 
            {
                setFormType("address");
            }
        }
    }, [ user ]);

    useEffect(() => 
    {
        tagManager4.checkoutStep(cart, "initiateCheckout", 1, user);
    }, []);

    // const errorUser = (result: any) =>
    // {
    //     return toast(
    //         (t) => (
    //             <span>
    //                 Usuário já possui cadastro com o e-mail: {result?.data?.email}.<br />
    //                 <br />
    //                 Clique aqui se deseja:
    //                 <br />
    //                 <br />
    //                 <button
    //                     onClick={() =>
    //                     {
    //                         setFormType("withPassword")
    //                         toast.dismiss(t.id)
    //                     }}
    //                 >
    //                     Fazer o Login
    //                 </button>
    //                 <button
    //                     onClick={() =>
    //                     {
    //                         recoveryPassword(result?.data?.email)
    //                         setFormType("withPassword")
    //                         toast.dismiss(t.id)
    //                     }}
    //                 >
    //                     Receber Uma Nova Senha
    //                 </button>
    //                 <button onClick={() => toast.dismiss(t.id)}>Fechar</button>
    //             </span>
    //         ),
    //         {
    //             className : "toast-custom",
    //             duration  : 999999,
    //         }
    //     )
    // }

    // const existingAccountNotice = (result: any) =>
    // {
    //     return toast(
    //         (t) => (
    //             <span>
    //                 Usuário já possui cadastro com o e-mail: {result?.data?.email}.<br />
    //                 <br />
    //                 Faça o login
    //                 <br />
    //                 <br />
    //                 {/* <button
    //                     onClick={() =>
    //                     {
    //                         recoveryPassword(result?.data?.email)
    //                         setFormType("withPassword")
    //                         toast.dismiss(t.id)
    //                     }}
    //                 >
    //                     Receber Uma Nova Senha
    //                 </button> */}
    //                 <button onClick={() => toast.dismiss(t.id)}>Fechar</button>
    //             </span>
    //         ),
    //         {
    //             className : "toast-custom",
    //             duration  : 999999,
    //         }
    //     )
    // }

    const handleBirthday = (value: any) => 
    {
        setBirthday(null);

        if (value?.split("_")?.length === 1) 
        {
            const testBirthday = new TDate({ value : value, mask : "dd/MM/yyyy" });

            if (testBirthday.isValid()) 
            {
                setBirthday(value);
            }
        }

        setValueInfo("birthday", value);
    };

    const onSubmitSignup = async (data: any) => 
    {
        setLoadingCart(true);

        let token = await getRecaptcha("addUser");

        if (token) 
        {
            const password = randomNumber(8);

            const newData: any = {
                token : token,
                data  : {
                    email           : data.email,
                    newPassword     : password,
                    password        : password,
                    confirmPassword : password,
                },
            };

            const result: any = await addUserAuth(AUTH_SETTING.merge(newData));

            if (!result.status) 
            {
                setLoadingCart(false);
                setValueLogin("login", result?.data?.email);
                setValueRecoveryPassword("login", result?.data?.email);
                setFormType("login");

                return toast.success(t("Informe sua Senha!"), {
                    duration : 2000,
                });
            }

            ("");

            token = await getRecaptcha("login");

            // LOGIN
            const result2 = await loginAuth(
                AUTH_SETTING.merge({
                    token    : token,
                    login    : data.email.toLowerCase(),
                    password : password,
                }),
            );

            // MERGE CART SESSION
            await mergeCart(
                CART_SETTING.merge({
                    document : {
                        referencePath : cart?.referencePath,
                    },
                }),
            );

            // LOGIN ANALYTICS
            userLoginAnalytics(result2.data);

            setUser(result2.data);
        }

        setLoadingCart(false);
    };

    const onSubmitUpdateUser = async (data: any) => 
    {
        setLoadingCart(true);

        data.referencePath = undefined;

        const token = await getRecaptcha("addUser");

        if (token) 
        {
            const birthday = new TDate({ value : data.birthday, mask : "dd/MM/yyyy" });

            if (!birthday.isValid()) 
            {
                await setLoadingCart(false);
                return toast.error(t("Data de aniversário inválida!"), {
                    duration : 2000,
                });
            }

            if (typePersonFormSelected === "F") 
            {
                data.personType = { label : "Física", value : "F", id : "F" };
            }
            else if (typePersonFormSelected === "J") 
            {
                data.personType = { label : "Juridica", value : "J", id : "J" };
            }

            const newData: any = {
                token : token,
                data  : {
                    name       : data.name,
                    phone      : data.phone,
                    email      : data.email,
                    birthday   : birthday.toDate(),
                    gender     : data.gender,
                    personType : data.personType,
                    // password        : data.password,
                    // confirmPassword : data.confirmPassword,
                },
            };

            if (data.cnpj) 
            {
                newData.data.cnpj = data.cnpj;
            }

            const result2 = await setUserAuth(AUTH_SETTING.merge(newData));

            setUser(result2.data);
        }

        setLoadingCart(false);
        setFormType("documents");

        window.scroll({
            top      : 0,
            left     : 0,
            behavior : "instant",
        });
    };

    const onSubmitDocumentsUser = async (data: any) => 
    {
        setLoadingCart(true);

        data.referencePath = undefined;

        const token = await getRecaptcha("addUser");

        if (token) 
        {
            const newData: any = {
                token : token,
                data  : {
                    cpf : data.cpf,
                },
            };

            if (testePaulo === "J") 
            {
                newData.data.cnpj = data?.cnpj;
            }

            const result2 = await setUserAuth(AUTH_SETTING.merge(newData));

            setUser(result2.data);
        }

        setLoadingCart(false);
        setFormType("address");
    };

    // async function getEmailToken()
    // {
    //     const result = await getTokenLoginAuth(
    //         AUTH_SETTING.merge({
    //             email : emailForToken.toLowerCase(),
    //         })
    //     )

    //     if (!result.status)
    //     {
    //         return toast.error("Erro! Verifique dos campos digitados.", { duration : 2000 })
    //     }

    //     toast(
    //         (t) => (
    //             <span>
    //                 {result.message}
    //                 <button onClick={() => toast.dismiss(t.id)}>Ok</button>
    //             </span>
    //         ),
    //         {
    //             className : "toast-custom",
    //             duration  : 999999,
    //         }
    //     )

    //     setFormType("loginWithToken")
    // }

    async function recoveryPassword(data: any) 
    {
        tagManager4.registerEvent("click-button", "lead", "Entrar", 0, null);

        const result = await recoveryPasswordAuth(
            AUTH_SETTING.merge({
                login : data?.login?.toLowerCase(),
            }),
        );

        if (!result.status) 
        {
            return toast.error(result.error);
        }

        toast(
            (t: any) => (
                <span>
                    {result.message}
                    <button onClick={() => toast.dismiss(t.id)}>Ok</button>
                </span>
            ),
            {
                className : "toast-custom",
                duration  : 999999,
            },
        );

        setValueLogin("login", data?.login?.toLowerCase());
        setFormType("login");
        //setFormType("withPassword")
    }

    // async function loginWithToken(email: string)
    // {
    //     const result = await loginTokenAuth(
    //         AUTH_SETTING.merge({
    //             token : email,
    //         })
    //     )

    //     if (!result.status)
    //     {
    //         return toast.error("Erro! Verifique dos campos digitados.", { duration : 2000 })
    //     }

    //     // MERGE CART SESSION
    //     await mergeCart(
    //         CART_SETTING.merge({
    //             document : {
    //                 referencePath : cart?.referencePath,
    //             },
    //         })
    //     )

    //     tagManager4.signUp("token")
    //     const toastInstance: any = toast("Login feito com sucesso!", { icon : "👏" })
    //     setTimeout(() => toast.dismiss(toastInstance.id), 2000)

    //     // LOGIN ANALYTICS
    //     userLoginAnalytics(result.data)

    //     push("/carrinho")
    // }

    async function handleLoginWithEmailAndPassword(formData: any) 
    {
        const token = await getRecaptcha("login");

        if (token) 
        {
            setLoadingCart(true);

            const result = await loginAuth(
                AUTH_SETTING.merge({
                    token    : token,
                    login    : formData.login.toLowerCase(),
                    password : formData.password,
                }),
            );

            // MERGE CART SESSION
            const cartResult = await mergeCart(
                CART_SETTING.merge({
                    document : {
                        referencePath : cart?.referencePath,
                    },
                }),
            );

            setLoadingCart(false);

            if (!result.status) 
            {
                return toast.error(result.error || "Erro ao fazer login.", {
                    duration : 2000,
                });
            }

            const toastInstance: any = toast("Login feito com sucesso!", {
                icon     : "👏",
                duration : 2000,
            });
            setTimeout(() => toast.dismiss(toastInstance.id), 2000);

            // LOGIN ANALYTICS
            userLoginAnalytics(result.data);

            setUser(result.data);
            setCart(cartResult.data);

            setFormType("info");
        }
    }

    const onWhatsapp = async (e: any) => 
    {
        setHasWhatsapp(!hasWhatsapp);
        const valueCheckbox = e.target.checked;
        // console.log("valueCheckbox", valueCheckbox)

        const result = await setUserAuth(
            AUTH_SETTING.merge({
                document : {
                    referencePath : user?.referencePath,
                },
                data : {
                    onWhatsapp : valueCheckbox,
                },
            }),
        );

        setUser(result.data);
    };

    useEffect(() => 
    {
        registerInfo("gender", { value : null });
    }, []);

    function handleChangeGender(e: any) 
    {
        setValueInfo("gender", e);
        setGender(e);
    }

    const onSubmitCartAddress = async (data: any) => 
    {
        setLoadingCart(true);

        data.referencePath = undefined;

        const token = await getRecaptcha("addUser");

        if (token) 
        {
            const newData: any = {
                token : token,
                data  : {
                    address : {
                        zipcode     : data?.address?.zipcode || "",
                        street      : data?.address?.street || "",
                        housenumber : data?.address?.housenumber || "",
                        complement  : data?.address?.complement || "",
                        district    : data?.address?.district || "",
                        city        : data?.address?.city || "",
                        state       : data?.address?.state || "",
                        country     : {
                            id       : "br",
                            label    : "Brasil",
                            selected : true,
                            value    : "br",
                        },
                    },
                },
            };

            const result2 = await setUserAuth(AUTH_SETTING.merge(newData));

            setUser(result2.data);
        }

        // SET CART SHIPPING
        const newData = {
            data : data.address,
        };

        const result = await setAddressCart(CART_SETTING.merge(newData));

        if (result.status) 
        {
            // ADDRESS ANALYTICS
            addressCheckoutAnalytics();

            setCart(result.data);
            setFormType("shipping");
        }
        else 
        {
            setLoadingCart(false);

            return toast.error(
                t("Verifique se os campos foram preenchidos corretamente."),
                { duration : 2000 },
            );
        }

        setLoadingCart(false);
        handleNext();
    };

    const onZipcode = async (value: any) => 
    {
        const data: any = await buscaCep2(value);

        if (data) 
        {
            setValueAddress("address", data);
            triggerAddress();
        }
        else 
        {
            //  setErrorAddress('address.zipcode', {
            //     type    : 'manual',
            //     message : 'CEP Invalido!',
            // });
        }
    };

    /*async function handleLogout() 
    {
    	  await clearCart(CART_SETTING);
    	  await logoutAuth(AUTH_SETTING);
    	  router.push("/");
    	  toast.success("Logout feito com sucesso!");
    }*/

    /*const onSubmitCartUser = async (data: any) => 
    {
		    if(!validateCpf(data.cpf)) 
        {
			      return toast.error("CPF inválido!", { duration : 2000 });
		    }

        const newData = {
            document: {
                referencePath: userData.referencePath,
            },
            data: data,
        };

        const result = await setUserAuth(AUTH_SETTING.merge(newData));

        setNewUserData(result);
    };*/

    return (
        <div className={styles.checkoutInformation}>
            {formType === "info" && (
                <>
                    <div className={styles.formHeader}>
                        <p className={styles.title}>{t("Completar cadastro")}</p>
                        <p className={styles.subtitle}>
							Conclua seu cadastro para começar a fazer compras
                        </p>
                    </div>

                    <form className={styles.formRegister}>
                        {THEME_SETTING.registrationLoginTypeForm === "CPFCNPJ" && (
                            <div className={styles.buttonsTop}>
                                {typesPersonsForm?.map((typePerson: any, index: any) => (
                                    <button
                                        key={index}
                                        onClick={() => setTypePersonFormSelected(typePerson?.value)}
                                        className={`${
                                            typePerson?.value === typePersonFormSelected
                                                ? styles.buttonSelected
                                                : ""
                                        }`}
                                    >
                                        {typePerson?.label}
                                    </button>
                                ))}
                            </div>
                        )}

                        <div className={styles.formInputs}>
                            {!user.email && (
                                <div className={styles.formItem}>
                                    <label>{t("E-mail")}</label>
                                    <input
                                        style={{ textTransform : "lowercase" }}
                                        type="email"
                                        className={styles.input}
                                        required
                                        placeholder="ex: maria.jose@gmail.com"
                                        autoComplete="new-password"
                                        {...registerInfo("email", {
                                            validate : (value) =>
                                                validateEmail(value) || t("E-mail inválido"),
                                        })}
                                    />
                                    {errorsInfo.email && (
                                        <ErrorMessage message={errorsInfo.email?.message} />
                                    )}
                                </div>
                            )}

                            {!user.cnpj && typePersonFormSelected === "J" && (
                                <div className={styles.formItem}>
                                    <label>{t("CNPJ")}</label>
                                    <Controller
                                        name="cnpj"
                                        control={controlInfo}
                                        rules={{
                                            validate : (value) =>
                                                validateCNPJ(value) || "CNPJ inválido!",
                                        }}
                                        render={({ field }) => (
                                            <PatternFormat
                                                {...field}
                                                format="##.###.###/####-##"
                                                type="tel"
                                                className={styles.input}
                                                allowEmptyFormatting={false}
                                                placeholder="ex: 07.322.649/0001-78"
                                                onValueChange={(values) => 
                                                {
                                                    field.onChange(values.value);
                                                    getCnpjInfo(values.value, setValueInfo, triggerInfo);
                                                }}
                                            />
                                        )}
                                    />
                                    {errorsInfo?.cnpj && (
                                        <ErrorMessage message={errorsInfo?.cnpj?.message} />
                                    )}
                                </div>
                            )}

                            {typePersonFormSelected === "J" && (
                                <div className={styles.formItem}>
                                    <label>{t("Inscrição Estadual")}</label>
                                    <input
                                        {...registerInfo("stateinscription")}
                                        className={styles.input}
                                        // placeholder='1111111111'
                                    />
                                    {errorsInfo?.stateinscription && (
                                        <ErrorMessage
                                            message={errorsInfo?.stateinscription?.message}
                                        />
                                    )}
                                </div>
                            )}

                            {typePersonFormSelected === "J" && (
                                <div className={styles.formItem}>
                                    <label>{t("Nome da Fantasia")}</label>
                                    <input
                                        type="text"
                                        className={styles.input}
                                        {...registerInfo("storeName", {
                                            required : "Este campo é obrigatório!",
                                        })}
                                        // placeholder='Nome da empresa'
                                    />
                                    {errorsInfo?.storeName && (
                                        <ErrorMessage message={errorsInfo?.storeName?.message} />
                                    )}
                                </div>
                            )}

                            {!user.name && (
                                <div className={styles.formItem}>
                                    <label>{t("Nome Completo")}</label>
                                    <input
                                        defaultValue={user?.name}
                                        type="text"
                                        placeholder="ex: Maria Jose"
                                        autoComplete="new-password"
                                        {...registerInfo("name", {
                                            validate : (value) =>
                                                validateFullName(value) || t("Nome Completo"),
                                        })}
                                    />
                                    {errorsInfo?.name && (
                                        <ErrorMessage message={errorsInfo?.name?.message} />
                                    )}
                                </div>
                            )}

                            <div className={styles.formItem}>
                                <label>{t("Celular")}</label>
                                <Controller
                                    name="phone"
                                    control={controlInfo}
                                    rules={{
                                        validate : (value) =>
                                            validatePhone(value) || t("Celular Invalido"),
                                    }}
                                    render={({ field }) => (
                                        <PatternFormat
                                            {...field}
                                            type="tel"
                                            format="(##) #####-####"
                                            autoComplete="new-password"
                                            placeholder="ex: (11) 99282-9222"
                                            allowEmptyFormatting={false}
                                            onValueChange={(values) => 
                                            {
                                                field.onChange(values.value);
                                            }}
                                        />
                                    )}
                                />
                                {errorsInfo?.phone && (
                                    <ErrorMessage message={errorsInfo?.phone?.message} />
                                )}
                            </div>

                            {!THEME_SETTING?.disabeldInputRegister && (
                                <>
                                    {typePersonFormSelected === "F" && (
                                        <div className={styles.formItem}>
                                            <label>{t("Data de Nascimento")}</label>
                                            <Controller
                                                name="birthday"
                                                control={controlInfo}
                                                rules={{
                                                    required : "Este campo é obrigatório!",
                                                    validate : (value) =>
                                                        validateDate(value) || t("Data inválida"),
                                                }}
                                                render={({ field }) => (
                                                    <PatternFormat
                                                        {...field}
                                                        format="##/##/####"
                                                        type="tel"
                                                        autoComplete="new-password"
                                                        placeholder="ex: 08/09/1994"
                                                        allowEmptyFormatting={false}
                                                        className={styles.input}
                                                        onValueChange={(values) => 
                                                        {
                                                            field.onChange(values.value);
                                                            handleBirthday(values.formattedValue);
                                                        }}
                                                    />
                                                )}
                                            />
                                            {isSubmittedInfo && !birthday && (
                                                <ErrorMessage message={"Data Invalida!"} />
                                            )}
                                        </div>
                                    )}
                                </>
                            )}

                            {!THEME_SETTING?.disabeldInputRegister && (
                                <>
                                    {typePersonFormSelected === "F" && (
                                        <div className={styles.formItem}>
                                            <label>{t("Gênero")}</label>
                                            <Select
                                                placeholder={t("Escolha seu gênero")}
                                                // placeholder=""
                                                className={styles.select}
                                                options={genderOptions}
                                                styles={customSelectStyles}
                                                isClearable
                                                value={gender}
                                                onChange={handleChangeGender}
                                            />
                                            {isSubmittedInfo && !gender && (
                                                <ErrorMessage message={t("Campo requiredo!")} />
                                            )}
                                        </div>
                                    )}
                                </>
                            )}

                            {/* <div className={styles.formItem}>
                                <label>{t("Senha")}</label>
                                <input
                                    type={passwordType}
                                    required
                                    className={styles.input}
                                    autoComplete='new-password'
                                    placeholder={t("Digite sua senha")}
                                    {...register("password", {
                                        validate : (value) =>
                                            value.length >= 8 ||
                                            t("Sua senha deve conter no mínimo 8 caracteres"),
                                    })}
                                />
                                {passwordType === "password" ? (
                                    <Image
                                        className={styles.iconPassword}
                                        onClick={() => setPasswordType("text")}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/AiOutlineEye.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />

                                ) : (
                                    <Image
                                        className={styles.iconPassword}
                                        onClick={() => setPasswordType("password")}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/AiOutlineEyeInvisible.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />
                                )}
                                {errors.password && (
                                    <ErrorMessage message={errors.password?.message} />
                                )}
                            </div> */}

                            {/* <div className={styles.formItem}>
                                <label>{t("Confirmar Senha")}</label>
                                <input
                                    type={passwordType}
                                    required
                                    className={styles.input}
                                    autoComplete='new-password'
                                    placeholder={t("Confirme sua senha")}
                                    {...register("confirmPassword", {
                                        validate : (value) =>
                                            value === password.current ||
                                            t("A confirmação de senha precisam ser iguais a senha"),
                                    })}
                                />
                                {passwordType === "password" ? (
                                     <Image
                                        className={styles.iconPassword}
                                        onClick={() => setPasswordType("text")}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/AiOutlineEye.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />
                                ) : (
                                    <Image
                                        className={styles.iconPassword}
                                        onClick={() => setPasswordType("password")}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/AiOutlineEyeInvisible.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />
                                )}
                                {errors.confirmPassword && (
                                    <ErrorMessage message={errors.confirmPassword?.message} />
                                )}
                            </div> */}
                        </div>
                    </form>

                    <div className={styles.buttons}>
                        <button
                            className={`block ${styles.submitted}`}
                            type="button"
                            disabled={loadingCart && true}
                            onClick={handleSubmitInfo(onSubmitUpdateUser)}
                        >
                            {loadingCart ? <AnimatedLoading /> : t("Próximo")}
                        </button>
                        {/* {!loadingCart && (
                            <button
                                className={'block '+ styles.cinza}
                                type="button"
                                onClick={() => back()}
                            >
                                {t("Voltar a loja")}
                            </button>
                        )} */}
                    </div>

                    <div className={styles.backLink}>
                        <a
                            onClick={() => 
                            {
                                goBack();
                            }}
                        >
                            <span>Voltar</span>
                        </a>
                    </div>
                </>
            )}
            {formType === "documents" && (
                <>
                    {typePersonFormSelected === "F" && (
                        <>
                            <div className={styles.formHeader}>
                                <p className={styles.title}>{t("Dados para fatura")}</p>
                                <p className={styles.subtitle}>
									Informação para a emissão da nota fiscal
                                </p>
                            </div>

                            <form className={styles.formAddress}>
                                {THEME_SETTING.registrationLoginTypeForm === "CPFCNPJ" ? (
                                    <div className={styles.buttonsOptions}>
                                        <div className={styles.typesButton}>
                                            {typesPersonsForm?.map((typePerson: any, index: any) => (
                                                <button
                                                    type="button"
                                                    key={index}
                                                    onClick={() => setTestepaulo(typePerson?.value)}
                                                    className={`${
                                                        typePerson?.value === testePaulo
                                                            ? styles.buttonSelected
                                                            : ""
                                                    }`}
                                                >
                                                    {typePerson?.label}
                                                </button>
                                            ))}
                                        </div>

                                        {testePaulo === "F" && (
                                            <div className={styles.formItem}>
                                                <label>{t("CPF")}</label>
                                                <Controller
                                                    name="cpf"
                                                    control={controlDocuments}
                                                    defaultValue={user?.cpf}
                                                    rules={{
                                                        validate : (value) =>
                                                            validateCpf(value) || t("CPF inválido"),
                                                    }}
                                                    render={({ field }) => (
                                                        <PatternFormat
                                                            {...field}
                                                            format="###.###.###-##"
                                                            type="tel"
                                                            autoComplete="new-password"
                                                            allowEmptyFormatting={false}
                                                            className={styles.input}
                                                            placeholder="ex: 123.456.789-10"
                                                            onValueChange={(values) => 
                                                            {
                                                                field.onChange(values.value); // Captura o valor não formatado (apenas números)
                                                            }}
                                                        />
                                                    )}
                                                />
                                                {errorsDocuments?.cpf && (
                                                    <ErrorMessage
                                                        message={errorsDocuments?.cpf?.message}
                                                    />
                                                )}
                                            </div>
                                        )}

                                        {testePaulo === "J" && (
                                            <div className={styles.formItem}>
                                                <label>{t("CNPJ")}</label>
                                                <Controller
                                                    name="cnpj"
                                                    control={controlDocuments}
                                                    rules={{
                                                        validate : (value) =>
                                                            validateCNPJ(value) || "CNPJ inválido!",
                                                    }}
                                                    render={({ field }) => (
                                                        <PatternFormat
                                                            {...field}
                                                            format="##.###.###/####-##"
                                                            type="tel"
                                                            className={styles.input}
                                                            placeholder="ex: 34.906.170/0001-51"
                                                            allowEmptyFormatting={false}
                                                            onValueChange={(values) => 
                                                            {
                                                                field.onChange(values.value); // Captura o valor não formatado
                                                                getCnpjInfo(
                                                                    values.value,
                                                                    setCnpjWs,
                                                                    setValueDocuments,
                                                                ); // Chama a função externa
                                                            }}
                                                        />
                                                    )}
                                                />
                                                {errorsDocuments?.cnpj && (
                                                    <ErrorMessage
                                                        message={errorsDocuments?.cnpj.message}
                                                    />
                                                )}
                                            </div>
                                        )}
                                    </div>
                                ) : (
                                    <div className={styles.formItem}>
                                        <label>{t("CPF")}</label>
                                        <Controller
                                            name="cpf"
                                            control={controlDocuments}
                                            defaultValue={user?.cpf}
                                            rules={{
                                                validate : (value) =>
                                                    validateCpf(value) || t("CPF inválido"),
                                            }}
                                            render={({ field }) => (
                                                <PatternFormat
                                                    {...field}
                                                    format="###.###.###-##"
                                                    type="tel"
                                                    autoComplete="new-password"
                                                    className={styles.input}
                                                    placeholder="ex: 123.456.789-10"
                                                    allowEmptyFormatting={false}
                                                    onValueChange={(values) => 
                                                    {
                                                        field.onChange(values.value); // Captura o valor não formatado (apenas números)
                                                    }}
                                                />
                                            )}
                                        />
                                        {errorsDocuments?.cpf && (
                                            <ErrorMessage message={errorsDocuments?.cpf?.message} />
                                        )}
                                    </div>
                                )}
                            </form>
                        </>
                    )}

                    <div className={styles.buttons}>
                        <button
                            className={`block ${styles.submitted}`}
                            type="button"
                            disabled={loadingCart && true}
                            onClick={handleSubmitDocuments(onSubmitDocumentsUser)}
                        >
                            {loadingCart ? <AnimatedLoading /> : t("Próximo")}
                        </button>
                        {/* {!loadingCart && (
                            <button
                                className={'block '+ styles.cinza}
                                type="button"
                                onClick={() => back()}
                            >
                                {t("Voltar a loja")}
                            </button>
                        )} */}
                    </div>

                    <div className={styles.backLink}>
                        <a
                            onClick={() => 
                            {
                                goBack();
                            }}
                        >
                            <span>Voltar</span>
                        </a>
                    </div>
                </>
            )}
            {formType === "address" && (
                <>
                    <div className={styles.formHeader}>
                        <p className={styles.title}>{t("Endereço de entrega")}</p>
                        <p className={styles.subtitle}>
							Informe o endereço onde deseja receber seu pedido
                        </p>
                    </div>
                    <form className={styles.formAddress}>
                        <div className={styles.formInputs}>
                            <div className={styles.formItem}>
                                <label>{t("CEP")}</label>
                                <Controller
                                    name="address.zipcode"
                                    control={controlAddress}
                                    defaultValue={user?.address?.zipcode}
                                    rules={{
                                        validate : (value) =>
                                            validateCEP(value) || t("CEP deve possuir 8 caracteres!"),
                                    }}
                                    render={({ field }) => (
                                        <PatternFormat
                                            {...field}
                                            format="#####-###"
                                            type="tel"
                                            placeholder="ex: 12345-678"
                                            autoComplete="new-password"
                                            allowEmptyFormatting={false}
                                            onValueChange={(values) => 
                                            {
                                                field.onChange(values.value);

                                                if (values.value.length === 8) 
                                                {
                                                    onZipcode(values.formattedValue);
                                                }
                                            }}
                                        />
                                    )}
                                />
                                <div>
                                    {isSubmittedAddress &&
										(errorsAddress?.address as any)?.zipcode?.message && (
                                        <ErrorMessage
                                            message={
                                                (errorsAddress?.address as any)?.zipcode?.message
                                            }
                                        />
                                    )}
                                </div>

                                {/* <div className={styles.infoWrapper}>
                                    
                                    <p
                                        className={styles.info}
                                        onClick={() =>
                                        window.open(
                                            'https://buscacepinter.correios.com.br/app/endereco/index.php'
                                        )
                                        }
                                    >
                                        {t('não sei meu CEP')}
                                    </p>
                                </div>                                 */}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Rua")}</label>
                                <input
                                    type="text"
                                    placeholder="ex: Rua Guaranabara"
                                    autoComplete="new-password"
                                    maxLength={50}
                                    defaultValue={user?.address?.street}
                                    {...registerAddress("address.street", {
                                        required : t("Este campo é obrigatório!"),
                                    })}
                                />
                                {isSubmittedAddress &&
									(errorsAddress?.address as any)?.street?.message && (
                                    <ErrorMessage
                                        message={(errorsAddress?.address as any)?.street?.message}
                                    />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Número")}</label>
                                <input
                                    type="text"
                                    defaultValue={user?.address?.housenumber}
                                    autoComplete="new-password"
                                    placeholder="ex: 17"
                                    maxLength={5}
                                    {...registerAddress("address.housenumber", {
                                        required : t("Este campo é obrigatório!"),
                                    })}
                                />
                                {isSubmittedAddress &&
									(errorsAddress?.address as any)?.housenumber?.message && (
                                    <ErrorMessage
                                        message={
                                            (errorsAddress?.address as any)?.housenumber?.message
                                        }
                                    />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Complemento")}</label>
                                <input
                                    type="text"
                                    autoComplete="new-password"
                                    defaultValue={user?.address?.complement}
                                    placeholder="ex: Ap 15B"
                                    maxLength={30}
                                    {...registerAddress("address.complement")}
                                />
                                {isSubmittedAddress &&
									(errorsAddress?.address as any)?.complement?.message && (
                                    <ErrorMessage
                                        message={
                                            (errorsAddress?.address as any)?.complement?.message
                                        }
                                    />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Bairro")}</label>
                                <input
                                    type="text"
                                    autoComplete="new-password"
                                    defaultValue={user?.address?.district}
                                    maxLength={30}
                                    placeholder="ex: Dom Bosco"
                                    {...registerAddress("address.district", {
                                        required : t("Este campo é obrigatório!"),
                                    })}
                                />
                                {isSubmittedAddress &&
									(errorsAddress?.address as any)?.district?.message && (
                                    <ErrorMessage
                                        message={
                                            (errorsAddress?.address as any)?.district?.message
                                        }
                                    />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Cidade")}</label>
                                <input
                                    type="text"
                                    placeholder="ex: São Paulo"
                                    defaultValue={user?.address?.city}
                                    autoComplete="new-password"
                                    {...registerAddress("address.city", {
                                        required : t("Este campo é obrigatório!"),
                                    })}
                                />
                                {isSubmittedAddress &&
									(errorsAddress?.address as any)?.city?.message && (
                                    <ErrorMessage
                                        message={(errorsAddress?.address as any)?.city?.message}
                                    />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Estado")}</label>
                                <input
                                    type="text"
                                    autoComplete="new-password"
                                    defaultValue={user?.address?.state}
                                    placeholder="ex: SP"
                                    maxLength={2}
                                    {...registerAddress("address.state", {
                                        required : t("Este campo é obrigatório!"),
                                    })}
                                />
                                {isSubmittedAddress &&
									(errorsAddress?.address as any)?.state?.message && (
                                    <ErrorMessage
                                        message={(errorsAddress?.address as any)?.state?.message}
                                    />
                                )}
                            </div>
                            <input
                                type="hidden"
                                defaultValue={user?.address?.country}
                                {...registerAddress("address.country")}
                            />
                        </div>
                    </form>

                    <div className={styles.checkbox}>
                        <Checkbox
                            color="success"
                            // checked={hasWhatsapp}
                            checked={!!user.onWhatsapp}
                            onChange={onWhatsapp}
                        >
                            {t("Quero receber notificações via whatsapp")}
                        </Checkbox>
                    </div>

                    <div className={styles.buttons}>
                        <button
                            className={`block ${styles.submitted}`}
                            type="button"
                            disabled={loadingCart && true}
                            onClick={handleSubmitAddress(onSubmitCartAddress)}
                        >
                            {loadingCart ? <AnimatedLoading /> : t("Próximo")}
                        </button>
                    </div>

                    <div className={styles.backLink}>
                        <a
                            onClick={() => 
                            {
                                goBack();
                            }}
                        >
                            <span>Voltar</span>
                        </a>
                    </div>
                </>
            )}
            {/* {formType === "onlyEmail" && (
                <div className={styles.contentForm}>
                    <div className={styles.title}>Por favor informe seu email</div>
                    <div className={styles.inputControl}>
                        <input
                            style={{ textTransform : "lowercase" }}
                            onChange={(e: any) => setEmailForToken(e.target.value)}
                            type='email'
                            name='email'
                            placeholder='Digite o seu email'
                            onKeyUp={(e) => 
                            {
                                if (e.code === "Enter") 
                                {
                                    setFormType("loginWithToken")
                                    getEmailToken()
                                }
                            }}
                        />
                        {errors.email && <ErrorMessage message={errors.email.message} />}
                    </div>
                    <div className={styles.buttons}>
                        <button
                            className={styles.submitted}
                            onClick={() => 
                            {
                                tagManager4.registerEvent(
                                    "click-button",
                                    "lead",
                                    "Entrar",
                                    0,
                                    null
                                )
                                getEmailToken()
                            }}
                            type='button'
                        >
                            Entrar
                        </button>
                        <button
                            type='button'
                            onClick={() => 
                            {
                                tagManager4.registerEvent(
                                    "click-button",
                                    "lead",
                                    "Voltar",
                                    0,
                                    null
                                )
                                setFormType("login")
                            }}
                        >
                            Voltar
                        </button>
                    </div>
                </div>
            )} */}
            {formType === "login" && (
                <form className={styles.formTop}>
                    <div className={styles.contentForm}>
                        <div className={styles.formHeader}>
                            <p className={styles.title}>Entre ou Crie uma Conta</p>
                            <p className={styles.subtitle}>
								Clique aqui para{" "}
                                <a onClick={() => setFormType("signup")}>CRIAR CONTA</a>
                            </p>
                        </div>
                        <div className={styles.inputsContentForm}>
                            <label>{t("E-mail")}</label>
                            <div className={styles.inputControl}>
                                <input
                                    id="loginInput"
                                    style={{ textTransform : "lowercase" }}
                                    type="email"
                                    placeholder="E-mail"
                                    {...registerLogin("login", {
                                        required : "Este campo é obrigatório!",
                                        pattern  : {
                                            value   : /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                            message : "Email inválido",
                                        },
                                    })}
                                />
                                {isSubmittedLogin && errorsLogin?.login && (
                                    <ErrorMessage message={errorsLogin?.login?.message} />
                                )}
                            </div>
                            <label>{t("Senha")}</label>
                            <div className={styles.inputControl}>
                                <div className={styles.inputPassword}>
                                    <input
                                        id="passwordInput"
                                        {...registerLogin("password", {
                                            required  : "Este campo é obrigatório!",
                                            minLength : {
                                                value   : 8,
                                                message : "Sua senha possui, no mínimo, 8 caracteres.",
                                            },
                                        })}
                                        type={passwordType}
                                        placeholder="Senha"
                                    />
                                    {passwordType === "password" ? (
                                        <ImageSet
                                            className={styles.iconPassword}
                                            onClick={() => setPasswordType("text")}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiOutlineEye.svg"
                                            responsive={false}
                                            alt=""
                                        />
                                    ) : (
                                        <ImageSet
                                            className={styles.iconPassword}
                                            onClick={() => setPasswordType("password")}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiOutlineEyeInvisible.svg"
                                            responsive={false}
                                            alt=""
                                        />
                                    )}
                                </div>
                                {isSubmittedLogin && errorsLogin?.password && (
                                    <ErrorMessage message={errorsLogin?.password?.message} />
                                )}
                            </div>
                        </div>
                        <div className={styles.inputControl}>
                            <a onClick={() => setFormType("recoveryPassword")}>
								Esqueceu a senha? <br />
								Clique aqui para recebê-la por e-mail
                            </a>
                        </div>
                        <div className={styles.buttons}>
                            <button
                                id="submitWithPasswordButton"
                                className={styles.submitted}
                                onClick={handleSubmitLogin(handleLoginWithEmailAndPassword)}
                                type="button"
                            >
                                {loadingCart ? <AnimatedLoading /> : "Entrar"}
                            </button>
                        </div>

                        <div className={styles.backLink}>
                            <a onClick={() => setFormType("signup")}>Voltar</a>
                        </div>
                    </div>
                </form>
            )}
            {formType === "signup" && (
                <form className={styles.formTop}>
                    <div className={styles.formHeader}>
                        <p className={styles.title}>{t("Qual o seu email?")}</p>
                        <p className={styles.subtitle}>
                            {t("Informe seu e-mail para continuar")}
                        </p>
                    </div>
                    <div className={styles.inputs}>
                        <div className={styles.formItem}>
                            <label>{t("E-mail")}</label>
                            <input
                                style={{ textTransform : "lowercase" }}
                                type="email"
                                className={styles.input}
                                required
                                placeholder="ex: maria.jose@gmail.com"
                                autoComplete="new-password"
                                {...registerSignup("email", {
                                    validate : (value) =>
                                        validateEmail(value) || t("E-mail inválido"),
                                })}
                            />
                            {errorsSignup.email && (
                                <ErrorMessage message={errorsSignup.email?.message} />
                            )}
                        </div>

                        {/* {typePersonFormSelected === "F" && (
                        <div className={styles.formItem}>
                            <label>{t("CPF")}</label>
                            <InputMask
                                {...register("cpf", {
                                    validate : (value) =>
                                        validateCpf(value) || t("CPF inválido"),
                                })}
                                type='tel'
                                autoComplete='new-password'
                                mask='999.999.999-99'
                                className={styles.input}
                                placeholder='ex: 123.456.789-10'
                            />
                            {errors.cpf && (
                                <ErrorMessage message={errors.cpf?.message} />
                            )}
                        </div>
                    )}
            
                    {THEME_SETTING.registrationLoginTypeForm === "CPFCNPJ" && (
                        <div className={styles.buttonsTop}>
                            {typesPersonsForm?.map((typePerson: any, index: any) => (
                                <button
                                    key={index}
                                    onClick={() =>
                                        setTypePersonFormSelected(typePerson?.value)
                                    }
                                    className={`${
                                        typePerson?.value === typePersonFormSelected
                                            ? styles.buttonSelected
                                            : ""
                                    }`}
                                >
                                    {typePerson?.label}
                                </button>
                            ))}
                        </div>
                    )} */}

                        {/* <div className={styles.inputControl}>
                        <a onClick={() => setViewPasswordField(true)}>
                            Clique para fazer Login
                        </a>
                    </div> */}
                    </div>
                    <button
                        type="button"
                        disabled={loadingCart && true}
                        className={`block ${styles.submitted}`}
                        onClick={handleSubmitSignup(onSubmitSignup)}
                    >
                        {loadingCart ? <AnimatedLoading /> : t("Confirmar E-mail")}
                    </button>

                    <div className={styles.inputControl}>
                        <a
                            onClick={() => 
                            {
                                goBack();
                            }}
                        >
                            <span>Voltar</span>
                        </a>
                    </div>
                </form>
            )}
            {formType === "recoveryPassword" && (
                <form className={styles.contentForm}>
                    <div className={styles.formHeader}>
                        <div className={styles.title}>Recuperar Senha</div>
                        <div className={styles.subtitle}>
							Vamos enviar um e-mail com uma nova senha
                        </div>
                    </div>
                    <div className={styles.inputControl}>
                        <input
                            id="loginInput3"
                            style={{ textTransform : "lowercase" }}
                            type="email"
                            placeholder="Email"
                            {...registerRecoveryPassword("login", {
                                required : "Este campo é obrigatório!",
                                pattern  : {
                                    value   : /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                    message : "Email inválido",
                                },
                            })}
                        />
                        {isSubmittedRecoveryPassword && errorsRecoveryPassword.login && (
                            <ErrorMessage message={errorsRecoveryPassword.login.message} />
                        )}
                    </div>
                    <div className={styles.buttons}>
                        <button
                            type="button"
                            disabled={loadingCart && true}
                            className={`block ${styles.submitted}`}
                            onClick={handleSubmitRecoveryPassword(recoveryPassword)}
                        >
							Enviar
                        </button>
                    </div>
                    <div className={styles.inputControl}>
                        <a onClick={() => setFormType("login")}>Voltar</a>
                    </div>
                </form>
            )}
            {/* {formType === "loginWithToken" && (
                <div className={styles.contentForm}>
                    <div className={styles.title}>
                        Por favor informe o token que foi enviado para o seu email
                    </div>
                    <div className={styles.inputControl}>
                        <input
                            onChange={(e: any) => setTokenForLogin(e.target.value)}
                            onKeyUp={(e) =>
                                e.code === "Enter" && loginWithToken(tokenForLogin)
                            }
                            type='tel'
                            name='token'
                            placeholder='Digite seu token'
                        />
                    </div>
                    <div className={styles.buttons}>
                        <button
                            className={styles.submitted}
                            onClick={() => loginWithToken(tokenForLogin)}
                            type='button'
                        >
                            Entrar
                        </button>
                        <button
                            type='button'
                            onClick={() => 
                            {
                                tagManager4.registerEvent(
                                    "click-button",
                                    "lead",
                                    "Voltar",
                                    0,
                                    null
                                )
                                setFormType("login")
                            }}
                        >
                            Voltar
                        </button>
                    </div>
                </div>
            )} */}
            {/* {formType === "withPassword" && (
                <div className={styles.contentForm}>
                    <div className={styles.title}>
                        Por favor informe seu email e senha
                    </div>
                    <div className={styles.inputControl}>
                        <input
                            id='loginInput'
                            style={{ textTransform : "lowercase" }}
                            type='email'
                            placeholder='Email'
                            {...registerLogin("login", {
                                required : "Este campo é obrigatório!",
                                pattern  : {
                                    value   : /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                    message : "Email inválido",
                                },
                            })}
                        />
                        {errors.login && <ErrorMessage message={errors.login.message} />}
                    </div>
                    <div className={styles.inputControl}>
                        <div className={styles.inputPassword}>
                            <input
                                id='passwordInput'
                                {...registerLogin("password", {
                                    required  : "Este campo é obrigatório!",
                                    minLength : {
                                        value   : 8,
                                        message : "Sua senha possui, no mínimo, 8 caracteres.",
                                    },
                                })}
                                type={passwordType}
                                placeholder='Senha'
                            />
                            {passwordType === "password" ? (
                                <AiOutlineEye
                                    className={styles.iconPassword}
                                    onClick={() => setPasswordType("text")}
                                />
                            ) : (
                                <AiOutlineEyeInvisible
                                    className={styles.iconPassword}
                                    onClick={() => setPasswordType("password")}
                                />
                            )}
                        </div>
                        {errors.password && (
                            <ErrorMessage message={errors.password.message} />
                        )}
                    </div>
                    <div className={styles.inputControl}>
                        <a onClick={() => setFormType("recoveryPassword")}>
                            Esqueceu sua senha?
                        </a>
                    </div>
                    <div className={styles.buttons}>
                        <button
                            id='submitWithPasswordButton'
                            className={styles.submitted}
                            onClick={handleSubmitLogin(handleLoginWithEmailAndPassword)}
                            type='button'
                        >
                            {loadingCart ? <AnimatedLoading /> : "Entrar"}
                        </button>
                        <button
                            type='button'
                            onClick={() => 
                            {
                                tagManager4.registerEvent(
                                    "click-button",
                                    "lead",
                                    "Voltar",
                                    0,
                                    null
                                )
                                setFormType("login")
                            }}
                        >
                            Voltar
                        </button>
                    </div>
                </div>
            )} */}
        </div>
    );
};
