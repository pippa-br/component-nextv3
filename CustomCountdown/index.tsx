import { useTranslations } from "next-intl";
import Countdown from "react-countdown";
import styles from "./styles.module.scss";
interface CustomCountdownProps {
	date: string;
	phrasePix: any;
	timerColor?: string;
	subtitleColor?: string;
	itemBackgroundColor?: string;
	contentBackgroundColor?: string;
	dateFormte?: string;
	hideDays?: boolean;
	hideHours?: boolean;
}

export const CustomCountdown = ({
    date,
    phrasePix,
    timerColor = "#212121",
    subtitleColor = "#212121",
    itemBackgroundColor = "white",
    contentBackgroundColor = "white",
    hideDays = false,
    hideHours = false,
}: CustomCountdownProps) => 
{
    const dateSeconds = new Date(date);
    const t = useTranslations();

    const renderer = ({ days, hours, minutes, seconds, completed }: any) => 
    {
        if (completed) 
        {
            return (
                <div className={styles.timeExpired}>
                    <p className={styles.titleCountdown}>{phrasePix?.TimeoutExpired}</p>
                </div>
            );
        }

        return (
            <div className={styles.customCountdown}>
                <div
                    className={styles.countdownContent}
                    style={{
                        background : `${contentBackgroundColor}`,
                    }}
                >
                    {/* <p className={styles.titleCountdown}>PARABÉNS! <br /> Falta pouco para você receber seus produtos em casa! <br /> Pague em até 00:15 GANHE 5% OFF!</p> */}
                    <p className={styles.titleCountdown}>{phrasePix?.successPhrase}</p>
                    <div className={styles.numbersCountdown}>
                        <span
                            className={hideDays ? styles.hide : ""}
                            style={{
                                background : `${itemBackgroundColor}`,
                                color      : `${timerColor}`,
                            }}
                        >
                            {days}
                            <small
                                style={{
                                    color : `${subtitleColor}`,
                                }}
                            >
                                {t("Dias")}
                            </small>
                        </span>
                        {/* <div className={styles.separator} /> */}
                        <span
                            className={hideHours ? styles.hide : ""}
                            style={{
                                background : `${itemBackgroundColor}`,
                                color      : `${timerColor}`,
                            }}
                        >
                            {hours < 10 ? `0${hours}` : hours}{" "}
                            <small
                                style={{
                                    color : `${subtitleColor}`,
                                }}
                            >
                                {t("Horas")}
                            </small>
                        </span>
                        {/* <div className={styles.separator} /> */}
                        <span
                            style={{
                                background : `${itemBackgroundColor}`,
                                color      : `${timerColor}`,
                            }}
                        >
                            {minutes < 10 ? `0${minutes}` : minutes}{" "}
                            <small
                                style={{
                                    color : `${subtitleColor}`,
                                }}
                            >
                                {t("Minutos")}
                            </small>
                        </span>
                        {/* <div className={styles.separator} /> */}
                        <span
                            style={{
                                background : `${itemBackgroundColor}`,
                                color      : `${timerColor}`,
                            }}
                        >
                            {seconds < 10 ? `0${seconds}` : seconds}{" "}
                            <small
                                style={{
                                    color : `${subtitleColor}`,
                                }}
                            >
                                {t("Segundos")}
                            </small>
                        </span>
                    </div>
                </div>
            </div>
        );
    };

    return <Countdown date={dateSeconds} renderer={renderer} />;
};
