import { cloudflareLoader } from "@/core-nextv3/util/util";
import Image from "next/image";
import { useState } from "react";
import { ModalPackaging } from "../ModalPackaging";
import styles from "./styles.module.scss";

export default function ButtonPackaging({ packaging }: any) 
{
    const [ modalpackaging, setModalPackaging ] = useState(false);

    return (
        <>
            <button
                type="button"
                className={styles.openModal}
                onClick={() => setModalPackaging(true)}
            >
                <Image
                    width={20}
                    height={20}
                    src="/assets/icons/BsBag.svg"
                    loader={cloudflareLoader}
                    alt=""
                />
				Lembre-se de adicionar a embalagem
            </button>

            {modalpackaging && (
                <ModalPackaging packaging={packaging} onClose={setModalPackaging} />
            )}
        </>
    );
}
