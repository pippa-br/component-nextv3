import { useState } from "react";
import { innerHTML } from "../../core-nextv3/util/util";
import { ImageSet } from "../ImageSet";
import { ModalPackaging } from "./ModalPackaging";
import styles from "./styles.module.scss";

export default function Packaging({ packaging }: any) 
{
    const [ modalpackaging, setModalPackaging ] = useState(false);

    return (
        <>
            <div className={styles.packaging}>
                <div className={styles.content}>
                    <div className={styles.body}>
                        <h1>{packaging?.name}</h1>
                        <div className={styles.message}>
                            <ImageSet
                                width={20}
                                height={20}
                                src="/assets/icons/GiPresent.svg"
                                responsive={false}
                                alt=""
                            />
                            <div className={styles.informationsMessage}>
                                <div
                                    dangerouslySetInnerHTML={innerHTML(packaging?.description)}
                                    className={styles.description}
                                />
                                <button onClick={() => setModalPackaging(true)}>
									Adicionar embalagem
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {modalpackaging && (
                <ModalPackaging packaging={packaging} onClose={setModalPackaging} />
            )}
        </>
    );
}
