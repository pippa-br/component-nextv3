import { ImageSet } from "../ImageSet";
import { Itempackaging } from "../ItemPackaging";
import styles from "./styles.module.scss";

export const ModalPackaging = ({ onClose, packaging }: any) => 
{
    return (
        <div className={styles.modalPackaging} onClick={() => onClose(false)}>
            <div className={styles.container} onClick={(e) => e.stopPropagation()}>
                <div className={styles.top}>
                    <ImageSet
                        onClick={() => onClose(false)}
                        width={20}
                        height={20}
                        src="/assets/icons/GrFormClose.svg"
                        responsive={false}
                        alt=""
                    />
                </div>

                <div className={styles.body}>
                    {packaging?.packaging?.map((product: any, index: number) => (
                        <Itempackaging key={index} product={product} />
                    ))}
                </div>
            </div>
        </div>
    );
};
