import styles from "./styles.module.scss";

export function ShippingTable({ data }: any) 
{
    return (
        data && (
            <div className={styles.shippingTable}>
                <div className={styles.content}>
                    <p className={styles.message}>Tabela de medidas</p>
                    <table>
                        <thead>
                            <tr>
                                <th>Comprimento</th>
                                <th>Largura</th>
                                <th>Altura</th>
                                <th>Peso</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{data.length} cm</td>
                                <td>{data.width} cm</td>
                                <td>{data.height} cm</td>
                                <td>{data.weight / 1000} Kg</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    );
}
