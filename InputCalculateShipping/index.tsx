"use client";

import { Fragment, useEffect, useState } from "react";
import { toast } from "react-hot-toast";
import { PatternFormat } from "react-number-format";
import { shippingCheckoutAnalytics } from "../../core-nextv3/analytics/analytics.api";
import {
    calculateZipCodeCart,
    setShippingMethodCart,
} from "../../core-nextv3/cart/cart.api";
import { useCore } from "../../core-nextv3/core/core";
import { getAddDays } from "../../core-nextv3/shipping/shipping.api";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { currencyMask } from "../../core-nextv3/util/mask";
import { formatAddress } from "../../core-nextv3/util/util";
import { CART_SETTING, THEME_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import styles from "./styles.module.scss";

import { useTranslations } from "next-intl";
import { ImageSet } from "../ImageSet";

declare const window: any;

export const InputCalculateShipping = ({
    product = null,
    listVariant = null,
    totalItems = 0,
    title = "Calcule o frete e prazo de entrega",
    disabledDiff = false,
    hasSelected = true,
    disabledTitles = true,
}: any) => 
{
    const [ destination, setDestination ] = useState("");
    const [ loadingShipping, setLoadingShipping ] = useState(false);
    const [ selectShipping, setSelectShipping ] = useState<any>(null);
    const [ shippingMethods, setShippingMethods ] = useState([]);
    const [ populate, setPopulate ] = useState(false);
    const { setCart, cart, user } = useCore();
    const t = useTranslations();

    async function handleCalculateShipping() 
    {
        setPopulate(true);
        setLoadingShipping(true);

        let weight = 0;
        let addDays = 0;

        if (product) 
        {
            weight = product.weight;
        }

        if (product && listVariant) 
        {
            addDays = getAddDays(product, listVariant);
        }

        const result = await calculateZipCodeCart(
            CART_SETTING.merge({
                zipcode    : destination,
                weight     : weight,
                addDays    : addDays,
                totalItems : totalItems,
                quantity   : 1,
            }),
        );

        if (result.status) 
        {
            setCart(result.data);
        }

        setLoadingShipping(false);
    }

    const onSubmitCartShipping = async (data: any) => 
    {
        if (hasSelected) 
        {
            if (!data.id) 
            {
                return toast.error(t("Selecione uma das formas de entrega!"), {
                    duration : 2000,
                });
            }

            const newData = {
                data : data,
            };

            //setLoadingCart(true);

            const result = await setShippingMethodCart(CART_SETTING.merge(newData));

            //setLoadingCart(false);

            if (result?.status) 
            {
                const shippingData = {
                    key   : "shipping_tier",
                    value : result.data.shipping.id,
                };
                tagManager4.checkoutStep(result.data, "checkoutShipping", 2, user, [
                    shippingData,
                ]);

                // ADDRESS ANALYTICS
                shippingCheckoutAnalytics(data.name);

                setCart(result.data);
            }
            else 
            {
                return toast.error(
                    t("Verifique se foi selecionado um método válido."),
                    { duration : 2000 },
                );
            }
        }
    };

    useEffect(() => 
    {
        if (cart) 
        {
            if (cart.address) 
            {
                setDestination(cart.address.zipcode);
            }

            if (populate) 
            {
                setSelectShipping(cart.shipping);
                setShippingMethods(cart?.shippingMethods?.full || []);
                //setFreeShipping(cartLocal?.freeShipping)
            }
        }
    }, [ cart ]);

    return (
        <div className={styles.inputShipping}>
            {!disabledTitles && (
            // biome-ignore lint/complexity/noUselessFragments: <explanation>
                <Fragment>{title && <p className={styles.title}>{title}</p>}</Fragment>
            )}

            <div>
                <div className={styles.control}>
                    <PatternFormat
                        format="#####-###"
                        allowEmptyFormatting={false}
                        type="tel"
                        value={destination}
                        onValueChange={(values: any) =>
                            setDestination(values.formattedValue)
                        }
                        placeholder={"CEP 00000-000"}
                    />
                    <button
                        type="button"
                        disabled={loadingShipping}
                        onClick={() => handleCalculateShipping()}
                        className="block"
                    >
                        {loadingShipping ? <AnimatedLoading /> : t("Consultar")}
                    </button>
                </div>
                {cart?.address && (
                    <p className={styles.address}>{formatAddress(cart?.address)}</p>
                )}
                {!cart?.address && (
                    <p
                        className={styles.address}
                        onClick={() =>
                            window.open(
                                "https://buscacepinter.correios.com.br/app/endereco/index.php",
                            )
                        }
                    >
                        {t("Não sei meu CEP")}
                    </p>
                )}
            </div>

            {THEME_SETTING.noteShipping && (
                <p className={styles.message}>{THEME_SETTING.noteShipping}</p>
            )}

            {!disabledDiff && cart?.freeShipping?.status && (
                <section className={styles.freeShipping}>
                    <div className={styles.content}>
                        {cart?.freeShipping?.diff !== -1 &&
							(cart?.freeShipping?.diff > 0 ? (
							    <>
									Falta {currencyMask(cart?.freeShipping?.diff)} para FRETE
									GRÁTIS!
							        <ImageSet
							            width={20}
							            height={20}
							            src="/assets/icons/FaTruck.svg"
							            responsive={false}
							            alt=""
							        />
							    </>
							) : (
							    <>
									Comprando mais esse produto você ganha FRETE GRÁTIS!{" "}
							        <ImageSet
							            width={20}
							            height={20}
							            src="/assets/icons/FaTruck.svg"
							            responsive={false}
							            alt=""
							        />
							    </>
							))}
                        {cart?.freeShipping?.diffQuantity !== -1 &&
							(cart?.freeShipping?.diffQuantity > 0 ? (
							    <>
									Falta {cart?.freeShipping?.diffQuantity} produto(s) para FRETE
									GRÁTIS!
							        <ImageSet
							            width={20}
							            height={20}
							            src="/assets/icons/FaTruck.svg"
							            responsive={false}
							            alt=""
							        />
							    </>
							) : (
							    <>
									Comprando mais esse produto você ganha FRETE GRÁTIS!{" "}
							        <ImageSet
							            width={20}
							            height={20}
							            src="/assets/icons/FaTruck.svg"
							            responsive={false}
							            alt=""
							        />
							    </>
							))}
                    </div>
                </section>
            )}

            <div className={styles.shippingMethods}>
                {shippingMethods?.length >= 1 &&
					shippingMethods?.map((shippingMethod: any) => (
					    <div
					        className={`${styles.shipping} ${styles[shippingMethod.id]} ${styles[selectShipping?.id === shippingMethod?.id ? "active" : ""]}`}
					        key={shippingMethod.id}
					        onClick={() => 
					        {
					            onSubmitCartShipping(shippingMethod);
					        }}
					    >
					        <p>
					            {hasSelected && (
					                <span>
					                    <ImageSet
					                        className={styles.noCheck}
					                        width={20}
					                        height={20}
					                        src="/assets/icons/BsCircle.svg"
					                        responsive={false}
					                        alt=""
					                    />
					                    <ImageSet
					                        className={styles.check}
					                        width={20}
					                        height={20}
					                        src="/assets/icons/AiOutlineCheckCircle.svg"
					                        responsive={false}
					                        alt=""
					                    />
					                </span>
					            )}
					            {shippingMethod?.label}
					        </p>
					        {shippingMethod?.note && <small>{shippingMethod?.note}</small>}
					    </div>
					))}
            </div>
        </div>
    );
};
