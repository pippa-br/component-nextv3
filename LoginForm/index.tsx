import {
    cloudflareLoader,
    getRecaptcha,
    getUrlSite,
    isInAppBrowser,
} from "@/core-nextv3/util/util";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { userLoginAnalytics } from "../../core-nextv3/analytics/analytics.api";
import {
    getTokenLoginAuth,
    loginAuth,
    loginTokenAuth,
    recoveryPasswordAuth,
} from "../../core-nextv3/auth/auth.api";
import { mergeCart } from "../../core-nextv3/cart/cart.api";
import { useCore } from "../../core-nextv3/core/core";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { AUTH_SETTING, CART_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import ErrorMessage from "../ErrorMessage";
import styles from "./styles.module.scss";

declare const window: any;

export default function LoginForm({ noLogin = false }) 
{
    const { query, push } = useRouter();

    const {
        register,
        setValue,
        formState: { errors },
        handleSubmit,
    } = useForm();

    const { cart } = useCore();
    const [ animateLoading, setAnimateLoading ] = useState(false);
    const [ formType, setFormType ] = useState("none");
    const [ emailForToken, setEmailForToken ] = useState("");
    const [ tokenForLogin, setTokenForLogin ] = useState("");
    const [ passwordType, setPasswordType ] = useState("password");

    // AUTO LOGIN VIA URL
    useEffect(() => 
    {
        if (query.email && query.token) 
        {
            handleLoginWithEmailAndPassword({
                login    : query.email,
                password : query.token,
                redirect : "/perfil/detalhes-da-conta/",
            });
        }
    }, [ query ]);

    async function getEmailToken() 
    {
        const result = await getTokenLoginAuth(
            AUTH_SETTING.merge({
                email : emailForToken.toLowerCase(),
            }),
        );

        if (!result.status) 
        {
            return toast.error("Erro! Verifique dos campos digitados.", {
                duration : 2000,
            });
        }

        toast(
            (t: any) => (
                <span>
                    {result.message}
                    <button onClick={() => toast.dismiss(t.id)}>Ok</button>
                </span>
            ),
            {
                className : "toast-custom",
                duration  : 999999,
            },
        );

        setFormType("loginWithToken");
    }

    async function recoveryPassword() 
    {
        const result = await recoveryPasswordAuth(
            AUTH_SETTING.merge({
                login : emailForToken.toLowerCase(),
            }),
        );

        if (!result.status) 
        {
            return toast.error(result.error, { duration : 2000 });
        }

        toast(
            (t: any) => (
                <span>
                    {result.message}
                    <button onClick={() => toast.dismiss(t.id)}>Ok</button>
                </span>
            ),
            {
                className : "toast-custom",
                duration  : 999999,
            },
        );

        setValue("login", emailForToken);
        setFormType("withPassword");
    }

    async function loginWithToken() 
    {
        const result = await loginTokenAuth(
            AUTH_SETTING.merge({
                token : tokenForLogin,
            }),
        );

        if (!result.status) 
        {
            return toast.error("Erro! Verifique dos campos digitados.", {
                duration : 2000,
            });
        }

        // MERGE CART SESSION
        await mergeCart(
            CART_SETTING.merge({
                document : {
                    referencePath : cart?.referencePath,
                },
            }),
        );

        tagManager4.signUp("token");
        const toastInstance: any = toast("Login feito com sucesso!", {
            icon : "👏",
        });
        setTimeout(() => toast.dismiss(toastInstance.id), 2000);

        // LOGIN ANALYTICS
        userLoginAnalytics(result.data);

        push("/carrinho");
    }

    async function handleLoginWithEmailAndPassword(formData: any) 
    {
        const token = await getRecaptcha("login");

        if (token) 
        {
            setAnimateLoading(true);

            const result = await loginAuth(
                AUTH_SETTING.merge({
                    token    : token,
                    login    : formData.login.toLowerCase(),
                    password : formData.password,
                }),
            );

            // MERGE CART SESSION
            await mergeCart(
                CART_SETTING.merge({
                    document : {
                        referencePath : cart?.referencePath,
                    },
                }),
            );

            setAnimateLoading(false);

            if (!result.status) 
            {
                return toast.error(result.error || "Erro ao fazer login.", {
                    duration : 2000,
                });
            }

            const toastInstance: any = toast("Login feito com sucesso!", {
                icon     : "👏",
                duration : 2000,
            });
            setTimeout(() => toast.dismiss(toastInstance.id), 2000);

            // LOGIN ANALYTICS
            userLoginAnalytics(result.data);

            if (formData.redirect) 
            {
                push(formData.redirect);
            }
            else 
            {
                push("/carrinho");
            }
        }
    }

    const checkBrowser = () => 
    {
        if (isInAppBrowser()) 
        {
            window.open(`${getUrlSite()}/cadastro/`, "_blank");
        }
        else 
        {
            push("/cadastro/");
        }
    };

    return (
        <div className={styles.container}>
            <div className={styles.content}>
                {formType === "none" && (
                    <div className={styles.contentButtons}>
                        <div className={styles.buttons}>
                            <div className={styles.right}>
                                <h1>Faça Login</h1>
                                {/* <div className={styles.title}>Use uma das opções para confirmar sua identidade</div> */}
                                <div className={styles.title}>
									Use uma das opções para fazer login
                                </div>
                                <button
                                    type="button"
                                    onClick={() => 
                                    {
                                        tagManager4.registerEvent(
                                            "click-button",
                                            "lead",
                                            "Entrar apenas com email",
                                            0,
                                            null,
                                        );
                                        setFormType("onlyEmail");
                                    }}
                                >
									Receber chave de acesso rápido por email
                                </button>
                                {!noLogin && (
                                    <button
                                        id="withPasswordButton"
                                        type="button"
                                        onClick={() => 
                                        {
                                            tagManager4.registerEvent(
                                                "click-button",
                                                "lead",
                                                "Entrar apenas com email",
                                                0,
                                                null,
                                            );
                                            setFormType("withPassword");
                                        }}
                                    >
										Entrar com email e senha
                                    </button>
                                )}
                                <div className={styles.inputControl}>
                                    <a onClick={() => setFormType("recoveryPassword")}>
										Esqueceu sua senha?
                                    </a>
                                </div>
                            </div>
                            <div className={styles.left}>
                                <h1>Crie Sua Conta</h1>
                                {/* <div className={styles.title}>Crie sua conta</div> */}
                                {!noLogin && (
                                    <button
                                        className={styles.submitted}
                                        type="button"
                                        onClick={() => checkBrowser()}
                                    >
										Criar Conta
                                    </button>
                                )}
                            </div>
                        </div>
                    </div>
                )}

                {formType === "onlyEmail" && (
                    <div className={styles.contentForm}>
                        <div className={styles.title}>Por favor informe seu email</div>
                        <div className={styles.inputControl}>
                            <input
                                style={{ textTransform : "lowercase" }}
                                onChange={(e: any) => setEmailForToken(e.target.value)}
                                type="email"
                                name="email"
                                placeholder="Digite o seu email"
                                onKeyUp={(e) => 
                                {
                                    if (e.code === "Enter") 
                                    {
                                        setFormType("loginWithToken");
                                        getEmailToken();
                                    }
                                }}
                            />
                            {errors.email && <ErrorMessage message={errors.email.message} />}
                        </div>
                        <div className={styles.buttons}>
                            <button
                                className={styles.submitted}
                                onClick={() => 
                                {
                                    tagManager4.registerEvent(
                                        "click-button",
                                        "lead",
                                        "Entrar",
                                        0,
                                        null,
                                    );
                                    getEmailToken();
                                }}
                                type="button"
                            >
								Entrar
                            </button>
                            <button
                                type="button"
                                onClick={() => 
                                {
                                    tagManager4.registerEvent(
                                        "click-button",
                                        "lead",
                                        "Voltar",
                                        0,
                                        null,
                                    );
                                    setFormType("none");
                                }}
                            >
								Voltar
                            </button>
                        </div>
                    </div>
                )}

                {formType === "recoveryPassword" && (
                    <div className={styles.contentForm}>
                        <div className={styles.title}>Por favor informe seu email</div>
                        <div className={styles.inputControl}>
                            <input
                                style={{ textTransform : "lowercase" }}
                                onChange={(e: any) => setEmailForToken(e.target.value)}
                                type="email"
                                name="email"
                                placeholder="Digite o seu email"
                                onKeyUp={(e) => 
                                {
                                    if (e.code === "Enter") 
                                    {
                                        setFormType("withPassword");
                                        recoveryPassword();
                                    }
                                }}
                            />
                            {errors.email && <ErrorMessage message={errors.email.message} />}
                        </div>
                        <div className={styles.buttons}>
                            <button
                                className={styles.submitted}
                                onClick={() => 
                                {
                                    tagManager4.registerEvent(
                                        "click-button",
                                        "lead",
                                        "Entrar",
                                        0,
                                        null,
                                    );
                                    recoveryPassword();
                                }}
                                type="button"
                            >
								Entrar
                            </button>
                            <button
                                type="button"
                                onClick={() => 
                                {
                                    tagManager4.registerEvent(
                                        "click-button",
                                        "lead",
                                        "Voltar",
                                        0,
                                        null,
                                    );
                                    setFormType("none");
                                }}
                            >
								Voltar
                            </button>
                        </div>
                    </div>
                )}

                {formType === "loginWithToken" && (
                    <div className={styles.contentForm}>
                        <div className={styles.title}>
							Por favor informe o token que foi enviado para o seu email
                        </div>
                        <div className={styles.inputControl}>
                            <input
                                onChange={(e: any) => setTokenForLogin(e.target.value)}
                                onKeyUp={(e) => e.code === "Enter" && loginWithToken()}
                                type="tel"
                                name="token"
                                placeholder="Digite seu token"
                            />
                        </div>
                        <div className={styles.buttons}>
                            <button
                                className={styles.submitted}
                                onClick={() => loginWithToken()}
                                type="button"
                            >
								entrar
                            </button>
                            <button type="button" onClick={() => setFormType("none")}>
								voltar
                            </button>
                        </div>
                    </div>
                )}

                {formType === "withPassword" && (
                    <div className={styles.contentForm}>
                        <div className={styles.title}>
							Por favor informe seu email e senha
                        </div>
                        <div className={styles.inputControl}>
                            <input
                                id="loginInput"
                                style={{ textTransform : "lowercase" }}
                                type="email"
                                placeholder="Email"
                                {...register("login", {
                                    required : "Este campo é obrigatório!",
                                    pattern  : {
                                        value   : /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                        message : "Email inválido",
                                    },
                                })}
                            />
                            {errors.login && <ErrorMessage message={errors.login.message} />}
                        </div>
                        <div className={styles.inputControl}>
                            <div className={styles.inputPassword}>
                                <input
                                    id="passwordInput"
                                    {...register("password", {
                                        required  : "Este campo é obrigatório!",
                                        minLength : {
                                            value   : 8,
                                            message : "Sua senha possui, no mínimo, 8 caracteres.",
                                        },
                                    })}
                                    type={passwordType}
                                    placeholder="Senha"
                                />
                                {passwordType === "password" ? (
                                    <Image
                                        className={styles.iconPassword}
                                        onClick={() => setPasswordType("text")}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/AiOutlineEye.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />
                                ) : (
                                    <Image
                                        className={styles.iconPassword}
                                        onClick={() => setPasswordType("password")}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/AiOutlineEyeInvisible.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />
                                )}
                            </div>
                            {errors.password && (
                                <ErrorMessage message={errors.password.message} />
                            )}
                        </div>
                        <div className={styles.inputControl}>
                            <a onClick={() => setFormType("recoveryPassword")}>
								Esqueceu sua senha?
                            </a>
                        </div>
                        <div className={styles.buttons}>
                            <button
                                id="submitWithPasswordButton"
                                className={styles.submitted}
                                onClick={handleSubmit(handleLoginWithEmailAndPassword)}
                                type="button"
                            >
                                {animateLoading ? <AnimatedLoading /> : "Entrar"}
                            </button>
                            <button type="button" onClick={() => setFormType("none")}>
								voltar
                            </button>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
}
