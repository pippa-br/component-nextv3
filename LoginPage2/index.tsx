import { Suspense } from "react";
import { PageTitle } from "../../component-nextv3/PageTitle";
import LoginForm2 from "../LoginForm2";
import styles from "./styles.module.scss";

const GenerateMetadata = async () => 
{
    return {
        title : "Login",
    };
};

const LoginPage2 = () => 
{
    return (
        <div className={`${styles.loginPage2} page`}>
            <div className={styles.content}>
                <PageTitle name={"Login"} />
                <Suspense>
                    <LoginForm2 />
                </Suspense>
            </div>
        </div>
    );
};

// const getStaticProps: GetStaticProps = () =>
//   withHeader(async (props: any) => {
//     return {
//       revalidate: THEME_SETTING.revalidate,
//       props: {
//         seo: props.seo.merge({ title: 'Login' }),
//       },
//     }
//   })

export { LoginPage2, GenerateMetadata };
