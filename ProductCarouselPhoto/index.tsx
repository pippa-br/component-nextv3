"use client";

import { cloudflareLoader } from "@/core-nextv3/util/util";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import type { Options } from "@splidejs/splide";
import { AnimatePresence } from "framer-motion";
import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import { ImageSet } from "../../component-nextv3/ImageSet";
import { useResponsive } from "../../core-nextv3/util/useResponsive";
import { THEME_SETTING } from "../../setting/setting";
import { ProductItemSkeleton } from "../ProductItemSkeleton";
import { ProductModalCarouselPhoto } from "../ProductModalCarouselPhoto";
import { VideosSet } from "../VideoSet/indx";
import styles from "./styles.module.scss";

export const ProductCarouselPhoto = ({
    images,
    videos,
    sizes = "100vw",
    width = 1920,
    height = 400,
    enabledGif = false,
    vertical = false,
    perPage = 1,
    thumbsperPage = 4,
    thumbsCss = {},
    thumbsArrow = true,
    arrows = true,
}: any) => 
{
    const { isDesktop } = useResponsive();
    const [ currentSlide, setCurrentSlide ] = useState(0);
    const [ openModalCarouselPhoto, setOpenModalCarouselPhoto ] = useState(false);

    const mainRef = useRef<any>(undefined);
    const thumbRef = useRef<any>(undefined);

    const mainOptions: Options = {
        perPage    : images.length > 1 ? perPage : 1,
        perMove    : images.length > 1 ? perPage : 1,
        gap        : "1rem",
        pagination : true,
        slideFocus : true,
        arrows     : arrows,
        autoHeight : true,
        start      : 0,
    };

    const thumbsOptions: Options = {
        gap          : "1rem",
        perPage      : thumbsperPage * (images.length > 1 ? perPage : 1),
        pagination   : true,
        padding      : "6rem",
        cover        : true,
        isNavigation : true,
        arrows       : thumbsArrow,
        start        : 0,
        //autoHeight   : !vertical,
        height       : vertical
            ? "100vh"
            : `calc(${((150 / thumbsperPage) * perPage) / (width / height)}px + ${0.875 * perPage}rem)`,
        direction : vertical ? "ttb" : "ltr",
    };

    if (vertical) 
    {
        // thumbsOptions.fixedWidth  = 150;
        // thumbsOptions.fixedHeight = 150 / (width/height);
        thumbsOptions.padding = "0rem";
    }

    useEffect(() => 
    {
        if (mainRef.current && thumbRef.current && thumbRef.current.splide) 
        {
            mainRef.current.sync(thumbRef.current.splide);

            const current: any = mainRef.current;

            current.splide.off("active");
            current.splide.on("active", (e: any) => 
            {
                setCurrentSlide(e.index - (videos ? videos.length : 0));

                if (e.slide.parentElement) 
                {
                    const divs = e.slide.getElementsByTagName("div");

                    if (divs.length > 0) 
                    {
                        const data = divs[0].getBoundingClientRect();
                        e.slide.parentElement.style.height = `${data.height}px`;
                    }
                }
            });
        }
    });

    return images.length > 0 ? (
        <>
            <div
                className={`${styles.productCarouselPhoto} ${vertical ? styles.vertical : ""} ${styles[`perPage-${perPage}`]}`}
            >
                {/* <div className={`${THEME_SETTING.carouselImagesSticky ? styles.carouselSticky : styles.productCarouselPhoto + ' ' + (vertical ? styles.vertical : '')}`}> */}

                <Splide
                    options={mainOptions}
                    aria-labelledby=""
                    ref={mainRef}
                    className={styles.slideContent}
                >
                    {videos?.map((video: any, index: any) => (
                        <SplideSlide key={index} className={styles.slideItem}>
                            <VideosSet
                                //aspectRatio={aspectRatio}
                                video={video}
                                width={width}
                                height={height}
                                autoplay={true}
                                loop={true}
                                controls={false}
                            />
                        </SplideSlide>
                    ))}
                    {images?.map((image: any, index: any) => (
                        <SplideSlide
                            className={styles.slideItem}
                            key={index}
                            onClick={() => 
                            {
                                setOpenModalCarouselPhoto(true);
                            }}
                        >
                            <ImageSet //aspectRatio={aspectRatio}
                                image={image}
                                //   domainFrom={THEME_SETTING.domainFrom}
                                //   domainTo={THEME_SETTING.domainTo}
                                width={width}
                                height={height}
                                sizes={sizes}
                                enabledGif={enabledGif}
                            />
                        </SplideSlide>
                    ))}
                </Splide>

                {THEME_SETTING?.disabledCarouselPhotosSmall ? (
                    <>
                        {isDesktop && (
                            <Splide
                                options={thumbsOptions}
                                aria-label=""
                                ref={thumbRef}
                                className={styles.thumbContent}
                                style={thumbsCss}
                            >
                                {videos?.map((video: any, index: any) => (
                                    <SplideSlide key={index}>
                                        <VideosSet
                                            //aspectRatio={aspectRatio}
                                            video={video}
                                            width={width}
                                            height={height}
                                            field="thumb"
                                            autoplay={true}
                                            loop={true}
                                            controls={false}
                                        />
                                    </SplideSlide>
                                ))}
                                {images?.map((image: any, index: any) => (
                                    <SplideSlide key={index}>
                                        <ImageSet
                                            //aspectRatio={aspectRatio}
                                            // domainFrom={THEME_SETTING.domainFrom}
                                            // domainTo={THEME_SETTING.domainTo}
                                            width={width}
                                            height={height}
                                            sizes={sizes}
                                            image={image}
                                        />
                                    </SplideSlide>
                                ))}
                            </Splide>
                        )}
                    </>
                ) : (
                    <Splide
                        options={thumbsOptions}
                        aria-label=""
                        ref={thumbRef}
                        className={styles.thumbContent}
                        style={thumbsCss}
                    >
                        {videos?.map((video: any, index: any) => (
                            <SplideSlide key={index}>
                                <VideosSet
                                    //aspectRatio={aspectRatio}
                                    video={video}
                                    width={width}
                                    height={height}
                                    field="thumb"
                                    autoplay={true}
                                    loop={true}
                                    controls={false}
                                />
                            </SplideSlide>
                        ))}
                        {images?.map((image: any, index: any) => (
                            <SplideSlide key={index}>
                                <ImageSet
                                    //aspectRatio={aspectRatio}
                                    // domainFrom={THEME_SETTING.domainFrom}
                                    // domainTo={THEME_SETTING.domainTo}
                                    image={image}
                                    width={width}
                                    height={height}
                                    sizes={sizes}
                                />
                            </SplideSlide>
                        ))}
                    </Splide>
                )}

                <Image
                    className={styles.carouselSvg}
                    width={20}
                    height={20}
                    src="/assets/icons/BiSearch.svg"
                    loader={cloudflareLoader}
                    alt=""
                />
            </div>

            <AnimatePresence initial={false} mode="wait" onExitComplete={() => null}>
                {openModalCarouselPhoto && (
                    <ProductModalCarouselPhoto
                        //aspectRatio={aspectRatio}
                        startIndex={currentSlide}
                        width={width}
                        height={height}
                        images={images}
                        setOpenModalCarouselPhoto={setOpenModalCarouselPhoto}
                    />
                )}
            </AnimatePresence>
        </>
    ) : (
        <ProductItemSkeleton />
    );
};
