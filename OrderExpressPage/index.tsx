import { useProtectedAuth } from "@/core-nextv3/auth/user.auth.api";
import type { GetStaticProps } from "next";
import { useTranslations } from "next-intl";
import { useRouter } from "next/router";
import { useEffect } from "react";
import toast from "react-hot-toast";
import { paymentMethodCheckoutAnalytics } from "../../core-nextv3/analytics/analytics.api";
import {
    setInstallmentCart,
    setPaymentMethodCart,
} from "../../core-nextv3/cart/cart.api";
import { useCore } from "../../core-nextv3/core/core";
import {
    collectionDocument,
    getDocument,
} from "../../core-nextv3/document/document.api";
import { getInstallmentsPagarme } from "../../core-nextv3/pagarme/pagarme.api";
import { getTrackCorreios } from "../../core-nextv3/shipping/shipping.api";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { calls } from "../../core-nextv3/util/call.api";
import { innerHTML, updateQueryString } from "../../core-nextv3/util/util";
import {
    AUTH_SETTING,
    CART_SETTING,
    COUNTDOWN_PAGE_SETTING,
    GATEWAY_SETTING,
    ORDER_EXPRESS_SETTING,
    ORDER_PAGE_SETTING,
    PAYMENT_METHODS_OPTION_SETTING,
    STATUS_BAR_PAGE_SETTING,
    TRACK_SETTING,
} from "../../setting/setting";
import withHeader from "../../utils/withHeader";
import { LoadingPage } from "../LoadingPage";
import { OrderViewer } from "../OrderViewer";
import { PageTitle } from "../PageTitle";
import styles from "./styles.module.scss";

const OrderExpressPage = ({
    order,
    account,
    orderTrack,
    paymentMethods,
    installments,
    countDownPage,
    stausBarPage,
    orderPage,
}: any) => 
{
    const { query, isReady } = useRouter();
    const { cart, setCart, user } = useCore();
    const t = useTranslations();

    const onSubmitCartPaymentMethod = async (data: any) => 
    {
        const newData = {
            data : data,
        };

        //setLoadingCart(true);

        const result = await setPaymentMethodCart(CART_SETTING.merge(newData));

        //setLoadingCart(false);

        if (result.data.paymentMethod) 
        {
            // PAYMENT ANALYTICS
            paymentMethodCheckoutAnalytics(data.label);

            setCart(result.data);
        }
        else 
        {
            toast.error(
                t("Ocorreu um erro! Verifique se foi selecionado um método válido."),
                { duration : 2000 },
            );
        }
    };

    async function handleCreditCardInstallment(installment: any) 
    {
        const newData = {
            data : installment,
        };

        //setLoadingCart(true);

        const result = await setInstallmentCart(CART_SETTING.merge(newData));

        //setLoadingCart(false);

        if (result.status) 
        {
            setCart(result.data);
        }
        else 
        {
            toast.error(
                t(
                    "Ocorreu um erro! Verifique se foi selecionado um parcelamento válido.",
                    { duration : 2000 },
                ),
            );
        }
    }

    useEffect(() => 
    {
        if (isReady && query.success) 
        {
            tagManager4.purchase(order, user);
            updateQueryString("success", "");
        }
    }, [ isReady, query ]);

    const loadProtectedAuth = useProtectedAuth(AUTH_SETTING, "", "/login");

    if (!loadProtectedAuth) 
    {
        return <LoadingPage />;
    }

    return (
        <>
            <div className={`${styles.orderPage} orderPage`}>
                <div className={styles.content}>
                    <PageTitle
                        parents={[ { url : "/perfil", name : t("Meus Pedidos") } ]}
                        name={"Pedido Express"}
                        noTitle={true}
                    />

                    {orderPage?.content && (
                        <div
                            className={styles.pageContent}
                            dangerouslySetInnerHTML={innerHTML(orderPage.content)}
                        />
                    )}

                    <OrderViewer
                        order={order}
                        account={account}
                        countDownPage={countDownPage}
                        stausBarPage={stausBarPage}
                        orderTrack={orderTrack}
                        paymentMethods={paymentMethods}
                        userCart={cart}
                        onSubmitPayment={onSubmitCartPaymentMethod}
                        redirectUrl="/pedidos-express/"
                        reorderUrl="/troca-express/"
                        avaliationUrl="/avaliar-express/"
                        onCreditCardInstallment={handleCreditCardInstallment}
                        installments={installments}
                    />
                </div>
            </div>
        </>
    );
};

const getStaticProps: GetStaticProps = ({ params }) =>
    withHeader(async (props: any) => 
    {
        const [ order ] = await calls(
            getDocument(
                ORDER_EXPRESS_SETTING.merge({
                    id : params?.id,
                }),
            ),
        );

        if (!order.status) 
        {
            return {
                notFound   : !order.serverError,
                revalidate : true,
            };
        }

        const promises = [];

        if (order.data?.trackingCode) 
        {
            promises.push(
                getTrackCorreios(
                    TRACK_SETTING.merge({
                        code : order.data?.trackingCode,
                    }),
                ),
            );
        }
        else 
        {
            promises.push({});
        }

        const setting = GATEWAY_SETTING.merge({
            subTotal      : order.data.totalItems,
            totalDiscount : order.data.totalDiscount,
            totalShipping : order.data.totalShipping,
        });

        promises.push(getInstallmentsPagarme(setting));
        promises.push(getDocument(PAYMENT_METHODS_OPTION_SETTING));
        promises.push(getDocument(COUNTDOWN_PAGE_SETTING));
        promises.push(getDocument(STATUS_BAR_PAGE_SETTING));
        promises.push(getDocument(ORDER_PAGE_SETTING));

        const [
            orderTrack,
            installments,
            paymentMethods,
            phrasePixResult,
            trackBackResult,
            orderPage,
        ] = await calls(...promises);

        return {
            revalidate : 1,
            props      : {
                seo            : props?.seo?.merge({ title : "Pedido" }) || {},
                order          : order.data,
                countDownPage  : phrasePixResult?.data || {},
                orderTrack     : orderTrack?.data || {},
                paymentMethods : paymentMethods?.data?.items || [],
                installments   : installments?.collection || [],
                stausBarPage   : trackBackResult?.data || {},
                orderPage      : orderPage?.data || {},
            },
        };
    });

const getStaticPaths = async () => 
{
    const order = await collectionDocument(
        ORDER_EXPRESS_SETTING.merge({
            perPage : 1,
        }),
    );

    let paths = [];

    if (order.collection) 
    {
        paths = order.collection.map((item: any) => ({
            params : { id : item.id },
        }));
    }

    return { paths, fallback : "blocking" };
};

export {
    getStaticProps as GetStaticProps,
    getStaticPaths as GetStaticPaths,
    OrderExpressPage,
};
