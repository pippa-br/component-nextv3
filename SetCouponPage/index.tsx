import {
    collectionDocument,
    getDocument,
} from "../../core-nextv3/document/document.api";
import { COUPON_SETTING } from "../../setting/setting";
import { CommentsDisplay } from "./comments.display";

const GenerateStaticParams = async () => 
{
    const couponResult = await collectionDocument(
        COUPON_SETTING.merge({
            where : [
                {
                    field    : "status",
                    operator : "==",
                    value    : true,
                },
            ],
        }),
    );

    let paths = [];

    if (couponResult.collection) 
    {
        paths = couponResult.collection.map((item: any) => ({
            coupon : item._code,
        }));
    }

    console.info("(comments Static:", paths.length, ")");

    return paths;
};

const getCollectionServer = async (coupon: string) => 
{
    const couponResult = await getDocument(
        COUPON_SETTING.merge({
            slug : coupon.toUpperCase().replace(/\s/g, ""),
        }),
    );

    return {
        cupomData : couponResult.data,
    };
};

const SetCouponPage = async ({ params }: any) => 
{
    const { coupon } = await params;
    const { cupomData } = await getCollectionServer(coupon);

    return <CommentsDisplay coupon={cupomData} />;
};

export { SetCouponPage, GenerateStaticParams };
