"use client";

import { useSetCouponCart } from "@/core-nextv3/cart/cart.use";
import { cloudflareLoader, isHttps } from "@/core-nextv3/util/util";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { CART_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import styles from "./styles.module.scss";

declare const window: any;

const CommentsDisplay = ({ coupon }: any) => 
{
    const router = useRouter();

    //console.error('coupon', coupon)

    useSetCouponCart(
        CART_SETTING.merge({
            _code : coupon?._code,
        }),
        () => 
        {
            setTimeout(() => 
            {
                if (coupon.redirectUrl && isHttps(coupon.redirectUrl)) 
                {
                    window.location.href = coupon.redirectUrl;
                }
                else 
                {
                    router.push("/");
                }
            }, 1000);
        },
    );

    return (
        <div className={styles.mergeCartPage}>
            <div className={styles.content}>
                <Image
                    width={20}
                    height={20}
                    src="/assets/icons/RiCouponLine.svg"
                    loader={cloudflareLoader}
                    alt=""
                />
                <p>
					Cupom {coupon?._code} {coupon?._percentage}% <br />
					Adicionado com sucesso! Boas Compras!
                </p>
                <br />
                <br />
                <AnimatedLoading />
            </div>
        </div>
    );
};

export { CommentsDisplay };
