import { useCore } from "@/core-nextv3/core/core";
import { cloudflareLoader } from "@/core-nextv3/util/util";
import { useTranslations } from "next-intl";
import Image from "next/image";
import { useState } from "react";
import { THEME_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import styles from "./styles.module.scss";

export const CheckoutShipping = ({ goBack, loadingCart, onSubmit }: any) => 
{
    const { cart } = useCore();

    const [ selectedShippingMethod, setSelectedShippingMethod ] = useState(
        cart?.shipping || {
            id    : "",
            label : "",
            type  : { value : "" },
        },
    );

    const [ setSelectedShippingRetirada ] = useState(
        cart?.shipping || {
            id    : "",
            label : "",
            type  : { value : "" },
        },
    );

    const [ selectedFinalShipping, setSelectedFinalShipping ] = useState(
        cart?.shipping || {
            id    : "",
            label : "",
            type  : { value : "" },
        },
    );

    const shippingTransportadora = cart?.shippingMethods?.full?.filter(
        (shipping: any) => shipping?.type?.value === "transportadora",
    )[0];

    const [ onibus, setOnibus ] = useState({});
    const t = useTranslations();

    return (
        <div className={styles.checkoutShipping}>
            <>
                <div className={styles.formHeader}>
                    <p className={styles.title}>{t("Método de entrega")}</p>
                    <p className={styles.subtitle}>
                        {t("Selecione a opção de entrega ideal para você")}
                    </p>
                </div>

                {THEME_SETTING.noteShipping && (
                    <p className={styles.message}>{THEME_SETTING.noteShipping}</p>
                )}

                <div className={styles.shippingMethods}>
                    {cart?.shippingMethods?.full?.map((shipping: any, index: any) => (
                        <p
                            key={index}
                            onClick={() => 
                            {
                                setSelectedFinalShipping(shipping);
                                setSelectedShippingMethod(shipping);
                            }}
                            className={
                                shipping?.id === selectedShippingMethod?.id
                                    ? `${styles.shippingOption} ${styles.active}`
                                    : styles.shippingOption
                            }
                        >
                            <span>
                                <Image
                                    className={styles.noCheck}
                                    width={20}
                                    height={20}
                                    src="/assets/icons/BsCircle.svg"
                                    loader={cloudflareLoader}
                                    alt=""
                                />
                                <Image
                                    className={styles.check}
                                    width={20}
                                    height={20}
                                    src="/assets/icons/AiOutlineCheckCircle.svg"
                                    loader={cloudflareLoader}
                                    alt=""
                                />
                            </span>
                            <span>
                                {shipping?.label}{" "}
                                {shipping?.note && <small> - {shipping?.note}</small>}
                            </span>
                        </p>
                    ))}
                </div>

                {selectedShippingMethod?.type?.value === "transportadora" && (
                    <>
                        <p className={styles.title}>
                            {t("Informe o nome da transportadora")}:
                        </p>
                        <div className={styles.shippingMethods}>
                            <input
                                placeholder="Ex: Expresso"
                                onChange={(e) => 
                                {
                                    setSelectedFinalShipping({
                                        ...shippingTransportadora,
                                        label : e.target.value,
                                    });
                                    setSelectedShippingRetirada({
                                        ...shippingTransportadora,
                                        label : e.target.value,
                                    });
                                }}
                            />
                        </div>
                    </>
                )}

                {selectedShippingMethod?.type?.value === "onibus" && (
                    <>
                        <p className={styles.title}>{t("Insira os dados abaixo")}:</p>
                        <div className={styles.shippingMethods}>
                            <input
                                placeholder={t("Estacionamento")}
                                style={{ marginBottom : 30 }}
                                onChange={(e) => 
                                {
                                    setOnibus({
                                        ...onibus,
                                        estacionamento : e.target.value,
                                    });
                                }}
                            />
                            <input
                                placeholder={t("Nome da empresa de ônibus")}
                                style={{ marginBottom : 30 }}
                                onChange={(e) => 
                                {
                                    setOnibus({
                                        ...onibus,
                                        nomeDaEmpresaOnibus : e.target.value,
                                    });
                                }}
                            />
                            <input
                                placeholder={t("Nome do guia")}
                                style={{ marginBottom : 30 }}
                                onChange={async (e) => 
                                {
                                    setOnibus({
                                        ...onibus,
                                        nomeDoGuia : e.target.value,
                                    });
                                }}
                            />
                        </div>
                    </>
                )}

                <div className={styles.buttons}>
                    {/* {!loadingCart && (
            <button
              className={'block ' + styles.cinza}
              type="button"
              onClick={() => router.push("/")}
            >
              {t("Voltar a loja")}
            </button>
            )} */}
                    {/* {loadingCart ? null : (
            <button
              className={'block ' + styles.cinza}
              type="button"
              onClick={() => goBack()}
            >
              {t("Voltar")}
            </button>
            )} */}
                    <button
                        disabled={loadingCart && true}
                        className={`block ${styles.submit}`}
                        type="button"
                        onClick={() => onSubmit({ ...selectedFinalShipping, ...onibus })}
                    >
                        {loadingCart ? <AnimatedLoading /> : t("Próximo")}
                    </button>
                </div>
            </>

            <div className={styles.backLink}>
                <a
                    onClick={() => 
                    {
                        goBack();
                    }}
                >
                    <span>Voltar</span>
                </a>
            </div>
        </div>
    );
};
