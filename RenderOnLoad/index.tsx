import { useEffect, useLayoutEffect, useRef, useState } from "react";
import styles from "./styles.module.scss";

export const RenderOnLoad = ({ children }: any) => 
{
    const [ loaded, setLoaded ] = useState(false);
    const [ isReady, setIsReady ] = useState(false);
    const containerRef = useRef(null);

    useEffect(() => 
    {
        if (containerRef.current) 
        {
            setLoaded(true);
        }
    }, []);

    useLayoutEffect(() => 
    {
        setIsReady(true);
    }, []);

    return (
        <div ref={containerRef} className={styles.renderOnLoad}>
            {loaded && isReady ? children : <div className={styles.content} />}
        </div>
    );
};
