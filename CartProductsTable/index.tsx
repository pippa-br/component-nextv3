import { useResponsive } from "@/core-nextv3/util/useResponsive";
import { useTranslations } from "next-intl";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { toast } from "react-hot-toast";
import { ImageSet } from "../../component-nextv3/ImageSet";
import {
    delGiftCart,
    delItemCart,
    setItemCart,
    validateCart,
} from "../../core-nextv3/cart/cart.api";
import { firstImageItemCart } from "../../core-nextv3/cart/cart.util";
import { useCore } from "../../core-nextv3/core/core";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { currencyMask } from "../../core-nextv3/util/mask";
import { CART_SETTING, THEME_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import { DiscountPaymentMethod } from "../DiscountPaymentMethod";
import { Gifts } from "../Gifts";
import { InputCalculateShipping } from "../InputCalculateShipping";
import { InputCoupon } from "../InputCoupon";
import { Itempackaging } from "../ItemPackaging";
import { ProductItemSkeletonCart } from "../ProductItemSkeletonCart";
import styles from "./styles.module.scss";

function CartProductsTable({
    packaging = null,
    settings = {},
    pageGifts,
    installmentRule,
}: any) 
{
    const router = useRouter();
    const [ hasProducts, setHasProducts ] = useState(false);
    const [ timer, setTimer ] = useState<any>(null);
    const { isDesktop } = useResponsive();
    const {
        setCart,
        cart,
        user,
        isLoadingCart,
        awaitLoadingHTTP,
        setAwaitLoadingHTTP,
    } = useCore();
    const t = useTranslations();

    useEffect(() => 
    {
        setHasProducts(false);

        if (cart && cart.items?.length > 0) 
        {
            if (isLoadingCart) 
            {
                tagManager4.cartView(cart, user);
            }

            // POIS PODE CONTER APENAS O BRIND
            for (const item of cart.items) 
            {
                if (!item.gift) 
                {
                    setHasProducts(true);
                    break;
                }
            }
        }
    }, [ cart ]);

    //console.log("cart", cart);

    //console.error('xxxx', installmentRule);

    async function handleChangeValue(event: any, item: any) 
    {
        item.quantity = event.target.value;
        handleTimer(item, event.target.value);
    }

    const handleTimer = (item: any, value: number) => 
    {
        if (timer) 
        {
            clearTimeout(timer);
        }

        setTimer(
            setTimeout(async () => 
            {
                if (value) 
                {
                    const product = item.product;
                    const variant = item.variant;
                    //const quantity = item.quantity

                    const newData: any = {
                        data : {
                            product : {
                                referencePath : product.referencePath,
                            },
                            quantity : value,
                        },
                    };

                    // PRODUCT VARIANT
                    if (variant) 
                    {
                        newData.data.variant = [ ...variant ];
                        product.id = item.id;
                    }

                    //tagManager4.addToCart(router.asPath, product, 1, user);

                    const result = await setItemCart(CART_SETTING.merge(newData));

                    if (result.status) 
                    {
                        toast.success(t("Quantidade alterada com sucesso!"), {
                            icon     : "👏",
                            duration : 2000,
                        });
                        setCart(result.data);
                    }
                    else 
                    {
                        return toast.error(result.error, {
                            duration : 2000,
                        });
                    }
                }
            }, 500),
        );
    };

    async function handleIncrementQuantity(item: any) 
    {
        const product = item.product;
        const variant = item.variant;
        const quantity = item.quantity;

        const newData: any = {
            data : {
                product : {
                    referencePath : product.referencePath,
                },
                quantity : quantity + 1,
            },
        };

        // PRODUCT VARIANT
        if (variant) 
        {
            newData.data.variant = [ ...variant ];
            product.id = item.id;
        }

        //tagManager4.addToCart(router.asPath, product, 1, user);

        const result = await setItemCart(CART_SETTING.merge(newData));

        if (result.status) 
        {
            toast.success(t("Quantidade alterada com sucesso!"), {
                icon     : "👏",
                duration : 2000,
            });
            setCart(result.data);
        }
        else 
        {
            return toast.error(result.error, {
                duration : 2000,
            });
        }
    }

    async function handleDecrementQuantity(item: any) 
    {
        const product = item.product;
        const variant = item.variant;
        const quantity = item.quantity;

        const newData: any = {
            data : {
                product : {
                    referencePath : product.referencePath,
                },
                quantity : quantity - 1,
            },
        };

        // PRODUCT VARIANT
        if (variant) 
        {
            newData.data.variant = [ ...variant ];
            product.id = item.id;
            product.variant = variant;
        }

        tagManager4.removeFromCart(product, 1, user);

        const result = await setItemCart(CART_SETTING.merge(newData));

        if (result.status) 
        {
            toast.success(t("Quantidade alterada com sucesso!"), {
                icon     : "👏",
                duration : 2000,
            });
            setCart(result.data);
        }
        else 
        {
            return toast.error(result.error, {
                duration : 2000,
            });
        }
    }

    async function handleDeleteProduct(item: any) 
    {
        const product = item.product;
        const variant = item.variant;
        product.id = item.id;

        product.id = item.id;
        product.variant = variant;

        tagManager4.removeFromCart(product, item.quantity, user);

        const newData: any = {
            data : {
                product : {
                    referencePath : product.referencePath,
                },
            },
        };

        // PRODUCT VARIANT
        if (variant) 
        {
            newData.data.variant = [ ...variant ];
        }

        let result: any;

        if (item.gift) 
        {
            result = await delGiftCart(CART_SETTING.merge(newData));
        }
        else 
        {
            result = await delItemCart(CART_SETTING.merge(newData));
        }

        toast.success("Produto removido!", { icon : "💔", duration : 2000 });

        setCart(result.data);
    }

    // async function handleClearCart()
    // {
    //     toast.success(t("Carrinho sendo limpo!"), { icon : "👏", duration : 2000 })

    //     cart.items.forEach((product: any) =>
    //     {
    //         tagManager4.removeFromCart(pathname, product, product.quantity, user)
    //     })

    //     const result = await clearCart(CART_SETTING)

    //     setCart(result.data)
    // }

    async function handleValidateCart() 
    {
        setAwaitLoadingHTTP(true);

        const result = await validateCart(CART_SETTING);
        setCart(result.data);

        setAwaitLoadingHTTP(false);

        if (result.status) 
        {
            router.push("/checkout");
        }
        else 
        {
            let message = "";

            if (result.error instanceof Object) 
            {
                for (const key in result.error) 
                {
                    message = result.error[key];
                    break;
                }
            }
            else 
            {
                message = result.error;
            }

            return toast.error(message, {
                duration : 2000,
            });
        }
    }

    return (
        <div className={styles.cartProductsTable}>
            {!isLoadingCart ? (
                <>
                    {Array.from({ length : 3 }).map((_product, index) => (
                        <ProductItemSkeletonCart key={`productSkeleton-${index}`} />
                    ))}
                </>
            ) : hasProducts ? (
                <div className={styles.content}>
                    <div className={styles.right}>
                        <div className={styles.buttons}>
                            {/* <button type="button" onClick={() => router.push("/produtos")}>
							</button> */}
                            {/* <button
                                className={styles.submit + " block"}
                                type='button'
                                onClick={() => handleValidateCart()}
                            >
                                {t("Finalizar Compra")}
                            </button> */}
                            {/* {isLoadingCart && (
                                <button
                                    className={styles.cinza}
                                    type='button'
                                    onClick={() => router.push(productsUrl)}
                                >
                                    <BsArrowLeftCircle /> {t("Continuar comprando")}
                                </button>
                            )}
                            {cart?.items?.length > 0 && (
                                <>
                                    <button className={styles.cinza} type="button" onClick={() => handleClearCart()}>
                                        <BsTrash />  {t("Limpar Carrinho")}
                                    </button>
                                </>
                            )} */}
                        </div>

                        {/* <header>
							<p>Item</p>
							<p>Quantidade</p>
							<p>Preço</p>
							<p></p>
						</header> */}
                        <div className={styles.body}>
                            {isDesktop && (
                                <div className={styles.titlesItems}>
                                    <span>Produtos</span>
                                    <span>Valor Unitário</span>
                                    <span className={styles.bold}>Valor Total</span>
                                    <span>Quantidade</span>
                                </div>
                            )}

                            {cart?.items?.map((item: any, index: any) => (
                                <div className={styles.item} key={index}>
                                    {isDesktop && (
                                        <>
                                            <div className={styles.product}>
                                                <div className={styles.img}>
                                                    <ImageSet
                                                        image={firstImageItemCart(item)}
                                                        width={THEME_SETTING.widthProductThumb}
                                                        height={THEME_SETTING.heightProductThumb}
                                                        sizes="25vw"
                                                    />
                                                </div>
                                                <div className={styles.productInfo}>
                                                    <div className={styles.productName}>
                                                        {item?.product?.name}
                                                    </div>
                                                    {item?.variant && (
                                                        <div className={styles.productDetails}>
                                                            {item?.variant?.map((item2: any, index: any) => (
                                                                <p className={styles.productColor} key={index}>
                                                                    {item2?.type?.name}: {item2?.label}
                                                                </p>
                                                            ))}
                                                        </div>
                                                    )}
                                                </div>
                                            </div>
                                            <div className={styles.prices}>
                                                {!item.gift && item?.promotionalPrice > 0 && (
                                                    <div className={styles.promotionalPrice}>
                                                        {currencyMask(item?.realPrice)}
                                                    </div>
                                                )}
                                                {!item.gift && (
                                                    <div className={styles.price}>
                                                        {currencyMask(item?.price)}
                                                    </div>
                                                )}
                                            </div>
                                            {!item.gift && (
                                                <div className={styles.price}>
                                                    {currencyMask(item?.total)}
                                                </div>
                                            )}
                                            {item.gift && (
                                                <div className={styles.price}>{t("Brinde")}</div>
                                            )}
                                            <div className={styles.actions}>
                                                <div className={styles.quantity}>
                                                    {!item.gift && (
                                                        <div className={styles.quantityContainer}>
                                                            <span
                                                                onClick={() => handleDecrementQuantity(item)}
                                                            >
																-
                                                            </span>
                                                            {!settings.editQuantity ? (
                                                                <span className={styles.value}>
                                                                    {item?.quantity}
                                                                </span>
                                                            ) : (
                                                                <input
                                                                    type="number"
                                                                    className={styles.value}
                                                                    defaultValue={item?.quantity}
                                                                    onChange={(e) => 
                                                                    {
                                                                        handleChangeValue(e, item);
                                                                    }}
                                                                />
                                                            )}
                                                            <span
                                                                onClick={() => handleIncrementQuantity(item)}
                                                            >
																+
                                                            </span>
                                                        </div>
                                                    )}
                                                </div>
                                                {item?.attachment && (
                                                    <a
                                                        href={item?.attachment?._url}
                                                        rel="noreferrer"
                                                        target="_blank"
                                                    >
                                                        <ImageSet
                                                            width={20}
                                                            height={20}
                                                            src="/assets/icons/RiAttachment2.svg"
                                                            responsive={false}
                                                            alt=""
                                                        />
                                                    </a>
                                                )}
                                                {item?.gift && (
                                                    <a>
                                                        <ImageSet
                                                            width={20}
                                                            height={20}
                                                            src="/assets/icons/GiPresent.svg"
                                                            responsive={false}
                                                            alt=""
                                                        />
                                                    </a>
                                                )}
                                            </div>
                                            <a onClick={() => handleDeleteProduct(item)}>
                                                <ImageSet
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/BsTrash.svg"
                                                    responsive={false}
                                                    alt=""
                                                />
                                            </a>
                                        </>
                                    )}

                                    {!isDesktop && (
                                        <div className={styles.product}>
                                            <div className={styles.img}>
                                                <ImageSet
                                                    image={firstImageItemCart(item)}
                                                    width={THEME_SETTING.widthProductThumb}
                                                    height={THEME_SETTING.heightProductThumb}
                                                    sizes="25vw"
                                                />
                                            </div>
                                            <div className={styles.productInfo}>
                                                <div className={styles.productName}>
                                                    {item?.product?.name}
                                                </div>
                                                {item.variant && (
                                                    <div className={styles.productDetails}>
                                                        {item?.variant?.map((item2: any, index: any) => (
                                                            <p className={styles.productColor} key={index}>
                                                                {item2?.type?.name}: {item2?.label}
                                                            </p>
                                                        ))}
                                                    </div>
                                                )}
                                                <div className={styles.prices}>
                                                    <div className={styles.priceItem}>
                                                        {!item.gift && item?.promotionalPrice > 0 && (
                                                            <div className={styles.promotionalPrice}>
                                                                {currencyMask(item?.realPrice)}
                                                            </div>
                                                        )}
                                                        {!item.gift && (
                                                            <div className={styles.price}>
                                                                {currencyMask(item?.price)}
                                                            </div>
                                                        )}
                                                    </div>
                                                    {!item.gift && (
                                                        <div className={styles.priceTotal}>
                                                            {currencyMask(item?.total)}
                                                        </div>
                                                    )}
                                                </div>
                                                {item.gift && (
                                                    <div className={styles.price}>{t("Brinde")}</div>
                                                )}
                                                <div className={styles.actions}>
                                                    <div className={styles.quantity}>
                                                        {!item.gift && (
                                                            <div className={styles.quantityContainer}>
                                                                <span
                                                                    onClick={() => handleDecrementQuantity(item)}
                                                                >
																	-
                                                                </span>
                                                                {!settings.editQuantity ? (
                                                                    <span className={styles.value}>
                                                                        {item?.quantity}
                                                                    </span>
                                                                ) : (
                                                                    <input
                                                                        type="number"
                                                                        className={styles.value}
                                                                        defaultValue={item?.quantity}
                                                                        onChange={(e) => 
                                                                        {
                                                                            handleChangeValue(e, item);
                                                                        }}
                                                                    />
                                                                )}
                                                                <span
                                                                    onClick={() => handleIncrementQuantity(item)}
                                                                >
																	+
                                                                </span>
                                                            </div>
                                                        )}
                                                    </div>
                                                    {item?.attachment && (
                                                        <a
                                                            href={item?.attachment?._url}
                                                            rel="noreferrer"
                                                            target="_blank"
                                                        >
                                                            <ImageSet
                                                                width={20}
                                                                height={20}
                                                                src="/assets/icons/RiAttachment2.svg"
                                                                responsive={false}
                                                                alt=""
                                                            />
                                                        </a>
                                                    )}
                                                    {item?.gift && (
                                                        <a>
                                                            <ImageSet
                                                                width={20}
                                                                height={20}
                                                                src="/assets/icons/GiPresent.svg"
                                                                responsive={false}
                                                                alt=""
                                                            />
                                                        </a>
                                                    )}
                                                </div>
                                            </div>
                                            <a onClick={() => handleDeleteProduct(item)}>
                                                <ImageSet
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/BsTrash.svg"
                                                    responsive={false}
                                                    alt=""
                                                />
                                            </a>
                                        </div>
                                    )}
                                </div>
                            ))}
                        </div>

                        {packaging?.published && (
                            <div className={styles.packaging}>
                                <p className={styles.title}>
									Embalagem para presentes por apenas:
                                </p>

                                <div className={styles.packagingBody}>
                                    {packaging?.packaging?.map((product: any, index: number) => (
                                        <Itempackaging key={index} product={product} />
                                    ))}
                                </div>
                            </div>
                        )}

                        <p className={styles.note}>
							*
                            {t(
                                "O carrinho de compras armazena temporariamente uma lista dos produtos e não garante a disponibilidade em estoque no momento da compra. O preço e a disponibilidade dos produtos estão sujeitos a alterações",
                            )}
							.
                        </p>

                        {/* <div className={styles.total}>
							<p>Total</p>
							<p>{currencyMask(cart?.totalItems)}</p>
						</div> */}
                    </div>
                    <div className={styles.left}>
                        {pageGifts?.published && (
                            <Gifts page={pageGifts} cart={cart} loadingCart={isLoadingCart} />
                        )}

                        {cart?.freeShipping?.status && (
                            <section className={styles.cartFreeShipping}>
                                <div className={styles.content}>
                                    {cart?.freeShipping?.diff !== -1 &&
										(cart?.freeShipping?.diff > 0 ? (
										    <>
												Falta {currencyMask(cart?.freeShipping?.diff)} para
												FRETE GRÁTIS!
										        <ImageSet
										            width={20}
										            height={20}
										            src="/assets/icons/FaTruck.svg"
										            responsive={false}
										            alt=""
										        />
										    </>
										) : (
										    <>
												Você Ganhou FRETE GRÁTIS!
										        <ImageSet
										            width={20}
										            height={20}
										            src="/assets/icons/FaTruck.svg"
										            responsive={false}
										            alt=""
										        />
										    </>
										))}
                                    {cart?.freeShipping?.diffQuantity !== -1 &&
										(cart?.freeShipping?.diffQuantity > 0 ? (
										    <>
												Falta {cart?.freeShipping?.diffQuantity} produto(s) para
												FRETE GRÁTIS!
										        <ImageSet
										            width={20}
										            height={20}
										            src="/assets/icons/FaTruck.svg"
										            responsive={false}
										            alt=""
										        />
										    </>
										) : (
										    <>
												Você Ganhou FRETE GRÁTIS!
										        <ImageSet
										            width={20}
										            height={20}
										            src="/assets/icons/FaTruck.svg"
										            responsive={false}
										            alt=""
										        />
										    </>
										))}
                                </div>
                            </section>
                        )}

                        <InputCoupon cart={cart} setCart={setCart} />

                        {!THEME_SETTING?.disabledShipping && (
                            <InputCalculateShipping
                                disabledDiff={true}
                                hasSelected={true}
                                title={t("Calcule o frete e prazo de entrega")}
                            />
                        )}

                        <div className={styles.subTotal}>
                            <p>{t("SubTotal")}:</p>
                            <p>{currencyMask(cart?.totalItems)}</p>
                        </div>

                        <div className={styles.dicount}>
                            <p>
                                {t("Desconto")}:
                                {cart?.discountPaymentMethod < 0 && (
                                    <span>({cart?.paymentMethod?.label})</span>
                                )}
                                {cart?.coupon && <span>({t("Cupom")})</span>}
                                {cart?.discountClient > 0 && <span>({t("Crédito")})</span>}
                            </p>
                            <p>{currencyMask(cart?.totalDiscount || 0)}</p>
                        </div>

                        <div className={styles.shipping}>
                            <p>{t("Frete")}:</p>
                            <p>{currencyMask(cart?.totalShipping || 0)}</p>
                        </div>

                        <div className={styles.total}>
                            <p>{t("Total")}:</p>
                            <p>
                                {currencyMask(
                                    cart?.totalItems + cart?.totalDiscount + cart?.totalShipping,
                                )}
                            </p>
                        </div>

                        <div className={styles.button}>
                            <button
                                className={`${styles.submit} block buttonBlock `}
                                type="button"
                                onClick={() => handleValidateCart()}
                            >
                                {awaitLoadingHTTP ? <AnimatedLoading /> : t("Finalizar Compra")}
                            </button>
                        </div>

                        {!THEME_SETTING.disabledInformationPaymenteCart && (
                            <DiscountPaymentMethod
                                totalItems={cart?.totalItems}
                                installmentRule={installmentRule}
                                totalDiscount={cart?.totalDiscount}
                                totalShipping={cart?.totalShipping}
                                discountPaymentMethod={cart?.discountPaymentMethod}
                            />
                        )}
                    </div>
                </div>
            ) : (
                <p className={styles.noProducts}>
                    <ImageSet
                        width={20}
                        height={20}
                        src="/assets/icons/BsBagFill.svg"
                        responsive={false}
                        alt=""
                    />
                    {t("Seu carrinho está vazio")}. :(
                </p>
            )}

            {/* <div className={styles.buttons}>
                {!isLoadingCart && (<button className={styles.cinza} type="button" onClick={() => router.push(productsUrl)}>
                    Continuar comprando
                </button>)}
                {cart?.items?.length > 0 && (
                    <>
                        <button className={styles.cinza} type="button" onClick={() => handleClearCart()}>
                            Limpar Carrinho
                        </button>
                        <button className={styles.submit} type="button" onClick={() => handleValidateCart()}>
                            Concluir compra
                        </button>
                    </>
                )}
            </div> */}
        </div>
    );
}

export default CartProductsTable;
