import Image from "next/image";
import { useState } from "react";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { cloudflareLoader, removePhoneMask } from "../../core-nextv3/util/util";
import CustomLink from "../CustomLink";
import styles from "./styles.module.scss";

export const SocialIcons = ({ account }: any) => 
{
    const [ isOpen, setIsOpen ] = useState(false);

    const handleChange = () => 
    {
        setIsOpen(!isOpen);
    };

    return (
        <div className={styles.content}>
            {/* <div type="checkbox" href="#"  className={styles.container}>
      {account?.pinterest && <div
        className={styles.pinterestIcon}
        onClick={() =>
          {
            window.open(
              account?.pinterest,
              "_blank"
            )
          }
        }
      >
        <BsPinterest />
      </div>}
      {account?.facebook && <div
        className={styles.facebookIcon}
        onClick={() =>
          {
            window.open(
              account?.facebook,
              "_blank"
            )
          }
        }
      >
        <BsFacebook />
      </div>}
      {account?.instagram && <div
        className={styles.instagramIcon}
        onClick={() =>
          {
            window.open(
              account?.instagram,
              "_blank"
            )
          }
        }
      >
        <BsInstagram />
      </div>}
      {account?.whatsapp &&<div
        className={styles.whatsAppIcon}
        onClick={() =>
          {
            tagManager4.registerEvent("contact", "whatsapp-button", "Contato", 0, null);
            window.open(
              `https://api.whatsapp.com/send?phone=${removePhoneMask(account?.whatsapp)}&text=Olá, gostaria de tirar algumas duvidas.`,
              "_blank"
            )
          }
        }
      >
        <BsWhatsapp />
      </div>}
    </div> */}

            <nav className={styles.menu}>
                <input
                    type="checkbox"
                    className={styles.menuopen}
                    name="menu-open"
                    id="menu-open"
                    onChange={handleChange}
                />
                <label className={styles.menuopenbutton} htmlFor="menu-open">
                    {isOpen ? (
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/GrFormClose.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    ) : (
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/BsChatDots.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    )}
                </label>

                {account?.pinterest && (
                    <CustomLink
                        href={account?.pinterest}
                        prefetch={true}
                        target="_blank"
                        className={`${styles.menuitem} ${styles.pinterestIcon}`}
                        rel="noreferrer"
                    >
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/BsPinterest.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    </CustomLink>
                )}
                {account?.facebook && (
                    <CustomLink
                        href={account?.facebook}
                        prefetch={true}
                        target="_blank"
                        className={`${styles.menuitem} ${styles.facebookIcon}`}
                        rel="noreferrer"
                    >
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/BsFacebook.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    </CustomLink>
                )}
                {account?.instagram && (
                    <CustomLink
                        href={account?.instagram}
                        prefetch={true}
                        target="_blank"
                        className={`${styles.menuitem} ${styles.instagramIcon}`}
                        rel="noreferrer"
                    >
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/BsInstagram.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    </CustomLink>
                )}
                {account?.whatsapp && (
                    <CustomLink
                        href={`https://api.whatsapp.com/send?phone=${removePhoneMask(account?.whatsapp)}&text=Olá, gostaria de tirar algumas duvidas.`}
                        prefetch={true}
                        target="_blank"
                        className={`${styles.menuitem} ${styles.whatsappIcon}`}
                        rel="noreferrer"
                        onClick={() =>
                            tagManager4.registerEvent(
                                "contact",
                                "whatsapp-button",
                                "Contato",
                                0,
                                null,
                            )
                        }
                    >
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/BsWhatsapp.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    </CustomLink>
                )}
            </nav>
        </div>
    );
};
