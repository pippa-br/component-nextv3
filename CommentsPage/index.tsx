import { collectionDocument } from "@/core-nextv3/document/document.api";
import { calls } from "@/core-nextv3/util/call.api";
import { COMMET_SETTING } from "@/setting/setting";
import { Suspense } from "react";
import { CommentsDisplay } from "./commnts.display";

const getCommentsPageServer = async () => 
{
    const [ commentsResult ]: any = await calls(
        collectionDocument(
            COMMET_SETTING.merge({
                cache : "force-cache",
                where : [
                    {
                        field    : "status",
                        operator : "==",
                        value    : true,
                    },
                ],
            }),
        ),
    );

    return {
        commentsData : commentsResult.collection ? commentsResult.collection : [],
    };
};

const GenerateMetadata = async () => 
{
    return {
        title : "Avaliações",
    };
};

const CommentsPage = async () => 
{
    const commentsPageServer = await getCommentsPageServer();

    return (
        <Suspense>
            <CommentsDisplay comments={commentsPageServer?.commentsData} />
        </Suspense>
    );
};

export { CommentsPage, GenerateMetadata };
