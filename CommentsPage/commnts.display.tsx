"use client";

import { useState } from "react";
import { Comments } from "../Comments";
import { CommentsRating } from "../CommentsRating";
import { ModalComments } from "../ModalComments";
import { PageTitle } from "../PageTitle";
import styles from "./styles.module.scss";

const CommentsDisplay = ({ comments }: any) => 
{
    const [ openModalComments, setOpenModalComments ] = useState(false);

    return (
        <>
            <div className={`${styles.assessmentsPage} page`}>
                <div className={styles.content}>
                    <PageTitle name={"Avaliações"} noTitle={true} />

                    <h3>
						Veja o que nosso clientes estão falando sobre nossos produtos
                        <br />e junte-se à comunidade de compradores satisfeitos!
                    </h3>

                    {comments?.length > 0 && <CommentsRating comments={comments} />}

                    <div className={styles.assessmentsGrid}>
                        {comments?.length > 0 &&
							comments?.map((coment: any, index: any) => (
							    <Comments coment={coment} comments={comments} key={index} />
							))}
                    </div>

                    <div className={styles.button}>
                        {comments?.length > 4 && (
                            <button onClick={() => setOpenModalComments(true)}>
								Ler mais comentários
                            </button>
                        )}
                    </div>
                </div>
            </div>

            {openModalComments && (
                <ModalComments comments={comments} close={setOpenModalComments} />
            )}
        </>
    );
};

export { CommentsDisplay };
