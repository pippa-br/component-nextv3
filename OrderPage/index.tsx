import { getAccount } from "@/core-nextv3/account/account.api";
import { getDocument } from "@/core-nextv3/document/document.api";
import { calls } from "@/core-nextv3/util/call.api";
import {
    ACCOUNT_SETTING,
    COUNTDOWN_PAGE_SETTING,
    GATEWAY_SETTING,
    ORDER_PAGE_SETTING,
    STATUS_BAR_PAGE_SETTING,
} from "@/setting/setting";
import { Suspense } from "react";
import { OrderDisplay } from "./order.display";

// const GenerateStaticParams = async () =>
// {
//     const pages = await collectionDocument(ORDER_SETTING.merge({
//         perPage : 1,
//     }));

//     let paths = [];

//     if (pages.collection)
//     {
//         paths = pages.collection.map((item:any) => ({
//             params : { id : item.id || "" },
//         }));
//     }

//     console.info("(Order Static:", paths.length, ")");

//     return paths;
// }

const getOrderPageServer = async () => 
{
    const [
        accountResult,
        countdownPageResult,
        statusBarPageResult,
        orderPageResult,
        installmentRuleResult,
    ] = await calls(
        getAccount(ACCOUNT_SETTING.merge({ cache : "force-cache" })), /// ACCOUNT TEM QUE PEGAR AQUI POIS NÃO TEM ACESSO AO PROVIDER DE CADA TEMA
        getDocument(COUNTDOWN_PAGE_SETTING.merge({ cache : "force-cache" })),
        getDocument(STATUS_BAR_PAGE_SETTING.merge({ cache : "force-cache" })),
        getDocument(ORDER_PAGE_SETTING.merge({ cache : "force-cache" })),
        getDocument(GATEWAY_SETTING.merge({ cache : "force-cache" })),
    );

    return {
        accountData       : accountResult?.data || {},
        countdownPageData : countdownPageResult?.data || {},
        statusBarPageData : statusBarPageResult?.data || {},
        orderPageData     : orderPageResult?.data || {},
        installmentRule   : installmentRuleResult?.data?.installmentRule || {},
    };
};

const GenerateMetadata = async () => 
{
    return {
        title : "Pedido",
    };
};

const OrderPage = async ({
    params,
}: {
	params: Promise<{ id: string }>;
}) => 
{
    const { id } = await params;

    const {
        accountData,
        countdownPageData,
        statusBarPageData,
        orderPageData,
        installmentRule,
    } = await getOrderPageServer();

    return (
        <Suspense>
            <OrderDisplay
                id={id}
                accountData={accountData}
                countDownPage={countdownPageData}
                stausBarPage={statusBarPageData}
                orderPage={orderPageData}
                installmentRule={installmentRule}
            />
        </Suspense>
    );
};

export {
    OrderPage,
    GenerateMetadata,
    //GenerateStaticParams,
};
