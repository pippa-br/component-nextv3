"use client";

import { productPurchasesAnalytics } from "@/core-nextv3/analytics/analytics.api";
import { useProtectedAuth } from "@/core-nextv3/auth/user.auth.api";
import { useGetDocument } from "@/core-nextv3/document/document.use";
import { purchaseCompleteRetail } from "@/core-nextv3/retail/retail.api";
import { useTranslations } from "next-intl";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { Suspense, useState } from "react";
import { useCore } from "../../core-nextv3/core/core";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { innerHTML } from "../../core-nextv3/util/util";
import { AUTH_SETTING, ORDER_SETTING } from "../../setting/setting";
import { LoadingPage } from "../LoadingPage";
import { OrderViewer } from "../OrderViewer";
import { PageTitle } from "../PageTitle";
import styles from "./styles.module.scss";

const OrderDisplay = ({
    id,
    accountData,
    countDownPage,
    stausBarPage,
    orderPage,
    installmentRule,
}: any) => 
{
    const router = useRouter();
    const searchParams = useSearchParams();
    const pathname = usePathname();
    const { user } = useCore();
    const [ order, setOrder ] = useState<any>();
    const t = useTranslations();

    useGetDocument(ORDER_SETTING.merge({ id : id }), (data: any) => 
    {
        const success = searchParams.get("success");

        setOrder(data);

        if (success) 
        {
            // PEDIDO CONCLUIDO
            productPurchasesAnalytics(data);

            // RETAIL EVENT
            purchaseCompleteRetail(
                ORDER_SETTING.merge({
                    document : {
                        referencePath : data.referencePath,
                    },
                }),
            );

            tagManager4.purchase(data, user);

            const currentParams = new URLSearchParams(searchParams.toString());
            currentParams.delete("success");

            router.replace(`${pathname}?${currentParams.toString()}`);
        }
    });

    const loadProtectedAuth = useProtectedAuth(AUTH_SETTING, "", "/login");

    if (!loadProtectedAuth || !order) 
    {
        return <LoadingPage />;
    }

    return (
        <Suspense>
            <div className={`${styles.orderPage} orderPage page`}>
                <div className={styles.content}>
                    <PageTitle
                        parents={[ { url : "/perfil", name : t("Meus Pedidos") } ]}
                        name={`Pedido Nº ${order._sequence}`}
                        noTitle={true}
                    />

                    {orderPage?.content && (
                        <div
                            className={styles.pageContent}
                            dangerouslySetInnerHTML={innerHTML(orderPage.content)}
                        />
                    )}

                    <OrderViewer
                        order={order}
                        account={accountData}
                        countDownPage={countDownPage}
                        stausBarPage={stausBarPage}
                        installmentRule={installmentRule}
                        redirectUrl="/pedido/"
                    />
                </div>
            </div>
        </Suspense>
    );
};

export { OrderDisplay };
