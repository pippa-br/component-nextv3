"use client";

import {
    useGetUserAuth,
    useProtectedAuth,
} from "@/core-nextv3/auth/user.auth.api";
import { buscaCep2, cloudflareLoader } from "@/core-nextv3/util/util";
import { customSelectStyles } from "@/core-nextv3/util/util.style";
import cleanDeep from "clean-deep";
import { useTranslations } from "next-intl";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { Checkbox } from "pretty-checkbox-react";
import { useEffect, useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { PatternFormat } from "react-number-format";
import Select from "react-select";
import { logoutAuth, setUserAuth } from "../../core-nextv3/auth/auth.api";
import { mergeAllCart } from "../../core-nextv3/cart/cart.api";
import { useCore } from "../../core-nextv3/core/core";
import { TDate } from "../../core-nextv3/model/TDate";
import { currencyMask } from "../../core-nextv3/util/mask";
import {
    validateCEP,
    validateCpf,
    validateDate,
    validateFullName,
} from "../../core-nextv3/util/validate";
import {
    AUTH_SETTING,
    CART_SETTING,
    THEME_SETTING,
} from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import ErrorMessage from "../ErrorMessage";
import { LoadingPage } from "../LoadingPage";
import { PageTitle } from "../PageTitle";
import styles from "./styles.module.scss";
declare const window: any;

const AccountDisplay = () => 
{
    const router = useRouter();
    const [ gender, setGender ] = useState<any>();
    const [ passwordType, setPasswordType ] = useState("password");
    const [ passwordTypeTwo, setPasswordTypeTwo ] = useState("password");
    const [ hasNewsletter, setHasNewsletter ] = useState(false);
    const [ hasWhatsapp, setHasWhatsapp ] = useState(false);
    const [ loadingUserData, setLoadingUserData ] = useState(false);
    const [ loadingUserPassword, setLoadingUserPassword ] = useState(false);
    const [ loadingUserUpdateAddress, setLoadingUserUpdateAddress ] =
		useState(false);
    const t = useTranslations();
    const { cart, user, setUser } = useCore();
    const [ birthdayError, setBirthdayError ] = useState<any>(null);
    const {
        register: registerInfo,
        handleSubmit: handleSubmitInfo,
        reset: resetInfo,
        setValue: setValueInfo,
        control: controlInfo,
        formState: { errors: errorsInfo, isSubmitted: isSubmittedInfo },
    } = useForm({
        mode : "onBlur",
    });
    const {
        register: registerPassword,
        formState: { errors: errorsPassword },
        watch: watchPassword,
        handleSubmit: handleSubmitPassword,
    } = useForm({
        mode : "onBlur",
    });
    const {
        register: registerAddress,
        formState: { errors: errorsAddress },
        control: controlAddress,
        trigger: triggerAddress,
        setValue: setValueAddress,
        handleSubmit: handleSubmitAddress,
    } = useForm({
        mode : "onBlur",
    });
    const password = useRef({});
    password.current = watchPassword("password", "");

    useEffect(() => 
    {
        registerInfo("gender", { value : null });
    }, []);

    useGetUserAuth(AUTH_SETTING, (data: any) => 
    {
        if (data) 
        {
            data.password = undefined;
            data.confirmPassword = undefined;
            //data.birthday = new TDate(data.birthday).format('DD/MM/YYYY');

            setUser(data);
            setGender(data.gender);

            resetInfo(cleanDeep(data));

            setHasNewsletter(data.newsletter);
            setHasWhatsapp(data.onWhatsapp);
        }
    });

    const loadProtectedAuth = useProtectedAuth(AUTH_SETTING, "", "/login");

    if (!loadProtectedAuth) 
    {
        return <LoadingPage />;
    }

    const genderOptions = [
        { label : "Não Quero Informar", value : "I", id : "IsQ9FFhx" },
        { label : "Masculino", value : "M", id : "yfcqOV15" },
        { label : "Feminino", value : "F", id : "BoXeFDWZ" },
    ];

    // const customSelectStyles: any = {
    //     control : (base: any, state: any) => ({
    //         ...base,
    //         borderRadius : "4px",
    //         borderColor  : state.isFocused
    //             ? "var(--border-color)"
    //             : "var(--border-color)",
    //         boxShadow : state.isFocused ? "none" : null,
    //         "&:hover" : {
    //             borderColor : "var(--border-color)",
    //         },
    //         width : "100%",
    //         color : "#000",
    //     }),
    //     menu        : (styles: any) => ({ ...styles, color : "#000", fontSize : "16px" }),
    //     placeholder : (styles: any) => ({ ...styles, fontSize : "16px" }),
    //     singleValue : (styles: any) => ({
    //         ...styles,
    //         color    : "#000",
    //         fontSize : "16px",
    //     }),
    //     indicatorSeparator : () => null,
    // }

    function handleChangeGender(e: any) 
    {
        setValueInfo("gender", e);
        setGender(e);
    }

    const handleBirthday = (e: any) => 
    {
        setBirthdayError(null);
        const value = e.target?.value;

        if (value?.split("_")?.length === 1) 
        {
            const birthday = new TDate({ value : value, mask : "dd/MM/yyyy" });

            if (!birthday.isValid()) 
            {
                setBirthdayError(t("Data Invalida"));
            }
        }

        setValueInfo("birthday", value);
    };

    const onSubmit = async (data: any) => 
    {
        const birthday = new TDate({ value : data.birthday, mask : "dd/MM/yyyy" });

        if (!birthday.isValid()) 
        {
            return toast.error(t("Data de aniversário inválida!"), {
                duration : 2000,
            });
        }

        if (!gender) 
        {
            return toast.error(t("Gênero requirido"), { duration : 2000 });
        }

        const newData = {
            document : {
                referencePath : user.referencePath, 
            },
            data : {
                name     : data.name,
                email    : data.email,
                cpf      : data.cpf,
                birthday : birthday.toDate(),
                phone    : data.phone,
                gender   : data.gender,
                // newsletter: hasNewsletter,
                // onWhatsapp: hasNewsletter,
            },
        };

        await setLoadingUserData(true);

        const result: any = await setUserAuth(AUTH_SETTING.merge(newData));

        await setLoadingUserData(false);

        if (result.status === false) 
        {
            return toast.error(result.error, { duration : 2000 });
        }

        toast.success(
            t("Dados atualizados com sucesso!", { icon : "👏", duration : 2000 }),
        );

        setUser(result.data);
    };

    const onSubmitUpdateAddress = async (data: any) => 
    {
        const newData = {
            document : {
                referencePath : user.referencePath,
            },
            data : data,
        };

        await setLoadingUserUpdateAddress(true);

        const result: any = await setUserAuth(AUTH_SETTING.merge(newData));

        await setLoadingUserUpdateAddress(false);

        if (result.status === false) 
        {
            return toast.error(result.error, { duration : 2000 });
        }

        toast.success(t("Endereço atualizado com sucesso!"), {
            icon     : "👏",
            duration : 2000,
        });

        setUser(result.data);
    };

    const changePassword = async (data: any) => 
    {
        if (data.password.length <= 0 && data.confirmPassword.length <= 0) 
        {
            return toast.error(t("Preencha os campos de senha!"), { duration : 2000 });
        }

        const newData = {
            document : {
                referencePath : user.referencePath,
            },
            data : {
                password        : data.password,
                confirmPassword : data.confirmPassword,
            },
        };

        await setLoadingUserPassword(true);

        const result: any = await setUserAuth(AUTH_SETTING.merge(newData));

        await setLoadingUserPassword(false);

        if (result.status === false) 
        {
            return toast.error(result.error, { duration : 2000 });
        }

        toast.success(t("Senha atualizada com sucesso!"), {
            icon     : "👏",
            duration : 2000,
        });        
    };

    const handleLogout = async () => 
    {
        await logoutAuth(AUTH_SETTING);

        // MERGE CART SESSION
        await mergeAllCart(
            CART_SETTING.merge({
                document : {
                    referencePath : cart?.referencePath,
                },
            }),
        );

        toast.success(t("Logout feito com sucesso!"), {
            icon     : "👏",
            duration : 2000,
        });
        window.location.reload();
    };

    const onNewsletter = async (e: any) => 
    {
        setHasNewsletter(!hasNewsletter);
        const valueCheckboxNewsletter = e.target.checked;
        // console.log("valueCheckbox", valueCheckbox)

        const result = await setUserAuth(
            AUTH_SETTING.merge({
                document : {
                    referencePath : user?.referencePath,
                },
                data : {
                    newsletter : valueCheckboxNewsletter,
                },
            }),
        );

        setUser(result.data);
    };

    const onWhatsapp = async (e: any) => 
    {
        setHasWhatsapp(!hasWhatsapp);
        const valueCheckboxWhatsapp = e.target.checked;
        // console.log("valueCheckbox", valueCheckbox)

        const result = await setUserAuth(
            AUTH_SETTING.merge({
                document : {
                    referencePath : user?.referencePath,
                },
                data : {
                    onWhatsapp : valueCheckboxWhatsapp,
                },
            }),
        );

        setUser(result.data);
    };

    const onZipcode = async (value: any) => 
    {
        const data: any = await buscaCep2(value);

        for (const key in data) 
        {
            setValueAddress(`address.${key}`, data[key]);
        }

        triggerAddress();
    };

    return (
        <>
            {loadProtectedAuth && (
                <div className={`${styles.accountDetailsPage} page`}>
                    <div className={styles.content}>
                        <PageTitle name={"Detalhes da conta"} />
                        <div className={styles.perfilData}>
                            <div className={styles.perfilMenu}>
                                <p onClick={() => router.push("/perfil/")}>
                                    {t("Meus Pedidos")}
                                </p>
                                {!THEME_SETTING.disabledExpress && (
                                    <p onClick={() => router.push("/pedidos-express/")}>
                                        {t("Pedidos Express")}
                                    </p>
                                )}
                                <p
                                    onClick={() => router.push("/perfil/detalhes-da-conta/")}
                                    className={styles.active}
                                >
                                    {t("Detalhes da conta")}
                                </p>
                                {!THEME_SETTING?.disabledFavorite && (
                                    <p onClick={() => router.push("/lista-de-desejos/")}>
                                        {t("Lista de Desejos")}
                                    </p>
                                )}
                                <p
                                    onClick={() => 
                                    {
                                        handleLogout();
                                    }}
                                >
                                    {t("Sair")}
                                </p>
                            </div>
                            <div className={styles.perfilContent}>
                                <div className={styles.perfilFlex}>
                                    <div className={styles.columns}>
                                        <form className={styles.form}>
                                            <div className={styles.formItem}>
                                                <label>{t("Créditos")}</label>
                                                <span className={styles.subtitle}>
                                                    {t("Valor disponíveis para uso em suas compras")}!
                                                </span>
                                                <div className={styles.points}>
                                                    {currencyMask(user?.points)}
                                                </div>
                                            </div>
                                        </form>

                                        <form
                                            className={styles.form}
                                            // onSubmit={handleSubmit(onSubmit)}
                                        >
                                            <div className={styles.formItem}>
                                                <label>{t("Senha")}</label>
                                                <span className={styles.subtitle}>
                                                    {t(
                                                        "Crie/Altere sua senha preenchendo os campos abaixo",
                                                    )}
                                                </span>
                                                <div className={styles.perfilPasswords}>
                                                    <div className={styles.inputPassword}>
                                                        <input
                                                            type={passwordType}
                                                            className={styles.input}
                                                            autoComplete="new-password"
                                                            placeholder={t("Digite sua senha")}
                                                            {...registerPassword("password")}
                                                        />
                                                        {passwordType === "password" ? (
                                                            <Image
                                                                className={styles.iconPassword}
                                                                onClick={() => setPasswordType("text")}
                                                                width={20}
                                                                height={20}
                                                                src="/assets/icons/AiOutlineEye.svg"
                                                                loader={cloudflareLoader}
                                                                alt=""
                                                            />
                                                        ) : (
                                                            <Image
                                                                className={styles.iconPassword}
                                                                onClick={() => setPasswordType("password")}
                                                                width={20}
                                                                height={20}
                                                                src="/assets/icons/AiOutlineEyeInvisible.svg"
                                                                loader={cloudflareLoader}
                                                                alt=""
                                                            />
                                                        )}
                                                    </div>

                                                    <div className={styles.inputPassword}>
                                                        <input
                                                            type={passwordTypeTwo}
                                                            className={styles.input}
                                                            autoComplete="new-password"
                                                            placeholder={t("Confirme sua senha")}
                                                            {...registerPassword("confirmPassword", {
                                                                validate : (value) =>
                                                                    value === password.current ||
																	t("As senhas precisam ser iguais!"),
                                                            })}
                                                        />
                                                        {passwordTypeTwo === "password" ? (
                                                            <Image
                                                                className={styles.iconPassword}
                                                                onClick={() => setPasswordTypeTwo("text")}
                                                                width={20}
                                                                height={20}
                                                                src="/assets/icons/AiOutlineEye.svg"
                                                                loader={cloudflareLoader}
                                                                alt=""
                                                            />
                                                        ) : (
                                                            <Image
                                                                className={styles.iconPassword}
                                                                onClick={() => setPasswordTypeTwo("password")}
                                                                width={20}
                                                                height={20}
                                                                src="/assets/icons/AiOutlineEyeInvisible.svg"
                                                                loader={cloudflareLoader}
                                                                alt=""
                                                            />
                                                        )}
                                                    </div>

                                                    {errorsPassword.confirmPassword && (
                                                        <ErrorMessage
                                                            message={errorsInfo.confirmPassword?.message}
                                                        />
                                                    )}
                                                    <button
                                                        className={styles.submit}
                                                        type="button"
                                                        disabled={loadingUserPassword && true}
                                                        onClick={handleSubmitPassword(changePassword)}
                                                    >
                                                        {loadingUserPassword ? (
                                                            <AnimatedLoading />
                                                        ) : (
                                                            t("Atualizar senha")
                                                        )}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div className={styles.columns}>
                                        <form
                                            className={styles.form}
                                            // onSubmit={handleSubmit(onSubmit)}
                                        >
                                            <div className={styles.formItem}>
                                                <label>{t("Nome completo")}</label>
                                                <input
                                                    type="text"
                                                    className={styles.input}
                                                    autoComplete="new-password"
                                                    {...registerInfo("name", {
                                                        validate : (value) =>
                                                            validateFullName(value) ||
															t(
															    "Seu nome deve possuir 8 caracteres no mínimo!",
															),
                                                    })}
                                                />
                                                {errorsInfo.name && (
                                                    <ErrorMessage message={errorsInfo.name?.message} />
                                                )}
                                            </div>

                                            <div className={styles.formItem}>
                                                <label>{t("E-mail")}</label>
                                                <input
                                                    style={{ textTransform : "lowercase" }}
                                                    type="email"
                                                    className={styles.input}
                                                    required
                                                    autoComplete="new-password"
                                                    {...registerInfo("email", {
                                                        required : t("Digite um e-mail válido!"),
                                                    })}
                                                />
                                                {errorsInfo.email && (
                                                    <ErrorMessage message={errorsInfo.email?.message} />
                                                )}
                                            </div>

                                            <div className={styles.formItem}>
                                                <label>{t("CPF")}</label>
                                                <Controller
                                                    name="cpf"
                                                    control={controlInfo}
                                                    defaultValue={user?.cpf}
                                                    rules={{
                                                        validate : (value) =>
                                                            validateCpf(value) || t("CPF inválido"),
                                                    }}
                                                    render={({ field }) => (
                                                        <PatternFormat
                                                            {...field}
                                                            format="###.###.###-##"
                                                            type="tel"
                                                            autoComplete="new-password"
                                                            allowEmptyFormatting={false}
                                                            className={styles.input}
                                                        />
                                                    )}
                                                />
                                                {errorsInfo.cpf && (
                                                    <ErrorMessage message={errorsInfo.cpf?.message} />
                                                )}
                                            </div>

                                            <div className={styles.formItem}>
                                                <label>{t("Data de nascimento")}</label>
                                                <Controller
                                                    name="birthday"
                                                    control={controlInfo}
                                                    defaultValue={user?.birthday}
                                                    rules={{
                                                        validate : (value) =>
                                                            validateDate(value) || t("Data Invalida!"),
                                                    }}
                                                    render={({ field }) => (
                                                        <PatternFormat
                                                            {...field}
                                                            format="##/##/####"
                                                            type="tel"
                                                            autoComplete="new-password"
                                                            placeholder="ex: 08/09/1994"
                                                            allowEmptyFormatting={false}
                                                            className={styles.input}
                                                            onValueChange={(values) => 
                                                            {
                                                                field.onChange(values.value);
                                                                handleBirthday(values.formattedValue);
                                                            }}
                                                        />
                                                    )}
                                                />
                                                {birthdayError && (
                                                    <ErrorMessage message={birthdayError} />
                                                )}
                                            </div>

                                            <div className={styles.formItem}>
                                                <label>{t("Celular")}</label>
                                                <Controller
                                                    name="phone"
                                                    control={controlInfo}
                                                    defaultValue={user?.phone}
                                                    rules={{
                                                        validate : (value) =>
                                                            value.length >= 15 || t("Verifique seu celular!"),
                                                    }}
                                                    render={({ field }) => (
                                                        <PatternFormat
                                                            {...field}
                                                            format="(##) #####-####"
                                                            type="tel"
                                                            autoComplete="new-password"
                                                            className={styles.input}
                                                            allowEmptyFormatting={false}
                                                            onValueChange={(values) => 
                                                            {
                                                                field.onChange(values.value); // Captura o valor sem formatação
                                                            }}
                                                        />
                                                    )}
                                                />
                                                {errorsInfo.phone && (
                                                    <ErrorMessage message={errorsInfo.phone?.message} />
                                                )}
                                            </div>

                                            <div className={styles.formItem}>
                                                <label>{t("Gênero")}</label>
                                                <Select
                                                    placeholder={t("Escolha seu gênero")}
                                                    className={styles.select}
                                                    options={genderOptions}
                                                    styles={customSelectStyles}
                                                    isClearable
                                                    value={gender}
                                                    onChange={handleChangeGender}
                                                />
                                                {isSubmittedInfo && !gender && (
                                                    <span className={styles.errorMessage}>
                                                        <Image
                                                            width={20}
                                                            height={20}
                                                            src="/assets/icons/AiOutlineExclamationCircle.svg"
                                                            loader={cloudflareLoader}
                                                            alt=""
                                                        />
                                                        {t("Campo required!")}
                                                    </span>
                                                )}
                                            </div>

                                            <button
                                                className={styles.submit}
                                                type="button"
                                                disabled={loadingUserData && true}
                                                onClick={handleSubmitInfo(onSubmit)}
                                            >
                                                {loadingUserData ? (
                                                    <AnimatedLoading />
                                                ) : (
                                                    t("Atualizar meus dados cadastrais")
                                                )}
                                            </button>
                                        </form>

                                        <form
                                            className={styles.form}
                                            // onSubmit={handleSubmit3(onSubmitUpdateAddress)}
                                        >
                                            {/* <h4>Endereço</h4> */}
                                            <div className={styles.formItem}>
                                                <label>{t("CEP")}</label>
                                                <Controller
                                                    name="address.zipcode"
                                                    control={controlAddress} // O `control` vem de `useForm()`
                                                    defaultValue={user?.address?.zipcode}
                                                    rules={{
                                                        validate : (value) =>
                                                            validateCEP(value) ||
															t("CEP deve possuir 8 caracteres!"),
                                                    }}
                                                    render={({ field }) => (
                                                        <PatternFormat
                                                            {...field}
                                                            format="#####-###"
                                                            type="tel"
                                                            placeholder="ex: 12345-678"
                                                            autoComplete="new-password"
                                                            allowEmptyFormatting={false}
                                                            className={styles.input}
                                                            onValueChange={(values) => 
                                                            {
                                                                field.onChange(values.value);

                                                                if (values.value.length === 8) 
                                                                {
                                                                    onZipcode(values.formattedValue);
                                                                }
                                                            }}
                                                        />
                                                    )}
                                                />
                                                {(errorsAddress.address as any)?.zipcode?.message && (
                                                    <ErrorMessage
                                                        message={
                                                            (errorsAddress.address as any)?.zipcode?.message
                                                        }
                                                    />
                                                )}
                                            </div>

                                            <div className={styles.formItem}>
                                                <label>{t("Rua")}</label>
                                                <input
                                                    type="text"
                                                    placeholder="ex: Rua Guaranabara"
                                                    autoComplete="new-password"
                                                    maxLength={50}
                                                    className={styles.input}
                                                    defaultValue={user?.address?.street}
                                                    {...registerAddress("address.street", {
                                                        required : t("Este campo é obrigatório!"),
                                                    })}
                                                />
                                                {(errorsAddress.address as any)?.street?.message && (
                                                    <ErrorMessage
                                                        message={
                                                            (errorsAddress.address as any)?.street?.message
                                                        }
                                                    />
                                                )}
                                            </div>

                                            <div className={styles.formItem}>
                                                <label>{t("Número")}</label>
                                                <input
                                                    type="text"
                                                    defaultValue={user?.address?.housenumber}
                                                    autoComplete="new-password"
                                                    placeholder="ex: 17"
                                                    maxLength={5}
                                                    className={styles.input}
                                                    {...registerAddress("address.housenumber", {
                                                        required : t("Este campo é obrigatório!"),
                                                    })}
                                                />
                                                {(errorsAddress.address as any)?.housenumber
                                                    ?.message && (
                                                    <ErrorMessage
                                                        message={
                                                            (errorsAddress.address as any)?.housenumber
                                                                ?.message
                                                        }
                                                    />
                                                )}
                                            </div>

                                            <div className={styles.formItem}>
                                                <label>{t("Complemento")}</label>
                                                <input
                                                    type="text"
                                                    autoComplete="new-password"
                                                    defaultValue={user?.address?.complement}
                                                    placeholder="ex: Ap 15B"
                                                    maxLength={30}
                                                    className={styles.input}
                                                    {...registerAddress("address.complement")}
                                                />
                                                {(errorsAddress.address as any)?.complement
                                                    ?.message && (
                                                    <ErrorMessage
                                                        message={
                                                            (errorsAddress.address as any)?.complement
                                                                ?.message
                                                        }
                                                    />
                                                )}
                                            </div>

                                            <div className={styles.formItem}>
                                                <label>{t("Bairro")}</label>
                                                <input
                                                    type="text"
                                                    autoComplete="new-password"
                                                    defaultValue={user?.address?.district}
                                                    maxLength={30}
                                                    className={styles.input}
                                                    placeholder="ex: Dom Bosco"
                                                    {...registerAddress("address.district", {
                                                        required : t("Este campo é obrigatório!"),
                                                    })}
                                                />
                                                {(errorsAddress.address as any)?.district?.message && (
                                                    <ErrorMessage
                                                        message={
                                                            (errorsAddress.address as any)?.district?.message
                                                        }
                                                    />
                                                )}
                                            </div>

                                            <div className={styles.formItem}>
                                                <label>{t("Cidade")}</label>
                                                <input
                                                    type="text"
                                                    placeholder="ex: São Paulo"
                                                    defaultValue={user?.address?.city}
                                                    autoComplete="new-password"
                                                    className={styles.input}
                                                    {...registerAddress("address.city", {
                                                        required : t("Este campo é obrigatório!"),
                                                    })}
                                                />
                                                {(errorsAddress.address as any)?.city?.message && (
                                                    <ErrorMessage
                                                        message={
                                                            (errorsAddress.address as any)?.city?.message
                                                        }
                                                    />
                                                )}
                                            </div>

                                            <div className={styles.formItem}>
                                                <label>{t("Estado")}</label>
                                                <input
                                                    type="text"
                                                    autoComplete="new-password"
                                                    defaultValue={user?.address?.state}
                                                    placeholder="ex: SP"
                                                    maxLength={2}
                                                    className={styles.input}
                                                    {...registerAddress("address.state", {
                                                        required : t("Este campo é obrigatório!"),
                                                    })}
                                                />
                                                {(errorsAddress.address as any)?.state?.message && (
                                                    <ErrorMessage
                                                        message={
                                                            (errorsAddress.address as any)?.state?.message
                                                        }
                                                    />
                                                )}
                                            </div>

                                            <button
                                                className={styles.submit}
                                                type="button"
                                                disabled={loadingUserUpdateAddress && true}
                                                onClick={handleSubmitAddress(onSubmitUpdateAddress)}
                                            >
                                                {loadingUserUpdateAddress ? (
                                                    <AnimatedLoading />
                                                ) : (
                                                    t("Atualizar endereço")
                                                )}
                                            </button>
                                        </form>
                                    </div>
                                </div>

                                <form className={styles.form}>
                                    <div className={styles.formItem}>
                                        <label>{t("Newsletter")}</label>
                                        <span className={styles.subtitle}>
                                            {t(
                                                "Deseja receber e-mails com promoções? Marque esta opção e atualize seus dados cadastrais",
                                            )}
											.
                                        </span>
                                        <div className={styles.checkbox}>
                                            {/* <input
                    autoComplete="new-password"
                    type="checkbox"
                    defaultChecked={user?.newsletter}
                    {...register("newsletter")}
                  /> */}
                                            <Checkbox
                                                color="success"
                                                checked={hasNewsletter}
                                                onChange={onNewsletter}
                                            >
                                                {t("Quero receber notificações via e-mails")}
                                            </Checkbox>
                                        </div>

                                        <div>
                                            <Checkbox
                                                color="success"
                                                checked={hasWhatsapp}
                                                onChange={onWhatsapp}
                                            >
                                                {t("Quero receber notificações via whatsapp")}
                                            </Checkbox>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
};

export { AccountDisplay };
