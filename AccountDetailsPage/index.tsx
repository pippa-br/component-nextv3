import { AccountDisplay } from "./account.display";

const GenerateMetadata = async () => 
{
    return {
        title : "Meus Dados",
    };
};

const AccountDetailsPage = () => 
{
    return <AccountDisplay />;
};

// const getStaticProps: GetStaticProps = () =>
//     withHeader(async (props: any) =>
//     {
//         return {
//             revalidate : THEME_SETTING.revalidate,
//             props      : {
//                 seo : props.seo.merge({ title : "Detalhes da Conta" }),
//             },
//         }
//     })

export { AccountDetailsPage, GenerateMetadata };
