import Marquee from "react-fast-marquee";
import styles from "./styles.module.scss";

export const CarouselTopHeader = ({ topHeader }: any) => 
{
    return (
        topHeader?.published === true && (
            <section className={styles.carouselTopHeader}>
                <Marquee className={styles.marquee}>
                    {topHeader?.list?.map((item: any, index: any) => (
                        <div
                            className={`${styles.item} ${topHeader?.list?.length - 1 === index ? styles.last : ""}`}
                            key={index}
                        >
                            {item?.name}
                        </div>
                    ))}
                </Marquee>
                {/* {isPlaying ? (
          <BsPause onClick={() => setIsPlaying(!isPlaying)} />
        ) : (
          <BsPlay onClick={() => setIsPlaying(!isPlaying)} />
        )} */}

                {/* <div className={styles.features}>
          <div className={`${styles.marquee} ${!isPlaying && styles.pause}`}>
            
          </div>
        </div> */}
            </section>
        )
    );
};
