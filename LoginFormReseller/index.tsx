import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { userLoginAnalytics } from "../../core-nextv3/analytics/analytics.api";
import {
    loginAuth,
    recoveryPasswordAuth,
} from "../../core-nextv3/auth/auth.api";
import { mergeCart } from "../../core-nextv3/cart/cart.api";
import { useOnEvent } from "../../core-nextv3/util/use.event";
import { CART_SETTING, RESELLER_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import ErrorMessage from "../ErrorMessage";
import styles from "./styles.module.scss";

export default function LoginFormReseller() 
{
    const router = useRouter();

    const {
        register,
        formState: { errors },
        handleSubmit,
    } = useForm();

    const [ cart, setCart ] = useState<any>();
    const [ animateLoading, setAnimateLoading ] = useState(false);
    const [ formType, setFormType ] = useState("login");
    const [ email, setEmail ] = useState("");

    useOnEvent("changeCart", (data: any) => 
    {
        setCart(data);
    });

    async function recoveryPassword() 
    {
        const result = await recoveryPasswordAuth(
            RESELLER_SETTING.merge({
                login : email.toLowerCase(),
            }),
        );

        if (!result.status) 
        {
            return toast.error(result.error, { duration : 2000 });
        }

        toast(
            (t: any) => (
                <span>
                    {result.message}
                    <button onClick={() => toast.dismiss(t.id)}>Ok</button>
                </span>
            ),
            {
                className : "toast-custom",
                duration  : 999999,
            },
        );

        setFormType("login");
    }

    async function handleLoginWithEmailAndPassword(formData: any) 
    {
        setAnimateLoading(true);

        const result = await loginAuth(
            RESELLER_SETTING.merge({
                login    : formData.login.toLowerCase(),
                password : formData.password,
            }),
        );

        // MERGE CART SESSION
        await mergeCart(
            CART_SETTING.merge({
                document : {
                    referencePath : cart?.referencePath,
                },
            }),
        );

        setAnimateLoading(false);

        if (!result.status) 
        {
            return toast.error(result.error || "Erro ao fazer login.", {
                duration : 2000,
            });
        }

        toast("Login feito com sucesso!", { icon : "👏", duration : 2000 });

        // LOGIN ANALYTICS
        userLoginAnalytics(result.data);

        router.push("/produtos/");
    }

    return (
        <div className={styles.container}>
            <div className={styles.content}>
                {formType === "recoveryEmail" && (
                    <>
                        <div className={styles.title}>Por favor informe seu email:</div>
                        <div className={styles.inputControl}>
                            <input
                                style={{ textTransform : "lowercase" }}
                                onChange={(e: any) => setEmail(e.target.value)}
                                type="email"
                                name="email"
                                placeholder="Digite o seu email"
                                onKeyUp={(e) => 
                                {
                                    if (e.code === "Enter") 
                                    {
                                        setFormType("login");
                                        recoveryPassword();
                                    }
                                }}
                            />
                            {errors.email && <ErrorMessage message={errors.email.message} />}
                        </div>
                        <div className={styles.buttons}>
                            <button
                                className={styles.submitted}
                                onClick={() => 
                                {
                                    recoveryPassword();
                                }}
                                type="button"
                            >
								Recuperar Senha
                            </button>
                            <button
                                type="button"
                                onClick={() => 
                                {
                                    setFormType("login");
                                }}
                            >
								voltar
                            </button>
                        </div>
                    </>
                )}

                {formType === "login" && (
                    <>
                        <div className={styles.title}>
							Por favor informe seu email e senha:
                        </div>
                        <div className={styles.inputControl}>
                            <input
                                style={{ textTransform : "lowercase" }}
                                type="email"
                                placeholder="Email"
                                {...register("login", {
                                    required : "Este campo é obrigatório!",
                                    pattern  : {
                                        value   : /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                        message : "Email inválido",
                                    },
                                })}
                            />
                            {errors.login && <ErrorMessage message={errors.login.message} />}
                        </div>
                        <div className={styles.inputControl}>
                            <input
                                {...register("password", {
                                    required  : "Este campo é obrigatório!",
                                    minLength : {
                                        value   : 8,
                                        message : "Sua senha possui, no mínimo, 8 caracteres.",
                                    },
                                })}
                                type="password"
                                placeholder="Senha"
                            />
                            {errors.password && (
                                <ErrorMessage message={errors.password.message} />
                            )}
                        </div>
                        <div className={styles.inputControl}>
                            <a onClick={() => setFormType("recoveryEmail")}>
								Esqueceu sua senha?
                            </a>
                        </div>
                        <div className={styles.buttons}>
                            <button
                                className={styles.submitted}
                                onClick={handleSubmit(handleLoginWithEmailAndPassword)}
                                type="button"
                            >
                                {animateLoading ? <AnimatedLoading /> : "Entrar"}
                            </button>
                            <button
                                type="button"
                                onClick={() => router.push("/revendedor/cadastro/")}
                            >
								Criar Conta
                            </button>
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
