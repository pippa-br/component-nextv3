import { useCore } from "@/core-nextv3/core/core";
import { cloudflareLoader } from "@/core-nextv3/util/util";
import { useTranslations } from "next-intl";
import Image from "next/image";
import { useEffect, useState } from "react";
import { THEME_SETTING } from "../../setting/setting";
import ErrorMessage from "../ErrorMessage";
import styles from "./styles.module.scss";

export const CheckoutShippingTwo = ({
    updateShipping,
    watch,
    errorsInfo,
    isSubmitted,
}: any) => 
{
    const { cart } = useCore();
    const t = useTranslations();
    const [ selectedShipping, setSelectedShipping ] = useState<any>(cart?.shipping);
    const watchedShipping = watch("shipping");

    useEffect(() => 
    {
        setSelectedShipping(watchedShipping);
    }, [ watchedShipping ]);

    return (
        <>
            {cart?.shippingMethods?.full && (
                <div className={styles.checkoutShippingTwo}>
                    <div className={styles.shippingOptions}>
                        {cart && (
                            <div className={styles.formHeader}>
                                <p className={styles.title}>{t("Método de entrega")}</p>
                                <p className={styles.subtitle}>
                                    {t("Selecione a opção de entrega ideal para você")}
                                </p>
                            </div>
                        )}

                        {THEME_SETTING.noteShipping && (
                            <p className={styles.message}>{THEME_SETTING.noteShipping}</p>
                        )}

                        <div className={styles.shippingMethods}>
                            {cart?.shippingMethods?.full?.map((shipping: any, index: any) => (
                                <p
                                    key={index}
                                    onClick={() => 
                                    {
                                        setSelectedShipping(shipping);
                                        updateShipping(shipping);
                                    }}
                                    className={
                                        shipping?.id === selectedShipping?.id
                                            ? `${styles.shippingOption} ${styles.active}`
                                            : styles.shippingOption
                                    }
                                >
                                    <span>
                                        <Image
                                            className={styles.noCheck}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/BsCircle.svg"
                                            loader={cloudflareLoader}
                                            alt=""
                                        />
                                        <Image
                                            className={styles.check}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiOutlineCheckCircle.svg"
                                            loader={cloudflareLoader}
                                            alt=""
                                        />
                                    </span>
                                    <span>
                                        {shipping?.label}{" "}
                                        {shipping?.note && <small> - {shipping?.note}</small>}
                                    </span>
                                </p>
                            ))}
                        </div>
                    </div>

                    {isSubmitted && errorsInfo?.shipping && (
                        <ErrorMessage message={"Selecione uma das formas de entrega!"} />
                    )}
                </div>
            )}
        </>
    );
};
