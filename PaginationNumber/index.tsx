"use client";

import { customImageLoader } from "@/core-nextv3/util/util";
import Image from "next/image";
import { useRouter, useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import styles from "./styles.module.scss";

export function PaginationNumber({
    total = 0,
    maxVisiblePages = 10,
    perPage = 24,
    page = 1,
    onPaging,
    infoPages = true,
    gotoPages = false,
}: any) 
{
    const router = useRouter();
    const searchParams = useSearchParams();
    const [ pages, setPages ] = useState<any>([]);
    const [ totalPage, setTotalPage ] = useState(0);

    const changePaging = (page: number) => 
    {
        const params = new URLSearchParams(searchParams.toString());

        params.set("page", page.toString());
        router.push(`?${params.toString()}`);

        onPaging(page);
    };

    useEffect(() => 
    {
        const totalPage = Math.ceil(total / perPage);
        let startPage = Math.max(1, page - Math.floor(maxVisiblePages / 2));

        if (startPage + maxVisiblePages - 1 > totalPage) 
        {
            startPage = Math.max(1, totalPage - maxVisiblePages + 1);
        }

        const endPage = Math.min(startPage + maxVisiblePages - 1, totalPage);
        const pages = Array.from(
            { length : endPage - startPage + 1 },
            (_, index) => startPage + index,
        );

        setPages(pages);
        setTotalPage(totalPage);
    }, [ total, page ]);

    const gotoFirstPage = () => 
    {
        onPaging(1);
    };

    const gotoLastPage = () => 
    {
        onPaging(totalPage);
    };

    return (
        <div className={styles.container}>
            <div className={styles.info} />
            <div className={styles.pages}>
                {gotoPages && page > 1 && (
                    <Image
                        onClick={() => gotoFirstPage()}
                        width={20}
                        height={20}
                        src="/assets/icons/MdKeyboardDoubleArrowLeft.svg"
                        alt=""
                        loader={customImageLoader}
                    />
                )}
                <div className={styles.pageNumbers}>
                    {pages.map((item: any, index: number) => (
                        <a
                            onClick={() => changePaging(item)}
                            key={index}
                            className={page === item ? styles.selected : ""}
                        >
                            {item}
                        </a>
                    ))}
                </div>
                {gotoPages && page !== totalPage && totalPage > 0 && (
                    <Image
                        onClick={() => gotoLastPage()}
                        width={20}
                        height={20}
                        src="/assets/icons/MdKeyboardDoubleArrowRight.svg"
                        alt=""
                        loader={customImageLoader}
                    />
                )}
            </div>
            <div className={styles.infoPages}>
                {infoPages && (
                    <>
                        <span>
							Página {page} de {totalPage}
                        </span>
                        <small>{total} Iten(s)</small>
                    </>
                )}
            </div>
        </div>
    );
}
