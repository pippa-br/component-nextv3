import type { GetStaticProps } from "next";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import { useState } from "react";
import ReactStars from "react-stars";
import { firstImageItemCart } from "../../core-nextv3/cart/cart.util";
import {
    collectionDocument,
    getDocument,
} from "../../core-nextv3/document/document.api";
import { calls } from "../../core-nextv3/util/call.api";
import { ORDER_EXPRESS_SETTING, THEME_SETTING } from "../../setting/setting";
import withHeader from "../../utils/withHeader";
import { CommentModal } from "../CommentModal";
import { ImageSet } from "../ImageSet";
import { PageTitle } from "../PageTitle";
import styles from "./styles.module.scss";

const AvaliationExpressPage = ({ order }: any) => 
{
    const { query } = useRouter();
    const [ selected, setSelected ] = useState();
    const [ assessment, setAssessment ] = useState(false);
    const { t } = useTranslation();

    return (
        <>
            <div className={styles.avaliation}>
                <div className={styles.content}>
                    <PageTitle
                        parents={[
                            {
                                url  : `/pedido-express/${query.id}`,
                                name : t(`Pedido: #${order._sequence}`),
                            },
                        ]}
                        name={"Avaliar Express"}
                        noTitle={true}
                    />

                    <p className={styles.text}>
						Valorizamos muito a opinião dos nossos clientes! Sua experiência é
						fundamental para nós.
                    </p>
                    <div className={styles.products}>
                        {order?.items?.map((item: any) => (
                            <div key={item.id} className={styles.item}>
                                {/* {reorderBadge(item)} */}
                                <div className={styles.product}>
                                    <div className={styles.img}>
                                        <ImageSet image={firstImageItemCart(item)} />
                                    </div>
                                    <div>
                                        <div className={styles.productName}>{item?.name}</div>
                                        <div className={styles.productDetails}>
                                            {item?.variant?.map((variant: any) => (
                                                <span key={variant?.id}>{variant?.label}&nbsp;</span>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                                {/* <div className={styles.quantity}>
                                        <span className={styles.label}>{t("Quantidade")}</span>
                                        <span className={styles.value}>
                                            {item.quantity}
                                        </span>
                                    </div>
                                    <div className={styles.price}>
                                        <span className={styles.label}>{t("Preço")}</span>
                                        <span className={styles.value}>
                                            {currencyMask(item?.price)}
                                        </span>
                                    </div>
                                    <div className={styles.price}>
                                        <span className={styles.label}>{t("Desconto")}</span>
                                        <span className={styles.value}>
                                            {currencyMask(item?.discountTotal || 0)}
                                        </span>										
                                    </div>
                                    <div className={styles.price}>
                                        <span className={styles.label}>{t("Valor Pago")}</span>
                                        <span className={styles.value}>
                                            {currencyMask(item?.total + (item?.discountTotal || 0))}
                                        </span>
                                    </div> */}
                                {order?.statusPayment === "Pago" ? (
                                    <div className={styles.btn}>
                                        <ReactStars
                                            count={5}
                                            // onChange={ratingChanged}
                                            edit={false}
                                            size={23}
                                            //activeColor={false}
                                            //isHalf={true}
                                        />
                                        <button
                                            onClick={() => 
                                            {
                                                setAssessment(true);
                                                setSelected(item);
                                            }}
                                        >
                                            {t("Avaliar Produto")}
                                        </button>
                                    </div>
                                ) : null}
                            </div>
                        ))}
                    </div>
                </div>
            </div>

            {assessment && (
                <CommentModal
                    order={order}
                    item={selected}
                    setAssessment={setAssessment}
                />
            )}
        </>
    );
};

const getStaticProps: GetStaticProps = ({ params }) =>
    withHeader(async (props: any) => 
    {
        const [ order ] = await calls(
            getDocument(
                ORDER_EXPRESS_SETTING.merge({
                    id : params?.id,
                }),
            ),
        );

        if (!order.status) 
        {
            return {
                notFound   : !order.serverError,
                revalidate : true,
            };
        }
        // const promises = [];

        // if(order.data?.trackingCode)
        // {
        //     promises.push(getTrackCorreios(
        //         TRACK_SETTING.merge({
        //             code : order.data?.trackingCode,
        //         })
        //     ));
        // }
        // else
        // {
        //     promises.push({});
        // }

        // const setting = GATEWAY_SETTING.merge({
        //     subTotal      : order.data.totalItems,
        //     totalDiscount : order.data.totalDiscount,
        //     totalShipping : order.data.totalShipping,
        // });

        // promises.push(getInstallmentsPagarme(setting));
        // promises.push(getDocument(PAYMENT_METHODS_OPTION_SETTING));
        // promises.push(getDocument(PAGE_PIX_SETTING));
        // promises.push(getDocument(TRACK_BACK_PAGE_SETTING));
        // promises.push(getDocument(ORDER_PAGE_SETTING));

        // const [orderTrack, installments, paymentMethods, phrasePixResult, trackBackResult, orderPage] = await calls(...promises);

        return {
            revalidate : THEME_SETTING.revalidate,
            props      : {
                seo   : props?.seo?.merge({ title : "Pedido" }) || {},
                order : order.data,
                // countDownPage  : phrasePixResult?.data || {},
                // orderTrack     : orderTrack?.data || {},
                // paymentMethods : paymentMethods?.data?.items || [],
                // installments   : installments?.collection || [],
                // stausBarPage   : trackBackResult?.data || {},
                // orderPage      : orderPage?.data || {},
            },
        };
    });

const getStaticPaths = async () => 
{
    const order = await collectionDocument(
        ORDER_EXPRESS_SETTING.merge({
            perPage : 1,
        }),
    );

    let paths = [];

    if (order.collection) 
    {
        paths = order.collection.map((item: any) => ({
            params : { id : item.id },
        }));
    }

    return { paths, fallback : "blocking" };
};

export {
    getStaticProps as GetStaticProps,
    getStaticPaths as GetStaticPaths,
    AvaliationExpressPage,
};
