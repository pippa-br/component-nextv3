import styles from "./styles.module.scss";

const Maintenance = () => 
{
    return (
        <main className={styles.maintenance}>
            <div className={styles.info}>
                <h1>PÁGINA EM MANUTENÇÃO</h1>
                <p>Agradecemos pela sua visita!</p>
                <p>
					Nossa página está passando por uma breve manutenção para aprimorar sua
					experiência.
                </p>
                <p>
					Estamos trabalhando arduamente nos bastidores para trazer a você uma
					versão ainda melhor do nosso site.
                </p>
            </div>
        </main>
    );
};

export { Maintenance };
