import { Suspense } from "react";
import { AvaliationDisplay } from "./avaliation.display";

// const GenerateStaticParams = async () =>
// {
//     const pages = await collectionDocument(ORDER_SETTING.merge({
//         perPage : 1,
//     }));

//     let paths = [];

//     if (pages.collection)
//     {
//         paths = pages.collection.map((item:any) => ({
//             params : { id : item.id || "" },
//         }));
//     }

//     console.info("(Avaliation Static:", paths.length, ")");

//     return paths;
// }

const GenerateMetadata = async () => 
{
    return {
        title : "Avaliar",
    };
};

const AvaliationPage = async ({
    params,
}: {
	params: Promise<{ id: string }>;
}) => 
{
    const { id } = await params;

    return (
        <Suspense>
            <AvaliationDisplay id={id} />
        </Suspense>
    );
};

export { AvaliationPage, GenerateMetadata };
