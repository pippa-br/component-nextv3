"use client";

import { useGetDocument } from "@/core-nextv3/document/document.use";
import { Rating } from "@smastrom/react-rating";
import { useTranslations } from "next-intl";
import { usePathname } from "next/navigation";
import { Suspense, useState } from "react";
import { firstImageItemCart } from "../../core-nextv3/cart/cart.util";
import { firstName } from "../../core-nextv3/util/util";
import { ORDER_SETTING, THEME_SETTING } from "../../setting/setting";
import { CommentModal } from "../CommentModal";
import { ImageSet } from "../ImageSet";
import { LoadingPage } from "../LoadingPage";
import { PageTitle } from "../PageTitle";
import styles from "./styles.module.scss";

const AvaliationDisplay = ({ id }: any) => 
{
    const pathname = usePathname();
    const [ selected, setSelected ] = useState();
    const [ assessment, setAssessment ] = useState(false);
    const [ order, setOrder ] = useState<any>();
    const t = useTranslations();

    useGetDocument(ORDER_SETTING.merge({ id : id }), (data: any) => 
    {
        setOrder(data);
    });

    if (!order) 
    {
        return <LoadingPage />;
    }

    return (
        <Suspense>
            <div className={`${styles.avaliation} page`}>
                <div className={styles.content}>
                    <PageTitle
                        parents={[
                            {
                                url  : pathname.replace("avaliar", "pedido"),
                                name : t(`Pedido: #${order._sequence}`),
                            },
                        ]}
                        name={"Avaliar"}
                        noTitle={true}
                    />

                    <p className={styles.text}>
						Olá {firstName(order?.client?.name)}, Valorizamos muito sua opinião!
						Sua experiência é fundamental para nós.
                    </p>

                    <div className={styles.products}>
                        {order?.items?.map((item: any) => (
                            <div key={item.id} className={styles.item}>
                                {/* {reorderBadge(item)} */}
                                <div className={styles.product}>
                                    <div className={styles.img}>
                                        <ImageSet
                                            image={firstImageItemCart(item)}
                                            width={THEME_SETTING.widthProductThumb}
                                            height={THEME_SETTING.heightProductThumb}
                                            sizes="25vw"
                                        />
                                    </div>
                                    <div>
                                        <div className={styles.productName}>{item?.name}</div>
                                        <div className={styles.productDetails}>
                                            {item?.variant && item?.variant?.map((variant: any) => (
                                                <span key={variant?.id}>{variant?.label}&nbsp;</span>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                                {/* <div className={styles.quantity}>
                                            <span className={styles.label}>{t("Quantidade")}</span>
                                            <span className={styles.value}>
                                                {item.quantity}
                                            </span>
                                        </div>
                                        <div className={styles.price}>
                                            <span className={styles.label}>{t("Preço")}</span>
                                            <span className={styles.value}>
                                                {currencyMask(item?.price)}
                                            </span>
                                        </div>
                                        <div className={styles.price}>
                                            <span className={styles.label}>{t("Desconto")}</span>
                                            <span className={styles.value}>
                                                {currencyMask(item?.discountTotal || 0)}
                                            </span>										
                                        </div>
                                        <div className={styles.price}>
                                            <span className={styles.label}>{t("Valor Pago")}</span>
                                            <span className={styles.value}>
                                                {currencyMask(item?.total + (item?.discountTotal || 0))}
                                            </span>
                                        </div> */}
                                {order?.statusPayment === "Pago" ? (
                                    <div className={styles.btn}>
                                        <Rating
                                            style={{ maxWidth : 100 }}
                                            readOnly={true}
                                            value={5}
                                        />
                                        <button
                                            onClick={() => 
                                            {
                                                setAssessment(true);
                                                setSelected(item);
                                            }}
                                        >
                                            {t("O que você achou desse produto?")}
                                        </button>
                                    </div>
                                ) : null}
                            </div>
                        ))}
                    </div>
                </div>
            </div>

            {assessment && (
                <CommentModal
                    order={order}
                    item={selected}
                    setAssessment={setAssessment}
                />
            )}
        </Suspense>
    );
};

export { AvaliationDisplay };
