"use client";

import { useRouter } from "next/navigation";
import { useEffect } from "react";
import styles from "./styles.module.scss";

const Custom404 = ({ redirect = false }) => 
{
    const router = useRouter();

    useEffect(() => 
    {
        if (redirect) 
        {
            router.push("/");
        }
    }, []);

    if (!redirect) 
    {
        return (
            <main className={styles.page404}>
                <div className={styles.info}>
                    <h1>PÁGINA NÃO ENCONTRADA</h1>
                    <p>
						LAMENTAMOS, MAS ESTA PÁGINA JÁ NÃO ESTÁ DISPONÍVEL NO NOSSO SITE.
                    </p>
                    <button onClick={() => router.push("/")}>
						Voltar à página principal
                    </button>
                </div>
            </main>
        );
    }
};

export { Custom404 };
