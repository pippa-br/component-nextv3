import styles from "./styles.module.scss";

const LoadingFallback = () => (
    <div className={styles.loadingDots}>
        <span />
        <span />
        <span />
    </div>
);

export default LoadingFallback;
