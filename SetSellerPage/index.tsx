import { useSetSellerCart } from "@/core-nextv3/cart/cart.use";
import { cloudflareLoader } from "@/core-nextv3/util/util";
import type { GetStaticProps } from "next";
import Image from "next/image";
import { useRouter } from "next/router";
import {
    collectionDocument,
    getDocument,
} from "../../core-nextv3/document/document.api";
import {
    CART_SETTING,
    SELLET_SETTING,
    THEME_SETTING,
} from "../../setting/setting";
import withHeader from "../../utils/withHeader";
import { AnimatedLoading } from "../AnimatedLoading";
import styles from "./styles.module.scss";

const SetSellerPage = ({ seller }: any) => 
{
    const router = useRouter();

    //console.error('coupon', coupon)

    useSetSellerCart(
        CART_SETTING.merge({
            document : {
                referencePath : seller.referencePath,
            },
        }),
        () => 
        {
            setTimeout(() => 
            {
                router.push("/");
            }, 1000);
        },
    );

    return (
        <div className={styles.mergeCartPage}>
            <div className={styles.content}>
                <Image
                    width={20}
                    height={20}
                    src="/assets/icons/RiCouponLine.svg"
                    loader={cloudflareLoader}
                    alt=""
                />
                <br />
                <br />
                <AnimatedLoading />
            </div>
        </div>
    );
};

const getStaticProps: GetStaticProps = ({ params }: any) =>
    withHeader(async (props: any) => 
    {
        const couponResult = await getDocument(
            SELLET_SETTING.merge({
                slug : params.code.replace(/\s/g, ""),
            }),
        );

        if (!couponResult.status) 
        {
            return {
                notFound   : !couponResult.serverError,
                revalidate : true,
            };
        }

        return {
            props : {
                seo    : props.seo.mergeProduct(couponResult.data),
                seller : couponResult.data,
            },
            revalidate : THEME_SETTING.revalidate,
        };
    });

const getStaticPaths = async () => 
{
    const result = await collectionDocument(
        SELLET_SETTING.merge({
            where : [
                {
                    field    : "status",
                    operator : "==",
                    value    : true,
                },
            ],
        }),
    );

    const paths = result.collection.map((item: any) => ({
        params : { code : item.code },
    }));

    return { paths, fallback : "blocking" };
};

export {
    getStaticProps as GetStaticProps,
    getStaticPaths as GetStaticPaths,
    SetSellerPage,
};
