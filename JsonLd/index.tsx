import { stripHtmlTags } from "@/core-nextv3/util/util";

const JsonLd = ({ name, description, price, images, url, videos }: any) => 
{
    const jsonLd: any = {
        "@context"  : "https://schema.org",
        "@type"     : "Product",
        name        : name,
        image       : images.map((item: any) => item._url),
        description : stripHtmlTags(description),
        offers      : {
            "@type"       : "Offer",
            price         : price,
            priceCurrency : "BRL",
            availability  : "https://schema.org/InStock",
            url           : url,
        },
    };

    if (videos && images && videos.length > 0 && images.length > 0) 
    {
        jsonLd.video = {
            "@type"      : "VideoObject",
            name         : name,
            description  : stripHtmlTags(description),
            thumbnailUrl : images[0]._url,
            contentUrl   : videos[0]._url,
            embedUrl     : videos[0]._url,
        };
    }

    return (
        <script
            type="application/ld+json"
            dangerouslySetInnerHTML={{ __html : JSON.stringify(jsonLd) }}
        />
    );
};

export default JsonLd;
