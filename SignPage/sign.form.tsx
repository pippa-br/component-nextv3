"use client";

import { useProtectedAuth } from "@/core-nextv3/auth/user.auth.api";
import { useGetUTMParameters } from "@/core-nextv3/util/util.use";
import { useTranslations } from "next-intl";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { Checkbox } from "pretty-checkbox-react";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { PatternFormat } from "react-number-format";
import Select from "react-select";
import { userLoginAnalytics } from "../../core-nextv3/analytics/analytics.api";
import { addUserAuth, loginAuth } from "../../core-nextv3/auth/auth.api";
import { mergeCart } from "../../core-nextv3/cart/cart.api";
import { useCore } from "../../core-nextv3/core/core";
import { TDate } from "../../core-nextv3/model/TDate";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import {
    buscaCep2,
    cloudflareLoader,
    getCnpjInfo,
    getRecaptcha,
    innerHTML,
    randomNumber,
} from "../../core-nextv3/util/util";
import { customSelectStyles } from "../../core-nextv3/util/util.style";
import {
    validateCEP,
    validateCNPJ,
    validateDate,
    validateEmail,
    validateFullName,
    validatePhone,
} from "../../core-nextv3/util/validate";
import {
    AUTH_SETTING,
    CART_SETTING,
    THEME_SETTING,
} from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import CustomLink from "../CustomLink";
import ErrorMessage from "../ErrorMessage";
import { LoadingPage } from "../LoadingPage";
import { PageTitle } from "../PageTitle";
import styles from "./styles.module.scss";

const SignForm = ({ signPage }: any) => 
{
    // console.log("vl~skdnv", registrationInformation)
    const { cart, cnpjWs, setCnpjWs, user, setCart, setUser } = useCore();
    const router = useRouter();
    const [ gender, setGender ] = useState<any>();
    const [ utm, setUTM ] = useState<any>(null);
    const [ typeStore, setTypeStore ] = useState<any>(null);
    const [ hasWhatsapp, setHasWhatsapp ] = useState(false);
    //const [ passwordType, setPasswordType ]                     = useState("password")
    const [ hasNewsletter, setHasNewsletter ] = useState(false);
    const [ loading, setLoading ] = useState(false);
    const [ typePersonFormSelected, setTypePersonFormSelected ] =
		useState<any>("F");
    const [ birthday, setBirthday ] = useState<any>(null);
    const t = useTranslations();
    const {
        register,
        handleSubmit,
        setValue,
        control,
        //watch: watch,
        formState: { errors, isSubmitted },
    } = useForm();

    // const password   = useRef({})
    // password.current = watch("password", "")

    useEffect(() => 
    {
        register("gender", { value : null });

        if (THEME_SETTING.registrationLoginTypeForm === "CPFCNPJ") 
        {
            setTypePersonFormSelected("F");
        }
        else if (THEME_SETTING.registrationLoginTypeForm === "CNPJ") 
        {
            setTypePersonFormSelected("J");
        }
        else 
        {
            setTypePersonFormSelected("F");
        }
    }, []);

    // useOnEvent("changeCart", (data: any) =>
    // {
    //     setCart(data);
    // });

    const typesPersonsForm = [
        { label : "Pessoa Fisica", value : "F", id : "F" },
        { label : "Pessoa Jurídica", value : "J", id : "J" },
    ];

    const genderOptions = [
        { label : "Não Quero Informar", value : "I", id : "I" },
        { label : "Masculino", value : "M", id : "M" },
        { label : "Feminino", value : "F", id : "F" },
    ];

    const storeOptions = [
        { label : "Loja Física", value : "F", id : "F" },
        { label : "Loja Virtual", value : "V", id : "V" },
        { label : "Ambas", value : "AB", id : "AB" },
    ];

    function handleChangeGender(e: any) 
    {
        setValue("gender", e);
        setGender(e);
    }

    function handleChangeTypeStore(e: any) 
    {
        setValue("typeStore", e);
        setTypeStore(e);
    }

    useGetUTMParameters((data: any) => 
    {
        setUTM(data);
    });

    const handleBirthday = (value: any) => 
    {
        setBirthday(null);

        if (value?.split("_")?.length === 1) 
        {
            const testBirthday = new TDate({ value : value, mask : "dd/MM/yyyy" });

            if (testBirthday.isValid()) 
            {
                setBirthday(value);
            }
        }

        setValue("birthday", value);
    };

    const onSubmit = async (data: any) => 
    {
        let token = await getRecaptcha("addUser");

        if (token) 
        {
            await setLoading(true);

            const birthday = new TDate({ value : data.birthday, mask : "dd/MM/yyyy" });

            if (!birthday.isValid()) 
            {
                await setLoading(false);
                return toast.error(t("Data de aniversário inválida!"), {
                    duration : 2000,
                });
            }

            const password = randomNumber(8);

            // CADASTRO
            const newData: any = {
                token : token,
                data  : {
                    // typePersonFormSelected : data.typePersonFormSelected,
                    name            : data.name,
                    email           : data.email,
                    phone           : data.phone,
                    newsletter      : hasNewsletter,
                    onWhatsapp      : hasWhatsapp,
                    instagram       : data.instagram,
                    typeStore       : data.typeStore,
                    newClient       : data.newClient,
                    newPassword     : password,
                    password        : password,
                    confirmPassword : password,
                    utm             : utm,
                },
            };

            if (typePersonFormSelected === "J") 
            {
                newData.data.personType = {
                    label : "Pessoa Jurídica",
                    value : "J",
                    id    : "J",
                };
                //newData.data.cnpj             = data?.cnpj
                newData.data.storeName = data.storeName;
                newData.data.stateinscription = data?.stateinscription;
            }
            else 
            {
                newData.data.personType = {
                    label : "Pessoa Fisica",
                    value : "F",
                    id    : "F",
                };
                //newData.data.cpf        = data?.cpf
                newData.data.birthday = birthday.toDate();
                newData.data.gender = data?.gender;
            }

            if (THEME_SETTING.hasAuthorization) 
            {
                newData.data.authorization = {
                    value : "waiting",
                    label : "Em Análise",
                    id    : "waiting",
                };
            }

            if (THEME_SETTING.disabledAddressUser) 
            {
                newData.data.address = data.address;
            }

            const result: any = await addUserAuth(AUTH_SETTING.merge(newData));

            tagManager4.signUp("form");

            if (!result.status) 
            {
                await setLoading(false);
                return toast.error(result.error, { duration : 2000 });
            }

            token = await getRecaptcha("login");

            // LOGIN
            const result2 = await loginAuth(
                AUTH_SETTING.merge({
                    token    : token,
                    login    : data.email.toLowerCase(),
                    password : password,
                }),
            );

            if (result2.status) 
            {
                setUser(result2.data);
            }
            else 
            {
                await setLoading(false);
                return toast.error(result2.error, { duration : 2000 });
            }

            // MERGE CART SESSION
            const resultMerge = await mergeCart(
                CART_SETTING.merge({
                    document : {
                        referencePath : cart?.referencePath,
                    },
                }),
            );

            if (resultMerge.status) 
            {
                setCart(resultMerge.data);
            }

            toast(t("Cadastro realizado com sucesso!"), {
                icon     : "👏",
                duration : 2000,
            });

            // LOGIN ANALYTICS
            userLoginAnalytics(result2.data);

            await setLoading(false);

            if (resultMerge.data?.items && resultMerge.data.items.length > 0) 
            {
                router.push("/checkout/");
            }
            else 
            {
                router.push("/");
            }
        }
        else 
        {
            console.error("token invalido!", token);
        }
    };

    const onNewsletter = async () => 
    {
        setHasNewsletter(!hasNewsletter);
    };

    const onWhatsapp = async () => 
    {
        setHasWhatsapp(!hasWhatsapp);
    };

    const onZipcode = async (value: any) => 
    {
        const data: any = await buscaCep2(value);
        setValue("address", data);
    };

    const loadProtectedAuth = useProtectedAuth(AUTH_SETTING, "/", "");

    if (!loadProtectedAuth) 
    {
        return <LoadingPage />;
    }

    return (
        <>
            <div className={`${styles.signPage} page`}>
                <div className={styles.content}>
                    <div className={styles.message}>
                        <div
                            className={styles.pageContent}
                            dangerouslySetInnerHTML={innerHTML(signPage?.content)}
                        />
                    </div>
                    <PageTitle
                        name={"Criar Conta"}
                        parents={[ { name : t("Login"), url : "/login" } ]}
                    />
                    <div className={styles.formData}>
                        <div className={styles.formContent}>
                            <div className={styles.columns}>
                                <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                                    {THEME_SETTING.registrationLoginTypeForm === "CPFCNPJ" && (
                                        <div className={styles.buttonsTop}>
                                            {typesPersonsForm?.map((typePerson: any, index: any) => (
                                                <button
                                                    key={index}
                                                    onClick={() =>
                                                        setTypePersonFormSelected(typePerson?.value)
                                                    }
                                                    className={`${
                                                        typePerson?.value === typePersonFormSelected
                                                            ? styles.buttonSelected
                                                            : ""
                                                    }`}
                                                >
                                                    {typePerson?.label}
                                                </button>
                                            ))}
                                        </div>
                                    )}

                                    <div className={styles.formImputs}>
                                        <div className={styles.formItem}>
                                            <label>{t("Nome completo")}</label>
                                            <input
                                                type="text"
                                                className={styles.input}
                                                autoComplete="new-password"
                                                placeholder="ex: maria jose"
                                                {...register("name", {
                                                    validate : (value) =>
                                                        validateFullName(value) || t("Nome Completo"),
                                                })}
                                            />
                                            {errors.name && (
                                                <ErrorMessage message={errors.name?.message} />
                                            )}
                                        </div>

                                        <div className={styles.formItem}>
                                            <label>{t("E-mail")}</label>
                                            <input
                                                style={{ textTransform : "lowercase" }}
                                                type="email"
                                                placeholder="ex: maria.jose@gmail.com"
                                                className={styles.input}
                                                required
                                                autoComplete="new-password"
                                                {...register("email", {
                                                    validate : (value) =>
                                                        validateEmail(value) || t("E-mail inválido"),
                                                })}
                                            />
                                            {errors.email && (
                                                <ErrorMessage message={errors.email?.message} />
                                            )}
                                        </div>

                                        {/* {typePersonFormSelected === "F" && (
                                            <div className={styles.formItem}>
                                                <label>{t("CPF")}</label>
                                                <InputMask
                                                    {...register("cpf", {
                                                        validate : (value) =>
                                                            validateCpf(value) || t("CPF inválido"),
                                                    })}
                                                    type='tel'
                                                    autoComplete='new-password'
                                                    placeholder='ex: 123.456.789-10'
                                                    mask='999.999.999-99'
                                                    className={styles.input}
                                                />
                                                {errors.cpf && (
                                                    <ErrorMessage message={errors.cpf?.message} />
                                                )}
                                            </div>
                                        )} */}

                                        {typePersonFormSelected === "J" && (
                                            <div className={styles.formItem}>
                                                <label>{t("CNPJ")}</label>
                                                <Controller
                                                    name="cnpj"
                                                    control={control} // O `control` é extraído de `useForm()`
                                                    rules={{
                                                        validate : (value) =>
                                                            validateCNPJ(value) || "CNPJ inválido!",
                                                    }}
                                                    render={({ field }) => (
                                                        <PatternFormat
                                                            {...field}
                                                            format="##.###.###/####-##"
                                                            type="tel"
                                                            placeholder="ex: 34.906.170/0001-51"
                                                            allowEmptyFormatting={false}
                                                            className={styles.input}
                                                            onValueChange={(values) => 
                                                            {
                                                                field.onChange(values.value); // Captura o valor sem formatação
                                                                getCnpjInfo(values.value, setCnpjWs, setValue); // Chama a função externa para buscar informações
                                                            }}
                                                        />
                                                    )}
                                                />
                                                {errors.cnpj && (
                                                    <ErrorMessage message={errors.cnpj.message} />
                                                )}
                                            </div>
                                        )}

                                        {typePersonFormSelected === "J" && (
                                            <div className={styles.formItem}>
                                                <label>{t("Inscrição Estadual")}</label>
                                                <input
                                                    {...register("stateinscription")}
                                                    defaultValue={cnpjWs?.stateinscription}
                                                    className={styles.input}
                                                />
                                                {errors.stateinscription && (
                                                    <ErrorMessage
                                                        message={errors.stateinscription.message}
                                                    />
                                                )}
                                            </div>
                                        )}

                                        {typePersonFormSelected === "J" && (
                                            <div className={styles.formItem}>
                                                <label>{t("Nome da Fantasia")}</label>
                                                <input
                                                    defaultValue={cnpjWs?.nome_fantasia}
                                                    type="text"
                                                    placeholder="ex: MARIA LTDA"
                                                    className={styles.input}
                                                    {...register("storeName", {
                                                        required : "Este campo é obrigatório!",
                                                    })}
                                                />
                                                {errors.storeName && (
                                                    <ErrorMessage message={errors.storeName.message} />
                                                )}
                                            </div>
                                        )}

                                        {typePersonFormSelected === "F" && (
                                            <div className={styles.formItem}>
                                                <label>{t("Data de nascimento")}</label>
                                                <Controller
                                                    name="birthday"
                                                    control={control}
                                                    rules={{
                                                        required : "Este campo é obrigatório!",
                                                        validate : (value) =>
                                                            validateDate(value) || t("Data inválida"),
                                                    }}
                                                    render={({ field }) => (
                                                        <PatternFormat
                                                            {...field}
                                                            format="##/##/####"
                                                            type="tel"
                                                            autoComplete="new-password"
                                                            placeholder="ex: 08/09/1994"
                                                            allowEmptyFormatting={false}
                                                            className={styles.input}
                                                            onValueChange={(values) => 
                                                            {
                                                                field.onChange(values.value);
                                                                handleBirthday(values.formattedValue);
                                                            }}
                                                        />
                                                    )}
                                                />
                                                {isSubmitted && !birthday && (
                                                    <ErrorMessage message={"Data Invalida!"} />
                                                )}
                                            </div>
                                        )}

                                        <div className={styles.formItem}>
                                            <label>{t("Celular")}</label>
                                            <Controller
                                                name="phone"
                                                control={control} // O `control` é extraído de `useForm()`
                                                rules={{
                                                    validate : (value) =>
                                                        validatePhone(value) || t("Celular inválido"),
                                                }}
                                                render={({ field }) => (
                                                    <PatternFormat
                                                        {...field}
                                                        format="(##) #####-####"
                                                        type="tel"
                                                        autoComplete="new-password"
                                                        placeholder="ex: (11) 99282-9222"
                                                        allowEmptyFormatting={false}
                                                        className={styles.input}
                                                    />
                                                )}
                                            />
                                            {errors.phone && (
                                                <ErrorMessage message={errors.phone?.message} />
                                            )}
                                        </div>

                                        {typePersonFormSelected === "F" && (
                                            <div className={styles.formItem}>
                                                <label>{t("Gênero")}</label>
                                                <Select
                                                    placeholder={t("Escolha seu gênero")}
                                                    className={styles.select}
                                                    options={genderOptions}
                                                    styles={customSelectStyles}
                                                    isClearable
                                                    value={gender}
                                                    onChange={handleChangeGender}
                                                />
                                                {isSubmitted && !gender && (
                                                    <ErrorMessage message={t("Campo requiredo!")} />
                                                )}
                                            </div>
                                        )}

                                        {/* <div className={styles.formItem}>
                                            <label>{t("Senha")}</label>
                                            <input
                                                type={passwordType}
                                                required
                                                className={styles.input}
                                                autoComplete='new-password'
                                                placeholder={t("Digite sua senha")}
                                                {...register("password", {
                                                    validate : (value) =>
                                                        value.length >= 8 || t("Sua senha deve conter no mínimo 8 caracteres"),
                                                })}
                                            />
                                            {passwordType === "password" ? (
                                                <Image
                                                    className={styles.iconPassword}
                                                    onClick={() => setPasswordType("text")}
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/AiOutlineEye.svg"
                                                    loader={cloudflareLoader}
                                                    alt=""
											    />
                                            ) : (
                                                <Image
                                                    className={styles.iconPassword}
                                                    onClick={() => setPasswordType("password")}
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/AiOutlineEyeInvisible.svg"
                                                    loader={cloudflareLoader}
                                                    alt=""
											    />
                                            )}
                                            {errors.password && (
                                                <ErrorMessage message={errors.password?.message} />
                                            )}
                                        </div> */}

                                        {/* <div className={styles.formItem}>
                                            <label>{t("Confirme sua senha")}</label>
                                            <input
                                                type={passwordType}
                                                required
                                                className={styles.input}
                                                autoComplete='new-password'
                                                placeholder={t("Confirme sua senha")}
                                                {...register("confirmPassword", {
                                                    validate : (value) =>
                                                        value === password.current ||
                                                    t(
                                                        "A confirmação de senha precisam ser iguais a senha"
                                                    ),
                                                })}
                                            />
                                            {passwordType === "password" ? (
                                                <AiOutlineEye
                                                    className={styles.iconPassword}
                                                    onClick={() => setPasswordType("text")}
                                                />
                                            ) : (
                                                <AiOutlineEyeInvisible
                                                    className={styles.iconPassword}
                                                    onClick={() => setPasswordType("password")}
                                                />
                                            )}
                                            {errors.confirmPassword && (
                                                <ErrorMessage
                                                    message={errors.confirmPassword?.message}
                                                />
                                            )}
                                        </div> */}

                                        {THEME_SETTING?.inputInstagram && (
                                            <div className={styles.formItem}>
                                                <label>{t("Instagram")}</label>
                                                <input
                                                    type="text"
                                                    className={styles.input}
                                                    autoComplete="new-password"
                                                    {...register("instagram", {
                                                        validate : (value) =>
                                                            value.length >= 1 || t("Campo Requerido"),
                                                    })}
                                                />
                                                {errors.instagram && (
                                                    <ErrorMessage message={errors.instagram?.message} />
                                                )}
                                            </div>
                                        )}

                                        {THEME_SETTING.typeStore && (
                                            <div className={styles.formItem}>
                                                <label>{t("Tipo de Loja")}</label>
                                                <Select
                                                    placeholder={t("Escolha o tipo de loja")}
                                                    className={styles.select}
                                                    options={storeOptions}
                                                    styles={customSelectStyles}
                                                    isClearable
                                                    value={typeStore}
                                                    onChange={handleChangeTypeStore}
                                                />
                                                {isSubmitted && !typeStore && (
                                                    <span className={styles.errorMessage}>
                                                        <Image
                                                            width={20}
                                                            height={20}
                                                            src="/assets/icons/AiOutlineExclamationCircle.svg"
                                                            loader={cloudflareLoader}
                                                            alt=""
                                                        />
                                                        {t("Campo requiredo!")}
                                                    </span>
                                                )}
                                            </div>
                                        )}

                                        {THEME_SETTING.disabledAddressUser && (
                                            <>
                                                <div className={styles.formItem}>
                                                    <label>{t("CEP")}</label>
                                                    <Controller
                                                        name="address.zipcode"
                                                        control={control}
                                                        defaultValue={user?.address?.zipcode}
                                                        rules={{
                                                            validate : (value) =>
                                                                validateCEP(value) ||
																t("CEP deve possuir 8 caracteres!"),
                                                        }}
                                                        render={({ field }) => (
                                                            <PatternFormat
                                                                {...field}
                                                                format="#####-###"
                                                                type="tel"
                                                                className={styles.input}
                                                                placeholder="ex: 12345-678"
                                                                allowEmptyFormatting={false}
                                                                autoComplete="new-password"
                                                                onKeyUp={(e: any) => 
                                                                {
                                                                    if (e.target.value.length === 9) 
                                                                    {
                                                                        onZipcode(e.target.value);
                                                                    }
                                                                }}
                                                            />
                                                        )}
                                                    />
                                                    {(errors.address as any)?.zipcode?.message && (
                                                        <ErrorMessage
                                                            message={
                                                                (errors.address as any)?.zipcode?.message
                                                            }
                                                        />
                                                    )}
                                                </div>
                                                <div className={styles.formItem}>
                                                    <label>{t("Rua")}</label>
                                                    <input
                                                        type="text"
                                                        className={styles.input}
                                                        placeholder="ex: Rua Guaranabara"
                                                        autoComplete="new-password"
                                                        maxLength={50}
                                                        defaultValue={user?.address?.street}
                                                        {...register("address.street", {
                                                            required : t("Este campo é obrigatório!"),
                                                        })}
                                                    />
                                                    {(errors.address as any)?.street?.message && (
                                                        <ErrorMessage
                                                            message={(errors.address as any)?.street?.message}
                                                        />
                                                    )}
                                                </div>
                                                <div className={styles.formItem}>
                                                    <label>{t("Número")}</label>
                                                    <input
                                                        type="text"
                                                        className={styles.input}
                                                        defaultValue={user?.address?.housenumber}
                                                        autoComplete="new-password"
                                                        placeholder="ex: 17"
                                                        maxLength={5}
                                                        {...register("address.housenumber", {
                                                            required : t("Este campo é obrigatório!"),
                                                        })}
                                                    />
                                                    {(errors.address as any)?.housenumber?.message && (
                                                        <ErrorMessage
                                                            message={
                                                                (errors.address as any)?.housenumber?.message
                                                            }
                                                        />
                                                    )}
                                                </div>
                                                <div className={styles.formItem}>
                                                    <label>{t("Complemento")}</label>
                                                    <input
                                                        type="text"
                                                        className={styles.input}
                                                        autoComplete="new-password"
                                                        defaultValue={user?.address?.complement}
                                                        placeholder="ex: Ap 15B"
                                                        maxLength={30}
                                                        {...register("address.complement")}
                                                    />
                                                    {(errors.address as any)?.complement?.message && (
                                                        <ErrorMessage
                                                            message={
                                                                (errors.address as any)?.complement?.message
                                                            }
                                                        />
                                                    )}
                                                </div>
                                                <div className={styles.formItem}>
                                                    <label>{t("Bairro")}</label>
                                                    <input
                                                        type="text"
                                                        className={styles.input}
                                                        autoComplete="new-password"
                                                        defaultValue={user?.address?.district}
                                                        maxLength={30}
                                                        placeholder="ex: Dom Bosco"
                                                        {...register("address.district", {
                                                            required : t("Este campo é obrigatório!"),
                                                        })}
                                                    />
                                                    {(errors.address as any)?.district?.message && (
                                                        <ErrorMessage
                                                            message={
                                                                (errors.address as any)?.district?.message
                                                            }
                                                        />
                                                    )}
                                                </div>
                                                <div className={styles.formItem}>
                                                    <label>{t("Cidade")}</label>
                                                    <input
                                                        type="text"
                                                        className={styles.input}
                                                        placeholder="ex: São Paulo"
                                                        defaultValue={user?.address?.city}
                                                        autoComplete="new-password"
                                                        {...register("address.city", {
                                                            required : t("Este campo é obrigatório!"),
                                                        })}
                                                    />
                                                    {(errors.address as any)?.city?.message && (
                                                        <ErrorMessage
                                                            message={(errors.address as any)?.city?.message}
                                                        />
                                                    )}
                                                </div>
                                                <div className={styles.formItem}>
                                                    <label>{t("Estado")}</label>
                                                    <input
                                                        type="text"
                                                        className={styles.input}
                                                        autoComplete="new-password"
                                                        defaultValue={user?.address?.state}
                                                        placeholder="ex: SP"
                                                        maxLength={2}
                                                        {...register("address.state", {
                                                            required : t("Este campo é obrigatório!"),
                                                        })}
                                                    />
                                                    {(errors.address as any)?.state?.message && (
                                                        <ErrorMessage
                                                            message={(errors.address as any)?.state?.message}
                                                        />
                                                    )}
                                                </div>
                                                <input
                                                    type="hidden"
                                                    defaultValue={user?.address?.country}
                                                    {...register("address.country")}
                                                />
                                            </>
                                        )}
                                    </div>

                                    <div className={styles.formFooter}>
                                        <div className={`${styles.item} ${styles.formInfo}`}>
                                            <div className={styles.row}>
                                                <label>{t("Notificações")}</label>
                                                <span className={styles.subtitle}>
                                                    {t(
                                                        "Deseja receber notificações de compras e promoções? Marque a opção e atualize seus dados cadastrais.",
                                                    )}
                                                </span>
                                            </div>
                                            <div className={styles.row}>
                                                <Checkbox
                                                    color="success"
                                                    checked={hasNewsletter}
                                                    onChange={onNewsletter}
                                                >
                                                    {t("Quero receber notificações via e-mails")}
                                                </Checkbox>
                                            </div>
                                            <div className={styles.row}>
                                                <Checkbox
                                                    color="success"
                                                    checked={hasWhatsapp}
                                                    onChange={onWhatsapp}
                                                >
                                                    {t("Quero receber notificações via whatsapp")}
                                                </Checkbox>
                                            </div>
                                        </div>

                                        <div className={styles.buttons}>
                                            <button
                                                className={styles.submitted}
                                                type="button"
                                                disabled={loading && true}
                                                onClick={handleSubmit(onSubmit)}
                                            >
                                                {loading ? <AnimatedLoading /> : t("Cadastrar")}
                                            </button>

                                            {/* <button
                                                type='button'
                                                onClick={() => router.push("/login")}
                                            >
                                                {t("Voltar")}
                                            </button> */}
                                        </div>

                                        <div className={styles.backLink}>
                                            <CustomLink href="/login/">{t("voltar")}</CustomLink>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export { SignForm };
