import { getDocument } from "@/core-nextv3/document/document.api";
import { SIGN_PAGE_SETTING } from "@/setting/setting";
import { SignForm } from "./sign.form";

const getSignPageServer = async () => 
{
    const signPageResult = await getDocument(
        SIGN_PAGE_SETTING.merge({ cache : "force-cache" }),
    );

    return {
        signPageData : signPageResult?.data || {},
    };
};

const GenerateMetadata = async () => 
{
    return {
        title : "Cadastro",
    };
};

const SignPage = async () => 
{
    const { signPageData } = await getSignPageServer();

    return <SignForm signPage={signPageData} />;
};

// const getStaticProps: GetStaticProps = () => withHeader(async (props: any) =>
// {
//     const signPage = await getDocument(SIGN_PAGE_SETTING)

//     return {
//         revalidate : THEME_SETTING.revalidate,
//         props      : {
//             seo      : props.seo.merge({ title : "Criar Conta" }),
//             signPage : signPage?.data || {},
//         },
//     }
// })

export { SignPage, GenerateMetadata };
