"use client";

import { TDate } from "@/core-nextv3/model/TDate";
import cleanDeep from "clean-deep";
import { useTranslations } from "next-intl";
import { Checkbox } from "pretty-checkbox-react";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { PatternFormat } from "react-number-format";
import {
    addressCheckoutAnalytics,
    shippingCheckoutAnalytics,
    userLoginAnalytics,
} from "../../core-nextv3/analytics/analytics.api";
import {
    addUserAuth,
    loginAuth,
    recoveryPasswordAuth,
    setUserAuth,
} from "../../core-nextv3/auth/auth.api";
import {
    calculateZipCodeCart,
    mergeCart,
    setAddressCart,
    setShippingMethodCart,
} from "../../core-nextv3/cart/cart.api";
import { useCore } from "../../core-nextv3/core/core";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import {
    buscaCep2,
    getCnpjInfo,
    getRecaptcha,
    randomNumber,
} from "../../core-nextv3/util/util";
import {
    validateCEP,
    validateCNPJ,
    validateCpf,
    validateDate,
    validateEmail,
    validateFullName,
    validatePhone,
} from "../../core-nextv3/util/validate";
import {
    AUTH_SETTING,
    CART_SETTING,
    THEME_SETTING,
} from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import { CheckoutShippingTwo } from "../CheckoutShippingTwo";
import ErrorMessage from "../ErrorMessage";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

export const CheckoutInformationTwo = ({
    goBack,
    handleNext,
    loadingCart,
    setLoadingCart,
}: any) => 
{
    const t = useTranslations();
    const [ formType, setFormType ] = useState("signup");
    const [ hasWhatsapp, setHasWhatsapp ] = useState(false);
    const [ existsZipcode, setExistsZipcode ] = useState(false);
    const [ passwordType, setPasswordType ] = useState("password");
    const { cart, user, setUser, setCart, setCnpjWs } = useCore();
    const [ typePerson, setTypePerson ] = useState<any>("F");

    const {
        register: registerInfo,
        handleSubmit: handleSubmitInfo,
        control: controlInfo,
        setValue: setValueInfo,
        //setError     : setErrorInfo,
        watch: watchInfo,
        trigger: triggerInfo,
        reset: resetInfo,
        formState: { errors: errorsInfo, isSubmitted: isSubmittedInfo },
    } = useForm();

    const {
        register: registerLogin,
        setValue: setValueLogin,
        handleSubmit: handleSubmitLogin,
        formState: { errors: errorsLogin, isSubmitted: isSubmittedLogin },
    } = useForm();

    const {
        register: registerRecoveryPassword,
        handleSubmit: handleSubmitRecoveryPassword,
        setValue: setValueRecoveryPassword,
        formState: {
            errors: errorsRecoveryPassword,
            isSubmitted: isSubmittedRecoveryPassword,
        },
    } = useForm();

    const typesPersonsForm = [
        { label : "Pessoa Fisica", value : "F", id : "f" },
        { label : "Pessoa Jurídica", value : "J", id : "j" },
    ];

    useEffect(() => 
    {
        if (THEME_SETTING.registrationLoginTypeForm === "CPFCNPJ") 
        {
            setTypePerson("F");
        }
        else if (THEME_SETTING.registrationLoginTypeForm === "CNPJ") 
        {
            setTypePerson("J");
        }
        else 
        {
            setTypePerson("F");
        }
    }, []);

    useEffect(() => 
    {
        if (user?.address && !cart?.shippingMethods?.full) 
        {
            onZipcode(user?.address?.zipcode);
        }
    }, [ cart, user ]);

    useEffect(() => 
    {
        resetInfo(cleanDeep(user));
        registerInfo("shipping", { value : cart?.shipping, required : true });
        setExistsZipcode(!!user?.address);
        setHasWhatsapp(user?.onWhatsapp);
    }, [ user ]);

    useEffect(() => 
    {
        tagManager4.checkoutStep(cart, "initiateCheckout", 1, user);
    }, []);

    const onSubmitSignup = async (data: any) => 
    {
        if (user) 
        {
            onSubmitUpdate(data);
        }
        else 
        {
            setLoadingCart(true);

            const birthday = new TDate({ value : data.birthday, mask : "dd/MM/yyyy" });

            if (!birthday.isValid()) 
            {
                await setLoadingCart(false);
                return toast.error(t("Data de aniversário inválida!"), {
                    duration : 2000,
                });
            }

            let token = await getRecaptcha("addUser");

            if (token) 
            {
                const password = randomNumber(8);

                const address = {
                    zipcode     : data?.address?.zipcode || "",
                    street      : data?.address?.street || "",
                    housenumber : data?.address?.housenumber || "",
                    complement  : data?.address?.complement || "",
                    district    : data?.address?.district || "",
                    city        : data?.address?.city || "",
                    state       : data?.address?.state || "",
                    country     : {
                        id       : "br",
                        label    : "Brasil",
                        selected : true,
                        value    : "br",
                    },
                };

                const newData: any = {
                    token : token,
                    data  : {
                        email           : data.email,
                        name            : data.name,
                        phone           : data.phone,
                        cpf             : data.cpf,
                        newPassword     : password,
                        birthday        : birthday.toDate(),
                        password        : password,
                        confirmPassword : password,
                        onWhatsapp      : hasWhatsapp,
                        address         : address,
                    },
                };

                const result: any = await addUserAuth(AUTH_SETTING.merge(newData));

                if (!result.status) 
                {
                    setLoadingCart(false);
                    setValueLogin("login", result?.data?.email);
                    setValueRecoveryPassword("login", result?.data?.email);
                    setFormType("login");

                    return toast.success(
                        t("Você já possui cadastro! Informe sua Senha!"),
                        {
                            duration : 3000,
                        },
                    );
                }

                ("");

                token = await getRecaptcha("login");

                // LOGIN
                const result2 = await loginAuth(
                    AUTH_SETTING.merge({
                        token    : token,
                        login    : data.email.toLowerCase(),
                        password : password,
                    }),
                );

                // MERGE CART SESSION
                await mergeCart(
                    CART_SETTING.merge({
                        document : {
                            referencePath : cart?.referencePath,
                        },
                    }),
                );

                // LOGIN ANALYTICS
                userLoginAnalytics(result2.data);

                setUser(result2.data);

                // SET ADDRESS
                await onSubmitCartAddress(address);

                // SET SHIPPING
                await onSubmitCartShipping(data);
            }

            setLoadingCart(false);
        }
    };

    const onSubmitUpdate = async (data: any) => 
    {
        setLoadingCart(true);

        let user: any = {};

        if (data.cpf) 
        {
            const birthday = new TDate({ value : data.birthday, mask : "dd/MM/yyyy" });

            if (!birthday.isValid()) 
            {
                await setLoadingCart(false);
                return toast.error(t("Data de aniversário inválida!"), {
                    duration : 2000,
                });
            }

            const password = randomNumber(8);

            user = {
                email           : data.email,
                name            : data.name,
                phone           : data.phone,
                cpf             : data.cpf,
                newPassword     : password,
                birthday        : birthday.toDate(),
                password        : password,
                confirmPassword : password,
            };
        }

        data.referencePath = undefined;

        const token = await getRecaptcha("updateUser");

        if (token) 
        {
            const address = {
                zipcode     : data?.address?.zipcode || "",
                street      : data?.address?.street || "",
                housenumber : data?.address?.housenumber || "",
                complement  : data?.address?.complement || "",
                district    : data?.address?.district || "",
                city        : data?.address?.city || "",
                state       : data?.address?.state || "",
                country     : {
                    id       : "br",
                    label    : "Brasil",
                    selected : true,
                    value    : "br",
                },
            };

            const newData: any = {
                token : token,
                data  : {
                    ...user,
                    onWhatsapp : hasWhatsapp,
                    address    : address,
                },
            };

            const result2 = await setUserAuth(AUTH_SETTING.merge(newData));

            setUser(result2.data);

            // SET ADDRESS
            await onSubmitCartAddress(address);

            // SET SHIPPING
            await onSubmitCartShipping(data);
        }

        setLoadingCart(false);
    };

    const onSubmitCartAddress = async (address: any) => 
    {
        setLoadingCart(true);

        // SET CART SHIPPING
        const newData = {
            data : address,
        };

        const result = await setAddressCart(CART_SETTING.merge(newData));

        if (result.status) 
        {
            // ADDRESS ANALYTICS
            addressCheckoutAnalytics();

            setCart(result.data);
        }
        else 
        {
            setLoadingCart(false);

            return toast.error(
                t("Verifique se os campos foram preenchidos corretamente."),
                { duration : 2000 },
            );
        }
    };

    const onSubmitCartShipping = async (data: any) => 
    {
        if (!data.shipping) 
        {
            return toast.error(t("Selecione uma das formas de entrega!"), {
                duration : 2000,
            });
        }

        const newData = {
            data : data.shipping,
        };

        setLoadingCart(true);

        const result = await setShippingMethodCart(CART_SETTING.merge(newData));

        if (result?.data?.shipping) 
        {
            const shippingData = {
                key   : "shipping_tier",
                value : result.data.shipping.id,
            };

            tagManager4.checkoutStep(result.data, "checkoutShipping", 2, user, [
                shippingData,
            ]);

            // ADDRESS ANALYTICS
            shippingCheckoutAnalytics(data.name);

            setCart(result.data);

            setLoadingCart(false);
            handleNext();
        }
        else 
        {
            return toast.error(t("Verifique se foi selecionado um método válido."), {
                duration : 2000,
            });
        }
    };

    async function recoveryPassword(data: any) 
    {
        tagManager4.registerEvent("click-button", "lead", "Entrar", 0, null);

        const result = await recoveryPasswordAuth(
            AUTH_SETTING.merge({
                login : data?.login?.toLowerCase(),
            }),
        );

        if (!result.status) 
        {
            return toast.error(result.error);
        }

        toast(
            (t: any) => (
                <span>
                    {result.message}
                    <button onClick={() => toast.dismiss(t.id)}>Ok</button>
                </span>
            ),
            {
                className : "toast-custom",
                duration  : 999999,
            },
        );

        setValueLogin("login", data?.login?.toLowerCase());
        setFormType("login");
    }

    async function handleLoginWithEmailAndPassword(formData: any) 
    {
        const token = await getRecaptcha("login");

        if (token) 
        {
            setLoadingCart(true);

            const result = await loginAuth(
                AUTH_SETTING.merge({
                    token    : token,
                    login    : formData.login.toLowerCase(),
                    password : formData.password,
                }),
            );

            // MERGE CART SESSION
            const cartResult = await mergeCart(
                CART_SETTING.merge({
                    document : {
                        referencePath : cart?.referencePath,
                    },
                }),
            );

            setLoadingCart(false);

            if (!result.status) 
            {
                return toast.error(result.error || "Erro ao fazer login.", {
                    duration : 2000,
                });
            }

            const toastInstance: any = toast("Login feito com sucesso!", {
                icon     : "👏",
                duration : 2000,
            });
            setTimeout(() => toast.dismiss(toastInstance.id), 2000);

            // LOGIN ANALYTICS
            userLoginAnalytics(result.data);

            setUser(result.data);
            setCart(cartResult.data);

            setFormType("signup");
        }
    }

    const onWhatsapp = async () => 
    {
        setHasWhatsapp(!hasWhatsapp);
    };

    useEffect(() => 
    {
        registerInfo("gender", { value : null });
    }, []);

    const onZipcode = async (value: any) => 
    {
        setExistsZipcode(true);

        const data: any = await buscaCep2(value);

        if (data) 
        {
            setValueInfo("address", data);
            triggerInfo();

            const result2 = await calculateZipCodeCart(
                CART_SETTING.merge({
                    zipcode : value,
                }),
            );

            if (result2.status) 
            {
                setCart(result2.data);
            }
        }
        else 
        {
            setExistsZipcode(false);
        }

        updateShipping(null);
    };

    const updateShipping = (value: any) => 
    {
        setValueInfo("shipping", value);
        triggerInfo();
    };

    return (
        <div className={styles.checkoutInformationTwo}>
            {formType === "login" && (
                <form className={styles.formTop}>
                    <div className={styles.contentForm}>
                        <div className={styles.formHeader}>
                            <p className={styles.title}>Entre ou Crie uma Conta</p>
                            <p className={styles.subtitle}>
								Clique aqui para{" "}
                                <a onClick={() => setFormType("signup")}>CRIAR CONTA</a>
                            </p>
                        </div>
                        <div className={styles.inputsContentForm}>
                            <label>{t("E-mail")}</label>
                            <div className={styles.inputControl}>
                                <input
                                    id="loginInput"
                                    style={{ textTransform : "lowercase" }}
                                    type="email"
                                    placeholder="E-mail"
                                    {...registerLogin("login", {
                                        required : "Este campo é obrigatório!",
                                        pattern  : {
                                            value   : /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                            message : "Email inválido",
                                        },
                                    })}
                                />
                                {isSubmittedLogin && errorsLogin?.login && (
                                    <ErrorMessage message={errorsLogin?.login?.message} />
                                )}
                            </div>
                            <label>{t("Senha")}</label>
                            <div className={styles.inputControl}>
                                <div className={styles.inputPassword}>
                                    <input
                                        id="passwordInput"
                                        {...registerLogin("password", {
                                            required  : "Este campo é obrigatório!",
                                            minLength : {
                                                value   : 8,
                                                message : "Sua senha possui, no mínimo, 8 caracteres.",
                                            },
                                        })}
                                        type={passwordType}
                                        placeholder="Senha"
                                    />
                                    {passwordType === "password" ? (
                                        <ImageSet
                                            className={styles.iconPassword}
                                            onClick={() => setPasswordType("text")}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiOutlineEye.svg"
                                            responsive={false}
                                            alt=""
                                        />
                                    ) : (
                                        <ImageSet
                                            className={styles.iconPassword}
                                            onClick={() => setPasswordType("password")}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/AiOutlineEyeInvisible.svg"
                                            responsive={false}
                                            alt=""
                                        />
                                    )}
                                </div>
                                {isSubmittedLogin && errorsLogin?.password && (
                                    <ErrorMessage message={errorsLogin?.password?.message} />
                                )}
                            </div>
                        </div>
                        <div className={styles.inputControl}>
                            <a onClick={() => setFormType("recoveryPassword")}>
								Esqueceu a senha? <br />
								Clique aqui para recebê-la por e-mail
                            </a>
                        </div>
                        <div className={styles.buttons}>
                            <button
                                id="submitWithPasswordButton"
                                className={styles.submitted}
                                onClick={handleSubmitLogin(handleLoginWithEmailAndPassword)}
                                type="button"
                            >
                                {loadingCart ? <AnimatedLoading /> : "Entrar"}
                            </button>
                        </div>

                        <div className={styles.backLink}>
                            <a onClick={() => setFormType("signup")}>Voltar</a>
                        </div>
                    </div>
                </form>
            )}

            {formType === "signup" && (
                <form className={styles.formTop}>
                    {!user?.cpf && (
                        <div className={styles.formHeader}>
                            <p className={styles.title}>{t("Entre ou Crie uma Conta")}</p>
                            <p
                                className={styles.subtitle}
                                onClick={() => setFormType("login")}
                            >
                                {t("Clique aqui se já tem conta.")} <a>{t("FAZER LOGIN")}</a>
                            </p>
                        </div>
                    )}

                    {!user?.cpf && (
                        <div className={styles.inputs}>
                            <div className={styles.formItem}>
                                <label>{t("Nome Completo")}</label>
                                <input
                                    defaultValue={user?.name}
                                    type="text"
                                    placeholder="ex: Maria Jose"
                                    autoComplete="new-password"
                                    {...registerInfo("name", {
                                        validate : (value) =>
                                            validateFullName(value) || t("Nome Completo"),
                                    })}
                                />
                                {errorsInfo?.name && (
                                    <ErrorMessage message={errorsInfo?.name?.message} />
                                )}
                            </div>

                            <div className={styles.formItem}>
                                <label>{t("E-mail")}</label>
                                <input
                                    style={{ textTransform : "lowercase" }}
                                    type="email"
                                    className={styles.input}
                                    required
                                    placeholder="ex: maria.jose@gmail.com"
                                    autoComplete="new-password"
                                    {...registerInfo("email", {
                                        validate : (value) =>
                                            validateEmail(value) || t("E-mail inválido"),
                                    })}
                                />
                                {errorsInfo.email && (
                                    <ErrorMessage message={errorsInfo.email?.message} />
                                )}
                            </div>

                            <div className={styles.formItem}>
                                <label>{t("Celular")}</label>
                                <Controller
                                    name="phone"
                                    control={controlInfo}
                                    rules={{
                                        validate : (value) =>
                                            validatePhone(value) || t("Celular Invalido"),
                                    }}
                                    render={({ field }) => (
                                        <PatternFormat
                                            {...field}
                                            type="tel"
                                            format="(##) #####-####"
                                            autoComplete="new-password"
                                            placeholder="ex: (11) 99282-9222"
                                            allowEmptyFormatting={false}
                                            onValueChange={(values) => 
                                            {
                                                field.onChange(values.value);
                                            }}
                                        />
                                    )}
                                />
                                {errorsInfo?.phone && (
                                    <ErrorMessage message={errorsInfo?.phone?.message} />
                                )}
                            </div>

                            {typePerson === "F" && (
                                <div className={styles.formItem}>
                                    <label>{t("Data de Nascimento")}</label>
                                    <Controller
                                        name="birthday"
                                        control={controlInfo}
                                        rules={{
                                            validate : (value) =>
                                                validateDate(value) || t("Data inválida"),
                                        }}
                                        render={({ field }) => (
                                            <PatternFormat
                                                {...field}
                                                format="##/##/####"
                                                type="tel"
                                                autoComplete="new-password"
                                                placeholder="ex: 08/09/1994"
                                                allowEmptyFormatting={false}
                                                className={styles.input}
                                                onValueChange={(values) => 
                                                {
                                                    field.onChange(values.value);
                                                }}
                                            />
                                        )}
                                    />
                                    {errorsInfo?.birthday && (
                                        <ErrorMessage message={errorsInfo?.birthday?.message} />
                                    )}
                                </div>
                            )}
                        </div>
                    )}

                    {!user?.cpf && (
                        <div className={styles.formHeader}>
                            <p className={styles.title}>{t("Dados para fatura")}</p>
                            <p className={styles.subtitle}>
								Informação para a emissão da nota fiscal
                            </p>
                        </div>
                    )}

                    {!user?.cpf && (
                        <div className={styles.formAddress}>
                            {THEME_SETTING.registrationLoginTypeForm === "CPFCNPJ" ? (
                                <div className={styles.buttonsOptions}>
                                    <div className={styles.typesButton}>
                                        {typesPersonsForm?.map((typePerson: any, index: any) => (
                                            <button
                                                type="button"
                                                key={index}
                                                onClick={() => setTypePerson(typePerson?.value)}
                                                className={`${
                                                    typePerson?.value === typePerson
                                                        ? styles.buttonSelected
                                                        : ""
                                                }`}
                                            >
                                                {typePerson?.label}
                                            </button>
                                        ))}
                                    </div>

                                    {typePerson === "F" && (
                                        <div className={styles.formItem}>
                                            <label>{t("CPF")}</label>
                                            <Controller
                                                name="cpf"
                                                control={controlInfo}
                                                defaultValue={user?.cpf}
                                                rules={{
                                                    validate : (value) =>
                                                        validateCpf(value) || t("CPF inválido"),
                                                }}
                                                render={({ field }) => (
                                                    <PatternFormat
                                                        {...field}
                                                        format="###.###.###-##"
                                                        type="tel"
                                                        autoComplete="new-password"
                                                        allowEmptyFormatting={false}
                                                        className={styles.input}
                                                        placeholder="ex: 123.456.789-10"
                                                        onValueChange={(values) => 
                                                        {
                                                            field.onChange(values.value); // Captura o valor não formatado (apenas números)
                                                        }}
                                                    />
                                                )}
                                            />
                                            {errorsInfo?.cpf && (
                                                <ErrorMessage message={errorsInfo?.cpf?.message} />
                                            )}
                                        </div>
                                    )}

                                    {typePerson === "J" && (
                                        <div className={styles.formItem}>
                                            <label>{t("CNPJ")}</label>
                                            <Controller
                                                name="cnpj"
                                                control={controlInfo}
                                                rules={{
                                                    validate : (value) =>
                                                        validateCNPJ(value) || "CNPJ inválido!",
                                                }}
                                                render={({ field }) => (
                                                    <PatternFormat
                                                        {...field}
                                                        format="##.###.###/####-##"
                                                        type="tel"
                                                        className={styles.input}
                                                        placeholder="ex: 34.906.170/0001-51"
                                                        allowEmptyFormatting={false}
                                                        onValueChange={(values) => 
                                                        {
                                                            field.onChange(values.value); // Captura o valor não formatado
                                                            getCnpjInfo(
                                                                values.value,
                                                                setCnpjWs,
                                                                setValueInfo,
                                                            ); // Chama a função externa
                                                        }}
                                                    />
                                                )}
                                            />
                                            {errorsInfo?.cnpj && (
                                                <ErrorMessage message={errorsInfo?.cnpj.message} />
                                            )}
                                        </div>
                                    )}
                                </div>
                            ) : (
                                <div className={styles.formItem}>
                                    <label>{t("CPF")}</label>
                                    <Controller
                                        name="cpf"
                                        control={controlInfo}
                                        defaultValue={user?.cpf}
                                        rules={{
                                            validate : (value) =>
                                                validateCpf(value) || t("CPF inválido"),
                                        }}
                                        render={({ field }) => (
                                            <PatternFormat
                                                {...field}
                                                format="###.###.###-##"
                                                type="tel"
                                                autoComplete="new-password"
                                                className={styles.input}
                                                placeholder="ex: 123.456.789-10"
                                                allowEmptyFormatting={false}
                                                onValueChange={(values) => 
                                                {
                                                    field.onChange(values.value);
                                                }}
                                            />
                                        )}
                                    />
                                    {isSubmittedInfo && errorsInfo?.cpf && (
                                        <ErrorMessage message={errorsInfo?.cpf?.message} />
                                    )}
                                </div>
                            )}
                        </div>
                    )}

                    <div className={styles.formHeader}>
                        <p className={styles.title}>{t("Endereço de entrega")}</p>
                        <p className={styles.subtitle}>
							Informe o endereço onde deseja receber seu pedido
                        </p>
                    </div>

                    <div className={styles.formAddress}>
                        <div className={styles.formInputs}>
                            <div className={styles.formItem}>
                                <label>{t("CEP")}</label>
                                <Controller
                                    name="address.zipcode"
                                    control={controlInfo}
                                    defaultValue={user?.address?.zipcode}
                                    rules={{
                                        validate : {
                                            length : (value) =>
                                                validateCEP(value) ||
												t("CEP deve possuir 8 caracteres!"),
                                            exists : () => existsZipcode || "CEP Invalido",
                                        },
                                    }}
                                    render={({ field }) => (
                                        <PatternFormat
                                            {...field}
                                            format="#####-###"
                                            type="tel"
                                            placeholder="ex: 12345-678"
                                            autoComplete="new-password"
                                            allowEmptyFormatting={false}
                                            onValueChange={(values) => 
                                            {
                                                field.onChange(values.value);

                                                if (values.value.length === 8) 
                                                {
                                                    onZipcode(values.formattedValue);
                                                }
                                            }}
                                        />
                                    )}
                                />
                                {isSubmittedInfo &&
									(errorsInfo.address as any)?.zipcode?.message && (
                                    <ErrorMessage
                                        message={(errorsInfo.address as any)?.zipcode?.message}
                                    />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Rua")}</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    placeholder="ex: Rua Guaranabara"
                                    autoComplete="new-password"
                                    maxLength={50}
                                    defaultValue={user?.address?.street}
                                    {...registerInfo("address.street", {
                                        required : t("Este campo é obrigatório!"),
                                    })}
                                />
                                {isSubmittedInfo &&
									(errorsInfo.address as any)?.street?.message && (
                                    <ErrorMessage
                                        message={(errorsInfo.address as any)?.street?.message}
                                    />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Número")}</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    defaultValue={user?.address?.housenumber}
                                    autoComplete="new-password"
                                    placeholder="ex: 17"
                                    maxLength={5}
                                    {...registerInfo("address.housenumber", {
                                        required : t("Este campo é obrigatório!"),
                                    })}
                                />
                                {isSubmittedInfo &&
									(errorsInfo.address as any)?.housenumber?.message && (
                                    <ErrorMessage
                                        message={
                                            (errorsInfo.address as any)?.housenumber?.message
                                        }
                                    />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Complemento")}</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    autoComplete="new-password"
                                    defaultValue={user?.address?.complement}
                                    placeholder="ex: Ap 15B"
                                    maxLength={30}
                                    {...registerInfo("address.complement")}
                                />
                                {isSubmittedInfo &&
									(errorsInfo.address as any)?.complement?.message && (
                                    <ErrorMessage
                                        message={(errorsInfo.address as any)?.complement?.message}
                                    />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Bairro")}</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    autoComplete="new-password"
                                    defaultValue={user?.address?.district}
                                    maxLength={30}
                                    placeholder="ex: Dom Bosco"
                                    {...registerInfo("address.district", {
                                        required : t("Este campo é obrigatório!"),
                                    })}
                                />
                                {isSubmittedInfo &&
									(errorsInfo.address as any)?.district?.message && (
                                    <ErrorMessage
                                        message={(errorsInfo.address as any)?.district?.message}
                                    />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Cidade")}</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    placeholder="ex: São Paulo"
                                    defaultValue={user?.address?.city}
                                    autoComplete="new-password"
                                    {...registerInfo("address.city", {
                                        required : t("Este campo é obrigatório!"),
                                    })}
                                />
                                {isSubmittedInfo &&
									(errorsInfo.address as any)?.city?.message && (
                                    <ErrorMessage
                                        message={(errorsInfo.address as any)?.city?.message}
                                    />
                                )}
                            </div>
                            <div className={styles.formItem}>
                                <label>{t("Estado")}</label>
                                <input
                                    type="text"
                                    className={styles.input}
                                    autoComplete="new-password"
                                    defaultValue={user?.address?.state}
                                    placeholder="ex: SP"
                                    maxLength={2}
                                    {...registerInfo("address.state", {
                                        required : t("Este campo é obrigatório!"),
                                    })}
                                />
                                {isSubmittedInfo &&
									(errorsInfo.address as any)?.state?.message && (
                                    <ErrorMessage
                                        message={(errorsInfo.address as any)?.state?.message}
                                    />
                                )}
                            </div>
                            <input
                                type="hidden"
                                defaultValue={user?.address?.country}
                                {...registerInfo("address.country")}
                            />
                        </div>
                    </div>

                    <div className={styles.checkbox}>
                        <Checkbox
                            checked={hasWhatsapp}
                            color="success"
                            onChange={onWhatsapp}
                        >
                            {t("Quero receber notificações via whatsapp")}
                        </Checkbox>
                    </div>

                    <CheckoutShippingTwo
                        updateShipping={updateShipping}
                        errorsInfo={errorsInfo}
                        isSubmitted={isSubmittedInfo}
                        watch={watchInfo}
                    />

                    <div className={styles.inputControl}>
                        <button
                            type="button"
                            disabled={loadingCart && true}
                            className={`block buttonBlock ${styles.submitted}`}
                            onClick={handleSubmitInfo(onSubmitSignup)}
                        >
                            {loadingCart ? <AnimatedLoading /> : t("Continuar")}
                        </button>

                        {isSubmittedInfo && Object.keys(errorsInfo).length > 0 && (
                            <ErrorMessage
                                message={
                                    "Dados inválidos. Por favor, verifique os campos preenchidos."
                                }
                            />
                        )}
                    </div>

                    <div className={styles.inputControl}>
                        <a onClick={() => goBack()}>Voltar</a>
                    </div>
                </form>
            )}

            {formType === "recoveryPassword" && (
                <form className={styles.contentForm}>
                    <div className={styles.formHeader}>
                        <div className={styles.title}>Recuperar Senha</div>
                        <div className={styles.subtitle}>
							Vamos enviar um e-mail com uma nova senha
                        </div>
                    </div>
                    <div className={styles.inputControl}>
                        <input
                            id="loginInput3"
                            style={{ textTransform : "lowercase" }}
                            type="email"
                            placeholder="Email"
                            {...registerRecoveryPassword("login", {
                                required : "Este campo é obrigatório!",
                                pattern  : {
                                    value   : /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                    message : "Email inválido",
                                },
                            })}
                        />
                        {isSubmittedRecoveryPassword && errorsRecoveryPassword.login && (
                            <ErrorMessage message={errorsRecoveryPassword.login.message} />
                        )}
                    </div>
                    <div className={styles.buttons}>
                        <button
                            type="button"
                            disabled={loadingCart && true}
                            className={`block ${styles.submitted}`}
                            onClick={handleSubmitRecoveryPassword(recoveryPassword)}
                        >
							Enviar
                        </button>
                    </div>
                    <div className={styles.inputControl}>
                        <a onClick={() => setFormType("login")}>Voltar</a>
                    </div>
                </form>
            )}
        </div>
    );
};
