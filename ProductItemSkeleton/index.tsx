import { Box, Skeleton } from "@material-ui/core";
import styles from "./styles.module.scss";

export const ProductItemSkeleton = ({ width = 300, height = 550 }: any) => 
{
    return (
        <Box className={styles.productItemSkeleton}>
            <div className={styles.productImage}>
                <div style={{ aspectRatio : width / height }}>
                    <Skeleton
                        animation="wave"
                        variant="rectangular"
                        style={{ width : "100%", height : "100%" }}
                    />
                </div>
            </div>
            <div className={styles.productActions}>
                <Skeleton animation="wave" variant="text" width={"100%"} height={30} />
            </div>
            <div className={styles.productName}>
                <Skeleton animation="wave" variant="text" width={"100%"} height={30} />
            </div>
            <div className={styles.actionButton}>
                <Skeleton
                    animation="wave"
                    variant="rectangular"
                    width={"100%"}
                    style={{ borderRadius : "4px", height : 36 }}
                />
            </div>
        </Box>
    );
};
