"use client";

import { useProtectedAuth } from "@/core-nextv3/auth/user.auth.api";
import { setting as CreateSetting } from "@/core-nextv3/util/setting";
import { formatDate } from "@/core-nextv3/util/util";
import { useTranslations } from "next-intl";
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import { toast } from "react-hot-toast";
import { logoutAuth } from "../../core-nextv3/auth/auth.api";
import { mergeAllCart } from "../../core-nextv3/cart/cart.api";
import { useCore } from "../../core-nextv3/core/core";
import { collectionOwnerDocument } from "../../core-nextv3/document/document.api";
import { orderStatusLabel } from "../../core-nextv3/order/order.util";
import { currencyMask } from "../../core-nextv3/util/mask";
import {
    AUTH_SETTING,
    CART_SETTING,
    THEME_SETTING,
} from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import CustomLink from "../CustomLink";
import { LoadingPage } from "../LoadingPage";
import { PageTitle } from "../PageTitle";
import { ProductItemSkeletonPerfil } from "../ProductItemSkeletonPerfil";
import styles from "./styles.module.scss";

declare const window: any;

const PerfilDisplay = ({ setting }: any) => 
{
    const pathname = usePathname();
    const [ orders, setOrders ] = useState([]);
    const [ orderItem ] = useState<any>();
    const { user, cart } = useCore();
    const [ navigationPage ] = useState(false);
    const [ loadingRequests, setLoadingRequests ] = useState(true);
    const t = useTranslations();

    useEffect(() => 
    {
        const getOrders = async () => 
        {
            if (user) 
            {
                const result = await collectionOwnerDocument(
                    CreateSetting(setting).merge({
                        ownerField : "client",
                    }),
                );

                setOrders(result.collection);
                setLoadingRequests(false);
            }
        };

        getOrders();
    }, [ user ]);

    const loadProtectedAuth = useProtectedAuth(AUTH_SETTING, "", "/login/");

    if (!loadProtectedAuth) 
    {
        return <LoadingPage />;
    }

    const handleLogout = async () => 
    {
        await logoutAuth(AUTH_SETTING);

        // MERGE CART SESSION
        await mergeAllCart(
            CART_SETTING.merge({
                document : {
                    referencePath : cart?.referencePath,
                },
            }),
        );

        toast.success(`${t("Logout feito com sucesso")}!`);
        window.location.reload();
    };

    // const handleNavigation = async (location: any) =>
    // {
    //     await setNavigationPage(true)

    //     router.push(location)
    // }

    return (
        <>
            <div className={`${styles.perfilPage} page`}>
                <div className={styles.content}>
                    <PageTitle name={"Meus Pedidos"} />
                    <div className={styles.perfilData}>
                        <div className={styles.perfilMenu}>
                            <CustomLink
                                href="/perfil/"
                                prefetch={true}
                                className={pathname === "/perfil/" ? styles.active : ""}
                            >
                                {t("Meus Pedidos")}
                            </CustomLink>
                            {!THEME_SETTING.disabledExpress && (
                                <CustomLink
                                    href="/pedidos-express/"
                                    prefetch={true}
                                    className={
                                        pathname === "/pedidos-express/" ? styles.active : ""
                                    }
                                >
                                    {t("Pedidos Express")}
                                </CustomLink>
                            )}
                            <CustomLink href="/perfil/detalhes-da-conta/" prefetch={true}>
                                {t("Detalhes da conta")}
                            </CustomLink>
                            {!THEME_SETTING?.disabledFavorite && (
                                <CustomLink href="/lista-de-desejos/" prefetch={true}>
                                    {t("Lista de Desejos")}
                                </CustomLink>
                            )}
                            <a
                                onClick={() => 
                                {
                                    handleLogout();
                                }}
                            >
                                {t("Sair")}
                            </a>
                        </div>

                        <div className={styles.perfilContent}>
                            {loadingRequests ? (
                                <>
                                    {Array.from({ length : 24 }).map((_product, index) => (
                                        <ProductItemSkeletonPerfil
                                            key={`productSkeleton-${index}`}
                                        />
                                    ))}
                                </>
                            ) : (
                                <>
                                    <div className={styles.top}>
                                        <p>
                                            <span>{user?.name && `${user?.name},`}</span>{" "}
                                            {t("Seja bem-vindo")}.
                                        </p>
                                        {orders?.length <= 0 && (
                                            <span>{t("Você ainda não possui pedidos")}.</span>
                                        )}
                                    </div>

                                    <div className={styles.perfilOrders}>
                                        {orders?.map((order: any) => (
                                            <div className={styles.perfilOrder} key={`${order.id}`}>
                                                <CustomLink
                                                    href={`/pedido/${order.id}/`}
                                                    prefetch={true}
                                                >
                                                    <p className={styles.orderItem}>
                                                        <strong>{t("Data do pedido")}: </strong>
                                                        <span>
                                                            {formatDate(order.postdate, "dd/MM/yyyy")}
                                                        </span>
                                                    </p>
                                                    <p className={styles.orderItem}>
                                                        <strong>{t("Número do pedido")}: </strong>
                                                        <span>{order._sequence}</span>
                                                    </p>
                                                    <p className={styles.orderItem}>
                                                        <strong>{t("Status do pagamento")}: </strong>
                                                        <span>{t(orderStatusLabel(order))}</span>
                                                    </p>
                                                    <p className={styles.orderItem}>
                                                        <strong>{t("Tipo de pagamento")}: </strong>
                                                        <span>{order?.paymentMethod?.label}</span>
                                                    </p>
                                                    <p className={`${styles.orderItem} shippingPerfil`}>
                                                        <strong>{t("Método de entrega")}: </strong>
                                                        <span>{order?.shipping?.type?.label}</span>
                                                    </p>
                                                    <p className={styles.orderItem}>
                                                        <strong>{t("Total do pedido")}: </strong>
                                                        <span>{currencyMask(order?.total)}</span>
                                                    </p>
                                                </CustomLink>
                                                <CustomLink
                                                    className="buttonBlock"
                                                    href={`/pedido/${order.id}/`}
                                                >
                                                    {orderItem?.id === order.id ? (
                                                        <>{navigationPage && <AnimatedLoading />}</>
                                                    ) : (
                                                        t("Visualizar Pedido")
                                                    )}
                                                </CustomLink>
                                            </div>
                                        ))}
                                    </div>
                                </>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

// const getStaticProps: GetStaticProps = () =>
//   withHeader(async (props: any) => {
//     const languages = props?.languages

//     return {
//       revalidate: THEME_SETTING.revalidate,
//       props: {
//         seo: props?.seo?.merge({ title: 'Perfil' }),
//         languages: languages || null,
//       },
//     }
//   })

export { PerfilDisplay };
