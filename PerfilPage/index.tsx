import { ORDER_SETTING } from "@/setting/setting";
import { PerfilDisplay } from "./perfil.display";

const GenerateMetadata = async () => 
{
    return {
        title : "Minha Conta",
    };
};

const PerfilPage = () => 
{
    return <PerfilDisplay setting={ORDER_SETTING.toJson()} />;
};

export { PerfilPage, GenerateMetadata };
