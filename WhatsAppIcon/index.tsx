"use client";

import { useResponsive } from "@/core-nextv3/util/useResponsive";
import Image from "next/image";
import { tagManager4 } from "../../core-nextv3/util/TagManager4";
import { cloudflareLoader, removePhoneMask } from "../../core-nextv3/util/util";
import styles from "./styles.module.scss";

declare const window: any;

export const WhatsAppIcon = ({ account }: any) => 
{
    const { isDesktop } = useResponsive();

    const openLink = () => 
    {
        if (isDesktop) 
        {
            window.open(
                `https://web.whatsapp.com/send?phone=${removePhoneMask(account?.whatsapp)}&text=Olá, gostaria de tirar algumas duvidas.`,
                "_blank",
            );
        }
        else 
        {
            window.open(
                `https://wa.me/${removePhoneMask(account?.whatsapp)}?text=Olá, gostaria de tirar algumas duvidas.`,
                "_blank",
            );
        }
    };

    return (
        <div
            className={styles.whatsAppIcon}
            onClick={() => 
            {
                tagManager4.registerEvent(
                    "contact",
                    "whatsapp-button",
                    "Contato",
                    0,
                    null,
                );
                openLink();
            }}
        >
            <Image
                width={20}
                height={20}
                src="/assets/icons/BsWhatsapp.svg"
                alt=""
                loader={cloudflareLoader}
            />
        </div>
    );
};
