"use client";

import { useTranslations } from "next-intl";
import CustomLink from "../CustomLink";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

export function PageTitle({ name, parents = null, noTitle = false }: any) 
{
    const t = useTranslations();

    return (
        <div className={styles.container}>
            <div className={styles.breadcrumb}>
                <CustomLink href="/" prefetch={true} className={styles.title}>
                    {t("Home")}
                </CustomLink>
                <ImageSet
                    width={20}
                    height={20}
                    src="/assets/icons/IoIosArrowForward.svg"
                    responsive={false}
                    alt=""
                />
                {parents?.map((item: any, index: number) => (
                    <div key={index} className={styles.innerItem}>
                        <CustomLink
                            href={item.url}
                            prefetch={true}
                            className={styles.title}
                        >
                            {item.name}
                        </CustomLink>
                        <ImageSet
                            width={20}
                            height={20}
                            src="/assets/icons/IoIosArrowForward.svg"
                            responsive={false}
                            alt=""
                        />
                    </div>
                ))}
                <span className={styles.lastName}>{t(name)}</span>
            </div>

            {!noTitle && <p className={styles.pageTitle}>{t(name)}</p>}
        </div>
    );
}
