import { orderStatusBarActive } from "../../core-nextv3/order/order.util";
import { innerHTML } from "../../core-nextv3/util/util";
import styles from "./styles.module.scss";

export const TrackBack = ({ order, trackBack }: any) => 
{
    // console.log("order", order)
    // console.log('page', trackBack)
    //sconst { isDesktop } = useResponsive();

    return (
        <div className={styles.trackBack}>
            <div className={styles.content}>
                <div className={styles.stages}>
                    {trackBack?.trackBack?.map((trackBackItem: any, index: any) => (
                        <div className={styles.stage} key={index}>
                            <section className={styles.stageItem}>
                                <div
                                    className={`${styles.item} ${styles[orderStatusBarActive(trackBackItem, order)]}`}
                                >
                                    <img
                                        alt=""
                                        src={trackBackItem?.imageNotActive?._url}
                                        className={`${styles.imageNotActive} ${styles[orderStatusBarActive(trackBackItem, order)]}`}
                                    />
                                    <img
                                        alt=""
                                        src={trackBackItem?.imageActive?._url}
                                        className={`${styles.imageActive} ${styles[orderStatusBarActive(trackBackItem, order)]}`}
                                    />
                                    <img
                                        alt=""
                                        src={trackBackItem?.imageActive?._url}
                                        className={`${styles.imageActiveCanceled} ${styles[orderStatusBarActive(trackBackItem, order)]}`}
                                    />
                                    <p
                                        className={`${styles.nameStatus} ${styles[orderStatusBarActive(trackBackItem, order)]}`}
                                    >
                                        {trackBackItem?.status?.label}
                                    </p>
                                    <p
                                        className={`${styles.nameStatusCanceled} ${styles[orderStatusBarActive(trackBackItem, order)]}`}
                                    >
										Pagamento Cancelado
                                    </p>
                                </div>
                                <div className={styles.divisor} />
                            </section>
                        </div>
                    ))}
                </div>

                {/* {isDesktop &&  */}
                <div className={styles.stagesDescription}>
                    {trackBack?.trackBack?.map((trackBackItem: any, index: any) => (
                        <div
                            key={index}
                            className={`${styles.status} ${styles[orderStatusBarActive(trackBackItem, order)]}`}
                        >
                            <div
                                className={styles.message}
                                dangerouslySetInnerHTML={innerHTML(trackBackItem?.description)}
                            />
                        </div>
                    ))}
                </div>
                {/* } */}

                {/* <div className={styles.interactions}>
                    {order.status.value === 'nf' || order.status.value === 'sent' || order.status.value == 'available' || order.status.value === 'delivered' ?
                        <div className={styles.interaction}>
                            <h2>Nota Fiscal</h2>
                            <p className={styles.message}>
                                Segeu em anexo a sua Nota Fiscal!
                            </p>
                            <button onClick={() => window.open(order?.linkDanfe)}>Nota Fiscal</button>
                        </div>
                     : null}
                    {order.status.value === 'sent' || order.status.value == 'available' || order.status.value === 'delivered' ?
                        <div className={styles.interaction}>
                            <h2>Acompanhe seu pedido</h2>
                            <p className={styles.message}>
                                Clique no botão para rastrear seu pedido!
                            </p>
                            <button>Rastrear Pedido</button>
                        </div>
                    : null}
                    {order.status.value === 'delivered' && 
                        <div className={styles.interaction}>
                            <h2>Troca e devoluções</h2>
                            <p className={styles.message}>
                                Recebemos a confirmação de entrega do seu pedido. Agradecemos por comprar no site da Muna
                            </p>
                            <button>Troca ou Devolver</button>
                        </div>
                    }
                </div> */}
            </div>
        </div>
    );
};
