import { cloudflareLoader } from "@/core-nextv3/util/util";
import type { GetStaticProps } from "next";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { Checkbox } from "pretty-checkbox-react";
import { useEffect, useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import toast from "react-hot-toast";
import { PatternFormat } from "react-number-format";
import Select from "react-select";
import { addUserAuth, loginAuth } from "../../core-nextv3/auth/auth.api";
import { mergeCart } from "../../core-nextv3/cart/cart.api";
import { TDate } from "../../core-nextv3/model/TDate";
import { useOnEvent } from "../../core-nextv3/util/use.event";
import {
    validateCpf,
    validateDate,
    validateEmail,
    validatePhone,
} from "../../core-nextv3/util/validate";
import {
    CART_SETTING,
    RESELLER_SETTING,
    THEME_SETTING,
} from "../../setting/setting";
import withHeader from "../../utils/withHeader";
import { AnimatedLoading } from "../AnimatedLoading";
import ErrorMessage from "../ErrorMessage";
import { PageTitle } from "../PageTitle";
import styles from "./styles.module.scss";

const ResellerRegistration = () => 
{
    const router = useRouter();
    const [ cart, setCart ] = useState<any>();
    const [ gender, setGender ] = useState<any>();
    const [ hasNewsletter, setHasNewsletter ] = useState(false);
    const [ loadingUserData, setLoadingUserData ] = useState(false);
    const {
        register,
        handleSubmit,
        setValue,
        control,
        watch,
        formState: { errors, isSubmitted },
    } = useForm();

    const password = useRef({});
    password.current = watch("password", "");

    useEffect(() => 
    {
        register("gender", { value : null });
    }, []);

    useOnEvent("changeCart", (data: any) => 
    {
        setCart(data);
    });

    const genderOptions = [
        { label : "Não Quero Informar", value : "I", id : "IsQ9FFhx" },
        { label : "Masculino", value : "M", id : "yfcqOV15" },
        { label : "Feminino", value : "F", id : "BoXeFDWZ" },
    ];

    const customSelectStyles: any = {
        control : (base: any, state: any) => ({
            ...base,
            borderRadius : "4px",
            borderColor  : state.isFocused
                ? "var(--border-color)"
                : "var(--border-color)",
            boxShadow : state.isFocused ? "none" : null,
            "&:hover" : {
                borderColor : "var(--border-color)",
            },
            width  : "100%",
            color  : "#000",
            height : "45px",
        }),
        menu        : (styles: any) => ({ ...styles, color : "#000", fontSize : "16px" }),
        placeholder : (styles: any) => ({ ...styles, fontSize : "16px" }),
        singleValue : (styles: any) => ({
            ...styles,
            color    : "#000",
            fontSize : "16px",
        }),
        indicatorSeparator : () => null,
    };

    function handleChangeGender(e: any) 
    {
        setValue("gender", e);
        setGender(e);
    }

    const onSubmit = async (data: any) => 
    {
        await setLoadingUserData(true);

        const birthday = new TDate({ value : data.birthday, mask : "DD/MM/YYYY" });

        // CADASTRO
        const newData = {
            data : {
                name            : data.name,
                email           : data.email,
                cpf             : data.cpf,
                birthday        : birthday.toDate(),
                phone           : data.phone,
                gender          : data.gender,
                newsletter      : hasNewsletter,
                password        : data.password,
                confirmPassword : data.confirmPassword,
            },
        };

        const result: any = await addUserAuth(RESELLER_SETTING.merge(newData));

        if (!result.status) 
        {
            await setLoadingUserData(false);
            return toast.error(result.error, { duration : 2000 });
        }

        // LOGIN
        await loginAuth(
            RESELLER_SETTING.merge({
                login    : data.email.toLowerCase(),
                password : data.password,
            }),
        );

        // MERGE CART SESSION
        await mergeCart(
            CART_SETTING.merge({
                document : {
                    referencePath : cart?.referencePath,
                },
            }),
        );

        toast("Cadastro realizado com sucesso!", { icon : "👏", duration : 2000 });

        // LOGIN ANALYTICS
        // userLoginAnalytics(result2.data);

        await setLoadingUserData(false);

        router.push("/produtos/");
    };

    const onNewsletter = async () => 
    {
        setHasNewsletter(!hasNewsletter);
    };

    return (
        <>
            <div className={styles.resellerRegistration}>
                <div className={styles.content}>
                    <PageTitle
                        name="Criar Conta Para Revender"
                        parents={[ { name : "Login", url : "/revendedor/login" } ]}
                    />
                    <div className={styles.formData}>
                        <div className={styles.formContent}>
                            <div className={styles.columns}>
                                <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                                    <div className={styles.formItem}>
                                        <label>Nome completo</label>
                                        <input
                                            type="text"
                                            className={styles.input}
                                            autoComplete="new-password"
                                            {...register("name", {
                                                validate : (value) =>
                                                    value.length >= 1 || "Campo Requerido",
                                            })}
                                        />
                                        {errors.name && (
                                            <ErrorMessage message={errors.name?.message} />
                                        )}
                                    </div>

                                    <div className={styles.formItem}>
                                        <label>E-mail</label>
                                        <input
                                            style={{ textTransform : "lowercase" }}
                                            type="email"
                                            className={styles.input}
                                            required
                                            autoComplete="new-password"
                                            {...register("email", {
                                                validate : (value) =>
                                                    validateEmail(value) || "E-mail inválido",
                                            })}
                                        />
                                        {errors.email && (
                                            <ErrorMessage message={errors.email?.message} />
                                        )}
                                    </div>

                                    <div className={styles.formItem}>
                                        <label>CPF</label>
                                        <Controller
                                            name="cpf"
                                            control={control}
                                            rules={{
                                                validate : (value) =>
                                                    validateCpf(value) || "CPF inválido",
                                            }}
                                            render={({ field }) => (
                                                <PatternFormat
                                                    {...field}
                                                    format="###.###.###-##"
                                                    type="tel"
                                                    autoComplete="new-password"
                                                    allowEmptyFormatting={false}
                                                    className={styles.input}
                                                />
                                            )}
                                        />
                                        {errors.cpf && (
                                            <ErrorMessage message={errors.cpf?.message} />
                                        )}
                                    </div>

                                    <div className={styles.formItem}>
                                        <label>Data de nascimento</label>
                                        <Controller
                                            name="birthday"
                                            control={control}
                                            rules={{
                                                validate : (value) =>
                                                    validateDate(value) || "Data inválida",
                                            }}
                                            render={({ field }) => (
                                                <PatternFormat
                                                    {...field}
                                                    format="##/##/####"
                                                    type="tel"
                                                    autoComplete="new-password"
                                                    allowEmptyFormatting={false}
                                                    className={styles.input}
                                                />
                                            )}
                                        />
                                        {errors.birthday && (
                                            <ErrorMessage message={errors.birthday?.message} />
                                        )}
                                    </div>

                                    <div className={styles.formItem}>
                                        <label>Celular</label>
                                        <Controller
                                            name="phone"
                                            control={control}
                                            rules={{
                                                validate : (value) =>
                                                    validatePhone(value) || "Celular inválido",
                                            }}
                                            render={({ field }) => (
                                                <PatternFormat
                                                    {...field}
                                                    format="(##) #####-####"
                                                    type="tel"
                                                    autoComplete="new-password"
                                                    allowEmptyFormatting={false}
                                                    className={styles.input}
                                                />
                                            )}
                                        />
                                        {errors.phone && (
                                            <ErrorMessage message={errors.phone?.message} />
                                        )}
                                    </div>

                                    <div className={styles.formItem}>
                                        <label>Gênero</label>
                                        <Select
                                            placeholder="Escolha seu gênero"
                                            className={styles.select}
                                            options={genderOptions}
                                            styles={customSelectStyles}
                                            isClearable
                                            value={gender}
                                            onChange={handleChangeGender}
                                        />
                                        {isSubmitted && !gender && (
                                            <span className={styles.errorMessage}>
                                                <Image
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/AiOutlineExclamationCircle.svg"
                                                    loader={cloudflareLoader}
                                                    alt=""
                                                />
												Campo requiredo!
                                            </span>
                                        )}
                                    </div>

                                    <div className={styles.formItem}>
                                        <label>Senha</label>
                                        <input
                                            type="password"
                                            required
                                            className={styles.input}
                                            autoComplete="new-password"
                                            placeholder="Digite sua senha"
                                            {...register("password", {
                                                validate : (value) =>
                                                    value.length >= 8 ||
													"Sua senha deve conter no mínimo 8 caracteres",
                                            })}
                                        />
                                        {errors.password && (
                                            <ErrorMessage message={errors.password?.message} />
                                        )}
                                    </div>

                                    <div className={styles.formItem}>
                                        <label>Senha</label>
                                        <input
                                            type="password"
                                            required
                                            className={styles.input}
                                            autoComplete="new-password"
                                            placeholder="Confirme sua senha"
                                            {...register("confirmPassword", {
                                                validate : (value) =>
                                                    value === password.current ||
													"A confirmação de senha precisam ser iguais a senha",
                                            })}
                                        />
                                        {errors.confirmPassword && (
                                            <ErrorMessage message={errors.confirmPassword?.message} />
                                        )}
                                    </div>

                                    <div className={styles.formItem}>
                                        <label>Newsletter</label>
                                        <span className={styles.subtitle}>
											Deseja receber e-mails com promoções? Marque esta opção e
											atualize seus dados cadastrais.
                                        </span>
                                        <div>
                                            {/* <input
											autoComplete="new-password"
											type="checkbox"
											defaultChecked={user?.newsletter}
											{...register("newsletter")}
										/> */}
                                            <Checkbox
                                                color="success"
                                                checked={hasNewsletter}
                                                onChange={onNewsletter}
                                            >
												Quero receber e-mails com promoções
                                            </Checkbox>
                                        </div>
                                    </div>

                                    <div className={styles.buttons}>
                                        <button
                                            className={styles.submitted}
                                            type="button"
                                            disabled={loadingUserData && true}
                                            onClick={handleSubmit(onSubmit)}
                                        >
                                            {loadingUserData ? <AnimatedLoading /> : "Cadastrar"}
                                        </button>

                                        <button
                                            type="button"
                                            onClick={() => router.push("/revendedor/login")}
                                        >
											voltar
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

const getStaticProps: GetStaticProps = () =>
    withHeader(async (props: any) => 
    {
        return {
            revalidate : THEME_SETTING.revalidate,
            props      : {
                seo : props.seo.merge({ title : "Criar Conta" }),
            },
        };
    });

export { getStaticProps as GetStaticProps, ResellerRegistration };
