import { useState } from "react";
import { toast } from "react-hot-toast";
import { setPackaging } from "../../core-nextv3/cart/cart.api";
import { useCore } from "../../core-nextv3/core/core";
import {
    firstProductImage,
    validateListVariant,
} from "../../core-nextv3/product/product.util";
import { currencyMask } from "../../core-nextv3/util/mask";
import { CART_SETTING, THEME_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

export const Itempackaging = ({ product }: any) => 
{
    // console.log("aquiaaa", product)
    const { setCart } = useCore();
    const [ listVariant ] = useState<any>();
    const [ loadingAddToCart, setLoadingAddToCart ] = useState<any>(false);

    const handleAddToCart = async () => 
    {
        if (!validateListVariant(product, listVariant)) 
        {
            toast.error("Seleciona a variante do produto", {
                duration : 1000,
            });

            return;
        }

        setLoadingAddToCart(true);

        const newData = {
            data : {
                product : {
                    referencePath : product.referencePath,
                },
                variant  : listVariant,
                quantity : 1,
            },
        };

        const result = await setPackaging(CART_SETTING.merge(newData));

        setLoadingAddToCart(false);

        if (result.status === false) 
        {
            return toast.error(result.error, {
                duration : 1000,
            });
        }

        setCart(result.data);

        toast.success("Brinde adiconado com sucesso!", {
            icon     : "👏",
            duration : 2000,
        });
    };

    // async function handleChangeVariant(listVariant:any)
    // {
    //     setListVariant(listVariant);
    // }

    return (
        <div className={styles.itemPackaging}>
            <div className={styles.product}>
                <div className={styles.img}>
                    <ImageSet
                        width={480}
                        image={firstProductImage(product)}
                        aspectRatio={THEME_SETTING.aspectRatio}
                    />
                </div>
                <p className={styles.productName}>{product?.name}</p>
                <p className={styles.price}>{currencyMask(product?.price)}</p>

                {/* <VariantSelector
                    product={product}
                    changeVariant={(event:any) => { handleChangeVariant(event)}}
                    disabledQuantity={true}
                    disabledAddCart={true}
                />  */}

                <div className={styles.actions}>
                    <button
                        type="button"
                        className="block"
                        onClick={() => handleAddToCart()}
                    >
                        {loadingAddToCart ? <AnimatedLoading /> : "Adicionar Embalagem"}
                    </button>
                </div>
            </div>
        </div>
    );
};
