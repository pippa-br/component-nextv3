"use client";

import { useState } from "react";
import { CommentItem } from "../CommentItem";
import { ModalComments } from "../ModalComments";
import styles from "./styles.module.scss";

export const Comments = ({ comments, coment }: any) => 
{
    const [ openModalComments, setOpenModalComments ] = useState(false);

    return (
        <>
            <div className={styles.comments}>
                {/* <CommentsAssessments comments={comments} product={product} /> */}

                <div className={styles.commentsGrid}>
                    {/* {Array.from({ length: 4 }).map((coment: any, index: any) => ( */}
                    {/* {comments?.slice(0, 4)?.map((coment: any, index: any) => ( */}
                    <CommentItem coment={coment} />
                    {/* ))} */}
                </div>
            </div>

            {openModalComments && (
                <ModalComments comments={comments} close={setOpenModalComments} />
            )}
        </>
    );
};
