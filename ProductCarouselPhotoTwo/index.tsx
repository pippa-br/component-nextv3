"use client";

import { Splide, SplideSlide } from "@splidejs/react-splide";
import type { Options } from "@splidejs/splide";
import { AnimatePresence } from "framer-motion";
import { useEffect, useRef, useState } from "react";
import { ImageSet } from "../../component-nextv3/ImageSet";
import { useResponsive } from "../../core-nextv3/util/useResponsive";
import { THEME_SETTING } from "../../setting/setting";
import { ProductItemSkeleton } from "../ProductItemSkeleton";
import { ProductModalCarouselPhoto } from "../ProductModalCarouselPhoto";
import { VideosSet } from "../VideoSet/indx";
import styles from "./styles.module.scss";

export const ProductCarouselPhotoTwo = ({
    images,
    videos,
    sizes = "100vw",
    width = 1920,
    height = 400,
    enabledGif = false,
    vertical = false,
    perPage = 1,
    thumbsperPage = 4,
    thumbsCss = {},
    thumbsArrow = true,
    arrows = true,
}: any) => 
{
    const { isDesktop } = useResponsive();
    const [ currentSlide, setCurrentSlide ] = useState(0);
    const [ openModalCarouselPhoto, setOpenModalCarouselPhoto ] = useState(false);

    const mainRef = useRef<any>(undefined);
    const thumbRef = useRef<any>(undefined);

    const mainOptions: Options = {
        perPage    : images.length > 1 ? perPage : 1,
        perMove    : images.length > 1 ? perPage : 1,
        gap        : "1rem",
        pagination : false,
        slideFocus : true,
        arrows     : arrows,
        autoHeight : true,
        start      : 0,
    };

    const thumbsOptions: Options = {
        gap          : "1rem",
        perPage      : thumbsperPage * (images.length > 1 ? perPage : 1),
        pagination   : false,
        padding      : "1rem",
        //cover        : true,
        isNavigation : true,
        arrows       : thumbsArrow,
        start        : 0,
        autoHeight   : true,
        // autoHeight   : !vertical,
        // height       : vertical ? "100vh" : 'auto',
        direction    : vertical ? "ttb" : "ltr",
    };

    if (vertical) 
    {
        thumbsOptions.fixedWidth = 150;
        thumbsOptions.fixedHeight = 150 / (width / height);
        thumbsOptions.padding = "0rem";
    }

    useEffect(() => 
    {
        if (mainRef.current && thumbRef.current && thumbRef.current.splide) 
        {
            mainRef.current.sync(thumbRef.current.splide);

            const current: any = mainRef.current;

            current.splide.off("active");
            current.splide.on("active", (e: any) => 
            {
                setCurrentSlide(e.index - (videos ? videos.length : 0));

                if (e.slide.parentElement) 
                {
                    const divs = e.slide.getElementsByTagName("div");

                    if (divs.length > 0) 
                    {
                        const data = divs[0].getBoundingClientRect();
                        e.slide.parentElement.style.height = `${data.height}px`;
                    }
                }
            });
        }
    }, []);

    return images.length > 0 ? (
        <>
            {/* <div className={styles.productCarouselPhoto + ' ' + (vertical ? styles.vertical : '')}> */}
            <div
                className={`${THEME_SETTING.carouselImagesSticky ? styles.carouselSticky : `${styles.productCarouselPhoto} ${vertical ? styles.vertical : ""} ${styles[`perPage-${perPage}`]}`}`}
            >
                <Splide
                    options={mainOptions}
                    aria-labelledby=""
                    ref={mainRef}
                    className={styles.slideContent}
                >
                    {videos?.map((video: any, index: any) => (
                        <SplideSlide key={index} className={styles.slideItem}>
                            <VideosSet
                                video={video}
                                autoplay={true}
                                loop={true}
                                width={width}
                                height={height}
                                controls={false}
                            />
                        </SplideSlide>
                    ))}
                    {images?.map((image: any, index: any) => (
                        <SplideSlide
                            className={styles.slideItem}
                            key={index}
                            onClick={() => 
                            {
                                setOpenModalCarouselPhoto(isDesktop);
                            }}
                        >
                            <ImageSet
                                image={image}
                                width={width}
                                height={height}
                                sizes={sizes}
                                enabledGif={enabledGif}
                            />
                        </SplideSlide>
                    ))}
                </Splide>

                {THEME_SETTING?.disabledCarouselPhotosSmall ? (
                    <div className={styles.desktop}>
                        <Splide
                            options={thumbsOptions}
                            aria-label=""
                            ref={thumbRef}
                            className={styles.thumbContent}
                            style={thumbsCss}
                        >
                            {videos?.map((video: any, index: any) => (
                                <SplideSlide key={index}>
                                    <VideosSet
                                        width={width}
                                        height={height}
                                        video={video}
                                        autoplay={true}
                                        loop={true}
                                        controls={false}
                                    />
                                </SplideSlide>
                            ))}
                            {images?.map((image: any, index: any) => (
                                <SplideSlide key={index}>
                                    <ImageSet
                                        width={width}
                                        height={height}
                                        image={image}
                                        sizes={sizes}
                                    />
                                </SplideSlide>
                            ))}
                        </Splide>
                    </div>
                ) : (
                    <div className={styles.mobile}>
                        <Splide
                            options={thumbsOptions}
                            aria-label=""
                            ref={thumbRef}
                            className={styles.thumbContent}
                            style={thumbsCss}
                        >
                            {videos?.map((video: any, index: any) => (
                                <SplideSlide key={index}>
                                    <VideosSet
                                        video={video}
                                        width={width}
                                        height={height}
                                        field="thumb"
                                        autoplay={true}
                                        loop={true}
                                        controls={false}
                                    />
                                </SplideSlide>
                            ))}
                            {images?.map((image: any, index: any) => (
                                <SplideSlide key={index}>
                                    <ImageSet
                                        image={image}
                                        width={width}
                                        height={height}
                                        sizes={sizes}
                                    />
                                </SplideSlide>
                            ))}
                        </Splide>
                    </div>
                )}

                <ImageSet
                    className={`${styles.carouselSvg} ${styles.desktop}`}
                    width={20}
                    height={20}
                    src="/assets/icons/BiSearch.svg"
                    responsive={false}
                    alt=""
                />
            </div>

            <AnimatePresence initial={false} mode="wait" onExitComplete={() => null}>
                {openModalCarouselPhoto && (
                    <ProductModalCarouselPhoto
                        //aspectRatio={aspectRatio}
                        width={width}
                        height={height}
                        startIndex={currentSlide}
                        images={images}
                        setOpenModalCarouselPhoto={setOpenModalCarouselPhoto}
                    />
                )}
            </AnimatePresence>
        </>
    ) : (
        <ProductItemSkeleton />
    );
};
