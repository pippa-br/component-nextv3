import { cloudflareLoader } from "@/core-nextv3/util/util";
import Image from "next/image";
import { useState } from "react";
import styles from "./styles.module.scss";

export const AccordionDescriptionProduct = ({ children, title }: any) => 
{
    const [ toggle, setToggle ] = useState(false);

    return (
        <div
            className={styles.accordionDescriptionProduct}
            onClick={() => setToggle(!toggle)}
        >
            <p
                className={`${styles.accordionTitle} ${toggle ? styles.open : styles.closed}`}
            >
                {title}
                {toggle ? (
                    <Image
                        className={styles.accordionSvg}
                        width={20}
                        height={20}
                        src="/assets/icons/TiArrowSortedUp.svg"
                        loader={cloudflareLoader}
                        alt=""
                    />
                ) : (
                    <Image
                        className={styles.accordionSvg}
                        width={20}
                        height={20}
                        src="/assets/icons/TiArrowSortedDown.svg"
                        loader={cloudflareLoader}
                        alt=""
                    />
                )}
            </p>
            <div
                className={`${styles.accordionContent} ${
                    toggle ? styles.open : styles.closed
                }`}
            >
                {children}
            </div>
        </div>
    );
};
