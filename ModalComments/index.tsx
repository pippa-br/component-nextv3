import { CommentItem } from "../CommentItem";
import { CommentsRating } from "../CommentsRating";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

export const ModalComments = ({ comments, close }: any) => 
{
    return (
        <div className={styles.modalComments} onClick={() => close(false)}>
            <div className={styles.container} onClick={(e) => e.stopPropagation()}>
                <div className={styles.top} onClick={() => close(false)}>
                    <ImageSet
                        className={styles.closeModal}
                        width={20}
                        height={20}
                        src="/assets/icons/GrFormClose.svg"
                        responsive={false}
                        alt=""
                    />
                </div>
                <h1>Todos os comentários</h1>

                <CommentsRating comments={comments} />

                <div className={styles.commentsGrid}>
                    {comments?.map((coment: any, index: any) => (
                        <CommentItem coment={coment} key={index} />
                    ))}
                </div>
            </div>
        </div>
    );
};
