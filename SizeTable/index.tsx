import styles from "./styles.module.scss";

export function SizeTable({ table }: any) 
{
    return (
        <div className={styles.sizeTable}>
            <div className={styles.content}>
                {/* <p className={styles.message}>Tabela de Medidas</p> */}
                <table>
                    <thead>
                        <tr>
                            {table[0].items.map((head: any, key: number) => (
                                <td key={key}>{head.label}</td>
                            ))}
                        </tr>
                    </thead>
                    <tbody>
                        {table.map((row: any, key: number) => (
                            <tr key={key}>
                                {row.items.map((column: any, key2: number) => (
                                    <td key={key2}>{column.value}</td>
                                ))}
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
