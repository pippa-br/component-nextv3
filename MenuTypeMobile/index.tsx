import { cloudflareLoader } from "@/core-nextv3/util/util";
import Image from "next/image";
import { useState } from "react";
import CustomLink from "../CustomLink";
import styles from "./styles.module.scss";

export const MenuTypeMobile = ({ closeModal, item }: any) => 
{
    const [ openSubMenu, setOpenSubMenu ] = useState(false);

    const getMenu = (item: any) => 
    {
        switch (item?.type?.value) 
        {
            case "path":
                return (
                    <p>
                        <CustomLink
                            href={`${item.link}`}
                            prefetch={true}
                            onClick={() => 
                            {
                                closeModal(false);
                            }}
                            key={item.id}
                        >
                            {item?.name}
                        </CustomLink>
                    </p>
                );
            case "category":
                return (
                    <>
                        {item?.category?._children?.length > 0 ? (
                            <li>
                                <p>
                                    <CustomLink
                                        href="#"
                                        prefetch={true}
                                        className={styles.subMenuNavigation}
                                        onClick={() => setOpenSubMenu(!openSubMenu)}
                                    >
                                        {item.name}
                                        <Image
                                            className={styles.Arrow}
                                            width={20}
                                            height={20}
                                            src="/assets/icons/IoIosArrowForward.svg"
                                            loader={cloudflareLoader}
                                            alt=""
                                        />
                                    </CustomLink>
                                </p>
                                {openSubMenu && (
                                    <div className={styles.subMenuChildren}>
                                        <p>
                                            <CustomLink
                                                href={`/categoria/${item?.category?.slug}/`}
                                                prefetch={true}
                                                onClick={() => 
                                                {
                                                    closeModal(false);
                                                }}
                                            >
												Ver Todos
                                            </CustomLink>
                                        </p>
                                        {item?.category?._children?.map((child: any) => (
                                            <p key={child?.any}>
                                                <CustomLink
                                                    href={`/categoria/${child?.slug}/`}
                                                    prefetch={true}
                                                    onClick={() => 
                                                    {
                                                        closeModal(false);
                                                    }}
                                                >
                                                    {child?.name}
                                                </CustomLink>
                                            </p>
                                        ))}
                                    </div>
                                )}
                            </li>
                        ) : (
                            <p>
                                <CustomLink
                                    href={`/categoria/${item?.category?.slug}/`}
                                    prefetch={true}
                                    onClick={() => 
                                    {
                                        closeModal(false);
                                    }}
                                    key={item.id}
                                >
                                    {item?.name}
                                </CustomLink>
                            </p>
                        )}
                    </>
                );
            case "collection":
                return (
                    <p>
                        <CustomLink
                            href={`/colecao/${item?.collection?.slug}/`}
                            prefetch={true}
                            onClick={() => 
                            {
                                closeModal(false);
                            }}
                            key={item.id}
                        >
                            {item?.name}
                        </CustomLink>
                    </p>
                );
            default:
                return null;
        }
    };

    return (
        <div className={styles.menuTypeMobile}>
            {item?._children?.length > 0 ? (
                <li className={styles.menuTypeChild}>
                    <p
                        className={styles.subMenuNavigation}
                        onClick={() => setOpenSubMenu(!openSubMenu)}
                    >
                        {item.name}
                        <Image
                            width={20}
                            height={20}
                            src="/assets/icons/IoIosArrowForward.svg"
                            loader={cloudflareLoader}
                            alt=""
                        />
                    </p>
                    {openSubMenu && (
                        <div className={styles.subMenuChildren}>
                            {item?._children?.map(
                                (child: any) =>
                                    child.status && <p key={child?.any}>{getMenu(child)}</p>,
                            )}
                        </div>
                    )}
                </li>
            ) : (
                item.status && getMenu(item)
            )}
        </div>
    );
};
