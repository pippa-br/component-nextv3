export const modalBagAnimation = {
    hidden : {
        opacity : 0,
        x       : 10,
    },
    show : {
        opacity    : 1,
        x          : 0,
        transition : {
            duration : 0.5,
            type     : "tween",
        },
    },
    exit : {
        opacity    : 0,
        x          : 10,
        transition : {
            duration : 0.5,
            type     : "tween",
        },
    },
};
