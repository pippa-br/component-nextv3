import { Suspense } from "react";
import { PerfilDisplay } from "../PerfilPage/perfil.display";

const PedidosExpress = () => 
{
    return (
        <Suspense>
            <PerfilDisplay />
        </Suspense>
    );
};

// const getStaticProps: GetStaticProps = () =>
//   withHeader(async (props: any) => {
//     return {
//       revalidate : THEME_SETTING.revalidate,
//       props: {
//         seo: props?.seo?.merge({ title: "Pedido Express" }),
//       },
//     };
//   });

export { PedidosExpress };
