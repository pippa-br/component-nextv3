import {
    calculateInstallments,
    discount,
    hasPaymentMethod,
    percentageByPaymentMethod,
} from "../../core-nextv3/price/price.util";
import { currencyMask } from "../../core-nextv3/util/mask";
import styles from "./styles.module.scss";

export const DiscountPaymentMethod = ({
    totalItems = 0,
    totalDiscount = 0,
    totalShipping = 0,
    discountPaymentMethod = 0,
    installmentRule = null,
}: any) => 
{
    return (
        <>
            {installmentRule && (
                <div className={styles.payments}>
                    {
                        <div className={styles.payment}>
                            <label>
								Pagamento no PIX{" "}
                                {percentageByPaymentMethod("pix", installmentRule) > 0 && (
                                    <span>
										({percentageByPaymentMethod("pix", installmentRule)}% OFF)
                                    </span>
                                )}
                            </label>
                            <div className={styles.items}>
                                <p>
                                    {currencyMask(
                                        discount(
                                            totalItems + totalDiscount - discountPaymentMethod,
                                            percentageByPaymentMethod("pix", installmentRule),
                                            false,
                                        ) + totalShipping,
                                    )}
                                </p>
                            </div>
                        </div>
                    }
                    <div className={styles.payment}>
                        <label>Parcelamento no Cartão</label>
                        <div className={styles.items}>
                            {calculateInstallments(
                                totalItems +
									totalShipping +
									totalDiscount -
									discountPaymentMethod,
                                installmentRule,
                            )?.map((item: any, index: number) => (
                                <p key={index}>{item.label}</p>
                            ))}
                        </div>
                    </div>
                    {hasPaymentMethod("boleto", installmentRule) && (
                        <div className={styles.payment}>
                            <label>
								Pagamento no boleto{" "}
                                {percentageByPaymentMethod("boleto", installmentRule) > 0 && (
                                    <span>
										({percentageByPaymentMethod("boleto", installmentRule)}%
										OFF)
                                    </span>
                                )}
                            </label>
                            <div className={styles.items}>
                                <p>
                                    {currencyMask(
                                        discount(
                                            totalItems + totalDiscount - discountPaymentMethod,
                                            percentageByPaymentMethod("boleto", installmentRule),
                                            false,
                                        ) + totalShipping,
                                    )}
                                </p>
                            </div>
                        </div>
                    )}
                </div>
            )}
        </>
    );
};
