import Types from "@/core-nextv3/type";
import { call } from "@/core-nextv3/util/call.api";
import parse from "html-react-parser";
import { useEffect, useState } from "react";
import ReactDOMServer from "react-dom/server";
import { useLocale } from "../LocaleProvider";

export const Translation = ({ children }: any) => 
{
    const { locale } = useLocale();
    const [ translatedText, setTranslatedText ] = useState(children);

    useEffect(() => 
    {
        const translateText = async () => 
        {
            try 
            {
                const result = await call(Types.TRANSLATE_UTIL_SERVER, {
                    text     : ReactDOMServer.renderToStaticMarkup(children),
                    language : locale,
                });

                setTranslatedText(result.data);
            }
            catch (_e) 
            {
                _e;
            }
        };

        if (children && locale && locale !== "pt") 
        {
            translateText();
        }
    }, [ children, locale ]);

    return (
        <>
            {typeof translatedText === "string"
                ? parse(translatedText)
                : translatedText}
        </>
    );
};
