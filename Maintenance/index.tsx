import { cloudflareLoader } from "@/core-nextv3/util/util";
import { THEME_SETTING } from "@/setting/setting";
import Image from "next/image";
import styles from "./styles.module.scss";

export default function MaintenancePage({ account }: any) 
{
    return (
        <div
            className={
                THEME_SETTING.bannerMaintenancePage
                    ? styles.maintenancePageHeight
                    : styles.maintenancePage
            }
        >
            <Image
                loader={cloudflareLoader}
                width={40}
                height={40}
                className={styles.banner}
                src="/assets/image-informative.png"
                alt=""
            />

            <div className={styles.content}>
                <img src={account?.logoLogin?._url} alt="" />
                <h1>Em breve!</h1>
            </div>
        </div>
    );
}

export { MaintenancePage };
