import { useCore } from "@/core-nextv3/core/core";
import { getRecaptcha } from "@/core-nextv3/util/util";
import { Step, StepLabel, Stepper } from "@material-ui/core";
import type { GetServerSideProps, NextPage } from "next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import toast from "react-hot-toast";
import { CheckoutPayment } from "../../component-nextv3/CheckoutPayment";
import { ImageSet } from "../../component-nextv3/ImageSet";
import {
    firstImageItemCart,
    hasVariantItemCart,
    variantItemCart,
} from "../../core-nextv3/cart/cart.util";
import { getDocument } from "../../core-nextv3/document/document.api";
import {
    addPaymentOrder,
    setCreditCardOrder,
    setInstallmentOrder,
    setPaymentMethodOrder,
} from "../../core-nextv3/order/order.api";
import { calls } from "../../core-nextv3/util/call.api";
import { currencyMask } from "../../core-nextv3/util/mask";
import { ORDER_SETTING, THEME_SETTING } from "../../setting/setting";
import withHeader from "../../utils/withHeader";
import styles from "./styles.module.scss";

const CheckoutOrderPage: NextPage = ({ account, gateway, order }: any) => 
{
    //console.error('xxx', gateway);

    const router = useRouter();
    const { cart, setCart } = useCore();
    const [ installmentRule, setInstallmentRule ] = useState();
    const [ paymentMethodsList, setPaymentMethodsList ] = useState([]);
    const [ loadingCart, setLoadingCart ] = useState(false);
    const [ checkoutHeader, setCheckoutHeader ] = useState(true);
    const [ sameAddressShiping, setSameAddressShiping ] = useState(true);
    const [ activeStep, setActiveStep ] = useState<number>(0);
    const steps = getSteps();
    // const {
    //     register,
    //     handleSubmit,
    //     setValue,
    //     formState: { errors },
    // } = useForm();

    function getSteps() 
    {
        return [ "Pagamento" ];
    }

    useEffect(() => 
    {
        if (!order.paymentMethod) 
        {
            onSubmitCartPaymentMethod({ id : "uDHiRIsa", label : "PIX", value : "pix" });
        }

        setCart(order);
    }, [ order ]);

    useEffect(() => 
    {
        if (gateway?.installmentRule) 
        {
            const list: any = [];

            for (const item of gateway.installmentRule.paymentMethods) 
            {
                list.push(item.type);
            }

            setPaymentMethodsList(list);
            setInstallmentRule(gateway.installmentRule);
        }
    }, [ gateway ]);

    function getStepContent(step: any) 
    {
        switch (step) 
        {
            case 0:
                return (
                    <CheckoutPayment
                        setLoadingCart={setLoadingCart}
                        loadingCart={loadingCart}
                        onSubmit={onSubmitCheckout}
                        onCreditCardInstallment={handleCreditCardInstallment}
                        onSubmitPayment={onSubmitCartPaymentMethod}
                        paymentMethods={paymentMethodsList}
                        installmentRule={installmentRule}
                        sameAddressShiping={sameAddressShiping}
                        setSameAddressShiping={setSameAddressShiping}
                        goBack={handleBack}
                    />
                );
            default:
                return "Unknown step";
        }
    }

    // const isStepSkipped = (step: any) =>
    // {
    //     return skipped.has(step);
    // };

    // const handleNext = () =>
    // {
    //     let newSkipped = skipped;

    //     if (isStepSkipped(activeStep))
    //     {
    //         newSkipped = new Set(newSkipped.values());
    //         newSkipped.delete(activeStep);
    //     }

    //     setActiveStep((prevActiveStep) => prevActiveStep + 1);
    //     setSkipped(newSkipped);
    // };

    const handleBack = () => 
    {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    async function handleCreditCardInstallment(installment: any) 
    {
        const newData = {
            document : {
                referencePath : order?.referencePath,
            },
            data : installment,
        };

        setLoadingCart(true);

        const result = await setInstallmentOrder(ORDER_SETTING.merge(newData));

        setLoadingCart(false);

        if (result.status) 
        {
            setCart(result.data);
        }
        else 
        {
            return toast.error(
                "Ocorreu um erro! Verifique se foi selecionado um parcelamento válido.",
                { duration : 2000 },
            );
        }
    }

    const onSubmitCartPaymentMethod = async (data: any) => 
    {
        const newData = {
            document : {
                referencePath : order?.referencePath,
            },
            data : data,
        };

        setLoadingCart(true);

        const result = await setPaymentMethodOrder(ORDER_SETTING.merge(newData));

        setLoadingCart(false);

        if (result.data) 
        {
            setCart(result.data);
        }
        else 
        {
            return toast.error(
                "Ocorreu um erro! Verifique se foi selecionado um método válido.",
                { duration : 2000 },
            );
        }
    };

    const onSubmitCheckout = async (data: any) => 
    {
        if (cart?.paymentMethod === "") 
        {
            return toast.error("Selecione a Forma de pagamento", { duration : 2000 });
        }

        if (cart?.paymentMethod?.value === "credit_card") 
        {
            if (!cart?.installment) 
            {
                return toast.error("Selecione o parcelamento", { duration : 2000 });
            }

            setLoadingCart(true);

            const creditCardData: any = {
                document : {
                    referencePath : order?.referencePath,
                },
                data : {
                    cardnumber : data.cardnumber,
                    owner      : data.owner,
                    cvv        : data.cvv,
                    expirydate : data.expirydate,
                    docType    : {
                        value : "cpf",
                        label : "CPF",
                        id    : "cpf",
                        type  : "individual",
                    },
                    _cpf               : data.cpf,
                    sameAddressShiping : sameAddressShiping,
                    address            : {
                        zipcode : sameAddressShiping
                            ? cart.address.zipcode
                            : data?.cep || "",
                        street : sameAddressShiping
                            ? cart.address.street
                            : data?.street || "",
                        housenumber : sameAddressShiping
                            ? cart.address.housenumber
                            : data?.housenumber || "",
                        complement : sameAddressShiping
                            ? cart.address.complement
                            : data?.complement || "",
                        district : sameAddressShiping
                            ? cart.address.district
                            : data?.district || "",
                        city    : sameAddressShiping ? cart.address.city : data?.city || "",
                        state   : sameAddressShiping ? cart.address.state : data?.state || "",
                        country : { id : "br", label : "Brasil", value : "br", selected : true },
                    },
                },
            };

            const token = await getRecaptcha("setCreditCard");

            if (token) 
            {
                creditCardData.token = token;

                const result = await setCreditCardOrder(
                    ORDER_SETTING.merge(creditCardData),
                );
                setLoadingCart(false);

                if (result?.status) 
                {
                    //cart.creditCard = result.data.creditCard;
                    //setCart(cart);
                }
                else 
                {
                    return toast.error("Erro. Verifique os dados do cartão.", {
                        duration : 2000,
                    });
                }
            }
            else 
            {
                setLoadingCart(false);
                return toast.error("token invalido!", { duration : 2000 });
            }
        }

        toast.success("Pedido sendo processado! Estamos te redirecionando...", {
            duration : 1000,
        });

        setLoadingCart(true);

        const token = await getRecaptcha("checkout");

        if (token) 
        {
            const result2 = await addPaymentOrder(
                ORDER_SETTING.merge({
                    token    : token,
                    document : {
                        referencePath : cart?.referencePath,
                    },
                }),
            );

            setLoadingCart(false);

            if (!result2.status) 
            {
                return toast.error(result2.error, { duration : 2000 });
            }

            router.push(
                `/sucesso/${result2.data.id}?success=true&key=${new Date().getTime()}`,
            );
        }
        else 
        {
            toast.error("token invalido!", { duration : 2000 });
            setLoadingCart(false);
        }
    };

    return (
        <main>
            <div className={styles.checkoutForm}>
                <div className={styles.content}>
                    <div className={styles.checkoutStepper}>
                        <img
                            className={styles.logo}
                            src={account?.logoLogin?._url}
                            alt={account?.name}
                        />
                        <Stepper activeStep={activeStep}>
                            {steps.map((label, index) => 
                            {
                                const stepProps = {};
                                const labelProps = {};
                                return (
                                    <Step key={index} {...stepProps}>
                                        <StepLabel {...labelProps}>{label}</StepLabel>
                                    </Step>
                                );
                            })}
                        </Stepper>
                        <div>
                            {activeStep === steps.length ? (
                                <div className={styles.instructions}>
									All steps completed - you&apos;re finished
                                </div>
                            ) : (
                                <div className={styles.instructions}>
                                    {getStepContent(activeStep)}
                                </div>
                            )}
                        </div>
                    </div>
                    <div className={styles.checkoutCart}>
                        <div className={styles.checkoutHeader}>
                            <p className={styles.title}>
								Resumo do pedido:{" "}
                                {!checkoutHeader && (
                                    <ImageSet
                                        onClick={() => setCheckoutHeader(true)}
                                        className={styles.noActive}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/TiArrowSortedDown.svg"
                                        responsive={false}
                                        alt=""
                                    />
                                )}
                                {checkoutHeader && (
                                    <ImageSet
                                        onClick={() => setCheckoutHeader(false)}
                                        className={styles.active}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/TiArrowSortedUp.svg"
                                        responsive={false}
                                        alt=""
                                    />
                                )}
                            </p>
                        </div>
                        {checkoutHeader && (
                            <>
                                <div className={styles.body}>
                                    {cart?.items?.length > 0 &&
										cart?.items.map((item: any) => (
										    <div key={item?.id} className={styles.item}>
										        <div className={styles.product}>
										            {item.images && (
										                <div className={styles.img}>
										                    <ImageSet
										                        image={firstImageItemCart(item)}
										                        width={THEME_SETTING.widthProductThumb}
										                        height={THEME_SETTING.heightProductThumb}
										                        sizes="25vw"
										                    />
										                </div>
										            )}
										            <div className={styles.info}>
										                <div className={styles.productName}>
										                    {item.name}
										                </div>
										                <div className={styles.productPrice}>
										                    {currencyMask(item.price)}
										                </div>
										                {hasVariantItemCart(item, 0) && (
										                    <p className={styles.productColor}>
																Cor: {variantItemCart(item, 0)}
										                    </p>
										                )}
										                {hasVariantItemCart(item, 1) && (
										                    <p className={styles.productColor}>
																Tamanho: {variantItemCart(item, 1)}
										                    </p>
										                )}
										                <p className={styles.productQuantity}>
															Quantidade: {item.quantity}
										                </p>
										            </div>
										        </div>
										        <div className={styles.price}>
										            {currencyMask(item.total)}
										        </div>
										    </div>
										))}
                                </div>
                                <div className={styles.checkoutResume}>
                                    <div className={styles.checkoutSubtotal}>
                                        <p>
											Subtotal <span>{currencyMask(cart?.totalItems)}</span>
                                        </p>
                                        <p>
											Desconto <span>{currencyMask(cart?.totalDiscount)}</span>
                                        </p>
                                        <p>
											Frete
                                            <span>{currencyMask(cart?.shipping?.value || 0)}</span>
                                        </p>
                                        <p>
											Juros
                                            <span>{currencyMask(cart?.totalInterest || 0)}</span>
                                        </p>
                                    </div>
                                    <div className={styles.checkoutTotal}>
                                        <p>
											Total <span>{currencyMask(cart?.total)}</span>
                                        </p>
                                    </div>
                                </div>
                            </>
                        )}
                    </div>
                </div>
                <div className={styles.security}>
                    <p>
                        <strong>Pagamento Seguro SSL</strong>
                        <br />
						Sua criptografia é protegida por criptografia SSL de 256 bits.
                    </p>
                    <ImageSet
                        width={300}
                        height={58}
                        responsive={false}
                        src="/assets/seguranca.png"
                        alt="Segurança"
                    />
                </div>
            </div>
        </main>
    );
};

const getServerSideProps: GetServerSideProps = ({ params }: any) =>
    withHeader(async () => 
    {
        const [ order ] = await calls(
            getDocument(
                ORDER_SETTING.merge({
                    id : `${params.order}`,
                }),
            ),
        );

        if (!order.data) 
        {
            return {
                redirect : {
                    destination : "/",
                },
            };
        }

        if (
            order.data.statusPayment === "Pago" ||
			order.data.statusPayment === "Cancelado"
        ) 
        {
            return {
                redirect : {
                    permanent   : false,
                    destination : `/sucesso/${order.data.id}`,
                },
                props : {},
            };
        }

        return {
            props : {
                order : order?.data || {},
            },
        };
    });

export { getServerSideProps as GetServerSideProps, CheckoutOrderPage };
