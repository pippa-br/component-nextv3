import type { GetStaticProps } from "next";
import { PageTitle } from "../../component-nextv3/PageTitle";
import { THEME_SETTING } from "../../setting/setting";
import withHeader from "../../utils/withHeader";
import LoginFormReseller from "../LoginFormReseller";
import styles from "./styles.module.scss";

const LoginPageReseller = () => 
{
    // const loadProtectedAuth = useProtectedAuth(RESELLER_SETTING, '/', '');

    // if(!loadProtectedAuth)
    // {
    //     return (<LoadingPage/>)
    // }

    return (
        <>
            <div className={styles.loginPageReseller}>
                <div className={styles.content}>
                    <PageTitle name="Login Revendedor" />

                    <LoginFormReseller />
                </div>
            </div>
        </>
    );
};

const getStaticProps: GetStaticProps = () =>
    withHeader(async (props: any) => 
    {
        return {
            revalidate : THEME_SETTING.revalidate,
            props      : {
                seo : props.seo.merge({ title : "Login Revendedor" }),
            },
        };
    });

export { getStaticProps as GetStaticProps, LoginPageReseller };
