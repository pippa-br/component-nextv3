import router from "next/router";
import styles from "./styles.module.scss";

const Custom500 = () => 
{
    return (
        <main className={styles.page500}>
            <div className={styles.info}>
                <h1>PÁGINA NÃO ENCONTRADA</h1>
                <p>LAMENTAMOS, MAS ESTA PÁGINA JÁ NÃO ESTÁ DISPONÍVEL NO NOSSO SITE.</p>
                <button onClick={() => router.push("/")}>
					Voltar à página principal
                </button>
            </div>
        </main>
    );
};

export { Custom500 };
