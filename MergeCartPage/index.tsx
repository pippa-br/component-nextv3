import { Suspense } from "react";
import { MergeCartDisplay } from "./merge.display";

const MergeCartPage = () => 
{
    return (
        <Suspense>
            <MergeCartDisplay />
        </Suspense>
    );
};

export { MergeCartPage };
