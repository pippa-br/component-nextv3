"use client";

import { useMergeCart } from "@/core-nextv3/cart/cart.use";
import { useCustomPush } from "@/core-nextv3/util/util.use";
import { CART_SETTING } from "../../setting/setting";
import styles from "./styles.module.scss";

const MergeCartDisplay = () => 
{
    const customPush = useCustomPush();

    useMergeCart(CART_SETTING, () => 
    {
        customPush("/checkout/");
    });

    return (
        <div className={styles.mergeCartPage}>
            <div className={styles.content}>
                <p>Carregando...</p>
            </div>
        </div>
    );
};

export { MergeCartDisplay };
