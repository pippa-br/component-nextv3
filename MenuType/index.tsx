import { generateKey } from "@/core-nextv3/util/util";
import CustomLink from "../CustomLink";
import styles from "./styles.module.scss";

export const MenuType = ({ menu }: any) => 
{
    const getMenu = (item: any, index: string) => 
    {
        if (item.status) 
        {
            switch (item?.type?.value) 
            {
                case "path":
                    return (
                        <>
                            {item?._children && item?._children?.length > 0 ? (
                                <li className={styles.dropdown} key={`path2-${index}`}>
                                    <CustomLink
                                        href={item?.link || "#"}
                                        prefetch={true}
                                        className={`${item.highlight ? styles.highlight : ""} ${item.bold ? styles.bold : ""} ${item.differentFont ? styles.differentFont : ""}`}
                                    >
                                        {item?.name}
                                    </CustomLink>
                                    <ul className={styles.dropdownMenu}>
                                        {item?.addSeeAll && (
                                            <li>
                                                <CustomLink href={item?.link || "#"} prefetch={true}>
													Ver Todos
                                                </CustomLink>
                                            </li>
                                        )}
                                        {item?._children?.map((child: any, index2: number) =>
                                            getMenu(child, `path-child-${index}-${index2}`),
                                        )}
                                    </ul>
                                </li>
                            ) : (
                                <li key={`path2-${index}`}>
                                    <CustomLink
                                        href={item?.link || "#"}
                                        prefetch={true}
                                        className={`${item.highlight ? styles.highlight : ""} ${item.bold ? styles.bold : ""} ${item.differentFont ? styles.differentFont : ""}`}
                                    >
                                        {item?.name}
                                    </CustomLink>
                                </li>
                            )}
                        </>
                    );
                case "category":
                    return (
                        <>
                            {item?.category?._children &&
							item?.category?._children?.length > 0 ? (
                                    <li className={styles.dropdown} key={`category2-${index}`}>
                                        <CustomLink
                                            href={`/categoria/${item?.category?.slug}/`}
                                            prefetch={true}
                                            className={`${item.highlight ? styles.highlight : ""} ${item.bold ? styles.bold : ""} ${item.differentFont ? styles.differentFont : ""}`}
                                        >
                                            {item?.category?.name}
                                        </CustomLink>
                                        <ul className={styles.dropdownMenu}>
                                            {item?.addSeeAll && item?.category?.slug && (
                                                <li>
                                                    <CustomLink
                                                        href={`/categoria/${item?.category?.slug}/`}
                                                        prefetch={true}
                                                    >
													Ver Todos
                                                    </CustomLink>
                                                </li>
                                            )}
                                            {item?.category?._children?.map(
                                                (child: any, index2: number) =>
                                                    getMenu(
                                                        { type : { value : "category" }, category : child },
                                                        `category-child-${index}-${index2}`,
                                                    ),
                                            )}
                                        </ul>
                                    </li>
                                ) : (
                                    <li key={`category2-${index}`}>
                                        <CustomLink
                                            href={`/categoria/${item?.category?.slug}/`}
                                            prefetch={true}
                                            className={`${item.highlight ? styles.highlight : ""} ${item.bold ? styles.bold : ""} ${item.differentFont ? styles.differentFont : ""}`}
                                        >
                                            {item?.name}
                                        </CustomLink>
                                    </li>
                                )}
                        </>
                    );
                case "collection":
                    return (
                        <>
                            {item?._children && item?._children?.length > 0 ? (
                                <li
                                    className={styles.dropdown}
                                    key={`collection2-${generateKey()}`}
                                >
                                    <CustomLink
                                        href={`/colecao/${item?.collection?.slug}/`}
                                        className={`${item.highlight ? styles.highlight : ""} ${item.bold ? styles.bold : ""} ${item.differentFont ? styles.differentFont : ""}`}
                                    >
                                        {item?.name}
                                    </CustomLink>
                                    <ul className={styles.dropdownMenu}>
                                        {item?.addSeeAll && item?.collection?.slug && (
                                            <li>
                                                <CustomLink
                                                    href={`/colecao/${item?.collection?.slug}/`}
                                                >
													Ver Todos
                                                </CustomLink>
                                            </li>
                                        )}
                                        {item?._children?.map((child: any, index2: number) =>
                                            getMenu(child, `collection-child-${index}-${index2}`),
                                        )}
                                    </ul>
                                </li>
                            ) : (
                                <li key={`collection2-${index}`}>
                                    <CustomLink
                                        href={`/colecao/${item?.collection?.slug}/`}
                                        className={`${item.highlight ? styles.highlight : ""} ${item.bold ? styles.bold : ""} ${item.differentFont ? styles.differentFont : ""}`}
                                    >
                                        {item?.name}
                                    </CustomLink>
                                </li>
                            )}
                        </>
                    );
                default:
                    return null;
            }
        }
    };

    return (
        <div className={styles.menuType}>
            {menu?.map(
                (item: any, index: number) =>
                    item.status && (
                        <ul className={styles.menu} key={`parent-${index}`}>
                            {getMenu(item, `parent-child-${index}`)}
                        </ul>
                    ),
            )}
        </div>
    );
};
