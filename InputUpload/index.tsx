import { STORAGE_SETTING } from "@/setting/setting";
import { useRef, useState } from "react";
import { toast } from "react-hot-toast";
import { uploadStorage } from "../../core-nextv3/storage/storage.api";
import { AnimatedLoading } from "../AnimatedLoading";
import CustomLink from "../CustomLink";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

export const InputUpload = ({ title, setUpload, maxFiles = 1 }: any) => 
{
    const [ loadingShipping, setLoadingShipping ] = useState(false);
    const [ files, setFiles ] = useState<any>([]);
    const inputFile = useRef<any>(null);

    const handleUploadFiles = async (uploads: any) => 
    {
        setLoadingShipping(true);

        if (uploads.length + files.length > maxFiles) 
        {
            setLoadingShipping(false);

            return toast.error(`Máximo de arquivos é ${maxFiles}`, {
                duration : 2000,
            });
        }

        for (let i = 0; i < uploads.length; i++) 
        {
            const file = uploads[i];

            if (file !== undefined) 
            {
                const result = await uploadStorage(
                    STORAGE_SETTING.merge({
                        file : file,
                    }),
                );

                if (result.status) 
                {
                    files.push(result.data);
                }
            }
        }

        if (files.length > 0) 
        {
            if (maxFiles === 1) 
            {
                setUpload(files[0]);
            }
            else 
            {
                setUpload(files);
            }
        }

        setLoadingShipping(false);

        toast.success("Upload realizado com sucesso!", {
            icon     : "👏",
            duration : 2000,
        });
    };

    const handleFileEvent = (e: any) => 
    {
        const chosenFiles = Array.prototype.slice.call(e.target.files);
        handleUploadFiles(chosenFiles);
        clear();
    };

    const clear = () => 
    {
        inputFile.current.value = null;
    };

    const onDelete = (item: any) => 
    {
        const _files = files.filter((file: any) => 
        {
            if (file.id !== item.id) 
            {
                return file;
            }
        });

        setFiles(_files);
    };

    return (
        <div className={styles.inputUpload}>
            <p className={styles.title}>{title}</p>

            <div className={styles.control}>
                <input
                    type="file"
                    ref={inputFile}
                    multiple
                    accept="application/pdf,image/png,image/jpg,image/jpge"
                    onChange={handleFileEvent}
                />
                <button
                    type="button"
                    disabled={loadingShipping}
                    onClick={() => inputFile?.current?.click()}
                    className={"block"}
                >
                    <ImageSet
                        width={20}
                        height={20}
                        src="/assets/icons/TiDocumentAdd.svg"
                        responsive={false}
                        alt=""
                    />
                    {loadingShipping ? <AnimatedLoading /> : "Anexar"}
                </button>
            </div>

            <div className={styles.shippingMethods}>
                {files?.length >= 1 &&
					files?.map((file: any) => (
					    <div className={styles.shipping} key={file.id}>
					        <p>
					            <CustomLink
					                href={file?._url}
					                prefetch={true}
					                rel="noreferrer"
					                target="_blank"
					            >
					                {file?.name}
					            </CustomLink>
					        </p>
					        <ImageSet
					            onClick={() => onDelete(file)}
					            width={20}
					            height={20}
					            src="/assets/icons/AiOutlineHeart.svg"
					            responsive={false}
					            alt=""
					        />
					    </div>
					))}
            </div>
        </div>
    );
};
