import { useRouter } from "next/router";
import { useState } from "react";
import { firstImageItemCart } from "../../core-nextv3/cart/cart.util";
import { collectionOwnerDocument } from "../../core-nextv3/document/document.api";
import { currencyMask } from "../../core-nextv3/util/mask";
import { useOnEvent } from "../../core-nextv3/util/use.event";
import { ORDER_SETTING } from "../../setting/setting";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

export const OrderList = () => 
{
    const router = useRouter();
    const [ orders, setOrders ] = useState([]);

    useOnEvent("changeUser", async () => 
    {
        const result = await collectionOwnerDocument(
            ORDER_SETTING.merge({
                ownerField : "client",
            }),
        );

        setOrders(result.collection);
    });

    return (
        <div className={styles.orderList}>
            <div className={styles.content}>
                <div className={styles.orders}>
                    <p className={styles.title}>Meus pedidos</p>
                    {orders && orders.length > 0 ? (
                        <>
                            {orders.map((order: any, index) => (
                                <>
                                    <div className={styles.order} key={order.id + index}>
                                        <p className={styles.title}>Pedido #{order?._sequence}</p>
                                        <p className={styles.subtitle}>
                                            {currencyMask(order?.total)} | {order?.items?.length}{" "}
											Produto(s)
                                        </p>

                                        <div className={styles.itens}>
                                            {order?.items?.map((item: any) => (
                                                <div key={item.id}>
                                                    <ImageSet image={firstImageItemCart(item)} />
                                                </div>
                                            ))}

                                            <button
                                                type="button"
                                                onClick={() =>
                                                    router.push(`/meus-pedidos/pedido/${order.id}`)
                                                }
                                            >
												Ver detalhes
                                            </button>
                                        </div>
                                        <div className={styles.line} />
                                    </div>
                                </>
                            ))}
                        </>
                    ) : (
                        <p className={styles.noOrders}>
							Você não possui pedidos no momento.
                        </p>
                    )}
                </div>
            </div>
        </div>
    );
};
