import { CountdownCircleTimer } from "react-countdown-circle-timer";
import styles from "./styles.module.scss";

const renderTime = (dimension: any, time: any) => 
{
    return (
        <div className="time-wrapper">
            <div className="time">{time}</div>
            <div>{dimension}</div>
        </div>
    );
};

export const CountDown = ({ date }: any) => 
{
    const minuteSeconds = 60;
    const hourSeconds = 3600;
    const daySeconds = 86400;

    const timerProps = {
        isPlaying   : true,
        size        : 120,
        strokeWidth : 6,
    };

    const getTimeSeconds = (time: any) => (minuteSeconds - time) | 0;
    const getTimeMinutes = (time: any) =>
        ((time % hourSeconds) / minuteSeconds) | 0;
    const getTimeHours = (time: any) => ((time % daySeconds) / hourSeconds) | 0;
    const getTimeDays = (time: any) => (time / daySeconds) | 0;

    const startTime = Date.now() / 1000; // use UNIX timestamp in seconds
    const endTime = date; // use UNIX timestamp in seconds

    const remainingTime = endTime - startTime;
    const days = Math.ceil(remainingTime / daySeconds);
    const daysDuration = days * daySeconds;

    return (
        <div className={styles.countdown}>
            <div className={styles.content}>
                <CountdownCircleTimer
                    {...timerProps}
                    colors="#000"
                    duration={daysDuration}
                    initialRemainingTime={remainingTime}
                >
                    {({ elapsedTime }: any) =>
                        renderTime("dias", getTimeDays(daysDuration - elapsedTime))
                    }
                </CountdownCircleTimer>
                <CountdownCircleTimer
                    {...timerProps}
                    colors="#000"
                    duration={daySeconds}
                    initialRemainingTime={remainingTime % daySeconds}
                    // onComplete={(totalElapsedTime: any) => [
                    //   remainingTime - totalElapsedTime > hourSeconds,
                    //   0,
                    // ]}
                >
                    {({ elapsedTime }: any) =>
                        renderTime("horas", getTimeHours(daySeconds - elapsedTime))
                    }
                </CountdownCircleTimer>
                <CountdownCircleTimer
                    {...timerProps}
                    colors="#000"
                    duration={hourSeconds}
                    initialRemainingTime={remainingTime % hourSeconds}
                    // onComplete={(totalElapsedTime) => [
                    //   remainingTime - totalElapsedTime > minuteSeconds,
                    //   0,
                    // ]}
                >
                    {({ elapsedTime }: any) =>
                        renderTime("minutos", getTimeMinutes(hourSeconds - elapsedTime))
                    }
                </CountdownCircleTimer>
                <CountdownCircleTimer
                    {...timerProps}
                    isPlaying
                    colors="#000"
                    duration={minuteSeconds}
                    initialRemainingTime={remainingTime % minuteSeconds}
                    // onComplete={(totalElapsedTime) => [
                    //   remainingTime - totalElapsedTime > 0,
                    //   0,
                    // ]}
                >
                    {({ elapsedTime }: any) =>
                        renderTime("segundos", getTimeSeconds(elapsedTime))
                    }
                </CountdownCircleTimer>
            </div>
        </div>
    );
};
