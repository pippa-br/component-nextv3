"use client";

import {
    clearCart,
    delGiftCart,
    delItemCart,
    setItemCart,
    validateCart,
} from "@/core-nextv3/cart/cart.api";
import { firstImageItemCart } from "@/core-nextv3/cart/cart.util";
import { useCore } from "@/core-nextv3/core/core";
import { tagManager4 } from "@/core-nextv3/util/TagManager4";
import { currencyMask } from "@/core-nextv3/util/mask";
import { useTranslations } from "next-intl";
import { useRouter } from "next/navigation";
import { useState } from "react";
import toast from "react-hot-toast";
import { CART_SETTING, THEME_SETTING } from "../../../setting/setting";
import { AnimatedLoading } from "../../AnimatedLoading";
import { ImageSet } from "../../ImageSet";
import styles from "./styles.module.scss";

declare const window: any;

export const CartItem = ({ setModalBag, user, navigation }: any) => 
{
    const router = useRouter();
    const t = useTranslations();
    const [ timer, setTimer ] = useState<any>(null);
    const { setCart, cart, awaitLoadingHTTP, setAwaitLoadingHTTP } = useCore();
    //console.log("cart", cart)

    async function handleIncrementQuantity(item: any) 
    {
        const product = item.product;
        const variant = item.variant;
        const quantity = item.quantity;

        const newData: any = {
            data : {
                product : {
                    referencePath : product.referencePath,
                },
                quantity : quantity + 1,
            },
        };

        // PRODUCT VARIANT
        if (variant) 
        {
            newData.data.variant = [ ...variant ];
            product.id = item.id;
        }

        tagManager4.addToCart(product, 1, user);

        const result = await setItemCart(CART_SETTING.merge(newData));

        if (result.status) 
        {
            toast.success("Quantidade alterada com sucesso!", {
                icon     : "👏",
                duration : 2000,
            });
            setCart(result.data);
        }
        else 
        {
            return toast.error(result.error, {
                duration : 2000,
            });
        }
    }

    async function handleDecrementQuantity(item: any) 
    {
        const product = item.product;
        const variant = item.variant;
        const quantity = item.quantity;

        const newData: any = {
            data : {
                product : {
                    referencePath : product.referencePath,
                },
                quantity : quantity - 1,
            },
        };

        // PRODUCT VARIANT
        if (variant) 
        {
            newData.data.variant = [ ...variant ];
            product.id = item.id;
        }

        tagManager4.removeFromCart(product, 1, user);

        const result = await setItemCart(CART_SETTING.merge(newData));

        if (result.status) 
        {
            toast.success("Quantidade alterada com sucesso!", {
                icon     : "👏",
                duration : 2000,
            });
            setCart(result.data);
        }
        else 
        {
            return toast.error(result.error, {
                duration : 2000,
            });
        }
    }

    async function handleDeleteProduct(item: any) 
    {
        const product = item.product;
        const variant = item.variant;
        product.id = item.id;

        tagManager4.removeFromCart(product, item.quantity, user);

        const newData: any = {
            data : {
                product : {
                    referencePath : product.referencePath,
                },
            },
        };

        // PRODUCT VARIANT
        if (variant) 
        {
            newData.data.variant = [ ...variant ];
        }

        let result: any;

        if (item.gift) 
        {
            result = await delGiftCart(CART_SETTING.merge(newData));
        }
        else 
        {
            result = await delItemCart(CART_SETTING.merge(newData));
        }

        toast.success("Produto removido!", { icon : "💔", duration : 2000 });

        setCart(result.data);
    }

    async function handleValidateCart() 
    {
        setAwaitLoadingHTTP(true);

        const result = await validateCart(CART_SETTING);

        setCart(result.data);

        if (result.status) 
        {
            setModalBag(false);

            router.push("/checkout");

            setAwaitLoadingHTTP(false);
        }
        else 
        {
            let message = "";

            if (result.error instanceof Object) 
            {
                for (const key in result.error) 
                {
                    message = result.error[key];
                    break;
                }
            }
            else 
            {
                message = result.error;
            }

            setAwaitLoadingHTTP(false);

            return toast.error(message, {
                duration : 2000,
            });
        }
    }

    async function handleChangeValue(event: any, item: any) 
    {
        item.quantity = event.target.value;
        handleTimer(item, event.target.value);
    }

    async function handleClearCart() 
    {
        toast.success(t("Carrinho sendo limpo!"), { icon : "👏", duration : 2000 });

        cart.items.forEach((product: any) => 
        {
            tagManager4.removeFromCart(product, product.quantity, user);
        });

        const result = await clearCart(CART_SETTING);

        setCart(result.data);
    }

    const handleTimer = (item: any, value: number) =>  
    {
        if (timer) 
        {
            clearTimeout(timer);
        }

        setTimer(
            setTimeout(async () => 
            {
                if (value) 
                {
                    const product = item.product;
                    const variant = item.variant;
                    //const quantity = item.quantity

                    const newData: any = {
                        data : {
                            product : {
                                referencePath : product.referencePath,
                            },
                            quantity : value,
                        },
                    };

                    // PRODUCT VARIANT
                    if (variant) 
                    {
                        newData.data.variant = [ ...variant ];
                        product.id = item.id;
                    }

                    //tagManager4.addToCart(router.asPath, product, 1, user);

                    const result = await setItemCart(CART_SETTING.merge(newData));

                    if (result.status) 
                    {
                        toast.success(t("Quantidade alterada com sucesso!"), {
                            icon     : "👏",
                            duration : 2000,
                        });
                        setCart(result.data);
                    }
                    else 
                    {
                        return toast.error(result.error, {
                            duration : 2000,
                        });
                    }
                }
            }, 500),
        );
    };

    const getProductLink = (item: any) => 
    {
        let url = `/${navigation}/${item?.product?.slug}/`;

        if (item?.variant && item.variant.length >= 2) 
        {
            url += `${item?.variant[0]?.value}/?size=${item?.variant[1]?.value || ""}`;
        }

        return url;
    };

    return (
        <div className={styles.cartItem}>
            {cart && cart?.items?.length > 0 ? (
                <>
                    {cart?.items?.map((item: any, index: any) => (
                        <div className={styles.content} key={index}>
                            <div className={styles.product}>
                                <div
                                    className={styles.img}
                                    onClick={() => window.open(getProductLink(item))}
                                >
                                    <ImageSet
                                        image={firstImageItemCart(item)}
                                        width={THEME_SETTING.widthProductThumb}
                                        height={THEME_SETTING.heightProductThumb}
                                        sizes="25vw"
                                    />
                                </div>
                                <div className={styles.descriptionProduct}>
                                    <p className={styles.title}>{item?.product?.name}</p>
                                    {/* <p className={styles.description}>Descrição</p> */}
                                    {item?.variant && <div className={styles.productInfo}>
                                        <div className={styles.productDetails}>
                                            {item?.variant?.map((item2: any, index2: any) => (
                                                <p className={styles.productColor} key={index2}>
                                                    {item2?.type?.name}: {item2?.label}
                                                </p>
                                            ))}
                                        </div>
                                    </div>}
                                    {!item?.gift ? (
                                        <div className={styles.prices}>
                                            <div className={styles.itemPrice}>
                                                {item?.promotionalPrice > 0 && (
                                                    <p className={styles.pricePromotional}>
                                                        {currencyMask(item?.realPrice)}
                                                    </p>
                                                )}
                                                <p className={styles.price}>
                                                    {currencyMask(item?.price)}
                                                </p>
                                            </div>
                                            <p className={styles.price}>
                                                {currencyMask(item?.total)}
                                            </p>
                                        </div>
                                    ) : (
                                        <div className={styles.prices}>
                                            <div className={styles.itemPrice}>
                                                <ImageSet
                                                    width={20}
                                                    height={20}
                                                    src="/assets/icons/GiPresent.svg"
                                                    responsive={false}
                                                    alt=""
                                                />
                                            </div>
                                            <p className={styles.price}>Brinde</p>
                                        </div>
                                    )}
                                    {!item?.gift && (
                                        <div className={styles.quantity}>
                                            <ImageSet
                                                onClick={() => handleDecrementQuantity(item)}
                                                width={20}
                                                height={20}
                                                src="/assets/icons/AiOutlineMinus.svg"
                                                responsive={false}
                                                alt=""
                                                className={styles.imageSvg}
                                            />
                                            {!THEME_SETTING?.editQuantityField ? (
                                                <p className={styles.number}>{item?.quantity}</p>
                                            ) : (
                                                <input
                                                    type="number"
                                                    className={styles.inputQuantity}
                                                    value={item?.quantity}
                                                    onChange={(e) => 
                                                    {
                                                        handleChangeValue(e, item);
                                                    }}
                                                />
                                            )}
                                            <ImageSet
                                                onClick={() => handleIncrementQuantity(item)}
                                                width={20}
                                                height={20}
                                                src="/assets/icons/AiOutlinePlus.svg"
                                                responsive={false}
                                                alt=""
                                                className={styles.imageSvg}
                                            />
                                        </div>
                                    )}
                                </div>
                                <ImageSet
                                    onClick={() => handleDeleteProduct(item)}
                                    width={20}
                                    height={20}
                                    src="/assets/icons/BsTrash.svg"
                                    responsive={false}
                                    alt=""
                                />
                            </div>
                        </div>
                    ))}
                </>
            ) : (
                <p className={styles.noProducts}>
                    <ImageSet
                        width={20}
                        height={20}
                        src="/assets/icons/BsBagFill.svg"
                        responsive={false}
                        alt=""
                    />
					Seu carrinho está vazio. :(
                </p>
            )}
            {cart && cart?.items?.length > 0 ? (
                <div className={styles.fixed}>
                    <div className={styles.priceTotal}>
                        {cart?.totalDiscount > 0 && (
                            <div className={styles.separator}>
                                <p className={styles.priceName}>Desconto:</p>
                                <p className={styles.discount}>
                                    {currencyMask(cart?.totalDiscount)}
                                </p>
                            </div>
                        )}

                        <div className={styles.separator}>
                            <p className={styles.priceName}>{t("SubTotal")}:</p>
                            <p className={styles.priceText}>
                                {currencyMask(cart?.totalItems)}
                            </p>
                        </div>

                        <div className={styles.dicount}>
                            <p>
                                {t("Desconto")}:
                                {cart?.discountPaymentMethod < 0 && (
                                    <span>({cart?.paymentMethod?.label})</span>
                                )}
                                {cart?.coupon && <span>({t("Cupom")})</span>}
                                {cart?.discountClient > 0 && <span>({t("Crédito")})</span>}
                            </p>
                            <p>{currencyMask(cart?.totalDiscount || 0)}</p>
                        </div>

                        <div className={styles.separator}>
                            <p className={styles.priceName}>Total:</p>
                            <p className={styles.priceText}>{currencyMask(cart?.total)}</p>
                        </div>
                    </div>

                    <div className={styles.buttons}>
                        <button
                            type="button"
                            disabled={awaitLoadingHTTP}
                            onClick={() => 
                            {
                                handleValidateCart();
                            }}
                        >
                            {awaitLoadingHTTP ? <AnimatedLoading /> : "Ir para o carrinho"}
                        </button>

                        <p className={styles.clearCart}>
                            <a onClick={() => handleClearCart()}>Limpa carrinho</a>
                        </p>

                        {/* <button
                            className={styles.text}
                            onClick={() => 
                            {
                                router.push("/carrinho/"); setModalBag(false);
                            }}
                        >
                            Ir Para o Carrinho
                        </button> */}
                    </div>
                </div>
            ) : null}
        </div>
    );
};
