"use client";

import { useProtectedAuth } from "@/core-nextv3/auth/user.auth.api";
import { cloudflareLoader, getRecaptcha } from "@/core-nextv3/util/util";
import { useTranslations } from "next-intl";
import Image from "next/image";
import { useRouter, useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { userLoginAnalytics } from "../../core-nextv3/analytics/analytics.api";
import {
    loginAuth,
    recoveryPasswordAuth,
} from "../../core-nextv3/auth/auth.api";
import { mergeCart } from "../../core-nextv3/cart/cart.api";
import { useCore } from "../../core-nextv3/core/core";
import { AUTH_SETTING, CART_SETTING } from "../../setting/setting";
import { AnimatedLoading } from "../AnimatedLoading";
import CustomLink from "../CustomLink";
import ErrorMessage from "../ErrorMessage";
import { LoadingPage } from "../LoadingPage";
import styles from "./styles.module.scss";

export default function LoginForm2() 
{
    const router = useRouter();
    const searchParams = useSearchParams();

    const {
        register,
        setValue,
        formState: { errors },
        handleSubmit,
    } = useForm();

    const t = useTranslations();
    const { cart, setCart, setUser } = useCore();
    const [ email, setEmail ] = useState("");
    const [ formType, setFormType ] = useState("login");
    const [ passwordType, setPasswordType ] = useState("password");
    const [ animateLoading, setAnimateLoading ] = useState(false);

    // AUTO LOGIN VIA URL
    useEffect(() => 
    {
        const email = searchParams.get("email");
        const token = searchParams.get("token");

        if (email && token) 
        {
            handleLoginWithEmailAndPassword({
                login    : email,
                password : token,
            });
        }
    }, []);

    async function recoveryPassword() 
    {
        const result = await recoveryPasswordAuth(
            AUTH_SETTING.merge({
                login : email.toLowerCase(),
            }),
        );

        if (!result.status) 
        {
            return toast.error(result.error);
        }

        toast(
            (t: any) => (
                <span>
                    {result.message}
                    <button onClick={() => toast.dismiss(t.id)}>Ok</button>
                </span>
            ),
            {
                className : "toast-custom",
                duration  : 999999,
            },
        );

        setValue("login", email);
        setFormType("login");
    }

    async function handleLoginWithEmailAndPassword(formData: any) 
    {
        const token = await getRecaptcha("login");

        if (token) 
        {
            setAnimateLoading(true);

            const result = await loginAuth(
                AUTH_SETTING.merge({
                    token    : token,
                    login    : formData.login.toLowerCase(),
                    password : formData.password,
                }),
            );

            setAnimateLoading(false);

            if (result.status) 
            {
                setUser(result.data);
            }
            else 
            {
                return toast.error(
                    result.error || t("Erro ao fazer login.", { duration : 2000 }),
                );
            }

            toast(t("Login feito com sucesso!"), { icon : "👏", duration : 2000 });

            // MERGE CART SESSION
            const resultMerge = await mergeCart(
                CART_SETTING.merge({
                    document : {
                        referencePath : cart?.referencePath,
                    },
                }),
            );

            if (resultMerge.status) 
            {
                setCart(resultMerge.data);
            }

            // LOGIN ANALYTICS
            userLoginAnalytics(result.data);

            if (formData.redirect) 
            {
                router.push(formData.redirect);
            }
            else if (resultMerge.data?.items && resultMerge.data.items.length > 0) 
            {
                router.push("/checkout/");
            }
            else 
            {
                router.push("/");
            }
        }
    }

    const loadProtectedAuth = useProtectedAuth(AUTH_SETTING, "/", "");

    if (!loadProtectedAuth) 
    {
        return <LoadingPage />;
    }

    return (
        <div className={styles.container}>
            <div className={styles.content}>
                {formType === "recoveryEmail" && (
                    <>
                        <div className={styles.title}>
                            {t("Vamos enviar um e-mail com uma nova senha")}:
                        </div>
                        <div className={styles.inputControl}>
                            <input
                                style={{ textTransform : "lowercase" }}
                                onChange={(e: any) => setEmail(e.target.value)}
                                type="email"
                                name="email"
                                placeholder={t("Digite o seu email")}
                                onKeyUp={(e) => 
                                {
                                    if (e.code === "Enter") 
                                    {
                                        setFormType("login");
                                        recoveryPassword();
                                    }
                                }}
                            />
                            {errors.email && <ErrorMessage message={errors.email.message} />}
                        </div>
                        <div className={styles.buttons}>
                            <button
                                className={styles.submitted}
                                onClick={() => 
                                {
                                    recoveryPassword();
                                }}
                                type="button"
                            >
                                {t("Recuperar Senha")}
                            </button>
                            {/* <button
                  type='button'
                  onClick={() => {
                    setFormType('login')
                  }}
                >
                  {t('voltar')}
                </button> */}
                        </div>
                        <div className={styles.backLink}>
                            <a onClick={() => setFormType("login")}>{t("voltar")}</a>
                        </div>
                    </>
                )}

                {formType === "login" && (
                    <>
                        <p className={styles.title}>{t("Entre ou Crie uma Conta")}</p>
                        <p className={styles.subtitle}>
							Clique aqui para{" "}
                            <CustomLink href="/cadastro/" prefetch={true}>
								CRIAR CONTA
                            </CustomLink>
                        </p>
                        <div className={styles.inputControl}>
                            <input
                                id="loginInput"
                                style={{ textTransform : "lowercase" }}
                                type="email"
                                placeholder="Email"
                                {...register("login", {
                                    required : "Este campo é obrigatório!",
                                    pattern  : {
                                        value   : /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                        message : "Email inválido",
                                    },
                                })}
                            />
                            {errors.login && <ErrorMessage message={errors.login.message} />}
                        </div>
                        <div className={styles.inputControl}>
                            <div className={styles.inputPassword}>
                                <input
                                    {...register("password", {
                                        required  : t("Este campo é obrigatório!"),
                                        minLength : {
                                            value   : 8,
                                            message : t("Sua senha possui, no mínimo, 8 caracteres."),
                                        },
                                    })}
                                    type={passwordType}
                                    placeholder="Senha"
                                />
                                {passwordType === "password" ? (
                                    <Image
                                        className={styles.iconPassword}
                                        onClick={() => setPasswordType("text")}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/AiOutlineEye.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />
                                ) : (
                                    <Image
                                        className={styles.iconPassword}
                                        onClick={() => setPasswordType("password")}
                                        width={20}
                                        height={20}
                                        src="/assets/icons/AiOutlineEyeInvisible.svg"
                                        loader={cloudflareLoader}
                                        alt=""
                                    />
                                )}
                                {errors.password && (
                                    <ErrorMessage message={errors.password.message} />
                                )}
                            </div>
                        </div>
                        <div className={styles.inputControl}>
                            <a onClick={() => setFormType("recoveryEmail")}>
                                {t("Esqueceu a senha?")}
                                <br />
                                {t("Clique aqui para recebê-la por e-mail")}
                            </a>
                        </div>
                        <div className={styles.buttons}>
                            <button
                                className={styles.submitted}
                                onClick={handleSubmit(handleLoginWithEmailAndPassword)}
                                type="button"
                            >
                                {animateLoading ? <AnimatedLoading /> : t("Entrar")}
                            </button>
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
