import { Rating } from "@smastrom/react-rating";
import { THEME_SETTING } from "../../setting/setting";
import CustomLink from "../CustomLink";
import { ImageSet } from "../ImageSet";
import styles from "./styles.module.scss";

export const CommentItem = ({ coment }: any) => 
{
    const getLink = () => 
    {
        let link = `${THEME_SETTING.productPath + coment?.product?.slug}/`;

        if (coment?.variant && coment?.variant.length > 0) 
        {
            link += `${coment?.variant[0]?.value}/`;
        }

        return link;
    };

    return (
        <div className={styles.commentItem}>
            <div className={styles.content}>
                <div className={styles.comment}>
                    <div className={styles.top}>
                        <div className={styles.userDatas}>
                            <ImageSet
                                width={20}
                                height={20}
                                src="/assets/icons/FaUserCircle.svg"
                                responsive={false}
                                alt=""
                            />
                            <div className={styles.published}>
                                <p className={styles.name}>
                                    {/* {!THEME_SETTING?.disabledDateComent &&
                                        <>
                                            <span>{formatDate(coment?.postdate)}</span> <small>- </small>
                                        </>
                                    } */}
                                    {coment?.name ? coment?.name : coment?.owner?.name}
                                    {coment?.height ? (
                                        <span> - Altura: {coment?.height}</span>
                                    ) : (
                                        ""
                                    )}
                                    {coment?.height ? <span> - Peso: {coment?.weigth}</span> : ""}
                                </p>
                                <div className={styles.informationProdut}>
                                    <CustomLink className={styles.nameProduct} href={getLink()}>
                                        {coment?.product?.name}
                                    </CustomLink>
                                    <div className={styles.variants}>
                                        {coment?.variant?.map((variantItem: any, index: any) => (
                                            <p className={styles.label} key={index}>
                                                {variantItem?.label}
                                            </p>
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className={styles.avaliation}>
                            <Rating
                                style={{ maxWidth : 100 }}
                                readOnly={true}
                                value={coment?.rating}
                            />
                        </div>
                    </div>
                    <p className={styles.description}>{coment?.message}</p>
                </div>
            </div>
        </div>
    );
};
