"use client";

import { useHomePageViewRetail } from "@/core-nextv3/retail/retail.use";

const HomeEvent = () => 
{
    // PAGE VIEW RETAIL
    useHomePageViewRetail();

    return <></>;
};

export { HomeEvent };
