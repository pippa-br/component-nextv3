"use client";

import { useProductClickAnalytics } from "@/core-nextv3/analytics/analytics.use";
import { useCore } from "@/core-nextv3/core/core";
import { useDetailPageViewRetail } from "@/core-nextv3/retail/retail.use";
import { tagManager4 } from "@/core-nextv3/util/TagManager4";
import { FULL_PRODUCT_SETTING } from "@/setting/setting";
import { useEffect } from "react";

const ProductEvent = ({ productData, variants }: any) => 
{
    const { user } = useCore();

    // PRODUCTS ANALYTICS
    useProductClickAnalytics(productData);

    // PAGE VIEW RETAIL
    useDetailPageViewRetail(
        FULL_PRODUCT_SETTING.merge({
            skuId    : productData.code,
            variants : variants,
        }),
    );

    useEffect(() => 
    {
        if (productData) 
        {
            let category = "";

            if (productData.categories && productData.categories.length > 0) 
            {
                const singleCategory =
					productData.categories[productData.categories.length - 1];
                category = `categoria-${singleCategory.name}`;
            }

            tagManager4.productView(productData, category, user);
        }
    }, []);

    return <></>;
};

export { ProductEvent };
