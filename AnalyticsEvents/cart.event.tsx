"use client";

import { useShoppingCartPageViewRetail } from "@/core-nextv3/retail/retail.use";

const CartEvent = () => 
{
    // PAGE VIEW RETAIL
    useShoppingCartPageViewRetail();

    return <></>;
};

export { CartEvent };
