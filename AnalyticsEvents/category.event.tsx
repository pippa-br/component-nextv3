"use client";

import { useCategoryPageViewRetail } from "@/core-nextv3/retail/retail.use";

const CategoryEvent = ({ categoryData }: any) => 
{
    console.info(categoryData);

    // CATEGORIES PAGE VIEW RETAIL
    useCategoryPageViewRetail(categoryData);

    return <></>;
};

export { CategoryEvent };
