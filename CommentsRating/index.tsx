"use client";

import { Rating } from "@smastrom/react-rating";
import { useState } from "react";
import styles from "./styles.module.scss";

export const CommentsRating = ({ comments }: any) => 
{
    const [ totalRating ] = useState(() => 
    {
        if (comments && comments.length > 0) 
        {
            let total = 0;

            for (const item of comments) 
            {
                total += item.rating;
            }

            total = +Number(total / comments.length).toFixed(1);

            return total;
        }

        return 0;
    });
    // const { user, setUser}              = useCore();

    // const [loadingShipping, setLoadingShipping] = useState(false);
    // const [star, setStar] = useState(0);
    // const {
    //     register,
    //     formState: { errors },
    //     handleSubmit,
    //     watch,
    //     setValue,
    //     reset,
    // } = useForm();

    // function ratingChanged(e: any) {
    //     setStar(e);
    // }

    // const onSubmitPray = async (data: any) => {
    //     setLoadingShipping(true);
    //     // console.log(data);

    //     data.client = {
    //         referencePath : user.referencePath
    //     };
    //     data.product = {
    //         referencePath : product.referencePath
    //     };
    //     data.status = true;

    //     data.rating = star;
    //     await addDocument(COMMET_SETTING.merge({
    //         data: data
    //     }));

    //     reset();
    //     menssageEmailSuccess();
    //     setLoadingShipping(false);
    // };

    // async function menssageEmailSuccess() {
    //     toast(
    //         (t) => (
    //             <span>
    //                 <p>Coméntario enviado com sucesso</p>
    //                 <button onClick={() => toast.dismiss(t.id)}>Ok</button>
    //             </span>
    //         ),
    //         {
    //             className: 'toast-custom',
    //             duration: 999999,
    //         }
    //     );

    // };

    return (
        <div className={styles.commentsRating}>
            <div className={styles.assessmentStar}>
                <Rating style={{ maxWidth : 100 }} readOnly={true} value={totalRating} />
                <p className={styles.stars}>{totalRating}</p>
            </div>{" "}
			-
            <p className={styles.text}>
				Calculado de {comments?.length} revisão(ões) de clientes.
            </p>
        </div>
    );
};
