import { getDocument } from "@/core-nextv3/document/document.api";
import { calls } from "@/core-nextv3/util/call.api";
import {
    ORDER_SETTING,
    REASONS_REORDER_OPTION,
    REORDER_PAGE_SETTING,
    REORDER_TYPE_OPTION,
} from "@/setting/setting";
import { Suspense } from "react";
import { ReorderDisplay } from "./reorder.display";

// const GenerateStaticParams = async () =>
// {
//     const pages = await collectionDocument(ORDER_SETTING.merge({
//         perPage : 1,
//     }));

//     let paths = [];

//     if (pages.collection)
//     {
//         paths = pages.collection.map((item:any) => ({
//             params : { id : item.id || "" },
//         }));
//     }

//     console.info("(Reorder Static:", paths.length, ")");

//     return paths;
// }

const getReorderPageServer = async () => 
{
    const [ reOrderTypeOptionResult, reasonsTypeOptionResult, reorderPageResult ] =
		await calls(
		    getDocument(REORDER_TYPE_OPTION.merge({ cache : "force-cache" })),
		    getDocument(REASONS_REORDER_OPTION.merge({ cache : "force-cache" })),
		    getDocument(REORDER_PAGE_SETTING.merge({ cache : "force-cache" })),
		);

    return {
        reOrderTypeOptionData : reOrderTypeOptionResult.data 
            ? reOrderTypeOptionResult.data
            : {},
        reasonsTypeOptionData : reasonsTypeOptionResult.data
            ? reasonsTypeOptionResult.data
            : {},
        reorderPageData : reorderPageResult.data ? reorderPageResult.data : {},
    };
};

const GenerateMetadata = async () => 
{
    return {
        title : "Troca",
    };
};

const ReorderPage = async ({
    params,
}: {
	params: Promise<{ id: string }>;
}) => 
{
    const { id } = await params;
    const { reOrderTypeOptionData, reasonsTypeOptionData, reorderPageData } =
		await getReorderPageServer();

    return (
        <Suspense>
            <ReorderDisplay
                id={id}
                setting={ORDER_SETTING}
                reorderType={reOrderTypeOptionData}
                reasonsType={reasonsTypeOptionData}
                reorderPage={reorderPageData}
            />
        </Suspense>
    );
};

export { ReorderPage, GenerateMetadata };
