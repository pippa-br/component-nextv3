"use client";

import styles from "./styles.module.scss";
import { useGetDocument } from "@/core-nextv3/document/document.use";
import { useTranslations } from "next-intl";
import { usePathname } from "next/navigation";
import { useState } from "react";
import { innerHTML } from "../../core-nextv3/util/util";
import { LoadingPage } from "../LoadingPage";
import { PageTitle } from "../PageTitle";
import { ReorderViewer } from "../ReorderViewer";

const ReorderDisplay = ({ id, reorderType, reasonsType, reorderPage, setting }: any) => 
{
    const pathname = usePathname();
    const [ order, setOrder ] = useState<any>();
    const t = useTranslations();

    // SETTING ID
    setting.id = id;

    useGetDocument(setting, (data: any) => 
    {
        setOrder(data);
    });

    if (!order) 
    {
        return <LoadingPage />;
    }

    return (
        <>
            <div className={`${styles.orderPage} orderPage`}>
                <div className={styles.content}>
                    <PageTitle
                        parents={[
                            {
                                url  : pathname.replace("/troca/", "/pedido/"),
                                name : t(`Pedido: #${order._sequence}`),
                            },
                        ]}
                        name={"Troca"}
                        noTitle={true}
                    />

                    {reorderPage?.published && (
                        <div
                            className={styles.pageContent}
                            dangerouslySetInnerHTML={innerHTML(reorderPage?.content)}
                        />
                    )}

                    <ReorderViewer
                        order={order}
                        reorderType={reorderType}
                        reasonsType={reasonsType}
                    />
                </div>
            </div>
        </>
    );
};

export { ReorderDisplay };
